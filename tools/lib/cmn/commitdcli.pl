#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)commitdcli.pl
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
{
#
#fsmcomm - handle communications to/from a fsm
#

use strict;

package fsmcomm;
my $pkgname = __PACKAGE__;

#imports:
#use socket i/o to receive events:
use IO::Socket;

#package variables:

sub new
{
    my ($invocant) = @_;
    shift @_;

    #allows this constructer to be invoked with reference or with explicit package name:
    my $class = ref($invocant) || $invocant;

    my ($fsmhost, $fsmport) = @_;

    #set up class attribute  hash and bless it into class:
    my $self = bless {
        'mFsmPort' => $fsmport,
        'mFsmHost' => $fsmhost,
        'mEventListenSocket' => 0,
        }, $class;

    #post-attribute init after we bless our $self (allows use of accessor methods):

    return $self;
}

################################### PACKAGE ####################################
sub closeEventListenSocket
#if the event socket is open (non-zero), then close it.
{
    my ($self) = @_;

    my $sock = $self->getEventListenSocket();

    close $sock unless ($sock == 0);
}

sub send_event
#open a connection to state machine, send a single event, and close connection.
{
    my ($self, $event) = @_;

    my $main_socket = IO::Socket::INET->new (
                      PeerAddr => $self->fsmHost(),
                      PeerPort => $self->fsmPort(),
                      Proto     => 'tcp'
                      );

    if (!defined($main_socket)) {
        printf STDERR "%s:  could not create socket '%s:%s': '%s'.\n",
            "fsmcomm", $self->fsmHost(), $self->fsmPort(), $!;
        return;
    }

#printf STDERR "client:  socket created, writing messages...";
#printf STDERR "%s:  Writing '%s' to server\n", $p, $event;
    print $main_socket "$event\n";
    close $main_socket;
}

sub fsmPort
#return value of mFsmPort
{
    my ($self) = @_;
    return $self->{'mFsmPort'};
}

sub fsmHost
#return value of mFsmHost
{
    my ($self) = @_;
    return $self->{'mFsmHost'};
}

sub getEventListenSocket
#return value of EventListenSocket
{
    my ($self) = @_;
    return $self->{'mEventListenSocket'};
}

sub setEventListenSocket
#set value of EventListenSocket and return value.
{
    my ($self, $value) = @_;
    $self->{'mEventListenSocket'} = $value;
    return $self->{'mEventListenSocket'};
}


1;
} #end of fsmcomm
{
#
#commitdcli - test client for feeding events to simple fsm
#

use strict;

package commitdcli;
my $pkgname = __PACKAGE__;

#imports:

#standard global options:
my $p = $main::p;
my ($VERBOSE, $HELPFLAG, $DEBUGFLAG, $DDEBUGFLAG, $QUIET) = (0,0,0,0,0);

#package global variables:
my $FSM_EVENT_HOST = "localhost";
my $FSM_EVENT_PORT = 1936;

&init;      #init globals

##################################### MAIN #####################################

sub main
{
    local(*ARGV, *ENV) = @_;

    &init;      #init globals

    return (1) if (&parse_args(*ARGV, *ENV) != 0);
    return (0) if ($HELPFLAG);

    #get a fms comm object:
    my $fsmcomm = new fsmcomm ($FSM_EVENT_HOST, $FSM_EVENT_PORT);
    #MAIN LOOP:
    my $exit_loop = 0;
    until ( $exit_loop ) {

        printf "Enter EVENT:  ";
        #read input:
        my $event = <STDIN>;
        last if (!defined($event));  #end of input
        chomp $event;

        $fsmcomm->send_event($event);
    }

    return 0;
}

################################### PACKAGE ####################################


#################################### USAGE #####################################

sub usage
{
    my($status) = @_;

    print STDERR <<"!";
Usage:  $p [options]

SYNOPSIS
  Test client for commitd server.
  Reads events from <stdin> and sends to commitd for
  processing.

OPTIONS
  -help             Display this help message.
  -verbose          Display additional informational messages.
  -debug            Display debug messages.
  -ddebug           Display deep debug messages.
  -quiet            Display severe errors only.
  -host <name>      the server host name. Defaults to "localhost".
  -port <number>    the server port number. Defaults to 1936.

EXAMPLE
  $p -host "localhost" -port 1936 
!
    return ($status);
}

sub parse_args
#proccess command-line aguments
{
    local(*ARGV, *ENV) = @_;


    #eat up flag args:
    my ($flag);
    while ($#ARGV+1 > 0 && $ARGV[0] =~ /^-/) {
        $flag = shift(@ARGV);

        if ($flag =~ '^-debug') {
            $DEBUGFLAG = 1;
        } elsif ($flag =~ '^-host') {
            #-host hostname
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $FSM_EVENT_HOST = shift(@ARGV);
            } else {
                printf STDERR "%s:  -host requires a host name\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-port') {
            #-port server_port
            if ($#ARGV+1 > 0 && $ARGV[0] !~ /^-/) {
                $FSM_EVENT_PORT = shift(@ARGV);
            } else {
                printf STDERR "%s:  -port requires a port number\n", $p;
                return 1;
            }
        } elsif ($flag =~ '^-dd') {
            $DDEBUGFLAG = 1;
        } elsif ($flag =~ '^-v') {
            $VERBOSE = 1;
        } elsif ($flag =~ '^-h') {
            $HELPFLAG = 1;
            return &usage(0);
        } else {
            printf STDERR "%s:  unrecognized option, '%s'\n", $p, $flag;
            return &usage(1);
        }
    }

    #eliminate empty args (this happens on some platforms):
    @ARGV = grep(!/^$/, @ARGV);

    return 0;
}

################################ INITIALIZATION ################################

sub init
{
}

sub cleanup
{
}
1;
} #end of commitdcli
