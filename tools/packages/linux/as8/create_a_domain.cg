#
# BEGIN_HEADER - DO NOT EDIT
# 
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)create_a_domain.cg - ver 1.1 - 01/04/2006
#
# Copyright 2004-2006 Sun Microsystems, Inc. All Rights Reserved.
# 
# END_HEADER - DO NOT EDIT
#

#create a single domain.  you must set the domain defs before including.

%ifndef FIXTREE_DEFS_INCLUDED 	%exit ${CG_INFILE}: you must include fixtree.defs to run this script.
%ifndef JBI_PORT_OFFSET JBI_PORT_OFFSET=0

%ifndef domain_name 	%exit ${CG_INFILE}: you must define domain_name
%ifndef DOMAIN_PORT_OFFSET	%exit ${CG_INFILE}: you must define DOMAIN_PORT_OFFSET

#this sets the domain port definitions, based on the DOMAIN_PORT_OFFSET & JBI_PORT_OFFSET:
%include set_domain_port.defs


stop_a_domain := << EOF
{
%echo
%echo STOPPING $domain_name
%shell asadmin stop-domain $domain_name
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadminstop-domain FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1
%shell sleep 5
}
EOF


%echo
%ifnot $IS_CLEAN_INSTALL  %echo DELETING domain $domain_name ...
%ifnot $IS_CLEAN_INSTALL  %shell asadmin delete-domain -t $domain_name
%echo CREATING domain $domain_name ...
%shell asadmin create-domain -e --adminport $admin_port --adminuser $admin_name --passwordfile $admin_passwdfile --instanceport $http_port --domainproperties domain.jmxPort=${jmx_connector_port}:http.ssl.port=${http_ssl_port}:orb.listener.port=${orb_listener_port}:orb.mutualauth.port=${orb_mutualauth_port}:orb.ssl.port=${orb_ssl_port}:jms.port=$jms_provider_port $domain_name
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadmin create-domain FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1

#####
#edit domain.xml to prevent startup hangs.
#####
%echo
%readtemplate ECHO_TXT domains/$domain_name/config/domain.xml

#add a jvm-option
%echo ADDING <jvm-options>-Dcom.sun.messaging.jms.ra.inACC=false</jvm-options> config for $domain_name
CG_SUBSTITUTE_SPEC := s/(<\/jvm-options>)/$1\n<jvm-options>-Dcom.sun.messaging.jms.ra.inACC=false<\/jvm-options>/
ECHO_TXT = $ECHO_TXT:substitute

%echo INCREASING JMS init time:  <jms-service ... init-timeout-in-seconds="180" ... >
CG_SUBSTITUTE_SPEC := s|<jms-service(.*)init-timeout-in-seconds="60"(.*)>|<jms-service$1init-timeout-in-seconds="180"$2>|
ECHO_TXT = $ECHO_TXT:substitute

#re-write domain.xml:
echo    domains/$domain_name/config/domain.xml
%undef CG_SUBSTITUTE_SPEC

######
#start appserver:
######
%echo
%echo STARTING $domain_name so we can create the jbi lifecycle
%shell asadmin start-domain -e -t -u $admin_name --passwordfile $admin_passwdfile $domain_name
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadmin start-domain FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1
%shell sleep 15

#####
#turn OFF security on the JMX connector so our tests will run with ee edition:
#####
%echo
%echo TURNING OFF JMX REMOTE SECURITY for $domain_name
%shell asadmin set -e -u $admin_name --passwordfile $admin_passwdfile -p $admin_port server.admin-service.jmx-connector.system.security-enabled=false
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadmin set FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1
%if $CG_SHELL_STATUS  %halt 1

####
#Add JBI lifecycle:
####
description= JBI Framework LifecycleModule
description= JBI

#this is what is installed in glassfish when we get it:
# <lifecycle-module class-name="com.sun.jbi.framework.sun.SunASJBIFramework" classpath="${com.sun.aas.installRoot}/jbi/lib/jbi_framework.jar" enabled="true" is-failure-fatal="false" name="JBIFramework">
#   <description>"JBI Framework LifecycleModule"</description>
#   <property name="com.sun.jbi.home" value="${com.sun.aas.installRoot}/jbi"/>
#   <property name="com.sun.jbi.defaultLogLevel" value="SEVERE"/>
# </lifecycle-module>
%echo
%echo REMOVING JBI lifecycle from $domain_name if it exists
%shell asadmin delete-lifecycle-module -e -p $admin_port --user $admin_name --passwordfile $admin_passwdfile JBIFramework

%echo
%echo ADDING JBI lifecycle to $domain_name
%shell asadmin create-lifecycle-module -e -p $admin_port --user $admin_name --passwordfile $admin_passwdfile --property "com.sun.jbi.management.HtmlAdaptorPort=${html_adaptor_port}" --classname "com.sun.jbi.framework.sun.SunASJBIBootstrap" --classpath "$JV_AS8BASE/jbi/lib/jbi_framework.jar" --loadorder 1 --failurefatal=false --description "$description" JBIFramework
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadmin create-lifecycle-module FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1

#NOTE:  legal defaultLogLevel names are:
#           SEVERE, WARNING, INFO, CONFIG, FINE, FINER, FINEST

####
#Add JBI_HOME property here, because of : in DOS path names:
####
PROPERTY=.com\.sun\.jbi\.home
VALUE=${JV_AS8BASE}/jbi
%echo
%echo
%shell asadmin set -e -u $admin_name --passwordfile $admin_passwdfile -p $admin_port server.applications.lifecycle-module.JBIFramework.property"${PROPERTY}"="${VALUE}"
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadmin set FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1

####
#Add http-listener-3, this is a SSL/Client Auth enabled http listener
####
%echo
%echo ADDING http-listener-3 to $domain_name
%shell asadmin create-http-listener -e -u $admin_name --passwordfile $admin_passwdfile  -p $admin_port --interactive=false --listeneraddress 0.0.0.0 --listenerport ${http_ssl_port_2} --defaultvs server --servername `hostname` --securityenabled=true http-listener-3
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadmin create-http-listener FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1

%echo
%echo ADDING ssl element for http-listener-3 to $domain_name
%shell asadmin create-ssl -e -u $admin_name --passwordfile $admin_passwdfile  -p $admin_port --interactive=false --type http-listener  --certname s1as --clientauthenabled=true http-listener-3
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadmin create-ssl FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1

#####
#store the port numbers for later use
#####
ECHO_TXT = << EOF
export ${domain_name}_admin_port ${domain_name}_jms_port ${domain_name}_jmx_port
${domain_name}_admin_port=$admin_port
${domain_name}_jms_port=$jms_provider_port
${domain_name}_jmx_port=$jmx_connector_port
EOF

%echo generating $CG_ROOT/${domain_name}_port.sh
echo /${domain_name}_port.sh


####
#stop the created domain, only if it is not CAS or domain1
#Optimization: CAS needs to be started again to create instances
#domain1 needs a jvm option
####
dostop = 1

CG_COMPARE_SPEC = CAS
%if $domain_name:eq dostop = 0

CG_COMPARE_SPEC = domain1
%if $domain_name:eq dostop = 0

%if $dostop %call stop_a_domain

%include create_instances_in_domain.cg

####
#this jvm option is set only for domain1
####
CG_COMPARE_SPEC = domain1
%ifnot $domain_name:eq %return

%echo
%echo Setting JVM Option com.sun.jbi.startOnDeploy for domain $domain_name ...
%echo
%shell asadmin create-jvm-options -e --port $admin_port --user $admin_name --passwordfile $admin_passwdfile "-Dcom.sun.jbi.startOnDeploy=false"
%if $CG_SHELL_STATUS  %echo create_a_domain.cg: ERROR: asadmin create-jvm-options FAILED with status $CG_SHELL_STATUS
%if $CG_SHELL_STATUS  %halt 1

%call stop_a_domain


