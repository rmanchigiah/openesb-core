<?xml version="1.0" encoding="UTF-8"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)pom.xml
 # Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->
<project>
  <modelVersion>4.0.0</modelVersion>
  <groupId>net.open-esb.core</groupId>
  <artifactId>jbi-common</artifactId>
  <packaging>pom</packaging>
  <name>jbi-common</name>
  <version>1.0</version>
  <description>jbi-common - common elements for jbi build and packaging projects</description>
  <url>https://open-esb.dev.java.net/</url>
  <inceptionYear>2005</inceptionYear>
  <build>
    <sourceDirectory>src</sourceDirectory>
    <scriptSourceDirectory>src/scripts</scriptSourceDirectory>
    <testSourceDirectory>regress</testSourceDirectory>
    <outputDirectory>bld/classes</outputDirectory>
    <testOutputDirectory>bld/test-classes</testOutputDirectory>
    <defaultGoal>install</defaultGoal>
    <directory>bld</directory>
    <pluginManagement>
      <plugins>
        <plugin>
          <artifactId>maven-jar-plugin</artifactId>
          <!-- NOTE:  version 2.2 has problems with maven 2.0.9.  RT 5/9/08-->
          <version>2.1</version>
          <inherited>true</inherited>
          <configuration>
            <archive>
              <addMavenDescriptor>false</addMavenDescriptor>
            </archive>
          </configuration>
        </plugin>
        <plugin>
          <artifactId>maven-war-plugin</artifactId>
          <!-- 2.1-alpha-1-SNAPSHOT is the default, but is not downloading. RT 8/7/08 -->
          <version>2.0.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>2.0.1</version>
          <inherited>true</inherited>
          <configuration>
            <!-- generate 1.5 class files: -->
            <source>1.5</source>
            <target>1.5</target>
          </configuration>
        </plugin>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <!-- pertest forkMode is broken in version 2.2. use version 2.1.3 instead.  RT 6/30/06 -->
          <!-- <version>2.1.3</version> -->
          <version>2.3</version>
          <inherited>true</inherited>
        </plugin>
        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>2.2</version>
          <inherited>true</inherited>
        </plugin>
        <plugin>
          <artifactId>maven-dependency-plugin</artifactId>
          <!-- latest snapshot has a regression in the <stripHeader> feature.  RT 12/14/06 -->
          <!-- <version>2.0-alpha-1</version> -->
          <!-- <version>2.0-alpha-1-20061114.043217-3</version> -->
          <!-- <version>2.0-alpha-1-SNAPSHOT</version> -->
          <!-- <version>2.0-alpha-2-SNAPSHOT</version> -->
          <version>2.0</version>
          <inherited>true</inherited>
        </plugin>
        <plugin>
          <artifactId>maven-antrun-plugin</artifactId>
          <version>1.7</version>
          <inherited>true</inherited>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>build-helper-maven-plugin</artifactId>
          <version>1.0</version>
        </plugin>
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>jboss-packaging-maven-plugin</artifactId>
          <version>2.0</version>
        </plugin>
        <plugin>
          <groupId>net.java.dev.jregress</groupId>
          <artifactId>maven-jregress-plugin</artifactId>
          <version>1.0-SNAPSHOT</version>
        </plugin>
        <plugin>
          <groupId>org.apache.felix</groupId>
          <artifactId>maven-bundle-plugin</artifactId>
          <version>1.4.0</version>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
  <distributionManagement>
    <repository>
      <id>openesb-release</id>
      <name>OpenESB repo release</name>
      <url>http://openesb-dev.org:8081/nexus/content/repositories/openesb-release/</url>
    </repository>
    <snapshotRepository>
      <id>openesb-snapshot</id>
      <name>OpenESB repo release</name>
      <url>http://nexus.openesb-dev.org:8081/nexus/content/repositories/openesb-snapshot/</url>
    </snapshotRepository>
  </distributionManagement>
  <pluginRepositories>
    <pluginRepository>
      <id>maven-snapshots</id>
      <name>Maven Snapshots</name>
      <url>http://people.apache.org/maven-snapshot-repository</url>
      <releases>
        <enabled>false</enabled>
      </releases>
      <!-- do not update standard plugins as a default, as it takes forever.  RT 6/10/08 -->
      <snapshots>
        <enabled>true</enabled>
        <updatePolicy>never</updatePolicy>
        <checksumPolicy>warn</checksumPolicy>
      </snapshots>
    </pluginRepository>
    <pluginRepository>
      <id>codehaus-snapshots</id>
      <name>Codehaus Snapshots</name>
      <url>http://snapshots.repository.codehaus.org</url>
      <releases>
        <enabled>false</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
        <updatePolicy>never</updatePolicy>
        <checksumPolicy>warn</checksumPolicy>
      </snapshots>
    </pluginRepository>
    <pluginRepository>
      <id>jnet-esb-repo</id>
      <name>dev.java.net open-esb repository</name>
      <url>http://download.java.net/maven/esb</url>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
    </pluginRepository>
    <pluginRepository>
      <id>openesb-release</id>
      <name>OpenESB repo release</name>
      <url>http://openesb-dev.org:8081/nexus/content/repositories/openesb-release/</url>
    </pluginRepository>
    <pluginRepository>
      <id>openesb-snapshot</id>
      <name>OpenESB repo release</name>
      <url>http://nexus.openesb-dev.org:8081/nexus/content/repositories/openesb-snapshot/</url>
    </pluginRepository>
  </pluginRepositories>
  <dependencyManagement>
    <dependencies>
      <!-- used in ri-clients/jbi-ant-tasks, runtime/manage runtime/ui. -->
      <dependency>
        <groupId>ant</groupId>
        <artifactId>ant</artifactId>
        <version>1.6.5</version>
      </dependency>
      <dependency>
        <groupId>ant-contrib</groupId>
        <artifactId>ant-contrib</artifactId>
        <version>1.0b2</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>ant-testutil</groupId>
        <artifactId>ant-testutil</artifactId>
        <version>DEV</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>jmx4ant</groupId>
        <artifactId>jmx4ant</artifactId>
        <version>1.2b1</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>bouncycastle</groupId>
        <artifactId>bcprov</artifactId>
        <version>jdk14-125</version>
        <scope>compile</scope>
      </dependency>
      <!-- used by esb-util, created from glassfish by jbi-compileconf. -->
      <dependency>
        <groupId>glassfish</groupId>
        <artifactId>appserv-ext</artifactId>
        <version>9.1</version>
      </dependency>
      <!-- used by runtime/manage tests, created from glassfish by jbi-compileconf. -->
      <dependency>
        <groupId>glassfish</groupId>
        <artifactId>webservices-rt</artifactId>
        <version>9.1</version>
      </dependency>
      <dependency>
        <groupId>jdmk</groupId>
        <artifactId>jdmkrt</artifactId>
        <version>5_1-b34.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jsf</groupId>
        <artifactId>jsf-api</artifactId>
        <version>1.1.02</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jsf</groupId>
        <artifactId>jsf-impl</artifactId>
        <version>1.1.02</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>jax-qname</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>jaxb-api</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>jaxb-impl</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>jaxb-libs</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>jaxb-xjc</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>namespace</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>relaxngDatatype</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>xercesImpl</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jwsdp</groupId>
        <artifactId>xsdlib</artifactId>
        <version>jwsdp-1.5</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>lockhart</groupId>
        <artifactId>dataprovider</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>lockhart</groupId>
        <artifactId>jh</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>lockhart</groupId>
        <artifactId>jhall</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>lockhart</groupId>
        <artifactId>jhbasic</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>lockhart</groupId>
        <artifactId>jsearch</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>lockhart</groupId>
        <artifactId>jstl</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>lockhart</groupId>
        <artifactId>suntheme</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>lockhart</groupId>
        <artifactId>webui</artifactId>
        <version>3.0.1</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>jsftemplating</groupId>
        <artifactId>jsftemplating</artifactId>
        <version>20060601-DEV</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>xmlbeans</groupId>
        <artifactId>xbean</artifactId>
        <version>2.2.0</version>
        <scope>compile</scope>
      </dependency>
      <dependency>
        <groupId>commons-httpclient</groupId>
        <artifactId>commons-httpclient</artifactId>
        <version>3.0-beta1</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>commons-logging</groupId>
        <artifactId>commons-logging</artifactId>
        <version>1.0.4</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>dom4j</groupId>
        <artifactId>dom4j</artifactId>
        <version>1.5</version>
        <type>jar</type>
        <exclusions>
          <exclusion>
            <artifactId>jsr173</artifactId>
            <groupId>javax.xml</groupId>
          </exclusion>
        </exclusions>
      </dependency>
      <dependency>
        <groupId>htmlunit</groupId>
        <artifactId>htmlunit</artifactId>
        <version>1.7</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>jaxen</groupId>
        <artifactId>jaxen</artifactId>
        <version>1.1-beta-6</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>jboss</groupId>
        <artifactId>jboss-schemaorg_apache_xmlbeans.system</artifactId>
        <version>4.0.2</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>jboss</groupId>
        <artifactId>jboss-jmx</artifactId>
        <version>4.0.2</version>
        <type>jar</type>
      </dependency>
      <!-- used by jbi 1.0 api: -->
      <dependency>
        <groupId>javax.transaction</groupId>
        <artifactId>transaction-api</artifactId>
        <version>${javax.transaction.version}</version>
      </dependency>
      <!-- used by jbi 1.0 api: -->
      <dependency>
        <groupId>javax.activation</groupId>
        <artifactId>activation</artifactId>
        <version>${javax.activation.version}</version>
      </dependency>
      <!-- used by runtime/base: -->
      <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>servlet-api</artifactId>
        <version>2.5</version>
      </dependency>
      <!-- used by runtime/manage: -->
      <dependency>
        <groupId>javax.xml.bind</groupId>
        <artifactId>jaxb-api</artifactId>
        <version>2.2</version>
      </dependency>
      <!-- used by runtime/base: -->
      <dependency>
        <groupId>javax.xml.soap</groupId>
        <artifactId>saaj-api</artifactId>
        <version>1.3</version>
        <exclusions>
          <exclusion>
            <!--
             ! maven central pom references the 1.0.2 version of this jar,
             ! which is unavailable.  Exclude it because we use 1.1.
             ! The java.net legacy repo has the corrected pom.  RT 10/12/07
             !-->
            <groupId>javax.activation</groupId>
            <artifactId>activation</artifactId>
          </exclusion>
        </exclusions>
      </dependency>
      <!-- used by runtime/wsdl: -->
      <dependency>
        <groupId>javax.xml.stream</groupId>
        <artifactId>jsr173_1.0_api</artifactId>
        <version>1.0</version>
      </dependency>
      <dependency>
        <groupId>nekohtml</groupId>
        <artifactId>nekohtml</artifactId>
        <version>0.9.5</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>org.apache.maven</groupId>
        <artifactId>maven-script-ant</artifactId>
        <version>2.0.1</version>
      </dependency>
      <dependency>
        <groupId>rhino</groupId>
        <artifactId>js</artifactId>
        <version>1.6R1</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>saxpath</groupId>
        <artifactId>saxpath</artifactId>
        <version>1.0-FCS</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>wsdl4j</groupId>
        <artifactId>wsdl4j</artifactId>
        <version>1.6.2</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>xerces</groupId>
        <artifactId>xmlParserAPIs</artifactId>
        <version>2.2.1</version>
        <type>jar</type>
      </dependency>
      <dependency>
        <groupId>xerces</groupId>
        <artifactId>xercesImpl</artifactId>
        <version>2.6.2</version>
        <type>jar</type>
      </dependency>
      <!--esb-packages/websphere-package overrides this jaxb version dependency-->
      <dependency>
        <groupId>com.sun.xml.bind</groupId>
        <artifactId>jaxb-impl</artifactId>
        <version>2.2.7</version>
      </dependency>
      <dependency>
        <groupId>jta</groupId>
        <artifactId>jta-1_1-classes</artifactId>
        <version>1.1</version>
        <type>zip</type>
      </dependency>
    </dependencies>
  </dependencyManagement>
  <properties>
    <!--
     ! Note - the order of these declarations is unimportant.
    -->
    <ASADMIN_USER>admin</ASADMIN_USER>
    <ASADMIN_PASSWORD>adminadmin</ASADMIN_PASSWORD>
    <!--
     ! openesb.currentVersion sets the pom.version for artifacts
     ! produced by all projects that inherit from this pom.
     ! This parent pom will remain at version 1.0, since it does
     ! not produce any artifacts except for the pom itself.
     ! RT 6/29/06.
     -->
    <openesb.currentVersion>2.4.0-SNAPSHOT</openesb.currentVersion>

    <AS8BASE>${as8base}</AS8BASE>
    <as8base>${env.JV_AS8BASE}</as8base>
    <JBI_HOME>${as8base}/jbi</JBI_HOME>
    <SRCROOT>${env.JV_SRCROOT}</SRCROOT>
    <codelineName>${env.CODELINE}</codelineName>
    <cvstimeStamp>${env.UCVSUPDATETIMEDOT}</cvstimeStamp>
    <TOOLROOT>${env.JV_TOOLROOT}</TOOLROOT>
    <!-- sure-fire (junit) props: -->
    <testFailureIgnore>true</testFailureIgnore>
    <maven.test.failure.ignore>${testFailureIgnore}</maven.test.failure.ignore>
    <localRepository>${maven.repo.local}</localRepository>

    <releaseRepositoryUrl>file:///${SRCROOT}/bld/publish/maven/release-version</releaseRepositoryUrl>
    <snapshotRepositoryUrl>file:///${SRCROOT}/bld/publish/maven/snapshot-version</snapshotRepositoryUrl>
    <snapshotTime>${env.JBI_SNAPSHOT_TIME}</snapshotTime>

    <!-- these properties are used in jbi/jbi1/packaging pom.  RT 11/20/08 -->
    <javax.transaction.version>1.1</javax.transaction.version>
    <javax.activation.version>1.1</javax.activation.version>
  </properties>
  <reporting>
    <outputDirectory>bld/site</outputDirectory>
  </reporting>
  <repositories>
    <repository>
      <id>download-java-net-legacy-repository</id>
      <name>download.java.net Maven 1.x repository</name>
      <url>http://download.java.net/maven/1</url>
      <layout>legacy</layout>
      <releases>
        <enabled>true</enabled>
      </releases>
      <snapshots>
        <enabled>true</enabled>
      </snapshots>
    </repository>
    <repository>
      <id>openesb-release</id>
      <name>OpenESB repo release</name>
      <url>http://openesb-dev.org:8081/nexus/content/repositories/openesb-release/</url>
    </repository>
    <repository>
      <id>openesb-snapshot</id>
      <name>OpenESB repo release</name>
      <url>http://nexus.openesb-dev.org:8081/nexus/content/repositories/openesb-snapshot/</url>
    </repository>
    <repository>
      <id>release</id>
      <name>OpenESB repo release</name>
      <url>http://nexus.openesb-dev.org:8081/nexus/content/repositories/releases/</url>
    </repository>
    <repository>
      <id>snapshot</id>
      <name>OpenESB repo release</name>
      <url>http://nexus.openesb-dev.org:8081/nexus/content/repositories/snapshots/</url>
    </repository>
  </repositories>
</project>
