/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JFChartConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property;

/**
 *
 * @author rdwivedi
 */
public interface JFChartConstants {
    
    public static String BAR_CHART = "barChart";
    public static String PIE_CHART = "pieChart";
    public static String XY_CHART = "xyChart";
    public static String METER_CHART = "meterChart";
    
    
    
    public static String TITLE = "_title";
    public static String CHART_TYPE = "_chartType";
    public static String X_AXIS_TITLE = "_xAxisTitle";
    public static String Y_AXIS_TITLE = "_yAxisTitle";
    
    public static String ENABLE_3D = "_enable3D";
    public static String CHART_QUERY = "_chartQuery";
    public static String CHART_REFRESH = "_refreshRate";
    
    
    public static String CATEGORY_COLUMN_NAME = "_categoryColName";
    public static String CATEGORY_COLUMN_NAME_DISPLAY = "Category Column";
    public static String SERIES_COLUMN_NAME = "_seriesColName";
    public static String SERIES_COLUMN_NAME_DISPLAY = "Series Column";
    public static String VALUE_COLUMN_NAME = "_valueColName";
    public static String VALUE_COLUMN_NAME_DISPLAY = "Value Column";
    
    public static String CHART_DB_TABLE_NAME = "_chartDBTableName";
    public static String NO_COLUMN = "_ncs";
    
    public static String X_COLUMN_NAME = "_xColName";
    public static String Y_COLUMN_NAME = "_yColName";
    public static String X_COLUMN_NAME_DISPLAY = "X Axis Column";
    public static String Y_COLUMN_NAME_DISPLAY = "Y Axis Column";
    
    
    
    
    // Temp way to identify the chart UI props , and data props
    public static String CHART_PROPERTIES = "_chartAllProps";
    public static String CHART_COMMON_PROPERTIES = "_chartCProps";
    public static String CHART_SPECIFIC_PROPERTIES = "_chartSProps";
    public static String CHART_DB_PROPERTIES = "_chartDBProps";
    public static String CHART_BAR_PROPERTIES = "_chartbarProps";
    
    
    
    // Data Set 
    public static String DATASET_TYPE = "_dataSetType";
    public static String DATASET_TYPE_SELECTED = "_selectedDataSetType";
    
    
    public static String XY_DATASET = "_xyDataSet";
    public static String XY_DATASET_DISPLAY = "XY Dataset";
    
    public static String PIE_DATASET = "_pieDataSet";
    public static String PIE_DATASET_DISPLAY = "Pie Dataset";
    
    public static String VALUE_DATASET = "_valueDataSet";
    public static String VALUE_DATASET_DISPLAY = "Single Value Dataset";
    
    public static String CATEGORY_DATASET = "_categoryDataSet";
    public static String CATEGORY_DATASET_DISPLAY = "Category Dataset";
    
    public static String IEP_GROUP = "_iepGrp";
    public static String IEP_SELECTED = "_iepSelected";
    
    public static String IEP_DB_TABLE_GROUP = "_dbGroup";
    public static String IEP_DB_TABLE_SELECTED = "_dbTableSelected";
    
    
    
    public static String DB_CONNECTION_PROPERTIES = "_dbConnectionProps";
    public static String DB_URL = "_dbURL";
    public static String DB_UID = "_dbUID";
    public static String DB_UPWD = "_dbUPWD";
    
    public static String CHARTING_TYPE_PROPERTIES = "_chartingType";
    public static String CHARTING_TYPE_SELECTED = "_chartingTS";
    public static String IEP_BASED_CHARTING = "_iepBased";
    public static String DB_TABLE_BASED_CHARTING = "_tableBased";
    
    
    public static String CHART_TYPES_PROPERTIES = "_chartTypes";
    public static String SELECT_CHART_TYPE = "_chartTypeSelected";
    
    
    public static String DATASOURCE_SELECTED = "_dataSourceSelected";
    
    
    
    
}
