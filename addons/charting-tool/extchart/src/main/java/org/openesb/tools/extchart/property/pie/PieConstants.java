/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PieConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property.pie;

import java.awt.Font;

/**
 * PieConstants.java
 *
 * @author Wei
 * @version :$Revision: 1.4 $
 */

public interface PieConstants {
    /** RADIUS_KEY is the key for a radius default. */
    public static final String RADIUS_KEY = "pd_radius";

    /** SECTION_LABEL_TYPE_KEY is the key for pie chart section labels type. */
    public static final String SECTION_LABEL_TYPE_KEY = "pd_sectionLabelsType";

    /** TOOLTIP_LABEL_FORMAT_KEY is the key for pie chart tooltip labels format. */
    public static final String TOOLTIP_LABEL_FORMAT_KEY = "pd_tooltipLabelsType";

    /** INDIVIDUAL_SLICE_LABEL_KEY is the key for pie chart labels. */
    public static final String INDIVIDUAL_SLICE_LABEL_KEY = "pd_label";

    /** INDIVIDUAL_EXPLODE_SLICE_KEY is the key for pie chart exploded slices on/off. */
    public static final String INDIVIDUAL_SLICE_IS_EXPLODED_KEY = "pd_isexploded";

    /** INDIVIDUAL_EXPLOEDED_SLICE_PERCENT_KEY is the key for exploded slices percentage. */
    public static final String INDIVIDUAL_SLICE_EXPLODE_PERCENT_KEY = "pd_explodepercent";

    /** Key for SectionLabelFont */
    public static final String SECTION_LABEL_FONT_KEY = "section-label-font";

    /** Key for SectionLabelGap */
    public static final String SECTION_LABEL_GAP_KEY = "section-label-gap";

    /** Key for SectionLabelPaint */
    public static final String SECTION_LABEL_PAINT_KEY = "section-label-paint";

    /** Key for showSeriesLabels */
    public static final String SHOW_SECTION_LABELS_KEY = "show-section-labels";

    /** Key for SeriesLabelFont */
    public static final String SERIES_LABEL_FONT_KEY = "series-label-font";

    /** Key for SeriesLabelPaint */
    public static final String SERIES_LABEL_PAINT_KEY = "series-label-paint";

    /** SECTION_NO_LABELS it the key for no label display. */
    public static final int SECTION_NO_LABELS = 0;

    /** SECTION_NAME_LABELS it the key for name label display. */
    public static final int SECTION_NAME_LABELS = 1;

    /** SECTION_VALUE_LABELS it the key for value label display. */
    public static final int SECTION_VALUE_LABELS = 2;

    /** SECTION_PERCENT_LABELS it the key for percent label display. */
    public static final int SECTION_PERCENT_LABELS = 3;

    /** SECTION_NAME_AND_VALUE_LABELS it the key for name/value label display. */
    public static final int SECTION_NAME_AND_VALUE_LABELS = 4;

    /** SECTION_NAME_AND_PERCENT_LABELS it the key for name/percent label display. */
    public static final int SECTION_NAME_AND_PERCENT_LABELS = 5;

    /** SECTION_VALUE_AND_PERCENT_LABELS it the key for value/percent label display. */
    public static final int SECTION_VALUE_AND_PERCENT_LABELS = 6;

    /** SECTION_NAME_VALUE_AND_PERCENT_LABELS it the key for value/percent label display. */
    public static final int SECTION_NAME_VALUE_AND_PERCENT_LABELS = 7;

    /** TOOLTIP_NO_LABELS it the key for no label display. */
    public static final int TOOLTIP_NO_LABELS = 0;

    /** TOOLTIP_NAME_LABELS it the key for name label display. */
    public static final int TOOLTIP_NAME_LABELS = 1;

    /** TOOLTIP_VALUE_LABELS it the key for value label display. */
    public static final int TOOLTIP_VALUE_LABELS = 2;

    /** TOOLTIP_PERCENT_LABELS it the key for percent label display. */
    public static final int TOOLTIP_PERCENT_LABELS = 3;

    /** TOOLTIP_NAME_AND_VALUE_LABELS it the key for name/value label display. */
    public static final int TOOLTIP_NAME_AND_VALUE_LABELS = 4;

    /** TOOLTIP_NAME_AND_PERCENT_LABELS it the key for name/percent label display. */
    public static final int TOOLTIP_NAME_AND_PERCENT_LABELS = 5;

    /** TOOLTIP_VALUE_AND_PERCENT_LABELS it the key for value/percent label display. */
    public static final int TOOLTIP_VALUE_AND_PERCENT_LABELS = 6;

    /** TOOLTIP_NAME_VALUE_AND_PERCENT_LABELS it the key for value/percent label display. */
    public static final int TOOLTIP_NAME_VALUE_AND_PERCENT_LABELS = 7;

    /** DEFAULT_PIE_CHART_TITLE is used as a hard default. */
    public static final String DEFAULT_PIE_CHART_TITLE = "Pie Chart";

    /** DEFAULT_RADIUS is used as a hard default. */
    public static final double DEFAULT_RADIUS = 0.70;

    /** DEFAULT_IS_EXPLODED is used as a hard default. */
    public static final boolean DEFAULT_IS_EXPLODED = false;

    /** DEFAULT_MINIMUM_EXPLODED_PERCENT is used as a hard default. */
    public static final int DEFAULT_MINIMUM_EXPLODED_PERCENT = 0;

    /** DEFAULT_MAXIMUM_EXPLODED_PERCENT is used as a hard default. */
    public static final int DEFAULT_MAXIMUM_EXPLODED_PERCENT = 100;

    /** DEFAULT_ALL_EXPLODED_PERCENT is used as a hard default. */
    public static final int DEFAULT_ALL_EXPLODED_PERCENT = 25; // DEFAULT_MINIMUM_EXPLODED_PERCENT;

    /** SECTION_NO_LABELS_STRING is used for XML persistency. */
    public static final String SECTION_NO_LABELS_STRING = "None";

    /** SECTION_NAME_LABELS_STRING is used for XML persistency. */
    public static final String SECTION_NAME_LABELS_STRING = "Name";

    /** SECTION_VALUE_LABELS_STRING is used for XML persistency. */
    public static final String SECTION_VALUE_LABELS_STRING = "Value";

    /** SECTION_PERCENT_LABELS_STRING is used for XML persistency. */
    public static final String SECTION_PERCENT_LABELS_STRING = "Percent";

    /** SECTION_NAME_AND_VALUE_LABELS_STRING is used for XML persistency. */
    public static final String SECTION_NAME_AND_VALUE_LABELS_STRING = "Name,Value";

    /** SECTION_NAME_AND_PERCENT_LABELS_STRING is used for XML persistency. */
    public static final String SECTION_NAME_AND_PERCENT_LABELS_STRING = "Name,Percent";

    /** SECTION_VALUE_AND_PERCENT_LABELS_STRING is used for XML persistency. */
    public static final String SECTION_VALUE_AND_PERCENT_LABELS_STRING = "Value,Percent";

    /** SECTION_NAME_VALUE_AND_PERCENT_LABELS_STRING is used for XML persistency. */
    public static final String SECTION_NAME_VALUE_AND_PERCENT_LABELS_STRING = "Name,Value,Percent";

    /** TOOLTIP_NO_LABELS_STRING is used for XML persistency. */
    public static final String TOOLTIP_NO_LABELS_STRING = "None";

    /** TOOLTIP_NAME_LABELS_STRING is used for XML persistency. */
    public static final String TOOLTIP_NAME_LABELS_STRING = "Name";

    /** TOOLTIP_VALUE_LABELS_STRING is used for XML persistency. */
    public static final String TOOLTIP_VALUE_LABELS_STRING = "Value";

    /** TOOLTIP_PERCENT_LABELS_STRING is used for XML persistency. */
    public static final String TOOLTIP_PERCENT_LABELS_STRING = "Percent";

    /** TOOLTIP_NAME_AND_VALUE_LABELS_STRING is used for XML persistency. */
    public static final String TOOLTIP_NAME_AND_VALUE_LABELS_STRING = "Name,Value";

    /** TOOLTIP_NAME_AND_PERCENT_LABELS_STRING is used for XML persistency. */
    public static final String TOOLTIP_NAME_AND_PERCENT_LABELS_STRING = "Name,Percent";

    /** TOOLTIP_VALUE_AND_PERCENT_LABELS_STRING is used for XML persistency. */
    public static final String TOOLTIP_VALUE_AND_PERCENT_LABELS_STRING = "Value,Percent";

    /** TOOLTIP_NAME_VALUE_AND_PERCENT_LABELS_STRING is used for XML persistency. */
    public static final String TOOLTIP_NAME_VALUE_AND_PERCENT_LABELS_STRING = "Name,Value,Percent";
    public static final int DEFAULT_ALL_FIELDS_FONT_STYLE  = Font.PLAIN ;
    public static final int DEFAULT_ALL_FIELDS_FONT_SIZE = 9;
    

}
