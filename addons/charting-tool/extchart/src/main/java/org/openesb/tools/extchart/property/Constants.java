/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Constants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property;

/**
 * Constants used by chart/report properties
 *
 * @author Wei Han, Rahul Dwivedi
 * @version :$Revision: 1.4 $
 */

public interface Constants {
    /** Left alignment */
    public static final int LEFT = 0;
    /** Center alignment */
    public static final int CENTER = 1;
    /** Right alignment */
    public static final int RIGHT = 2;
    /** Top alignment */
    public static final int TOP = 3;
    /** Bottom alignment */
    public static final int BOTTOM = 4;

    /** font name suffix */
    public static final String FONT_NAME_SUFFIX = "-name";

    /** font name suffix */
    public static final String FONT_STYLE_SUFFIX = "-style";

    /** font name suffix */
    public static final String FONT_SIZE_SUFFIX = "-size";

    /** DISPLAY_TITLE_KEY is a key to a default. */
    public static final String DISPLAY_TITLE_KEY = "vd_displayTitleKey";

    /** TITLE_KEY is a key to a default. */
    public static final String TITLE_KEY = "vd_titleKey";

    /** TITLE_FONT_SIZE_KEY is a key to a default. */
    public static final String TITLE_FONT_SIZE_KEY = "vd_titleFontSizeKey";

    /** TITLE_FONT_KEY is a key to a default. */
    public static final String TITLE_FONT_KEY = "title-font";

    /** TITLE_ALIGNMENT_KEY is a key to a default. */
    public static final String TITLE_ALIGNMENT_KEY = "vd_titleAlignmentKey";

    /** Key for title text color */
    public static final String TITLE_COLOR_KEY = "title-color";

    /** Key for title background color */
    public static final String TITLE_BACKGROUND_KEY = "title-background";

    /** BACKGROUND_COLOR_KEY is a key to a default. */
    public static final String BACKGROUND_COLOR_KEY = "vd_backgroundColorKey";

    /** TEXT_COLOR_KEY is a key to a default. */
    public static final String TEXT_COLOR_KEY = "vd_textColorKey";

    /** NULL_REAL_KEY is a key to a default. */
    public static final String NULL_REAL_KEY = "vd_nullRealKey";

    /** NULL_INTEGRAL_KEY is a key to a default. */
    public static final String NULL_INTEGRAL_KEY = "vd_nullIntegralKey";

    /** NULL_STRING_KEY is a key to a default. */
    public static final String NULL_STRING_KEY = "vd_nullStringKey";

    /** INTEGRAL_NUMBER_FORMAT_KEY is a key to a default. */
    public static final String INTEGRAL_NUMBER_FORMAT_KEY = "vd_IntegralNumberFormatKey";

    /** REAL_NUMBER_FORMAT_KEY is a key to a default. */
    public static final String REAL_NUMBER_FORMAT_KEY = "vd_RealNumberFormatKey";

    /** Key for BorderVisible property */
    public static final String BORDER_VISIBLE_KEY = "border-visible";

    /** Key for BorderPaint property */
    public static final String BORDER_PAINT_KEY = "border-paint";

    /** Key for refreshing frequency */
    public static final String FREQUENCY = "Frequency";

      /** Key for data limit property */
    public static final String DATA_LIMIT_KEY = "data-limit";
}
