/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DataBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.charting.persist;

import org.openesb.tools.extchart.exception.ChartException;
import org.openesb.tools.extchart.jfchart.data.DataAccess;
import org.openesb.tools.extchart.jfchart.data.SQLDataAccess;
import org.openesb.tools.extchart.jfchart.data.SampleDemoDataAccess;

/**
 *
 * @author rdwivedi
 */
public class DataBean {
    
    private String dbJndiName = null;
    private String displayName = null;
    private String mId = null;
    private String mQuery = null;
    private String mParentName = null;
    private String dataSetType = null;
    private String grpID = null;

    public DataBean(String dN, String pName) {
        displayName = dN;
        mParentName = pName;
        grpID = "12";
    }
    public String getDisplayName() {
        return displayName;
    }
    public String getJNDIName() {
        return dbJndiName;
    }
     public void setJNDIName(String jn) {
        dbJndiName = jn;
    }
    public void setID(String id) {
        mId = id;
    }
    public String getGroupID() {
     return grpID;   
    }
    public String getParentDisplayName() {
        return mParentName;
    }
    public void setQuery(String query) {
        mQuery = query;
    }
    public String getQuery() {
        return mQuery;
    }
     public void createNewID() {
        mId = "_d" + System.currentTimeMillis();
    }
     public String getID() {
         return mId;
     }
     public String getDataSetType() {
         return dataSetType;
     }
     public void setDataSetType(String ddsType) {
         dataSetType = ddsType;
     }
     public DataAccess getDataAccessObject() {
         if(dbJndiName == null || dbJndiName.length() < 1) {
             return new SampleDemoDataAccess(dataSetType);
         } else {
             return new SQLDataAccess(dbJndiName, dataSetType,getQuery());
         }
         
     }
     public boolean validate() throws ChartException {
         Object obj = getDataAccessObject().getDataSet();
         return obj!=null;
           
    }
}
