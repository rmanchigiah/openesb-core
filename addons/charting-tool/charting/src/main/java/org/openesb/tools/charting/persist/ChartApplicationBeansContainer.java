/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ChartApplicationBeansContainer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.charting.persist;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;
import org.openesb.tools.charting.exception.ChartingException;

/**
 *
 * @author rdwivedi
 */
public class ChartApplicationBeansContainer {
    
    private TreeMap  map = null;
    ChartBean mCurrent = null;
    private Map mDAMap = null;
    private String dbEditorID = null;
    ChartDataPersistence helper = new ChartDataPersistence();
    
    private static Logger mLogger = Logger.getLogger(ChartApplicationBeansContainer.class.getName());
    public ChartApplicationBeansContainer() {
        map = new TreeMap();
        mDAMap = new HashMap();
        init();
    }
    public String getTitle() {
        return "Charting";
    }
    public void init() {
        try {
            
            
           List daList = helper.getAllAvailableDataAccess();
           if(daList!=null) {
               Iterator iter = daList.iterator();
               while(iter.hasNext()) {
                    DataBean b = (DataBean)iter.next();
                    addADataBean(b);
                    
                }
           }
           
           List list = helper.getAllAvailableChartIDS();
            if(list!= null) {
                Iterator iter = list.iterator();
                while(iter.hasNext()) {
                    ChartBean b = (ChartBean)iter.next();
                    addAChartBean(b);
                    
                }
            }
        } catch(Exception e) {
            mLogger.severe("Error on init" + e.toString());
            e.printStackTrace();
        }
    }

        
 public void addAChartBean(ChartBean bean) {
        TreeMap o = (TreeMap)map.get(bean.getChartParentName());
        if(o == null) {
            o = new TreeMap();
            map.put(bean.getChartParentName(),o);
        }
        o.put(bean.getChartID(),bean);
    }
    
    public ChartBean getChartBeanByID(String id) {
        ChartBean b = null;
        Iterator iter = map.values().iterator();
        while (iter.hasNext()) {
            TreeMap tm = (TreeMap)iter.next();
            if(tm.containsKey(id)) {
                b = (ChartBean)tm.get(id);
            }
            
        }
        return b;
    }
    
    public Set getAllChartGroups() {
        return map.keySet();
    }
    public Map getAllChartsForGroup(String gName) {
        Map m = null;
        if(map.containsKey(gName)) {
            m =(TreeMap) map.get(gName);
        }
        return m;
    }
    
    public ChartBean  createNewChart(String name , String pName) {
        ChartBean b = new ChartBean();
        b.setChartName(name);
        b.setChartParentName(pName);
        b.createNewChartID();
        this.addAChartBean(b);
        return b;
    }
    
    public ChartBean getCurrentChartBeanOnEditor() {
        return mCurrent;
    }
    public void setCurrentChartBeanOnEditor(ChartBean n) {
        mCurrent = n;
    }
    
    
    public void addADataBean(DataBean bean) {
        mDAMap.put(bean.getID(),bean);
    }
    
     public DataBean  createNewDataBean(String name , String pName) {
        DataBean b = new DataBean(name,pName);
        b.createNewID();
        mDAMap.put(b.getID(),b);
        mLogger.info("Adding new DBBean" + b.getID() + "Name " + name + "parent name" + pName);
        return b;
    }
     
     public Map getAllDataBeans() {
         return mDAMap;
     }
     public DataBean getDataBeanByID(String id) {
         
         return (DataBean)mDAMap.get(id);
     }
     
     public void setCurrentDBABeanOnEditorID(String id) {
         mLogger.info("Setting id = " + id + " prv id was =" + dbEditorID );
         dbEditorID = id;
         
     }
     public String getCurrentDBABeanOnEditorID() {
         return dbEditorID;
     }
     
     public void persistDataBean(String id) throws ChartingException {
         mLogger.info("Saving DB " + id + "Current bean is " + dbEditorID );
         if(dbEditorID.equals(id)){
            DataBean bean =  getDataBeanByID(id);
            helper.upsertGroup(bean.getGroupID(), bean.getParentDisplayName());
            helper.persistDataAccessBean(bean);
         }
     }
     
     public void persistCurrentChartOnEditor() throws ChartingException {
         helper.upsertGroup(mCurrent.getParentID(),mCurrent.getChartParentName());  
         helper.persistChartBean(mCurrent);
     }
     
     public DataBean getDataAccessBeanForCurrentChart() {
         String dbID = mCurrent.getChartDataBeanID();
         return this.getDataBeanByID(dbID);
     }
    
}
