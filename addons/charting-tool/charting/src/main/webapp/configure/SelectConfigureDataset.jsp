<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:ui="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <jsp:directive.page import="java.util.logging.Logger,org.openesb.tools.charting.persist.ChartBean,org.openesb.tools.charting.persist.ChartApplicationBeansContainer"/>
    <jsp:scriptlet>
        Logger mLogger = Logger.getLogger("[WebJsp.configure.SelectConfigureeDataset_JSP");
        String cID = request.getParameter("_chartUID"); 
        boolean isNew = true; 
        if(cID == null || cID.length()&lt;1) { 
        } else {
        mLogger.info("CID :" + cID); 
        ChartApplicationBeansContainer container = (ChartApplicationBeansContainer)session.getAttribute("ChartApplicationBeansContainer");
        ChartBean bean = container.getChartBeanByID(cID);
        container.setCurrentChartBeanOnEditor(bean); 
        mLogger.info("The bean is " + bean); 
        if(bean!= null) { 
        if(bean.getChartDataBeanID()== null) 
        { isNew = true; 
        } else { 
        isNew = false; 
        }
        }
        }
        if(isNew) {
    </jsp:scriptlet>
    <f:view>
        <ui:page binding="#{configure$SelectConfigureDataset.page1}" id="page1">
            <ui:html binding="#{configure$SelectConfigureDataset.html1}" id="html1">
                <ui:head binding="#{configure$SelectConfigureDataset.head1}" id="head1">
                    <ui:link binding="#{configure$SelectConfigureDataset.link1}" id="link1" url="/resources/stylesheet.css"/>
                </ui:head>
                <ui:body binding="#{configure$SelectConfigureDataset.body1}" id="body1" style="-rave-layout: grid">
                    <ui:form binding="#{configure$SelectConfigureDataset.form1}" id="form1">
                        <ui:dropDown binding="#{configure$SelectConfigureDataset.dropDown1}" id="dropDown1"
                                     items="#{configure$SelectConfigureDataset.options.options}" onChange="common_timeoutSubmitForm(this.form, 'dropDown1');"
                                     style="left: 312px; top: 48px; position: absolute" valueChangeListener="#{configure$SelectConfigureDataset.dropDown1_processValueChange}"/>
                        <ui:label binding="#{configure$SelectConfigureDataset.label1}" id="label1"
                                  style="left: 96px; top: 72px; position: absolute; width: 118px" text="Select Dataset Type"/>
                        <ui:button action="#{configure$SelectConfigureDataset.button1_action}" binding="#{configure$SelectConfigureDataset.button1}"
                                   id="button1" style="left: 191px; top: 192px; position: absolute" text="DONEeee"/>
                        <ui:label binding="#{configure$SelectConfigureDataset.label3}" id="label3"
                                  style="left: 48px; top: 192px; position: absolute; width: 118px" text="Select Chart Type"/>
                        <ui:dropDown binding="#{configure$SelectConfigureDataset.dropDown2}" id="dropDown2"
                                     items="#{configure$SelectConfigureDataset.chartOptions.options}" style="left: 216px; top: 312px; position: absolute"/>
                        <ui:dropDown binding="#{configure$SelectConfigureDataset.dropDown4}" id="dropDown4"
                                     items="#{configure$SelectConfigureDataset.dropDown4DefaultOptions.options}"
                                     onChange="common_timeoutSubmitForm(this.form, 'dropDown4');" style="left: 192px; top: 144px; position: absolute" valueChangeListener="#{configure$SelectConfigureDataset.dropDown4_processValueChange}"/>
                        <ui:label binding="#{configure$SelectConfigureDataset.label2}" id="label2" style="position: absolute; left: 528px; top: 240px" text="asadasdasdasd"/>
                        <ui:textField binding="#{configure$SelectConfigureDataset.textField1}" id="textField1"
                                      style="position: absolute; left: 480px; top: 408px" valueChangeListener="#{configure$SelectConfigureDataset.textField1_processValueChange}"/>
                    </ui:form>
                </ui:body>
            </ui:html>
        </ui:page>
    </f:view>
    <jsp:scriptlet> } else { </jsp:scriptlet>
    <jsp:forward page="../layout/ChartConfigLayout.jsp"/>
    <jsp:scriptlet>}</jsp:scriptlet>
</jsp:root>
