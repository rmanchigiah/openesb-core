<?xml version="1.0" encoding="UTF-8"?>
<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/web/ui">
    <jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>
    <f:view>

    <webuijsf:page>
   <!-- <f:loadBundle basename="com.sun.jbi.cam.common.resources.Bundle" var="msgs" /> -->

    <webuijsf:html>
        <webuijsf:head id="banner" title="Banner" />
        <webuijsf:body>
            <webuijsf:form id="form3">
               <webuijsf:head id="Masthead">
                 
                 <f:facet name="versionLink" >
                    <webuijsf:hyperlink id="version" immediate="true" />
                 </f:facet>

                 <f:facet name="helpLink" >
                    <webuijsf:hyperlink id="help" immediate="true" />
                 </f:facet>
                 
                 
               </webuijsf:head> 
             </webuijsf:form>
       </webuijsf:body>
    </webuijsf:html>
</webuijsf:page>
    
</f:view>

</jsp:root>