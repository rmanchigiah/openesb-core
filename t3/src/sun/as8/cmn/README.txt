#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)README.txt
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

This directory configures the "installas8" tool, but also
contains a couple of common files that are installed in the
platform package directories:

    fixtree.defs - these are shared definitions used by the platform fixtree.cg scripts.
    jbicopy.cg  -  copy jbi libraries into an appsever installation (this is done
                   to "prime" the install.  It is also done during the normal build.

See the make.mmf,install.map config in the platform
directories (nt, solsparc, ...) to see how these files
are configured.

RT 4/2/04
