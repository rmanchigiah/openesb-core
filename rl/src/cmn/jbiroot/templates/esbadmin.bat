@echo off
REM Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
REM Use is subject to license terms.
REM
setlocal
java -classpath @com.sun.jbi.home@/appserver/ant/lib/esb-admin-client.jar;@com.sun.aas.installRoot@/lib/appserv-rt.jar;@com.sun.aas.installRoot@/lib/j2ee.jar;@com.sun.aas.installRoot@/lib/jmxremote.jar;@com.sun.aas.installRoot@/lib/jmxremote_optional.jar;@com.sun.jbi.home@/lib/jbi_rt.jar %ESBADMIN_SYSTEM_PROPERTIES%  com/sun/jbi/ui/admin/cli/JMXCLI --definitionfile @com.sun.jbi.home@/config/esbcli.xml --defaultport @com.sun.jbi.management.JmxRmiPort@ --defaulthost @com.sun.aas.hostName@ %*
endlocal
cmd /c exit %errorlevel%

