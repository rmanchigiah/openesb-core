#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbienv.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#
# WARNING - THIS FILE IS GENERATED.  DO NOT MODIFY
#

export JBI_PS
JBI_PS="@path.separator@"

#set export all:
set -a

AS_INSTALL="@com.sun.aas.installRoot@"
AS_JMX_REMOTE_URL="service:jmx:rmi:///jndi/rmi://@com.sun.aas.hostName@:@com.sun.jbi.management.JmxRmiPort@/management/rmi-jmx-connector"
JBI_HOME="@com.sun.jbi.home@"
JBI_INSTANCE_NAME="@com.sun.aas.instanceName@"
JBI_ADMIN_PORT="@com.sun.jbi.management.JmxRmiPort@"
JBI_ADMIN_HOST="@com.sun.aas.hostName@"
JBI_DOMAIN_ROOT="@com.sun.jbi.domain.root@"
JBI_DOMAIN_DIR="@com.sun.aas.domainsRoot@"
JBI_DOMAIN_NAME="@com.sun.jbi.domain.name@"
JBI_HADAPTOR_PORT="@com.sun.jbi.management.HtmlAdaptorPort@"
JBI_DOMAIN_PROPS="$JBI_DOMAIN_ROOT/jbi/config/jbienv.properties"
JBI_DOMAIN_STARTED="$JBI_DOMAIN_ROOT/jbi/tmp/.jbi_admin_running"
JBI_DOMAIN_STOPPED="$JBI_DOMAIN_ROOT/jbi/tmp/.jbi_admin_stopped"

#clear export all:
set +a

#setup classpath:
export JBI_CLASSPATH
JBI_CLASSPATH=
for jar in $JBI_HOME/lib/*.jar
do
    if [ "$JBI_CLASSPATH" = "" ]; then
        JBI_CLASSPATH="$jar"
    else
        JBI_CLASSPATH="$JBI_CLASSPATH${JBI_PS}$jar"
    fi
done
