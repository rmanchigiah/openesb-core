<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<sun:page>
    <!beforeCreate
       setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
       setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
       setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks");
       setPageSessionAttribute(key="name" value="#{name}");
       setPageSessionAttribute(key="type" value="I18Napplication");
       
       setSessionAttribute(key="managedTargetUrl",        value="../../jbi/cluster/manageJBITargets.jsf")
       setSessionAttribute(key="isJBIArchiveAvailable",  value="$boolean{false}")

       jbiSetManageTargetInstallConfigPS(
          compName="#{ShowBean.name}"
          compType="#{ShowBean.type}")
    />
    
        <sun:html>
            <sun:head id="manageTargets" title="$resource{i18n2.manageTargets.PageTitle}"/>
            <sun:body>
                <sun:form id="manageTargetsForm">
                
		    <sun:hidden id="helpKey" value="$resource{help.jbi.cluster.manageJBITargets}" />			    
                    <sun:breadcrumbs  id="manageJBITargetsBreadcrumbs">
                        <sun:hyperlink url="../../jbi/cluster/root.jsf" text="$resource{i18n.jbi.breadcrumbs.jbiroot.link.text}" />
                        <sun:hyperlink url="#{sessionScope.listUrl}" text="#{sessionScope.listBreadcrumbText}" />
			<sun:hyperlink url="javascript:history.back(-1)" text="#{sessionScope.showBreadcrumbText}" />
                        <sun:hyperlink url="#{sessionScope.managedTargetUrl}" text="$resource{i18n.jbi.breadcrumbs.manage.targets.link.text}" />
                    </sun:breadcrumbs>

#include "jbi/cluster/inc/alertBox.inc"

                    <sun:title id="propertyContentPage" title="#{ShowBean.name} - $resource{i18n2.manageTargets.PageTitle}" 
                        helpText="#{sessionScope.jbiManageTargetsPageHelp}">
                        <!-- Buttons  -->
                        <!facet pageButtonsTop>
                            <sun:panelGroup id="topButtons">
                                <sun:button id="okButton"
                                    text="$resource{i18n2.button.OK}"
                                    primary="#{true}"
                                    onClick="javascript: return submitAndDisable(this, '$resource{i18n2.button.Processing}');">
		                            <!command
# tbd process comp install config...
										# extract the selected target from the available target list
										getUIComponent(clientId="$pageSession{tableRowGroupId}", component=>$attribute{availableTargetTableRowGroup});
										jbiGetSelectedTargetsFromAvailableTable(availableTableRowGroup="$attribute{availableTargetTableRowGroup}", selectedAvailableTargetsList=>$attribute{installTargets});
										getUIComponent(clientId="$pageSession{tableRowGroupId4}", component=>$attribute{installedTargetTableRowGroup});
										jbiGetSelectedTargetsFromInstalledTable(installedTableRowGroup="$attribute{installedTargetTableRowGroup}", selectedInstalledTargetsList=>$attribute{uninstallTargets});
                                       
                                        
                                        jbiManageTargets(installList="$attribute{installTargets}",
                                                         uninstallList="$attribute{uninstallTargets}",
                                                         isAlertNeeded=>$session{isJbiAlertNeeded}, 
                                                         alertSummary=>$session{jbiAlertSummary}, 
                                                         alertDetails=>$session{jbiAlertDetails});
                                        jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}')
                                        redirect(page="showTargets.jsf?type=#{sessionScope.sharedShowType}&name=#{sessionScope.sharedShowName}");
                                    />
                                </sun:button>
                                
                                <sun:button id="cancelStep" 
                                    immediate="#{true}"
                                    primary="$boolean{false}" 
                                    text="$resource{i18n.jbi.wizard.cancel.button}">
                                    <!command
                                         redirect(page="showTargets.jsf?type=#{sessionScope.sharedShowType}&name=#{sessionScope.sharedShowName}");
                                    />
                                </sun:button>

                            </sun:panelGroup>
                            </facet>
                    </sun:title>

"<br />
"<br />
#include "jbi/cluster/inc/sharedAddTarget.inc"

/* the following code only applicable  to jbi components
   ie BC or SE */ 
<!if #{sessionScope.sharedShowType} & 
      ((#{sessionScope.sharedShowType} = binding-component) | 
      (#{sessionScope.sharedShowType} = service-engine)) >
"<br />
"<br />
#include "jbi/cluster/inc/sharedCompInstallationConfig.inc
"<br />
"<br />
</!if>

#include "jbi/cluster/inc/sharedRemoveTarget.inc"
"<br />
"<br />
<!-- TBD replace with inline expansion (.inc file not shared)
#include "jbi/cluster/inc/manageTargetsControlButtons.inc"


                </sun:form>
            </sun:body>
	    
#include "changeButtonsJS.inc"           
	    
        </sun:html>
</sun:page>
