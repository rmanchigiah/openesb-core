<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/cluster/monitorComponent.jsf -->
<sun:page>
    <!beforeCreate
     getRequestValue(key="name", value=>$attribute{name});
     getRequestValue(key="type", value=>$attribute{type});
     //setJBIComponentId(JBIComponentName="$requestParameter{name}",JBIComponentType="$requestParameter{type}");
     setJBIComponentId(JBIComponentName="$attribute{name}",JBIComponentType="$attribute{type}");
     setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
     setResourceBundle(key="i18n2" bundle="com.sun.enterprise.tools.admingui.resources.Strings")
     setResourceBundle(key="help" bundle="com.sun.enterprise.tools.admingui.resources.Helplinks");

     setSessionAttribute(key="showNameLinkUrl", value="monitorComponent.jsf")
     
<!------------------------------------------------------------------------------------>
<!-- Retrieve the list of instances that will be displayed in the update listbox.   -->
<!------------------------------------------------------------------------------------>
jbiGetInstances(type="#{sessionScope.sharedRequestType}"
                compOrSaName="#{sessionScope.sharedShowName}"
                instancesList=>$page{instancesList}
                firstInstance=>$page{firstInstance}
                isAlertNeeded=>$session{isJbiAlertNeeded}
                alertSummary=>$session{jbiAlertSummary}
                alertDetails=>$session{jbiAlertDetails}
                installedOnZeroTargets=>$session{installedOnZeroTargets}
);


<!-- If not set, initialize the "from value" (Happens when page is first displayed)  -->
if (!$attribute{fromVal}) {
    setAttribute(key="fromVal" value="$pageSession{firstInstance}");
    jbiInitUpdateList(instanceName="$attribute{fromVal}");
}
<!--------------------------------------------------------------------------------->
<!-- Since we are using navigate, we need to set the request variables that are  -->
<!-- used by the backing bean.                                                   -->
<!--------------------------------------------------------------------------------->
jbiSetCompStatsForInstance(
  compName="$session{sharedShowName}" 
  compType="$session{sharedShowType}" 
  instanceName="$attribute{fromVal}"
  isAlertNeeded=>$session{isJbiAlertNeeded}
  alertSummary=>$session{jbiAlertSummary}
  alertDetails=>$session{jbiAlertDetails}
  showStartButton=>$session{showStartButton}
);

     <!-- Make sure the name and type session variables are set -->
     if (!$session{sharedShowName}) {
        //setSessionAttribute(key="sharedShowName" value="$requestParameter{name}")
        setSessionAttribute(key="sharedShowName" value="$attribute{name}")
     }
     if (!$session{sharedShowType}) {
        //setSessionAttribute(key="sharedShowType" value="$requestParameter{type}")
        setSessionAttribute(key="sharedShowType" value="$attribute{type}")
     }
     
setSessionAttribute(key="jbiSelectedInstanceValue", value="$attribute{fromVal}" );
          
    />

    <sun:html>
        <sun:head id="jbiMonitorComponentHead" />
        <sun:body>
            <sun:form id="breadcrumbsForm">
		<sun:hidden id="helpKey" value="$resource{help.jbi.cluster.monitorComponent}" />
#include treeBreadcrumbs.inc
                 </sun:form>
            
             <sun:form id="tabsForm">
#include "jbi/cluster/inc/showTabs.inc"
             </sun:form>

#include "jbi/cluster/inc/alertBox.inc"

            <sun:form id="jbiMonitorComponentForm">
            
                "<br />
                <sun:image id="indentDropDown"
                    align  = "top"
                    height = "$int{10}"
                    url    = "/resource/images/dot.gif"
                    width  = "$int{8}"
                />
                <sun:dropDown id="FromInstance"
                    immediate  = "#{true}"
                    items      = "$pageSession{instancesList}"
                    label      = "$resource{i18n.jbi.monitor.component.page.instance.dropdown.label}"
                    selected   = "#{fromVal}"
                    submitForm = "#{true}"
                    rendered   = "#{sessionScope.installedOnZeroTargets == 'false'}"
                    >
                    <!command
                        setAttribute(key="click" value="$this{component}");
                        setAttribute(key="fromVal" value="#{click.selected}")
                        setAttribute(key="dropDownChange" value="true")
                        navigate(page="jbi/cluster/monitorComponent.jsf");
                    />
                </sun:dropDown>


                <sun:title id="jbiMonitorComponentPage"
                    title="#{sessionScope.sharedShowName} - $resource{i18n.jbi.monitor.component.page.title.suffix.text}"
                    helpText="$resource{i18n.jbi.statistics.helpInline}"
                    style="text-align:left;">
                 </sun:title>

#include "jbi/cluster/inc/startComponent.inc"
                 
                 "<br />
<!-- -->

                <sun:propertySheet 
                    id="monitorCompPS" 
                    jumpLinks="true" 
                    style="text-align:left;" 
                    binding="#{ShowBean.compStatsPropertySheet}"> 
                </sun:propertySheet>           	      		 

<!-- -->


            </sun:form>
        </sun:body>
     </sun:html>
</sun:page>
