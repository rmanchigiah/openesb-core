<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/pe/inc/componentOperate.inc -->
<!beforeCreate
  <!-- compute padding-left pixel size in order to line up the values in the property sheet -->
  if (#{ShowBean.showVersionInfo}) {
     setSessionAttribute(key="paddingPixel" value="padding-left: 30px")
     setSessionAttribute(key="blankSpace" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
  }
  
  if (!#{ShowBean.showVersionInfo}) {
     setSessionAttribute(key="paddingPixel" value="padding-left: 20px")
     setSessionAttribute(key="blankSpace" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
  }
    
/>

 <sun:propertySheet id="statePropertySheet">			 
    <!-- Text Field section -->               
    <sun:propertySheetSection id="statePropertySheetSection" label="$resource{i18n.jbi.component.operation.label}">
        <sun:property id="showState"  
            label="$resource{i18n.jbi.show.state.property.label}"
            labelAlign="left" 
            noWrap="#{true}" 
            overlapLabel="#{false}" 
            rendered="#{(ShowBean.type == 'binding-component') || (ShowBean.type == 'service-engine') || (ShowBean.type == 'service-assembly')}"            
            >  
            <sun:staticText id="showStateText"
                style="#{paddingPixel}"   
                text="#{ShowBean.summaryStatus}"
                />                                     
        </sun:property>
        
        <sun:property id="showActions"  
            label="$resource{i18n.jbi.show.actions.property.label}"
            labelAlign="left" 
            noWrap="#{true}" 
            overlapLabel="#{false}"
            helpText="#{blankSpace}#{ShowBean.type=='service-assembly' ? 
                        requestScope.i18n['jbi.developer.sa.general.actions.helpInline'] :
                        requestScope.i18n['jbi.developer.component.general.actions.helpInline']}"
            >
                                                        
            <!-- this tag is a place holder to line up the value -->
            <sun:staticText id="showBlankText"
                 style="#{paddingPixel}"
                 text=""
            />
                          
            <sun:panelGroup id="topButtons">
            <sun:button id="startButton"
                 disabled="#{ShowBean.state == ShowBean.STARTED_STATE}"
                 primary="$boolean{true}"
                 text="$resource{i18n.jbi.operations.start}"
            >
<!command    
    jbiOperateSelectedComponent(componentBean="#{ShowBean}", operation="#{OperationBean.valueStart}", instanceName="#{jbiSelectedInstanceValue}",isAlertNeeded=>$session{isJbiAlertNeeded}, alertSummary=>$session{jbiAlertSummary}, alertDetails=>$session{jbiAlertDetails});
    jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}')
    setPageSessionAttribute(key="anchor", value="#{sessionScope.isJbiAlertNeeded ? 'list'  : 'show' }")
    redirect(page="#{sessionScope.showNameLinkUrl}?type=#{ShowBean.type}&name=#{ShowBean.name}");    
/>            
            </sun:button>
            
            <sun:button id="stopButton"
                 disabled="#{(ShowBean.state == ShowBean.STOPPED_STATE) || (ShowBean.state == ShowBean.SHUTDOWN_STATE)}"
                 primary="$boolean{true}"
                 text="$resource{i18n.jbi.operations.stop}"
            >
<!command    
    jbiOperateSelectedComponent(componentBean="#{ShowBean}", operation="#{OperationBean.valueStop}", instanceName="#{jbiSelectedInstanceValue}", isAlertNeeded=>$session{isJbiAlertNeeded}, alertSummary=>$session{jbiAlertSummary}, alertDetails=>$session{jbiAlertDetails});
    jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}')
    setPageSessionAttribute(key="anchor", value="#{sessionScope.isJbiAlertNeeded ? 'list'  : 'show' }")
    redirect(page="#{sessionScope.showNameLinkUrl}?type=#{ShowBean.type}&name=#{ShowBean.name}");    
/>
            </sun:button>

            <sun:button id="shutdownButton"
                 disabled="#{ShowBean.state == ShowBean.SHUTDOWN_STATE}"
                 primary="$boolean{false}"
                 text="$resource{i18n.jbi.operations.shutdown}"
            >            
<!command    
    jbiOperateSelectedComponent(componentBean="#{ShowBean}", operation="#{OperationBean.valueShutDown}", instanceName="#{jbiSelectedInstanceValue}", isAlertNeeded=>$session{isJbiAlertNeeded}, alertSummary=>$session{jbiAlertSummary}, alertDetails=>$session{jbiAlertDetails});
    jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}')
    setPageSessionAttribute(key="anchor", value="#{sessionScope.isJbiAlertNeeded ? 'list'  : 'show' }")
    redirect(page="#{sessionScope.showNameLinkUrl}?type=#{ShowBean.type}&name=#{ShowBean.name}");    
/>
            </sun:button>
            </sun:panelGroup>
            
        </sun:property>
                
    </sun:propertySheetSection>
 </sun:propertySheet>
