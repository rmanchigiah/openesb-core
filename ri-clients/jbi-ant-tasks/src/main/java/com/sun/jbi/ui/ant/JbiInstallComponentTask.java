/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiInstallComponentTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for installing service engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiInstallComponentTask extends JbiTargetTask
{
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.install.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.install.failed";
    /** Holds value of property installFile. */
    private File mInstallFile;
    
    /** Holds Param Nested elements */
    private List mParamList;
    
    /** Holds Params File **/
    private File mParamsFile = null;
    
    /** Holds value of property componentName. */
    private String mComponentName = null;
    
    /** Getter for property componentName.
     * @return Value of property componentName.
     *
     */
    public String getName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param name component name.
     */
    public void setName(String name)
    {
        this.mComponentName = name;
    }
    
    /** Getter for property component installer zip file.
     * @return Value of property  installer zip file.
     *
     */
    public File getFile()
    {
        return this.mInstallFile;
    }
    
    /** Setter for property  installer zip file.
     * @param file New value of property  installer zip file.
     *
     */
    public void setFile(File file)
    {
        this.mInstallFile = file;
    }
    
    /** Getter for property componentJar.
     * @return Value of property componentJar.
     *
     */
    public File getParams()
    {
        return this.mParamsFile;
    }
    
    /**
     * Sets the params file location to the absolute filename of the
     * given file. If the value of this attribute is an absolute path, it
     * is left unchanged (with / and \ characters converted to the
     * current platforms conventions). Otherwise it is taken as a path
     * relative to the project's basedir and expanded.
     * @param paramsFile path to set
     */
    
    public void setParams(File paramsFile)
    {
        this.mParamsFile = paramsFile;
    }
    
    /**
     * validates the file
     */
    protected void validateInstallFile(File installFile) throws BuildException
    {
        if ( installFile == null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.comp.archive.file.path.null");
        }
        
        if ( installFile.getPath().trim().length() <= 0 )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.comp.archive.file.path.required");
        }
        
        if ( !installFile.exists() )
        {
            
            throwTaskBuildException(
                "jbi.ui.ant.install.error.comp.archive.file.not.exist",
                installFile.getName());
        }
        
        if ( installFile.isDirectory() )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.comp.archive.file.is.directory");
        }
        
    }
    /**
     * validate compName with valid target
     * @param compName name of the component in the repository
     * @param target value, can not be domain
     */
    protected void validateInstallFromDomainAttributes(String compName, String target)  throws BuildException
    {
        if ( compName.trim().length() == 0 )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.from.domain.error.comp.name.required");
        }
        if ( "domain".equals(target))
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.comp.invalid.target.with.name.attrib");
        }
    }
    
    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {
        String installFileAbsolutePath = null;
        boolean installFromDoamin = false;
        
        String compName = getName();
        
        File installFile = getFile();
        
        String target = getValidTarget();
        
        
        if ( compName == null && installFile == null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.comp.name.or.file.required");
        }
        
        if ( compName != null && installFile != null )
        {
            throwTaskBuildException(
                "jbi.ui.ant.install.error.comp.name.and.file.set");
        }
        
        if ( compName != null )
        {
            validateInstallFromDomainAttributes(compName, target);
            installFromDoamin = true;
        }
        else
        {
            validateInstallFile(installFile);
            installFileAbsolutePath = installFile.getAbsolutePath();
        }
        
        try
        {
            String result = null;
            Properties params = this.getParamsAsProperties();
            if ( installFromDoamin )
            {
                result = this.getJBIAdminCommands().installComponentFromDomain(compName, params, target);
            }
            else
            {
                result = this.getJBIAdminCommands().installComponent(installFileAbsolutePath, params, target);
            }
            
            printTaskSuccess(result);
            
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
        
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
    
    /**
     * returns param element list
     * @return Paramter List
     */
    protected List getParamList()
    {
        if ( this.mParamList == null )
        {
            this.mParamList = new ArrayList();
        }
        return this.mParamList;
    }
    /**
     * load properties from a file
     * @return the Loaded properties
     * @param file file to load
     * @throws BuildException on error
     */
    protected Properties loadParamsFromFile(File file) throws BuildException
    {
        String absFilePath = null;
        String fileName = null;
        if ( file != null )
        {
            absFilePath = file.getAbsolutePath();
            fileName = file.getName();
        }
        if ( file == null || !file.exists() )
        {
            String msg =
                createFailedFormattedJbiAdminResult("jbi.ui.ant.task.error.params.file.not.exist",
                new Object[] {fileName});
            throw new BuildException(msg,getLocation());
        }
        
        if ( file.isDirectory() )
        {
            String msg =
                createFailedFormattedJbiAdminResult("jbi.ui.ant.task.error.params.file.is.directory", null);
            throw new BuildException(msg,getLocation());
        }
        
        Properties props = new Properties();
        this.logDebug("Loading " + file.getAbsolutePath());
        try
        {
            FileInputStream fis = new FileInputStream(file);
            try
            {
                props.load(fis);
            }
            finally
            {
                if (fis != null)
                {
                    fis.close();
                }
            }
            return props;
        }
        catch (IOException ex)
        {
            throw new BuildException(ex, getLocation());
        }
    }
    /**
     * converts the Params list to the properties object with &lt;name>&lt;value> pair
     * @return Properties object that will be passed to the install method of UIMBean
     */
    protected Properties getParamsAsProperties() throws BuildException
    {
        Properties props = new Properties();
        // add params from the nested param elements
        for ( Iterator itr = getParamList().iterator(); itr.hasNext() ; )
        {
            Param param = (Param) itr.next();
            // non null name is gaurenteed.
            String name = param.getName().trim();
            // non null value is gaurenteed.
            String value = param.getValue().trim();
            if ( name.length() <= 0 || value.length() <= 0 )
            {
                String msg =
                    createFailedFormattedJbiAdminResult("jbi.ui.ant.task.error.param.invalid",
                    new Object[] {name,value});
                throw new BuildException(msg,getLocation());
            }
            if ( name.length() > 0 )
            {
                props.setProperty(name, value);
            }
            else
            {
                // log warning
                this.logDebug(
                    "Blank parameter name passed to the install task using <param> nested element.");
            }
        }
        
        Properties paramsProps = null;
        
        paramsProps = null;
        File paramsFile = this.getParams();
        if ( paramsFile != null )
        {
            paramsProps = this.loadParamsFromFile(paramsFile);
            props.putAll(paramsProps);
        }
        else
        {
            this.logDebug("No File based Parameters passed to installer Task via nested params element");
        }
        
        return props;
    }
    /**
     * factory method for creating the nested element &lt;param>
     * @return Param Object
     */
    public Param createParam()
    {
        Param param = new Param();
        this.getParamList().add(param);
        return param;
    }
    
    /**
     * configuration parameters nested element
     */
    public class Param
    {
        
        /**
         * Holds value of property name.
         */
        private String mName;
        
        /**
         * Holds value of property value.
         */
        private String mValue;
        
        /**
         * default public constructor
         */
        public Param()
        {
            this.mName = "";
            this.mValue = "";
        }
        
        /**
         * Getter for property name.
         * @return Value of property name.
         */
        public String getName()
        {
            return this.mName;
        }
        
        /**
         * Setter for property name.
         * @param name New value of property name.
         */
        public void setName(String name)
        {
            this.mName = name;
        }
        
        /**
         * Getter for property value.
         * @return Value of property value.
         */
        public String getValue()
        {
            return this.mValue;
        }
        
        /**
         * Setter for property value.
         * @param value New value of property value.
         */
        public void setValue(String value)
        {
            this.mValue = value;
        }
        
    }
    
}
