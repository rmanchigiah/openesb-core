/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Param.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

/** This class is nested element &lt;param>
 *
 * @author Sun Microsystems, Inc.
 */

public class Param
{
    /**
     * Holds value of property name.
     */
    private String mName;

    /**
     * Holds value of property value.
     */
    private String mValue;

    /**
     * Holds reference of an application variable.
     * This is only for application configuration.
     */
    private String mAppVariableRef;

    /**
     * default public constructor
     */
    public Param()
    {
        this.mName = "";
        this.mValue = "";
        this.mAppVariableRef = "";
    }

    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public String getName()
    {
        return this.mName;
    }

    /**
     * Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name)
    {
        this.mName = name;
    }

    /**
     * Getter for property value.
     * @return Value of property value.
     */
    public String getValue()
    {
        return this.mValue;
    }

    /**
     * Setter for property value.
     * @param value New value of property value.
     */
    public void setValue(String value)
    {
        this.mValue = value;
    }

    /**
     * Getter for application variable reference.
     * @return reference of application variable.
     */
    public String getAppVariable()
    {
        return this.mAppVariableRef;
    }

    /**
     * Setter for application variable reference.
     * @param appVariableRef New reference of an application variable.
     */
    public void setAppVariable(String appVariableRef)
    {
        this.mAppVariableRef = appVariableRef;
    }
}
