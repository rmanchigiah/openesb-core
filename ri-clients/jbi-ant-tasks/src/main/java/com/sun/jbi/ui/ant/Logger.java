/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Param.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

/**
 * configuration parameters nested element
 */
public class Logger
{
    /**
     * Holds value of property name.
     */
    private String mName;

    /**
     * Holds value of property value.
     */
    private String mLevel;

    /**
     * default public constructor
     */
    public Logger()
    {
	this.mName = "";
	this.mLevel = "";
    }

    /**
     * Getter for property name.
     * @return Value of property name.
     */
    public String getName()
    {
	return this.mName;
    }

    /**
     * Setter for property name.
     * @param name New value of property name.
     */
    public void setName(String name)
    {
	this.mName = name;
    }

    /**
     * Getter for property value.
     * @return Value of property value.
     */
    public String getLevel()
    {
	return this.mLevel;
    }

    /**
     * Setter for property value.
     * @param level New value of property level.
     */
    public void setLevel(String level)
    {
	this.mLevel = level;
    }
}
