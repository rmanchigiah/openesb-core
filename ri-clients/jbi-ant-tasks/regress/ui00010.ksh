#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)ui00012.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

#
# test upgrade component
#

test_upgrade_component()
{
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-engine4.jar install-component
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.component.name=ant_test_engine4 start-component
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=ant_test_engine4 shut-down-component
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.install.file=$UI_REGRESS_DIST_DIR/ant-test-engine4-1.jar -Djbi.component.name=ant_test_engine4 upgrade-component
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=ant_test_engine4 start-component
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=ant_test_engine4 stop-component
$JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=ant_test_engine4 shut-down-component
# $JBI_ANT -Djbi.task.fail.on.error=true -Djbi.component.name=ant_test_engine4 uninstall-component
}


run_test()
{
build_test_artifacts
test_upgrade_component
}

################## MAIN ##################
####
# Execute the test
####

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test | tr -d '\r' | sed -e '/^$/d'

exit 0
