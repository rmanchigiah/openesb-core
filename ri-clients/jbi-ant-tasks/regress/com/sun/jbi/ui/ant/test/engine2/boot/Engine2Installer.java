/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Engine2Installer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant.test.engine2.boot;

import javax.jbi.component.Bootstrap;
import javax.jbi.component.InstallationContext;
import java.util.logging.Logger;
import javax.jbi.JBIException;
import javax.jbi.management.MBeanNames;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.StandardMBean;


/**
 * Class to install the  engine.
 *
 * @author Sun Microsystems, Inc.
 */
public class Engine2Installer implements Bootstrap
{
    
    /**
     * Internal handle to the logger instance
     */
    private Logger mLogger ;
    /** fix it */
    private ObjectName mConfigMBeanName;
    /** fix it */
    private ConfigMBean mConfigMBean;
    /**
     * fix it
     */
    private InstallationContext mContext;
    
    /**
     * Creates a new instance of StockQuoteEngineBootstrap
     */
    public Engine2Installer()
    {
        mLogger = Logger.getLogger("com.sun.jbi.ui.ant.test.engine2.boot");
        mLogger.info("Engine2Installer Constructor called");
    }
    
    /**
     * Cleans up any resources allocated by the bootstrap implementation,
     * including deregistration of the extension MBean, if applicable.
     * This method will be called after the onInstall() or onUninstall() method
     * is called, whether it succeeds or fails.
     * @throws javax.jbi.JBIException when cleanup processing fails to complete
     * successfully.
     */
    public void cleanUp()
    throws javax.jbi.JBIException
    {
        deregisterConfigMBean();
    }
    
    /**
     * Called to initialize the BC bootstrap.
     * @param installContext is the context containing information
     * from the install command and from the BC jar file.
     * @throws javax.jbi.JBIException when there is an error requiring that
     * the installation be terminated.
     */
    public void init(InstallationContext installContext)
    throws javax.jbi.JBIException
    {
        mLogger.info("Engine2Installer INIT Called");
        this.mContext = installContext;
        // throw JBIException("Test Error on Engine2Installer init");
        registerConfigMBean();
        return;
    }
    
    /**
     * Get the JMX ObjectName for the optional installation configuration MBean
     * for this BC. If there is none, the value is null.
     * @return ObjectName the JMX object name of the installation configuration
     * MBean or null if there is no MBean.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        mLogger.info("Engine2Installer getExtensionMBeanName Called");
        return this.mConfigMBeanName;
    }
    
    /**
     * Called at the beginning of installation of BPEL Engine. For this
     * Engine, all the required installation tasks have been taken care
     * by the InstallationService.
     * @throws javax.jbi.JBIException when there is an error requiring that
     * the installation be terminated.
     */
    public void onInstall()
    throws javax.jbi.JBIException
    {
        mLogger.info("Engine2Installer onInstall Called.");
        //  throw new JBIException("*** Test Error Engine2Installer on Install");
        
        if ( this.mConfigMBean != null )
        {
            if ( !("chikkala".equalsIgnoreCase(this.mConfigMBean.getUser())) )
            {
                throw new JBIException("user is not chikkala. can not install");
            }
            
            if ( !("7777".equalsIgnoreCase(this.mConfigMBean.getPort())) )
            {
                throw new JBIException("Port is not 7777. can not install");
            }
            
            if ( this.mConfigMBean.getIntValue() != 1234 ) {
                throw new JBIException("IntValue is not 1234. can not install");
            }
            
            if ( this.mConfigMBean.getIntegerValue().intValue() != 9876 ) {
                throw new JBIException("IntegerValue is not 9876. can not install");
            }
            
        }
        else
        {
            throw new JBIException("Configuration MBean not initialized");
        }
    }
    
    /**
     * Called at the beginning of uninstallation of FileEngine . For this
     * file engine, all the required uninstallation tasks have been taken care
     * of by the InstallationService
     * @throws javax.jbi.JBIException when there is an error requiring that
     * the uninstallation be terminated.
     */
    public void onUninstall()
    throws javax.jbi.JBIException
    {
        mLogger.info("*** Engine2Installer onUninstall Called");
        // throw new JBIException("Test Error Engine2Installer on UnInstall");
    }
    
    /**
     * fix it
     * @throws JBIException fix it
     */
    public void createConfigMBean() throws JBIException
    {
        try
        {
            if ( this.mConfigMBean == null )
            {
                this.mConfigMBean = new ConfigMBeanImpl();
            }
            
            if ( this.mConfigMBeanName == null )
            {
                MBeanNames mbnHndl =
                    this.mContext.getContext().getMBeanNames();
                
                this.mConfigMBeanName =
                    mbnHndl.createCustomComponentMBeanName("InstallerConfig");
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new JBIException(
                "Ant Test Engine2 Installer ConfigurationMBean Creation Failed", e);
        }
    }
    
    /**
     * fix it
     * @throws JBIException fix it
     */
    public void registerConfigMBean() throws JBIException
    {
        createConfigMBean(); // make sure it is created.
        
        try
        {
            MBeanServer mbServer = this.mContext.getContext().getMBeanServer();
            StandardMBean configBean = new StandardMBean(this.mConfigMBean,
                ConfigMBean.class);
            mbServer.registerMBean(configBean, this.mConfigMBeanName);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new JBIException(
                "Ant Test Engine2 Installer Config MBean Registration Failed", e);
        }
        
    }
    
    /**
     * fix it
     * @throws JBIException fix it
     */
    public void deregisterConfigMBean() throws JBIException
    {
        if (this.mConfigMBeanName == null )
        {
            this.mLogger.warning(
                "trying to unregister null Installer Config MBean in Ant Test Engine 2 inst");
        }
        
        try
        {
            MBeanServer mbServer = this.mContext.getContext().getMBeanServer();
            mbServer.unregisterMBean(this.mConfigMBeanName);
            this.mConfigMBeanName = null;
            this.mConfigMBean = null;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            throw new JBIException("Ant Test Engine2" +
                "Installer Config MBean Deregistration Failed", e);
        }
        
    }
    
    public interface ConfigMBean
    {
        public void setUser(String user);
        public String getUser();
        public void setPort(String port);
        public String getPort();
        public void setIntValue(int intValue);
        public int getIntValue();
        public void setIntegerValue(Integer integerValue);
        public Integer getIntegerValue();
    }
    
    public class ConfigMBeanImpl implements ConfigMBean
    {
        private String mUser;
        private String mPort;
        private int mIntValue;
        private Integer mIntegerValue;
        
        public String getUser()
        {
            return mUser;
        }
        
        public void setUser(String user)
        {
            System.out.println("Setting the ConfigMBean attribute \"User\" : " + user);
            mUser = user;
        }
        public String getPort()
        {
            return mPort;
        }
        
        public void setPort(String port)
        {
            mLogger.info("Setting the ConfigMBean attribute \"Port\" : " + port);
            mPort = port;
        }
        
        public int getIntValue()
        {
            return this.mIntValue;
        }
        
        public void setIntValue(int intValue)
        {
            mLogger.info("Setting the ConfigMBean attribute \"IntValue\" : " + intValue);
            this.mIntValue = intValue;
        }
        
        public Integer getIntegerValue()
        {
            return this.mIntegerValue;
        }
        
        public void setIntegerValue(Integer integerValue)
        {
            mLogger.info("Setting the ConfigMBean attribute \"IntegerValue\" : " + integerValue);
            this.mIntegerValue = integerValue;
        }
        
        
    }
}
