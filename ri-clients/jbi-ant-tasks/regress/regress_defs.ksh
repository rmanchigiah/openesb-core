#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)regress_defs.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#common definitions for regression tests.

#set this prop to the test domain. default is JBITest
#my_test_domain=domain1

#use global regress setup:
. $SRCROOT/antbld/regress/common_defs.ksh

export UI_REGRESS_DIST_DIR
if [ $OPENESB_BUILD -eq 1 ]; then
    UI_REGRESS_DIST_DIR=$JV_SVC_TEST_CLASSES/testdata
else
    UI_REGRESS_DIST_DIR=$JV_SVC_TEST_CLASSES/dist
fi

# base definition for JBI_ANT command was moved to antbld/regress/common_defs.ksh - override it here as required.
#JBI_ANT="asant -emacs -q -lib $JV_JBI_HOME/lib/jbi-ant-tasks.jar -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$ASADMIN_PORT -Djbi.secure=false "

#
# test cases
#

build_test_artifacts()
{
# package components
ant -emacs -q -f $JV_SVC_REGRESS/scripts/build-test-components.ant
return 0
}
