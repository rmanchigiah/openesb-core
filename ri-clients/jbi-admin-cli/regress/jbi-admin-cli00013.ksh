#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test
  build_test_engine_with_endpoints_artifacts
  test_cleanup
          
  #--------------------------------------------------------------------------------
  # Make sure http binding component is started. (output redirected to TEMP file)
  #--------------------------------------------------------------------------------
  start_component server sun-http-binding
    
  #--------------------------------------------------------------------------------
  # Make sure PingApp service assembly has been deployed and started
  #--------------------------------------------------------------------------------
  deploy_assembly server ${JV_SVC_BLD}/regress/dist/ping-sa.jar
  start_assembly server PingApp
  
  #--------------------------------------------------------------------------------
  # Install and stop the simple test engine component
  #--------------------------------------------------------------------------------
  install_component server ${JV_SVC_BLD}/regress/dist/simpletestengine.jar
  start_component server SimpleTestEngine
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the service assembly with the descriptor for the PingApp"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-service-assembly --descriptormsg PingApp"  
  $AS8BASE/bin/asadmin show-jbi-service-assembly --port $ADMIN_PORT --descriptor  --user $ADMIN_USER $ASADMIN_PW_OPTS PingApp
   
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set a Runtime Configuration value msgSvcTimingStatisticsEnabled=false"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration msgSvcTimingStatisticsEnabled=false"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS msgSvcTimingStatisticsEnabled="false"
          
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Framework"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --framework"
  $AS8BASE/bin/asadmin show-jbi-statistics --framework --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --component=SimpleTestEngine"
  $AS8BASE/bin/asadmin show-jbi-statistics --component=SimpleTestEngine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Service Assembly"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --serviceassembly=PingApp"
  $AS8BASE/bin/asadmin show-jbi-statistics --serviceassembly=PingApp --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Endpoint (Provider)"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --endpoint=http://ping,PingService,PingPort"
  $AS8BASE/bin/asadmin show-jbi-statistics --endpoint=http://ping,PingService,PingPort --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Endpoint (Consumer)"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --endpoint=http://ping,PingService,ConsumingPingPort"
  $AS8BASE/bin/asadmin show-jbi-statistics --endpoint=http://ping,PingService,ConsumingPingPort --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set a Runtime Configuration value msgSvcTimingStatisticsEnabled=true"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration msgSvcTimingStatisticsEnabled=true"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS msgSvcTimingStatisticsEnabled="true"
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "stop-jbi-component SimpleTestEngine"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SimpleTestEngine
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut down the component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component SimpleTestEngine"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SimpleTestEngine
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component SimpleTestEngine"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SimpleTestEngine
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --component=SimpleTestEngine"
  $AS8BASE/bin/asadmin show-jbi-statistics --component=SimpleTestEngine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Endpoint (Provider)"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --endpoint=http://ping,PingService,PingPort"
  $AS8BASE/bin/asadmin show-jbi-statistics --endpoint=http://ping,PingService,PingPort --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Endpoint (Consumer)"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --endpoint=http://ping,PingService,ConsumingPingPort"
  $AS8BASE/bin/asadmin show-jbi-statistics --endpoint=http://ping,PingService,ConsumingPingPort --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set a Runtime Configuration value msgSvcTimingStatisticsEnabled=false"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration msgSvcTimingStatisticsEnabled=false"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS msgSvcTimingStatisticsEnabled="false"
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --component=SimpleTestEngine"
  $AS8BASE/bin/asadmin show-jbi-statistics --component=SimpleTestEngine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Endpoint (Provider)"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --endpoint=http://ping,PingService,PingPort"
  $AS8BASE/bin/asadmin show-jbi-statistics --endpoint=http://ping,PingService,PingPort --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Endpoint (Consumer)"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --endpoint=http://ping,PingService,ConsumingPingPort"
  $AS8BASE/bin/asadmin show-jbi-statistics --endpoint=http://ping,PingService,ConsumingPingPort --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "stop-jbi-component SimpleTestEngine"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SimpleTestEngine
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut down the component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component SimpleTestEngine"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SimpleTestEngine
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component SimpleTestEngine"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SimpleTestEngine
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Component SimpleTestEngine"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --component=SimpleTestEngine"
  $AS8BASE/bin/asadmin show-jbi-statistics --component=SimpleTestEngine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Endpoint (Provider)"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --endpoint=http://ping,PingService,PingPort"
  $AS8BASE/bin/asadmin show-jbi-statistics --endpoint=http://ping,PingService,PingPort --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Endpoint (Consumer)"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --endpoint=http://ping,PingService,ConsumingPingPort"
  $AS8BASE/bin/asadmin show-jbi-statistics --endpoint=http://ping,PingService,ConsumingPingPort --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the stats for the Component sun-http-binding"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-statistics --component=sun-http-binding"
  $AS8BASE/bin/asadmin show-jbi-statistics --component=sun-http-binding --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
     
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00013"
TEST_DESCRIPTION="Test Show Statistics"
. ./regress_defs.ksh
run_test

exit 0


