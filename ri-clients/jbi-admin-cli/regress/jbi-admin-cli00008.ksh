#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
      
  #--------------------------------------------------------------------------------
  # Test setting and showing the runtime configuration values
  #--------------------------------------------------------------------------------
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set a Runtime Configuration value using command line arguments"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration heartBeatInterval=5000"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS heartBeatInterval=5000
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Runtime Configuration Values."
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-configuration"
  $AS8BASE/bin/asadmin show-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set a Runtime Configuration value using command line arguments"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration heartBeatInterval=6000"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS heartBeatInterval=6000
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show Runtime Configuration Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-configuration"
  $AS8BASE/bin/asadmin show-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set the two Runtime Configuration values using command line arguments"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration heartBeatInterval=7000,startOnDeploy=true"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS heartBeatInterval=7000,startOnDeploy=true
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Runtime Configuration Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-configuration"
  $AS8BASE/bin/asadmin show-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Using a property file, set the heartbeat value"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-configuration runtimeConfig.property"
  $AS8BASE/bin/asadmin set-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/runtimeConfig.property
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Runtime Configuration Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-configuration"
  $AS8BASE/bin/asadmin show-jbi-runtime-configuration --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS 
  
  #--------------------------------------------------------------------------------
  # Test setting and showing the runtime logger levels
  #--------------------------------------------------------------------------------
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Using the property file, set all the logger levels to INFO"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger runtimeLogger.property"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/runtimeLogger.property
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Runtime Logger Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-loggers"
  $AS8BASE/bin/asadmin show-jbi-runtime-loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS 
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show only the com.sun.jbi.management logger level"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-loggers | grep com.sun.jbi.management"
  $AS8BASE/bin/asadmin show-jbi-runtime-loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS | grep com.sun.jbi.management
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set the Runtime Logger Level for the com.sun.jbi.management Logger from the command line"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger com.sun.jbi.management=WARNING"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS com.sun.jbi.management=WARNING

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Runtime Logger Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-loggers"
  $AS8BASE/bin/asadmin show-jbi-runtime-loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS 

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Set two Runtime Logger Level values from the command line"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger com.sun.jbi.management=WARNING"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS com.sun.jbi.management=FINE,com.sun.jbi.messaging=FINE

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Runtime Logger Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-loggers"
  $AS8BASE/bin/asadmin show-jbi-runtime-loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS 

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Using the property file, set all the logger levels to INFO"
  echo "-------------------------------------------------------------------"
  echo "set-jbi-runtime-logger runtimeLogger.property"
  $AS8BASE/bin/asadmin set-jbi-runtime-logger --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $JV_SVC_BLD/regress/runtimeLogger.property

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Runtime Logger Values"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-runtime-loggers"
  $AS8BASE/bin/asadmin show-jbi-runtime-loggers --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS 
 
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00008"
TEST_DESCRIPTION="Test the set and show commands for Runtime Configuration and Runtime Loggers"
. ./regress_defs.ksh
run_test

exit 0


