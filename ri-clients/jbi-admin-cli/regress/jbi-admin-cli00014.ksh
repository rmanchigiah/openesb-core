#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
  test_cleanup
                
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install test components test-component and test-component1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component test-component.jar"
  echo "install-jbi-component test-component1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/test-component.jar 
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/test-component1.jar 
      
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Verify the jbi application sa1.jar"
  echo "-------------------------------------------------------------------"
  echo "verify-jbi-application-environment sa1.jar"
  $AS8BASE/bin/asadmin verify-jbi-application-environment --templatedir=${JV_SVC_BLD}/regress/testdata/tmp/verify --includedeploy --target server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/sa1.jar
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS test-component
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS test-component1
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Create the Application Variables for the cli-config-binding component using the command line"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-variable --component=cli-test-component1 test-component1-app-var.properties"
  $AS8BASE/bin/asadmin create-jbi-application-variable --component=test-component1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/test-component1-app-var.properties
        	
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Create another Application Configuration for the test-component1-app-config2.properties"
  echo "-------------------------------------------------------------------"
  echo "create-jbi-application-configuration --component=test-component1 --configname=MyAppConfig3 test-component1-app-config2.properties"
  $AS8BASE/bin/asadmin create-jbi-application-configuration --component=test-component1 --configname=MyAppConfig3 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/test-component1-app-config2.properties
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Verify the jbi application sa2.jar"
  echo "-------------------------------------------------------------------"
  echo "verify-jbi-application-environment sa2.jar"
  $AS8BASE/bin/asadmin verify-jbi-application-environment --templatedir=${JV_SVC_BLD}/regress/testdata/tmp/verify --includedeploy --target server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/sa2.jar
    
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Deploy the service assembly sa2.jar"
  echo "-------------------------------------------------------------------"
  echo "deploy-jbi-service-assembly sa2.jar"
  $AS8BASE/bin/asadmin deploy-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS ${JV_SVC_BLD}/regress/sa2.jar 
     
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Export the jbi application SA2"
  echo "-------------------------------------------------------------------"
  echo "export-jbi-application-environment SA2"
  $AS8BASE/bin/asadmin export-jbi-application-environment --configdir=${JV_SVC_BLD}/regress/testdata/tmp/export --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS SA2
   
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00014"
TEST_DESCRIPTION="Test Export Command"
. ./regress_defs.ksh
run_test

exit 0


