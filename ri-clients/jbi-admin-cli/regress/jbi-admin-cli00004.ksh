#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
  cleanup domain
}


#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
  initilize_test 
 
  # Clean up the (Remove uninstall everything so this regression test will work
  test_cleanup
 
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install shared library cli_test_sns1 to the domain"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library cli-test-sns1.jar"
  $AS8BASE/bin/asadmin install-jbi-shared-library --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-sns1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install binding component cli_test_binding1 to the domain"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-binding1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-binding1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install service engine cli_test_engine1 to the domain"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-engine1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-engine1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Deploy Service Assembly cli_test_assembly_unit_1 to the domain target"
  echo "-------------------------------------------------------------------"
  echo "deploy-jbi-service-assembly --target=domain cli-test-au1.zip"
  $AS8BASE/bin/asadmin deploy-jbi-service-assembly --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-au1.zip 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the service engines from the domain"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-engines --target=domain"
  $AS8BASE/bin/asadmin list-jbi-service-engines --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the binding components from the domain"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-binding-components --target=domain"
  $AS8BASE/bin/asadmin list-jbi-binding-components --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the shared libraries from the domain"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-shared-libraries --target=domain"
  $AS8BASE/bin/asadmin list-jbi-shared-libraries --target=domain --componentname=cli_test_binding1 --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the service assemblies from the domain"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-assemblies --target=domain"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install the Shared Library from the domain to the server"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library --target=server cli_test_sns1"
  $AS8BASE/bin/asadmin install-jbi-shared-library --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install the components from the domain to the server"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component --target=server cli_test_binding1"
  $AS8BASE/bin/asadmin install-jbi-component  --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1 
  echo "install-jbi-component --target=server cli_test_engine1"
  $AS8BASE/bin/asadmin install-jbi-component  --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the server, should be in the Shut Down state"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component  cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine  cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the Components on the server"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "start-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the server"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --target=server cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine --target=server cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the domaine"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --target=domain cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine --target=domain cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Assemblies from the server and from the domain "
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-assemblies --target=server cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  echo "list-jbi-service-assemblies --target=domain cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Deploy Service Assembly cli_test_assembly_unit_1 from the domain to the server"
  echo "-------------------------------------------------------------------"
  echo "deploy-jbi-service-assembly --target=server cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin deploy-jbi-service-assembly --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Assemblies from the server and from the domain"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-assemblies --target=server cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  echo "list-jbi-service-assemblies --target=domain cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS

  echo ""
  echo "-------------------------------------------------------------------"
  echo " Undeploy Service Assembly from the server, but --keeparchive cli_test_assembly_unit_1"
  echo "-------------------------------------------------------------------"
  echo "undeploy-jbi-service-assembly --keeparchive cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly --keeparchive --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Assemblies from the server and the domain"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-assemblies --target=server cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  echo "list-jbi-service-assemblies --target=domain cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Undeploy Service Assembly cli_test_assembly_unit_1 from the domain"
  echo "-------------------------------------------------------------------"
  echo "undeploy-jbi-service-assembly --target=domain cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List the Service Assemblies from the server and domain"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-service-assemblies --target=server cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  echo "list-jbi-service-assemblies --target=domain cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin list-jbi-service-assemblies --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the Components on the server"
  echo "-------------------------------------------------------------------"
  echo "stop-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "stop-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the server"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --target=server cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine --target=server cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the Components on the server"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "shut-down--jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the server"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --target=server cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine --target=server cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the Components from the server"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component --keeparchive cli_test_binding1"
  $AS8BASE/bin/asadmin uninstall-jbi-component --keeparchive --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "uninstall-jbi-component --keeparchive cli_test_engine1"
  $AS8BASE/bin/asadmin uninstall-jbi-component --keeparchive --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the server"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --target=server cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine --target=server cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install the components from the domain to the server"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component --target=server cli_test_binding1"
  $AS8BASE/bin/asadmin install-jbi-component  --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1 
  echo "install-jbi-component --target=server cli_test_engine1"
  $AS8BASE/bin/asadmin install-jbi-component  --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the server"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --target=server cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine --target=server cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the domain"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --target=domain cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine --target=domain cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the Components from the server and domain"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin uninstall-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "uninstall-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin uninstall-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the components on the domain"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component --target=domain cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo "show-jbi-service-engine --target=domain cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the Shared Libraries from the server"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-shared-library --keeparchive cli_test_sns1"
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library --keeparchive --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List Shared Libraries"
  echo "-------------------------------------------------------------------"
  echo "list-jbi-shared-libraries --target=server"
  $AS8BASE/bin/asadmin list-jbi-shared-libraries --target=server --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS | grep cli_test_sns1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List Shared Libraries"
  echo "-------------------------------------------------------------------"
   echo "list-jbi-shared-libraries --target=domain"
  $AS8BASE/bin/asadmin list-jbi-shared-libraries --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS | grep cli_test_sns1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the Shared Libraries from the domain"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-shared-library --target=domain cli_test_sns1"
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " List Shared Libraries"
  echo "-------------------------------------------------------------------"
   echo "list-jbi-shared-libraries --target=domain"
  $AS8BASE/bin/asadmin list-jbi-shared-libraries --target=domain --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS | grep cli_test_sns1
  
}


#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00004"
TEST_DESCRIPTION="Test Install, Deploy, Lifecycle and Show commands on server and domain targets."
. ./regress_defs.ksh
run_test

exit 0


