/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyStatisticsDataXMLConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.Serializable;

/**
 * @author graj
 * 
 */
public interface ServiceAssemblyStatisticsDataXMLConstants extends Serializable {
    public static final String VERSION_KEY                                = "version";
    
    public static final String ASSEMBLY_VERSION_VALUE                     = "1.0";
    
    public static final String UNIT_VERSION_VALUE                         = "1.0";
    
    public static final String NAMESPACE_KEY                              = "xmlns";
    
    public static final String ASSEMBLY_NAMESPACE_VALUE                   = "http://java.sun.com/xml/ns/esb/management/ServiceAssemblyStatisticsDataList";
    
    public static final String UNIT_NAMESPACE_VALUE                       = "http://java.sun.com/xml/ns/esb/management/ServiceUnitStatisticsDataList";
    
    public static final String ASSEMBLY_STATISTICS_DATA_LIST_KEY          = "ServiceAssemblyStatisticsDataList";
    
    public static final String UNIT_STATISTICS_DATA_LIST_KEY              = "ServiceUnitStatisticsDataList";
    
    public static final String ASSEMBLY_STATISTICS_DATA_KEY               = "ServiceAssemblyStatisticsData";
    
    public static final String UNIT_STATISTICS_DATA_KEY                   = "ServiceUnitStatisticsData";
    
    public static final String INSTANCE_NAME_KEY                          = "InstanceName";
    
    public static final String SERVICE_ASSEMBLY_NAME_KEY                  = "ServiceAssemblyName";
    
    public static final String SERVICE_ASSEMBLY_LAST_STARTUP_TIME_KEY     = "ServiceAssemblyLastStartupTime";
    
    public static final String SERVICE_ASSEMBLY_STARTUP_TIME_AVERAGE_KEY  = "ServiceAssemblyStartupTimeAverage";
    
    public static final String SERVICE_ASSEMBLY_SHUTDOWN_TIME_AVERAGE_KEY = "ServiceAssemblyShutdownTimeAverage";
    
    public static final String SERVICE_ASSEMBLY_STOP_TIME_AVERAGE_KEY     = "ServiceAssemblyStopTimeAverage";
    
    public static final String SERVICE_ASSEMBLY_UP_TIME_KEY               = "ServiceAssemblyUpTime";

    public static final String SERVICE_UNIT_NAME_KEY                      = "ServiceUnitName";
    
    public static final String SERVICE_UNIT_STARTUP_TIME_AVERAGE_KEY      = "ServiceUnitStartupTimeAverage";
    
    public static final String SERVICE_UNIT_SHUTDOWN_TIME_AVERAGE_KEY     = "ServiceUnitShutdownTimeAverage";
    
    public static final String SERVICE_UNIT_STOP_TIME_AVERAGE_KEY         = "ServiceUnitStopTimeAverage";
    
    public static final String SERVICE_UNIT_ENDPOINT_NAME_LIST_KEY        = "EndpointNameList";
    
    public static final String SERVICE_UNIT_ENDPOINT_NAME_KEY             = "EndpointName";
}
