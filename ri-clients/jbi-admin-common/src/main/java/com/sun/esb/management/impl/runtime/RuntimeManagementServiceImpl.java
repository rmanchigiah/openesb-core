/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RuntimeManagementServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.runtime;

import java.io.Serializable;
import java.util.Map;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.sun.esb.management.api.runtime.RuntimeManagementService;
import com.sun.esb.management.base.services.AbstractServiceImpl;
import com.sun.esb.management.common.ManagementRemoteException;

/**
 * Defines client operations for common runtime management services for the
 * clients. Common runtime management operations include listing component
 * containers available in the runtime, composite applications deployed,
 * controlling lifecycle across the runtime and composite applications, getting
 * state of each container and composite application, etc.
 * 
 * @author graj
 */
public class RuntimeManagementServiceImpl extends AbstractServiceImpl
        implements Serializable, RuntimeManagementService {
    
    static final long serialVersionUID = -1L;
    
    /**
     * Constructor - Constructs a new instance of
     * RuntimeManagementServiceImpl
     */
    public RuntimeManagementServiceImpl() {
        super(null, false);
    }
    
    /**
     * Constructor - Constructs a new instance of
     * RuntimeManagementServiceImpl
     * 
     * @param serverConnection
     */
    public RuntimeManagementServiceImpl(
            MBeanServerConnection serverConnection) {
        super(serverConnection, false);
    }
    
    /**
     * Constructor - Constructs a new instance of
     * RuntimeManagementServiceImpl
     * 
     * @param serverConnection
     * @param isRemoteConnection
     */
    public RuntimeManagementServiceImpl(
            MBeanServerConnection serverConnection, boolean isRemoteConnection) {
        super(serverConnection, isRemoteConnection);
    }
    
    /**
     * return component info xml text that has only binding component infos.
     * 
     * @return the component info xml text.
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listBindingComponents(java.lang.String)
     */
    public String listBindingComponents(String targetName)
            throws ManagementRemoteException {
        return listBindingComponents(null, null, null, targetName);
    }
    
    /**
     * return component info xml text that has only binding component infos
     * which satisfies the options passed to the method.
     * 
     * @param state
     *            return all the binding components that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the binding components that have a dependency on
     *            the specified shared library. null value to ignore this
     *            option.
     * @param serviceAssemblyName
     *            return all the binding components that have the specified
     *            service assembly deployed on them. null value to ignore this
     *            option.
     * @param targetName
     * @return xml text contain the list of binding component infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listBindingComponents(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String listBindingComponents(String state, String sharedLibraryName,
            String serviceAssemblyName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = state;
        params[1] = sharedLibraryName;
        params[2] = serviceAssemblyName;
        params[3] = targetName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "listBindingComponents", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * return component info xml text that has only binding component infos.
     * 
     * @return the component info xml text.
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listBindingComponents(java.lang.String[])
     */
    public Map<String, String> listBindingComponents(String[] targetNames)
            throws ManagementRemoteException {
        return this.listBindingComponents(null, null, null, targetNames);
    }
    
    /**
     * return component info xml text that has only binding component infos
     * which satisfies the options passed to the method.
     * 
     * @param state
     *            return all the binding components that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the binding components that have a dependency on
     *            the specified shared library. null value to ignore this
     *            option.
     * @param serviceAssemblyName
     *            return all the binding components that have the specified
     *            service assembly deployed on them. null value to ignore this
     *            option.
     * @param targetNames
     * @return xml text contain the list of binding component infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listBindingComponents(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> listBindingComponents(String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = state;
        params[1] = sharedLibraryName;
        params[2] = serviceAssemblyName;
        params[3] = targetNames;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "listBindingComponents", params, signature);
        
        return resultObject;
    }
    
    /**
     * returns a list of Service Assembly Infos in a xml format.
     * 
     * @param targetName
     * @return xml text containing the Service Assembly infos
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String)
     */
    public String listServiceAssemblies(String targetName)
            throws ManagementRemoteException {
        return listServiceAssemblies(null, null, targetName);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetName
     * @return xml string contain the list of service assembly infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String,
     *      java.lang.String)
     */
    public String listServiceAssemblies(String componentName, String targetName)
            throws ManagementRemoteException {
        return listServiceAssemblies(null, componentName, targetName);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param state
     *            to return all the service assemblies that are in the specified
     *            state. JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or
     *            null for ANY state
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetName
     * @return xml string contain the list of service assembly infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String listServiceAssemblies(String state, String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = state;
        params[1] = componentName;
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "listServiceAssemblies", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * returns a list of Service Assembly Infos in a xml format.
     * 
     * @param targetNames
     * @return xml text containing the Service Assembly infos
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String[])
     */
    public Map<String, String> listServiceAssemblies(String[] targetNames)
            throws ManagementRemoteException {
        return listServiceAssemblies(null, null, targetNames);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetNames
     * @return xml string contain the list of service assembly infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> listServiceAssemblies(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        return listServiceAssemblies(null, componentName, targetNames);
    }
    
    /**
     * returns the list of service asssembly infos in a xml format that have the
     * service unit deployed on the specified component.
     * 
     * @param state
     *            to return all the service assemblies that are in the specified
     *            state. JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or
     *            null for ANY state
     * @param componentName
     *            to list all the service assemblies that have some deployments
     *            on this component.
     * @param targetNames
     * @return xml string contain the list of service assembly infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceAssemblies(java.lang.String,
     *      java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> listServiceAssemblies(String state,
            String componentName, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = state;
        params[1] = componentName;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "listServiceAssemblies", params, signature);
        
        return resultObject;
    }
    
    /**
     * return component info xml text that has only service engine infos.
     * 
     * @param targetName
     * @return the component info xml text.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceEngines(java.lang.String)
     */
    public String listServiceEngines(String targetName)
            throws ManagementRemoteException {
        return this.listServiceEngines(null, null, null, targetName);
    }
    
    /**
     * return component info xml text that has only service engine infos which
     * satisfies the options passed to the method.
     * 
     * @param state
     *            return all the service engines that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the service engines that have a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return all the service engines that have the specified service
     *            assembly deployed on them. null value to ignore this option.
     * @param targetName
     * @return xml text contain the list of service engine component infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceEngines(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String listServiceEngines(String state, String sharedLibraryName,
            String serviceAssemblyName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = state;
        params[1] = sharedLibraryName;
        params[2] = serviceAssemblyName;
        params[3] = targetName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "listServiceEngines", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * return component info xml text that has only service engine infos.
     * 
     * @param targetNames
     * @return the component info xml text.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceEngines(java.lang.String[])
     */
    public Map<String, String> listServiceEngines(String[] targetNames)
            throws ManagementRemoteException {
        return this.listServiceEngines(null, null, null, targetNames);
    }
    
    /**
     * return component info xml text that has only service engine infos which
     * satisfies the options passed to the method.
     * 
     * @param state
     *            return all the service engines that are in the specified
     *            state. valid states are JBIComponentInfo.STARTED, STOPPED,
     *            INSTALLED or null for ANY state
     * @param sharedLibraryName
     *            return all the service engines that have a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return all the service engines that have the specified service
     *            assembly deployed on them. null value to ignore this option.
     * @param targetNames
     * @return xml text contain the list of service engine component infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listServiceEngines(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> listServiceEngines(String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = state;
        params[1] = sharedLibraryName;
        params[2] = serviceAssemblyName;
        params[3] = targetNames;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "listServiceEngines", params, signature);
        
        return resultObject;
    }
    
    /**
     * return component info xml text that has only shared library infos.
     * 
     * @param targetName
     * @return the component info xml text.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraries(java.lang.String)
     */
    public String listSharedLibraries(String targetName)
            throws ManagementRemoteException {
        return this.listSharedLibraries(null, targetName);
    }
    
    /**
     * returns the list of Shared Library infos in the in a xml format
     * 
     * @param componentName
     *            to return only the shared libraries that are this component
     *            dependents. null for listing all the shared libraries in the
     *            schemaorg_apache_xmlbeans.system.
     * @param targetName
     * @return xml string contain the list of componentinfos for shared
     *         libraries.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraries(java.lang.String,
     *      java.lang.String)
     */
    public String listSharedLibraries(String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "listSharedLibraries", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * return component info xml text that has only shared library infos.
     * 
     * @param targetNames
     * @return the component info xml text.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraries(java.lang.String[])
     */
    public Map<String, String> listSharedLibraries(String[] targetNames)
            throws ManagementRemoteException {
        return this.listSharedLibraries(null, targetNames);
    }
    
    /**
     * returns the list of Shared Library infos in the in a xml format
     * 
     * @param componentName
     *            to return only the shared libraries that are this component
     *            dependents. null for listing all the shared libraries in the
     *            schemaorg_apache_xmlbeans.system.
     * @param targetNames
     * @return xml string contain the list of componentinfos for shared
     *         libraries.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraries(java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> listSharedLibraries(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "listSharedLibraries", params, signature);
        
        return resultObject;
    }
    
    /**
     * returns a list of Binding Component and Service Engine infos in xml
     * format, that are dependent upon a specified Shared Library
     * 
     * @param sharedLibraryName
     *            the shared library name
     * @param targetName
     * @return xml string containing the list of componentInfos
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraryDependents(java.lang.String,
     *      java.lang.String)
     */
    public String listSharedLibraryDependents(String sharedLibraryName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = sharedLibraryName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "listSharedLibraryDependents", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * returns a list of Binding Component and Service Engine infos in xml
     * format, that are dependent upon a specified Shared Library
     * 
     * @param sharedLibraryName
     *            the shared library name
     * @param targetNames
     * @return xml string containing the list of componentInfos
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#listSharedLibraryDependents(java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> listSharedLibraryDependents(
            String sharedLibraryName, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = sharedLibraryName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "listSharedLibraryDependents", params, signature);
        
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified binding component if
     * exists. If no binding component with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the binding component to lookup
     * @param state
     *            return the binding component that is in the specified state.
     *            valid states are JBIComponentInfo.STARTED, STOPPED, INSTALLED
     *            or null for ANY state
     * @param sharedLibraryName
     *            return the binding component that has a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return the binding component that has the specified service
     *            assembly deployed on it. null value to ignore this option.
     * @param targetName
     * @return xml text contain the binding component info that confirms to the
     *         component info list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showBindingComponent(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String)
     */
    public String showBindingComponent(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[5];
        params[0] = name;
        params[1] = state;
        params[2] = sharedLibraryName;
        params[3] = serviceAssemblyName;
        params[4] = targetName;
        
        String[] signature = new String[5];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        signature[4] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "showBindingComponent", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * return component info xml text for the specified binding component if
     * exists. If no binding component with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the binding component to lookup
     * @param state
     *            return the binding component that is in the specified state.
     *            valid states are JBIComponentInfo.STARTED, STOPPED, INSTALLED
     *            or null for ANY state
     * @param sharedLibraryName
     *            return the binding component that has a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return the binding component that has the specified service
     *            assembly deployed on it. null value to ignore this option.
     * @param targetNames
     * @return xml text contain the binding component info that confirms to the
     *         component info list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showBindingComponent(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> showBindingComponent(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[5];
        params[0] = name;
        params[1] = state;
        params[2] = sharedLibraryName;
        params[3] = serviceAssemblyName;
        params[4] = targetNames;
        
        String[] signature = new String[5];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        signature[4] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "showBindingComponent", params, signature);
        
        return resultObject;
    }
    
    /**
     * return service assembly info xml text for the specified service assembly
     * if exists. If no service assembly with that name exists, it returns the
     * xml with empty list.
     * 
     * @param name
     *            name of the service assembly to lookup
     * @param state
     *            return the service assembly that is in the specified state.
     *            JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or null for
     *            ANY state
     * @param componentName
     *            return the service assembly that has service units on this
     *            component.
     * @param targetName
     * @return xml string contain service assembly info that confirms to the
     *         service assembly list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showServiceAssembly(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String)
     */
    public String showServiceAssembly(String name, String state,
            String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = name;
        params[1] = state;
        params[2] = componentName;
        params[3] = targetName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "showServiceAssembly", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * return service assembly info xml text for the specified service assembly
     * if exists. If no service assembly with that name exists, it returns the
     * xml with empty list.
     * 
     * @param name
     *            name of the service assembly to lookup
     * @param state
     *            return the service assembly that is in the specified state.
     *            JBIServiceAssemblyInfo.STARTED, STOPPED, SHUTDOWN or null for
     *            ANY state
     * @param componentName
     *            return the service assembly that has service units on this
     *            component.
     * @param targetNames
     * @return xml string contain service assembly info that confirms to the
     *         service assembly list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showServiceAssembly(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> showServiceAssembly(String name, String state,
            String componentName, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = name;
        params[1] = state;
        params[2] = componentName;
        params[3] = targetNames;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "showServiceAssembly", params, signature);
        
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified service engine if
     * exists. If no service engine with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the service engine to lookup
     * @param state
     *            to return all the service engines that are in the specified
     *            state. JBIComponentInfo.STARTED, STOPPED, INSTALLED or null
     *            for ANY state
     * @param sharedLibraryName
     *            to return all the service engines that have a dependency on
     *            the specified shared library. Could be null for not filtering
     *            the service engines for this dependency.
     * @param serviceAssemblyName
     *            to return all the service engines that have the specified
     *            service assembly deployed on them. Could be null for not
     *            filtering the service engines for this dependency.
     * @param targetName
     * @return xml string contain service engine component info
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showServiceEngine(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String)
     */
    public String showServiceEngine(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[5];
        params[0] = name;
        params[1] = state;
        params[2] = sharedLibraryName;
        params[3] = serviceAssemblyName;
        params[4] = targetName;
        
        String[] signature = new String[5];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        signature[4] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "showServiceEngine", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * return component info xml text for the specified service engine if
     * exists. If no service engine with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the service engine to lookup
     * @param state
     *            to return all the service engines that are in the specified
     *            state. JBIComponentInfo.STARTED, STOPPED, INSTALLED or null
     *            for ANY state
     * @param sharedLibraryName
     *            to return all the service engines that have a dependency on
     *            the specified shared library. Could be null for not filtering
     *            the service engines for this dependency.
     * @param serviceAssemblyName
     *            to return all the service engines that have the specified
     *            service assembly deployed on them. Could be null for not
     *            filtering the service engines for this dependency.
     * @param targetNames
     * @return xml string contain service engine component info
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showServiceEngine(java.lang.String,
     *      java.lang.String, java.lang.String, java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> showServiceEngine(String name, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[5];
        params[0] = name;
        params[1] = state;
        params[2] = sharedLibraryName;
        params[3] = serviceAssemblyName;
        params[4] = targetNames;
        
        String[] signature = new String[5];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        signature[4] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "showServiceEngine", params, signature);
        
        return resultObject;
    }
    
    /**
     * return component info xml text for the specified shared library if
     * exists. If no shared library with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the shared library to lookup
     * @param componentName
     *            return the shared library that is this component dependents.
     *            null to ignore this option.
     * @param targetName
     * @return xml string contain shared library component info that confirms to
     *         the component info list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showSharedLibrary(java.lang.String,
     *      java.lang.String, java.lang.String)
     */
    public String showSharedLibrary(String name, String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = name;
        params[1] = componentName;
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "showSharedLibrary", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * return component info xml text for the specified shared library if
     * exists. If no shared library with that name exists, it returns the xml
     * with empty list.
     * 
     * @param name
     *            name of the shared library to lookup
     * @param componentName
     *            return the shared library that is this component dependents.
     *            null to ignore this option.
     * @param targetNames
     * @return xml string contain shared library component info that confirms to
     *         the component info list xml grammer.
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#showSharedLibrary(java.lang.String,
     *      java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> showSharedLibrary(String name,
            String componentName, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = name;
        params[1] = componentName;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "showSharedLibrary", params, signature);
        
        return resultObject;
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param force
     *            true if component should be shutdown in any case, else false.
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownComponent(java.lang.String,
     *      boolean, java.lang.String)
     */
    public String shutdownComponent(String componentName, boolean force,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = force;
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "shutdownComponent", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownComponent(java.lang.String,
     *      java.lang.String)
     */
    public String shutdownComponent(String componentName, String targetName)
            throws ManagementRemoteException {
        boolean force = false;
        return this.shutdownComponent(componentName, force, targetName);
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownComponent(java.lang.String,
     *      java.lang.String[])
     */
    public Map<String, String> shutdownComponent(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        boolean force = false;
        return this.shutdownComponent(componentName, force, targetNames);
    }
    
    /**
     * shuts down component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param force
     *            true if component should be shutdown in any case, else false.
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownComponent(java.lang.String,
     *      boolean, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> shutdownComponent(String componentName,
            boolean force, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = force;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "shutdownComponent", params, signature);
        
        return resultObject;
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceShutdown
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownServiceAssembly(java.lang.String,
     *      boolean, java.lang.String)
     */
    public String shutdownServiceAssembly(String serviceAssemblyName,
            boolean forceShutdown, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = serviceAssemblyName;
        params[1] = forceShutdown;
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "shutdownServiceAssembly", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String shutdownServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "shutdownServiceAssembly", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownServiceAssembly(java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> shutdownServiceAssembly(
            String serviceAssemblyName, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "shutdownServiceAssembly", params, signature);
        
        return resultObject;
    }
    
    /**
     * shuts down service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param forceShutdown
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and result as a management message xml text
     *         strings.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#shutdownServiceAssembly(java.lang.String,
     *      boolean, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> shutdownServiceAssembly(
            String serviceAssemblyName, boolean forceShutdown,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = serviceAssemblyName;
        params[1] = forceShutdown;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "shutdownServiceAssembly", params, signature);
        
        return resultObject;
    }
    
    /**
     * starts component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#startComponent(java.lang.String,
     *      java.lang.String)
     */
    public String startComponent(String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName, "startComponent",
                params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * starts component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#startComponent(java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> startComponent(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "startComponent", params, signature);
        
        return resultObject;
    }
    
    /**
     * starts service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#startServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String startServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "startServiceAssembly", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * starts service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#startServiceAssembly(java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> startServiceAssembly(String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "startServiceAssembly", params, signature);
        
        return resultObject;
    }
    
    /**
     * stops component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#stopComponent(java.lang.String,
     *      java.lang.String)
     */
    public String stopComponent(String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName, "stopComponent",
                params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * stops component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return name of the component
     * 
     * @see com.sun.jbi.ui.common.JBIAdminCommands#stopComponent(java.lang.String,
     *      java.lang.String)
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#stopComponent(java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> stopComponent(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "stopComponent", params, signature);
        
        return resultObject;
    }
    
    /**
     * stops service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#stopServiceAssembly(java.lang.String,
     *      java.lang.String)
     */
    public String stopServiceAssembly(String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "stopServiceAssembly", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * stops service assembly
     * 
     * @param serviceAssemblyName
     *            name of the service assembly
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * @return result as a management message xml text
     * 
     * @see com.sun.esb.management.api.runtime.RuntimeManagementService#stopServiceAssembly(java.lang.String,
     *      java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> stopServiceAssembly(String serviceAssemblyName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this
                .getRuntimeManagementServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = serviceAssemblyName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "stopServiceAssembly", params, signature);
        
        return resultObject;
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        
    }
    
}
