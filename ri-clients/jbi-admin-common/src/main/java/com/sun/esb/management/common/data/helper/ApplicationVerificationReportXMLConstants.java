/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ApplicationVerificationReportXMLConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.Serializable;

/**
 * XML constants required to parse or create Application Verification Report
 *
 * @author graj
 *
 */
public interface ApplicationVerificationReportXMLConstants extends Serializable {

    // XML TAGS
    public static final String VERSION_KEY                         = "version";

    public static final String VERSION_VALUE                       = "1.0";

    public static final String NAMESPACE_KEY                       = "xmlns";

    public static final String NAMESPACE_VALUE                     = "http://java.sun.com/xml/ns/esb/management/ApplicationVerificationReport";

    public static final String APPLICATION_VERIFICATION_REPORT_KEY = "ApplicationVerificationReport";

    public static final String SERVICE_ASSEMBLY_NAME_KEY           = "ServiceAssemblyName";

    public static final String SERVICE_ASSEMBLY_DESCRIPTION_KEY    = "ServiceAssemblyDescription";

    public static final String NUMBER_OF_SERVICE_UNITS_KEY         = "NumberOfServiceUnits";

    public static final String ALL_COMPONENTS_INSTALLED_KEY        = "AllComponentsInstalled";

    public static final String MISSING_COMPONENTS_LIST_KEY         = "MissingComponentsList";

    public static final String MISSING_COMPONENT_NAME_KEY          = "MissingComponentName";

    public static final String ENDPOINT_INFORMATION_LIST_KEY       = "EndpointInformationList";

    public static final String TEMPLATE_ZIPID_KEY                  = "TemplateZIPID";

    public static final String ENDPOINT_KEY                        = "Endpoint";

    public static final String ENDPOINT_NAME_KEY                   = "EndpointName";

    public static final String SERVICE_UNIT_NAME_KEY               = "ServiceUnitName";

    public static final String COMPONENT_NAME_KEY                  = "ComponentName";

    public static final String STATUS_KEY                          = "Status";
    
    public static final String MISSING_APPVARS_KEY                 = "MissingApplicationVariables";
        
    public static final String MISSING_APPVAR_NAME_KEY             = "ApplicationVariable";
    
    public static final String MISSING_APPCONFIGS_KEY              = "MissingApplicationConfigurations";    
    
    public static final String MISSING_APPCONFIG_NAME_KEY          = "ApplicationConfiguration";                
    
    public static final String JAVAEE_VERIFIER_REPORTS_LIST_KEY    = "JavaEEVerifierReports";    
    
    public static final String JAVAEE_VERIFIER_REPORT_KEY          = "JavaEEVerifierReport";      
    
    public static final String JAVAEE_VERIFIER_SERVICE_UNIT_NAME   = "JavaEESUName";    
    
    public static final String JAVAEE_VERIFIER_REPORT_TABLE_KEY    = "JavaEEReportTable";    
    
    public static final String JAVAEE_VERIFIER_REPORT_ITEM_KEY     = "ReportItem";        
    
    public static final String JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_KEY   = "ReportItemContent";        

    public static final String JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_NAME_KEY   = "Key";        
    
    public static final String JAVAEE_VERIFIER_REPORT_ITEM_CONTENT_VALUE_KEY   = "Value";            

}
