/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIDescriptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.io.Reader;
import java.io.StringReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * This class is the base class for reading the jbi.xml.
 *
 * @author  Sun Microsystems, Inc.
 */
public abstract class JBIDescriptor
{
    /**
     * check for shared library descriptor.
     * @return true if it is shared library desciptor else false.
     */
    public abstract boolean isSharedLibraryDescriptor();
    /**
     * check for ServiceEngine descriptor.
     * @return true if it is ServiceEngine desciptor else false.
     */
    public abstract boolean isServiceEngineDescriptor();
    /**
     * check for BindingComponen descriptor.
     * @return true if it is BindingComponen desciptor else false.
     */
    public abstract boolean isBindingComponentDescriptor();
    /**
     * check for service assembly descriptor.
     * @return true if it is service assembly desciptor else false.
     */
    public abstract boolean isServiceAssemblyDescriptor();
    
    /**
     * creates jbi descriptor object .
     * @param xmlReader Reader object.
     * @throws java.lang.Exception on error.
     * @return jbi descriptor object.
     */
    public static JBIDescriptor createJBIDescriptor(Reader xmlReader )
    throws Exception
    {
        Document xmlDoc = DOMUtil.UTIL.buildDOMDocument(xmlReader);
        
        Element jbiElement = DOMUtil.UTIL.getElement(xmlDoc, "jbi");
        if ( jbiElement == null )
        {
            throw new Exception(
                JBIResultXmlBuilder.createFailedJbiResultXml(Util.getCommonI18NBundle(),
                "not.a.jbi.descriptor", null));
        }
        // get the main element (component, shared-library, service-assembly)
        Element saEl = DOMUtil.UTIL.getChildElement(jbiElement, "service-assembly");
        if ( saEl != null )
        {
            return ServiceAssemblyDD.createServiceAssemblyDD(saEl);
        }
        
        Element slibEl = DOMUtil.UTIL.getChildElement(jbiElement, "shared-library");
        if ( slibEl != null )
        {
            return SharedLibraryDD.createSharedLibraryDD(slibEl);
        }
        
        Element compEl = DOMUtil.UTIL.getChildElement(jbiElement, "component");
        if ( compEl != null )
        {
            String type = compEl.getAttribute("type");
            // System.out.println("COMPONENT TYPE: " + type);
            if ( "service-engine".equals(type))
            {
                return ServiceEngineDD.createServiceEngineDD(compEl);
            }
            else if ("binding-component".equals(type))
            {
                return BindingComponentDD.createBindingComponentDD(compEl);
            }
        }
        
        // not a jbi decriptor
        throw new Exception(
        JBIResultXmlBuilder.createFailedJbiResultXml(Util.getCommonI18NBundle(),
            "not.a.jbi.descriptor", null));
        
    }
    
    /**
     * creates JBI Descriptor object.
     * @param xmlText text.
     * @throws java.lang.Exception on error.
     * @return object.
     */
    public static JBIDescriptor createJBIDescriptor(String xmlText )
    throws Exception
    {
        return createJBIDescriptor(new StringReader(xmlText));
    }
    
    /**
     * This class represents shared library descriptor.
     */
    public static class SharedLibraryDD extends JBIDescriptor
    {
        /**
         * constructor.
         */
        protected SharedLibraryDD()
        {
            super();
        }
        /**
         * check for shared library descriptor.
         * @return true if it is shared library else false.
         */
        public boolean isSharedLibraryDescriptor()
        {
            return true;
        }
        /**
         * check for ServiceEngine descriptor.
         * @return true if it is ServiceEngine desciptor else false.
         */
        public boolean isServiceEngineDescriptor()
        {
            return false;
        }
        /**
         * check for BindingComponent descriptor.
         * @return true if it is BindingComponent desciptor else false.
         */
        public boolean isBindingComponentDescriptor()
        {
            return false;
        }
        /**
         * check for ServiceAssembly descriptor.
         * @return true if it is ServiceAssembly desciptor else false.
         */
        public boolean isServiceAssemblyDescriptor()
        {
            return false;
        }
        
        /**
         * creates slib dd.
         * @param slibEl xml element.
         * @return object.
         */
        public static SharedLibraryDD createSharedLibraryDD(Element slibEl)
        {
            return new SharedLibraryDD();
        }
    }
    /**
     * Common clas for component descriptor
     */
    public static abstract class ComponentDD extends JBIDescriptor
    {
        /**
         * constructor.
         */
        protected ComponentDD()
        {
            super();
        }
    }
    
    /**
     * Service engine descritor.
     */
    public static class ServiceEngineDD extends ComponentDD
    {
        /**
         * constructor.
         */
        protected ServiceEngineDD()
        {
            super();
        }
        /**
         * check for shared library descriptor.
         * @return true if it is shared library desciptor else false.
         */
        public boolean isSharedLibraryDescriptor()
        {
            return false;
        }
        /**
         * check for ServiceEngine descriptor.
         * @return true if it is ServiceEngine desciptor else false.
         */
        public boolean isServiceEngineDescriptor()
        {
            return true;
        }
        /**
         * check for BindingComponen descriptor.
         * @return true if it is BindingComponen desciptor else false.
         */
        public boolean isBindingComponentDescriptor()
        {
            return false;
        }
        /**
         * check for service assembly descriptor.
         * @return true if it is service assembly desciptor else false.
         */
        public boolean isServiceAssemblyDescriptor()
        {
            return false;
        }
        
        /**
         * cretes service engine dd.
         * @param compEl xml element.
         * @return object.
         */
        public static ServiceEngineDD createServiceEngineDD(Element compEl)
        {
            return new ServiceEngineDD();
        }
        
    }
    
    /**
     * class for bc dd.
     */
    public static class BindingComponentDD extends ComponentDD
    {
        /**
         * constructor.
         */
        protected BindingComponentDD()
        {
            super();
        }
        /**
         * check for shared library descriptor.
         * @return true if it is shared library desciptor else false.
         */
        public boolean isSharedLibraryDescriptor()
        {
            return false;
        }
        /**
         * check for ServiceEngine descriptor.
         * @return true if it is ServiceEngine desciptor else false.
         */
        public boolean isServiceEngineDescriptor()
        {
            return false;
        }
        /**
         * check for BindingComponen descriptor.
         * @return true if it is BindingComponen desciptor else false.
         */
        public boolean isBindingComponentDescriptor()
        
        {
            return true;
        }
        /**
         * check for service assembly descriptor.
         * @return true if it is service assembly desciptor else false.
         */
        public boolean isServiceAssemblyDescriptor()
        {
            return false;
        }
        /**
         * creates dd.
         * @param compEl xml element.
         * @return object.
         */
        public static BindingComponentDD createBindingComponentDD(Element compEl)
        {
            return new BindingComponentDD();
        }
    }
    
}
