/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ToolsLogManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Defines the loggers for the Tools on client and server side
 *
 * @author Sun Microsystems, Inc.
 */
public class ToolsLogManager
{   
    /** logger for tools runtime logger **/
    private static Logger sRuntimeLogger = null;
    /** logger for tools jmx client logger **/
    private static Logger sClientLogger = null;
    /** logger for tools ant logger **/
    private static Logger sAntLogger = null;
    /** logger for tools common package **/
    private static Logger sCommonLogger = null;
    
    /**
     * Get the logger for the tools runtime.
     * @return java.util.Logger is the logger
     */
    public static Logger getRuntimeLogger()
    {
        // late binding.
        if ( sRuntimeLogger == null )
        {
            sRuntimeLogger = Logger.getLogger("com.sun.jbi.ui.runtime");
        }
        return sRuntimeLogger;
    }
    
    /**
     * Get the logger for the tools runtime.
     * @return java.util.Logger is the logger
     */
    public static Logger getClientLogger()
    {
        // late binding.
        if ( sClientLogger == null )
        {
            sClientLogger = Logger.getLogger("com.sun.jbi.ui.client");
        }
        return sClientLogger;
    }
    
    /**
     * Get the logger for the tools runtime.
     * @return java.util.Logger is the logger
     */
    public static Logger getAntLogger()
    {
        // late binding.
        if ( sAntLogger == null )
        {
            sAntLogger = Logger.getLogger("com.sun.jbi.ui.ant");
        }
        return sAntLogger;
    }
    
    /**
     * Get the logger for the tools runtime.
     * @return java.util.Logger is the logger
     */
    public static Logger getCommonLogger()
    {
        // late binding.
        if ( sCommonLogger == null )
        {
            sCommonLogger = Logger.getLogger("com.sun.jbi.ui.common");
        }
        return sCommonLogger;
    }
    
    
}
