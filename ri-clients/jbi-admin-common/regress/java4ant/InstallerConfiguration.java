/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CustomConfig.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;


public class InstallerConfiguration
    implements InstallerConfigurationMBean
{

    private String mA, mB, mC;
  
    public InstallerConfiguration()
    {
        
    }
    
    /**
     * Attribute getters/setters
     */   
    public String getA()
    {
        return mA;
    }
    
    public void setA(String a)
    {
        mA = a;
        System.setProperty("A", a);
        System.out.println("Successfully set install time component configuration A = " + a);
    }
    
    public String getB()
    {
        return mB;
    }
    public void setB(String b)
    {
        mB = b;
        System.setProperty("B", b);
        System.out.println("Successfully set install time component configuration B = " + b);
    }
 
}
