/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.management.openmbean.*;
import javax.management.remote.*;
import javax.management.*;

/**
 */
public class TestComponentConfigMetaData
{
     
    private MBeanServerConnection mbns;
    private static final String RESULT_PREFIX = "##### Result of ";
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String COMPONENT_NAME = "component.name";
    private static final String TARGET   = "target";
    
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
  
    
    public ObjectName getJbiAdminCommandsUI()
        throws Exception
    {
        String admin = "com.sun.jbi:" +
            "ServiceName=JbiReferenceAdminUiService,ComponentType=System";
        return new ObjectName(admin);
    }
    

    public void retrieveConfigurationDisplayData(boolean expectingException)
        throws Exception
    {
    
        String target = System.getProperty(TARGET);
        Object[] params = new Object[2];
        params[0] = System.getProperty(COMPONENT_NAME);
        params[1] = target;
    
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        String data = null;
    
        try
        {
            data = (String) mbns.invoke(getJbiAdminCommandsUI(),
                                     "retrieveConfigurationDisplayData", params, signature);
        
            System.out.println(RESULT_PREFIX + " retrieveConfigurationDisplayData( target=" +
                               target + " ):" );
            if (expectingException)
            {
                System.out.println("*** Was expecting exception, but didn't get one! ***");
            }
            else
            {
                System.out.print(" " + data);
            }
                
        }
        catch (Exception e)
        {
            if (expectingException)
            {
                System.out.println(RESULT_PREFIX + " retrieveConfigurationDisplayData( target=" +
                                   target + " ): caught expected exception.");
            }
            else
            { 
                System.out.println(RESULT_PREFIX + " retrieveConfigurationDisplayData( target=" +
                                   target + " ): caught unexpected Exception: ");
            }
            printExceptionMessage(e, "retrieveConfigurationDisplayData");
        }
    }

    public void retrieveConfigurationDisplaySchema(boolean expectingException)
        throws Exception
    {
    
        String target = System.getProperty(TARGET);
        Object[] params = new Object[2];
        params[0] = System.getProperty(COMPONENT_NAME);
        params[1] = target;
    
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        String schema = null;
    
        try
        {
            schema = (String) mbns.invoke(getJbiAdminCommandsUI(),
                                     "retrieveConfigurationDisplaySchema", params, signature);
        
            System.out.println(RESULT_PREFIX + " retrieveConfigurationDisplaySchema( target=" +
                               target + " ):" );
            if (expectingException)
            {
                System.out.println("*** Was expecting exception, but didn't get one! ***");
            }
            else
            {
                System.out.print( " " + schema); 
            }
        }
        catch (Exception e)
        {
            if (expectingException)
            {
                System.out.println(RESULT_PREFIX + " retrieveConfigurationDisplaySchema( target=" +
                                   target + " ): caught expected exception.");
            }
            else
            {
                System.out.println(RESULT_PREFIX + " retrieveConfigurationDisplaySchema( target=" +
                                   target + " ): caught unexpected exception");
            }
            printExceptionMessage(e, "retrieveConfigurationDisplaySchema");
        }
    }
    
    public void retrieveAppConfigType(boolean expectingException)
        throws Exception
    {
    
        String target = System.getProperty(TARGET);
        Object[] params = new Object[2];
        params[0] = System.getProperty(COMPONENT_NAME);
        params[1] = target;
    
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
    
        try
        {
            CompositeType ct = (CompositeType) mbns.invoke(getJbiAdminCommandsUI(),
                                     "queryApplicationConfigurationType", params, signature);
        
            System.out.println(RESULT_PREFIX + " queryApplicationConfigurationType( target=" +
                               target + " ):"  + ct);
            if (expectingException)
            {
                System.out.println("*** Was expecting exception, but didn't get one! ***");
            }
        }
        catch (Exception e)
        {
            if (expectingException)
            {
                System.out.println(RESULT_PREFIX + " queryApplicationConfigurationType( target=" +
                                   target + " ): caught expected exception.");
            }
            else
            {
                System.out.println(RESULT_PREFIX + " queryApplicationConfigurationType( target=" +
                                   target + " ): caught unexpected exception");
            }
            printExceptionMessage(e, "queryApplicationConfigurationType");
        }
    }
        
    private void printExceptionMessage(Exception ex, String opName)
    {
        Throwable cause = ex;
                
        while ( cause instanceof javax.management.JMException && cause != null )
        {
            cause = cause.getCause();
        }
        
        
        System.out.println("Exception " + cause.getClass() + " thrown by "
                    + opName + " with Message \n" + 
                                    cause.getMessage().replace("en_US", "en"));
    }

    public static void main (String[] params)
        throws Exception 
    {
        // test setup
        TestComponentConfigMetaData test = new TestComponentConfigMetaData();
        test.initMBeanServerConnection();
     
        // test
        test.retrieveConfigurationDisplayData(true);
        test.retrieveConfigurationDisplaySchema(true);
        test.retrieveAppConfigType(true);

        
    }
}
