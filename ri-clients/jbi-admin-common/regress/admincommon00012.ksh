#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


echo "admincommon00012: Test component configuration support API's for a component which does not have any component/app configuration."

#regress setup
. ./regress_defs.ksh

COMPONENT_ARCHIVE=$UI_REGRESS_DIST_DIR/component-with-empty-config-mbean.jar
COMPONENT_NAME=dummy-binding

##
# Test using a component which has a empty config:Configuration Element - new schema 
##
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f admincommon00012.xml pkg.noconfig.test.component.1

$JBI_ANT -Djbi.install.file=$COMPONENT_ARCHIVE  install-component

# Operations to determining application configuration support should return true. The config meta-data is bundled in jbi.xml so this should return true
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00012.xml test.app.config.support

$JBI_ANT -Djbi.component.name=$COMPONENT_NAME start-component

# Operations to determining application configuration support should return true
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00012.xml test.app.config.support


# Test the application variable / configuration operations, they should fail gracefully indicating that the component does not support application vars/config.
asadmin create-jbi-application-configuration --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME --configname=testConfig2 configurationName=testConfig2,name1=value1,name2=value2
asadmin update-jbi-application-configuration --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME --configname=testConfig2 configurationName=testConfig2,name1=value1,name2=value2
asadmin delete-jbi-application-configuration --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME testConfig2

#asadmin create-jbi-application-variable --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME FisrtName=[STRING]John
#asadmin update-jbi-application-variable --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME FisrtName=[STRING]Jane
#asadmin delete-jbi-application-variable --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME FisrtName
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.app.variable.name=Name -Djbi.app.variable.type=STRING -Djbi.app.variable.value=John create-application-variable
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.app.variable.name=Name -Djbi.app.variable.type=STRING -Djbi.app.variable.value=Jane update-application-variable
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.app.variable.name=abc delete-application-variable

# component cleanup
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME shut-down-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME uninstall-component


##
# Test using a component which has no config:Configuration Element - old schema 
##
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f admincommon00012.xml pkg.noconfig.test.component.2

$JBI_ANT -Djbi.install.file=$COMPONENT_ARCHIVE  install-component

# Operations to determining application configuration support should return false
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00012.xml test.app.config.support

$JBI_ANT -Djbi.component.name=$COMPONENT_NAME start-component

# Operations to determining application configuration support should return false
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00012.xml test.app.config.support

# Test the application variable / configuration operations, they should fail gracefully indicating that the component does not support application vars/config ( test fix for issue 104,154 ).
asadmin create-jbi-application-configuration --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME --configname=testConfig2 configurationName=testConfig2,name1=value1,name2=value2
asadmin update-jbi-application-configuration --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME --configname=testConfig2 configurationName=testConfig2,name1=value1,name2=value2
asadmin delete-jbi-application-configuration --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME testConfig2

#asadmin create-jbi-application-variable --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME FisrtName=[STRING]John
#asadmin update-jbi-application-variable --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME FisrtName=[STRING]Jane
#asadmin delete-jbi-application-variable --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --component=$COMPONENT_NAME FisrtName

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.app.variable.name=Name -Djbi.app.variable.type=STRING -Djbi.app.variable.value=John create-application-variable
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.app.variable.name=Name -Djbi.app.variable.type=STRING -Djbi.app.variable.value=Jane update-application-variable
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.app.variable.name=abc delete-application-variable


# component cleanup
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME shut-down-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME uninstall-component

exit 0
