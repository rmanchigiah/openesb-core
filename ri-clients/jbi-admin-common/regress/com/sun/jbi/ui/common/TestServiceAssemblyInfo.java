/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceAssemblyInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestServiceAssemblyInfo extends TestCase
{
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestServiceAssemblyInfo(String aTestName)
    {
        super(aTestName);
    }
    
    /**
     * fix it
     * @throws Exception fix it
     */
    public void testLoad() throws Exception
    {
        
        InputStream res = this.getClass().getResourceAsStream("test_service_assembly_info.xml");
        InputStreamReader xmlReader = new InputStreamReader(res);
        
        List testList = ServiceAssemblyInfo.readFromXmlTextWithProlog(xmlReader);
        for ( Iterator itr = testList.iterator(); itr.hasNext();)
        {
            ServiceAssemblyInfo info = (ServiceAssemblyInfo)itr.next();
            System.out.println(info);
            List suList = info.getServiceUnitInfoList();
            for ( Iterator suItr = suList.iterator(); suItr.hasNext();)
            {
                System.out.println(suItr.next());
            }
        }
        
        
        String xmlText = ServiceAssemblyInfo.writeAsXmlTextWithProlog(testList);
        System.out.println("Serialized Service Assembly Inof List XML");
        System.out.println(xmlText);
        testList = ServiceAssemblyInfo.readFromXmlTextWithProlog(xmlText);
        System.out.println("Deserialized Service Assembly Inof List XML");
        for ( Iterator itr = testList.iterator(); itr.hasNext();)
        {
            ServiceAssemblyInfo info = (ServiceAssemblyInfo)itr.next();
            System.out.println(info);
            List suList = info.getServiceUnitInfoList();
            for ( Iterator suItr = suList.iterator(); suItr.hasNext();)
            {
                System.out.println(suItr.next());
            }
        }
        
    }
    
    /**
     * fix it
     * @throws Exception fix it
     */
    public void testLoadFromDD() throws Exception
    {
        InputStream res = this.getClass().getResourceAsStream("test_jbi.xml");
        InputStreamReader reader = new InputStreamReader(res);
        ServiceAssemblyInfo saInfo = ServiceAssemblyInfo.createFromServiceAssemblyDD(reader);
        System.out.println(saInfo);
        List suList = saInfo.getServiceUnitInfoList();
        for ( Iterator suItr = suList.iterator(); suItr.hasNext();)
        {
            System.out.println(suItr.next());
        }
    }
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            TestServiceAssemblyInfo test = new TestServiceAssemblyInfo("test");
            test.testLoad();
            test.testLoadFromDD();
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
}
