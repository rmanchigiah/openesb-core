/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJBIDescriptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.common;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import junit.framework.TestCase;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJBIDescriptor extends TestCase
{
    
    /**
     * Creates a new instance of TestMgmtMessage
     * @param aTestName name
     */
    public TestJBIDescriptor(String aTestName)
    {
        super(aTestName);
    }

    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testServiceAssemblyDD() throws Exception
    {
        InputStream res = this.getClass().getResourceAsStream("test_jbi.xml");
        InputStreamReader reader = new InputStreamReader(res);
        JBIDescriptor dd = JBIDescriptor.createJBIDescriptor(reader);
        this.assertTrue("Expected Service Assembly DD", dd.isServiceAssemblyDescriptor());
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testSharedLibraryDD() throws Exception
    {
        InputStream res = this.getClass().getResourceAsStream("test_slib_dd.xml");
        InputStreamReader reader = new InputStreamReader(res);
        JBIDescriptor dd = JBIDescriptor.createJBIDescriptor(reader);
        this.assertTrue("Expected Shared Library DD", dd.isSharedLibraryDescriptor());
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testEngineDD() throws Exception
    {
        InputStream res = this.getClass().getResourceAsStream("test_engine_dd.xml");
        InputStreamReader reader = new InputStreamReader(res);
        JBIDescriptor dd = JBIDescriptor.createJBIDescriptor(reader);
        System.out.println("Engine DD Class :" + dd.getClass().getName());
        this.assertTrue("Expected Engine DD", dd.isServiceEngineDescriptor());
    }
    
    /**
     * test sucess msg
     * @throws Exception on error
     */
    public void testBindingDD() throws Exception
    {
        InputStream res = this.getClass().getResourceAsStream("test_binding_dd.xml");
        InputStreamReader reader = new InputStreamReader(res);
        JBIDescriptor dd = JBIDescriptor.createJBIDescriptor(reader);
        System.out.println("Binding DD Class :" + dd.getClass().getName());
        this.assertTrue("Expected Binding DD", dd.isBindingComponentDescriptor());
    }
    
    /**
     * main
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        // TODO code application logic here
        try
        {
            new TestJBIDescriptor("test").testSharedLibraryDD();
        } catch ( Exception ex )
        {
            ex.printStackTrace();
        }
    }
} 
