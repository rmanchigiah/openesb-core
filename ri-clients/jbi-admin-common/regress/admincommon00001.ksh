#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)admincommon00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo admincommon00001.ksh

#regress setup
. ./regress_defs.ksh

run_test()
#
#uninstall the glassfish JavaEEServiceEngine
#
{
    asant -emacs -q -f $JBI_ADMIN_XML -Djbi.username=$AS_ADMIN_USER -Djbi.password=$AS_ADMIN_PASSWD -Djbi.port=$JBI_ADMIN_PORT -Djbi.component.name="JavaEEServiceEngine" uninstall-component
}

################## MAIN ##################
####
# Execute the test
####

local_test_setup

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
#re-direct output to stderr because we don't care if this fails on as8.1.  RT 5/2/06
1>&2 run_test | tr -d '\r' | sed -e '/^$/d'

local_test_cleanup

exit 0
