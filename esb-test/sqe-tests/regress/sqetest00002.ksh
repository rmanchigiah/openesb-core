#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)sqetest00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo testname is sqetest00002.ksh

. ./regress_defs.ksh
my_test_domain=JBITest
. $SRCROOT/antbld/regress/common_defs.ksh

asadmin  stop-jbi-component  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 1>&2
asadmin  shut-down-jbi-component  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-http-binding 1>&2
asadmin  list-jbi-binding-components  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --lifecyclestate shutdown | grep sun-http-binding 2>&1

asadmin  stop-jbi-component  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 1>&2
asadmin  shut-down-jbi-component  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT sun-javaee-engine 1>&2
asadmin  list-jbi-service-engines -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT --lifecyclestate shutdown | grep sun-javaee-engine 2>&1
