/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.test.binding1.rt;

import java.util.Iterator;
import javax.xml.namespace.QName;
import javax.jbi.messaging.MessageExchange.Role;

/**
 * Interface that defines an Endpoint.
 *
 * @author Sun Microsystems, Inc.
 */
public class EndpointImpl
    implements com.sun.jbi.binding.security.Endpoint
{
    
    /**
     * Service QName
     */
    private QName mServiceName;    
    
    /**
     * Name
     */
    private String mEndpointName;
    
    /**
     * Path to Sec Cfg file,
     */
    private String mSecCfgFile;
    
    /**
     * Role
     */
    private javax.jbi.messaging.MessageExchange.Role mRole;
    
    /**
     * Operations
     */    
    private java.util.Vector mOps;
    
    /**
     * Component id
     */
    private String mComponentId;
    
    public EndpointImpl (String epName, String serviceName, String serviceTns, 
        String compId, Role role, String secCfgFile)
    {
        mComponentId = compId;
        mEndpointName = epName;
        mOps = new java.util.Vector();
        mRole = role;
        mSecCfgFile = secCfgFile;
        mServiceName = new QName(serviceTns, serviceName);
    }    

    
    /**
     * Returns the service QName.
     *
     * @return service Qname
     */
    public QName getServiceName()
    {
        return mServiceName;
    }

    /**
     * Returns the endpoint name.
     *
     * @return endpoint name.
     */
    public String getEndpointName()
    {
        return mEndpointName;
    }

    /**
     * Gets list of operations supported by this endpoint.
     *
     * @return list of operations.
     */
    public Iterator getOperationNames()
    {
        return mOps.iterator();
    }
    
    /**
     * Get the component id of the Component the endpoint is deployed to.
     *
     * @return Component Id of the Component this Endpoint id deployed to
     */
    public String getComponentId()
    {
        return mComponentId;
    }
    
    /**
     * Get the name of the SecurityConfiguration provided with the Endpoint deployment.
     * @return The name of the security configuration file.
     */
    public String getSecurityConfigFileName()
    {
        return mSecCfgFile;
    }
    
    /**
     * Get the Endpoint Role ( Provider / Consumer )
     *
     * @return the Endpoint role
     */
    public javax.jbi.messaging.MessageExchange.Role getRole()
    {
        return mRole;
    }
}
