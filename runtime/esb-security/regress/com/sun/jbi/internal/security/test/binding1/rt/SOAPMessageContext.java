/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SOAPMessageContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.test.binding1.rt;

import javax.xml.soap.SOAPMessage;
import com.sun.jbi.binding.security.MessageContext;


/**
 * Implementation of the MessageContext interface ( mostly domain of SOAP binding ).
 * This is the runtime context passed to the interceptor at execution time and may vary
 * depending on how generic we want to make it.
 *
 * @author Sun Microsystems, Inc.
 */
public class SOAPMessageContext 
    extends ContextImpl implements MessageContext
{
    /** The SOAP Message */
    private SOAPMessage mSoapMessage;
    
    /** The SOAP Error Message ( mostly a FAULT ) */
    private SOAPMessage mRespSoapMessage;
    
    /** 
     * Creates a new instance of SampleSoapMessageContext.
     *
     * @param msg is the SOAPMessage
     */
    public SOAPMessageContext(SOAPMessage msg)
    {
        mSoapMessage = msg;
        mRespSoapMessage = null;
    }
    
    /* ------------------------------------------------------------------------------- *\
     *                                Message Ctx                                      *
    \* ------------------------------------------------------------------------------- */
    
    /**
     * Get the SOAP Message associated with this Context.
     *
     * @return the SOAP Message in the Context
     */
    public Object getMessage()
    {
        return mSoapMessage;
    };
    
    /**
     * Set the SOAP Message associated with this Context.
     *
     * @param msg is the SOAP Message in the Context
     */
    public void setMessage(Object msg)
    {
        mSoapMessage = (SOAPMessage) msg;
    };
    
    /**
     * Get the ResponseMessage, a response message is present in the Context only when 
     * validation of an incoming request message, results in a response message to
     * be sent to the sender of the request
     *
     * @return the Response Message in the Context
     */
    public Object getResponseMessage()
    {
        return mRespSoapMessage;
    }
    
    /**
     * Set the ResponseMessage, a response message is present in the Context only when 
     * validation of an incoming request message, results in a respponse message to
     * be sent to the sender of the request
     *
     * @param respMsg is the Error Message in the Context
     */
    public void setResponseMessage(Object respMsg)
    {
        mRespSoapMessage = (SOAPMessage) respMsg;
    }
    
}
