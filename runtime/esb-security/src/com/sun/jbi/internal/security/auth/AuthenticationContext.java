/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AuthenticationContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  FileUserDomain.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 */

package com.sun.jbi.internal.security.auth;

import java.util.logging.Logger;

/**
 * FileUserDomain is the handle to getting the user information.
 * This interface will be refined further based on requirements.
 *
 * The methods in this interface throw Exception and no specific exception has been 
 * defined to keep the interface non-proprietary.
 * @author Sun Microsystems, Inc.
 */
public class AuthenticationContext 
    extends com.sun.jbi.internal.security.ContextImpl
    implements com.sun.jbi.internal.security.UserDomain
{
    /** UserDomain Name. */
    private String mName;
    
    /** Logger. */
    private Logger mLogger;
    
    /** The StringTranslator. */
    private AuthenticatorType mAuthType;
    
    /**
     * Constructor.
     *
     * @param name is the logical name that identifies this user domain.
     */
    public AuthenticationContext(String name)
    {
        super(null); 
        mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
        mName = name;

    }
    /**
     * Get the name of the User Domain.
     * @return the name of the User Domain
     */
    public String getName()
    {
        return mName;
    }
    
    /**
     * Set the User Domain Name.
     * @param name is the name of the User Domain
     */
    public void setName(String name)
    {
        mName = name;
    }
    
    /**
     * Get the Authentication Type for the domain.
     *
     * @return the Authentication Type.
     */
    public String getAuthType()
    {
        return mAuthType.getValue();
    }
    
    /**
     * Set the Authentication Type for this domain.
     *
     * @param authType is the authentication type string, the only supported
     * type in Shasta 1.0 is JAAS.
     */
    public void setAuthType(String authType)
    {
        mAuthType = AuthenticatorType.valueOf(authType);
    }
    
    /**
     * Print the Contents of the Context to the Logger.
     *
     * @param logger is the java.util.Logger to use for printing out the contents.
     * @param level is the logging level
     */
    public void print(java.util.logging.Logger logger, java.util.logging.Level level)
    {
    }
}
