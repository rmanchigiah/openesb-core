/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AuthenticatorFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  AuthenticatorFactory.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 8, 2005, 11:58 AM
 */

package com.sun.jbi.internal.security.auth;

import com.sun.jbi.internal.security.UserDomain;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class AuthenticatorFactory
{
    /** JAAS Authenticator Class Name. */
    private static final String JAAS_AUTHENTICATOR = 
        "com.sun.jbi.internal.security.auth.JaasAuthenticator";
        
    /**
     * Create a Authenticator based on the UserDomain.
     *
     * @param domain is the UserDomain instance.
     * @throws IllegalArgumentException if the UserDomain has invalid values.
     * @return the newly created Authenticator based on the UserDomain.
     */
    public static Authenticator createAuthenticator(UserDomain domain)
        throws IllegalArgumentException
    {
        // -- Use domain.getAuthType();
        //AuthenticatorType authType = AuthenticatorType.valueOf("JAAS");
        AuthenticatorType authType = AuthenticatorType.valueOf(domain.getAuthType());
        Authenticator ator = null;
        
        if ( authType.equals(AuthenticatorType.JAAS) )
        {
            ator = new JaasAuthenticator();  
        }
        else if ( authType.equals(AuthenticatorType.SJSAS) )
        {
            ator = new AppSrvAuthenticator();
        }
        else
        {
            throw new IllegalArgumentException(domain.getAuthType());
        }
        ator.initialize(domain);
        return ator;
  
    }
    
}
