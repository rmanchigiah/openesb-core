/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigErrorHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.config;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.internal.security.LocalStringConstants;

import java.util.logging.Logger;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Implementation of the ErrorHandler.
 * @author Sun Microsystems, Inc.
 */
public class ConfigErrorHandler 
    implements ErrorHandler
{
    /** Private Logger for logging information. */
    private Logger mLogger;
    
    /** The String Translator. */
    private StringTranslator mTranslator;
    
    /** 
     * Creates a new instance of ConfigErrorHandler. 
     * @param translator is the String Translator
     */
    public ConfigErrorHandler(StringTranslator translator)
    {
        mTranslator = translator;
        mLogger = Logger.getLogger(com.sun.jbi.internal.security.Constants.PACKAGE);
    }
    
    /**
     * Callback for parser errors.
     * @param exception is the SAXParseException
     * @throws org.xml.sax.SAXException on errors
     */
    public void error (SAXParseException exception) throws SAXException
    {
        mLogger.severe(mTranslator.getString(LocalStringConstants.BC_ERR_SAX_PARSING_EX,
            new Integer(exception.getLineNumber()), 
            new Integer(exception.getColumnNumber()), 
            exception.toString()));
        throw exception;
    }    
    
    /**
     * Callback for parser fatal errors.
     * @param exception is the SAXParseException
     * @throws org.xml.sax.SAXException on errors
     */
    public void fatalError (SAXParseException exception) throws SAXException
    {
        mLogger.severe(mTranslator.getString(LocalStringConstants.BC_ERR_SAX_PARSING_EX,
            new Integer(exception.getLineNumber()), 
            new Integer(exception.getColumnNumber()), 
            exception.toString()));
        throw exception;
    }    
    
    /**
     * Callback for parser warnings.
     * @param exception is the SAXParseException
     * @throws org.xml.sax.SAXException on warnings
     */
    public void warning (SAXParseException exception) throws SAXException
    {
        
        mLogger.severe(mTranslator.getString(LocalStringConstants.BC_ERR_SAX_PARSING_EX,
            new Integer(exception.getLineNumber()), 
            new Integer(exception.getColumnNumber()), 
            exception.toString()));
        throw exception;
    }
    
}
