/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)KeyStoreManagerType.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  KeyStoreManagerType.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary intypeion of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 8, 2005, 12:02 PM
 */

package com.sun.jbi.internal.security.keymgt;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * Type Safe Enumeration of KeyStoreManagerTypes.
 *
 * @author Sun Microsystems, Inc.
 */
public final class KeyStoreManagerType
{
    /** 
     * The only manager type supported currently is JavaStandard,
     * this KeyStoreManager supoorts KeyStore types whose provided
     * by the standard JDK, namely JKS, JCEKS and PKCS12. 
     */
    
    /** JAVA_STD. */
    public static final KeyStoreManagerType JAVA_STD = 
        new KeyStoreManagerType("JavaStandard");
    
    /** SJSAS. */
    public static final KeyStoreManagerType SJSAS = 
        new KeyStoreManagerType("SJSAS");

    /**
     * The VALUES.
     */
    private static final KeyStoreManagerType[] VALUES = {JAVA_STD, SJSAS};
    
    /**
     * The allowed Values.
     */
    public static final List LIST = Collections.unmodifiableList(
        Arrays.asList(VALUES));

    
    /** The type value. */
    private String mValue;
    
    /** 
     * Creates a new instance of KeyStoreManagerType 
     *
     * @param type is the type String
     */
    private KeyStoreManagerType (String type)
    {
        mValue = type;
    }
    
    /**
     * Get the String type value.
     *
     * @return the type string.
     */
    public String getValue()
    {
        return mValue;
    }
    
    /**
     * @return a KeyStoreManagerType based on the string value passed.
     *
     * @param typeStr is the type String.
     */
    public static KeyStoreManagerType valueOf(String typeStr)
    {
        Iterator itr = LIST.iterator();
        while (itr.hasNext()) 
        {
            KeyStoreManagerType type = (KeyStoreManagerType) itr.next();
            if ( typeStr.equals(type.getValue()) )
            {
                return type;
            }
        }       
        throw new IllegalArgumentException(typeStr);
    }  
    
    /**
     * Equality check.
     *
     * @param type is the type to test with for equality.
     * @return true if type is equal to this type.
     */
    public boolean equals(KeyStoreManagerType type)
    {
        return ( getValue().equals(type.getValue()));
    }
    
    /**
     * @return the HashCode for this Object.
     */
    public int hashCode()
    {
        return getValue().hashCode();
    }
}
