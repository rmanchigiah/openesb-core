/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityHandlerImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 8, 2004, 6:28 PM
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.component.ComponentContext;
import com.sun.jbi.binding.security.DeploymentListener;
import com.sun.jbi.binding.security.Interceptor;

import com.sun.jbi.internal.security.config.SecurityConfiguration;
import com.sun.jbi.internal.security.msg.SecurityProcessor;
import com.sun.jbi.internal.security.https.SSLHelper;

import java.util.logging.Logger;



/**
 * The SecurityHandler interface will be used a Binding Component to
 * secure outgoing Messages and validate incoming messages. There is a 1:1 association 
 * between a Binding Component and a Security Handler.
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityHandlerImpl 
    implements com.sun.jbi.binding.security.SecurityHandler
{   
    /** The Instalation Security Configuration associated with this handler */
    private SecurityConfiguration mInstallSecConfig = null;
    
    /** The Logger. */
    private static Logger sLogger = null;
    
    /** The Security Processor */
    private SecurityProcessor mSecurityProcessor;
    
    /** 
     * The Environment Context of the Component associated with the Security Handler
     */
    private com.sun.jbi.internal.security.ComponentContext mEnvCtx;
    
    /** The String translator */
    private StringTranslator mTranslator;
    
    /** The SSLHelper Util instance. */
    private SSLHelper mSSLHelper;
    
    /** The DeploymentListenerImpl. */
    private DeploymentListenerImpl mDeploymentListener;
    
    /** The SecurityHandlers Handle to the SecurityService. */
    private SecurityService mSecSvc;

    /**
     * This method creates and initializes the SecurityHandler with the
     * SecurityConfiguration and the ComponentContext.
     *
     * @param secConfig is the installation SecurityConfiguration
     * @param cmpCtx is the Component Context
     * @param envCtx is the Security Service Environment Context
     * @param authLayer is the authentication layer ex. SOAP.
     * @param secSvc is a handle to the Security Service that created this instance.
     * @throws IllegalStateException if the SecurityHandler cannot be initialized
     * successfully.
     * 
     */
    protected SecurityHandlerImpl(SecurityConfiguration secConfig, 
        ComponentContext cmpCtx, EnvironmentContext envCtx, SecurityService secSvc,
        String authLayer) 
        throws IllegalStateException
    {
        initialize(secConfig, cmpCtx, envCtx, secSvc, authLayer);
    }
    
    /**
     * This method initializes the SecurityHandler with the
     * SecurityConfiguration and the ComponentContext.
     *
     * @param secConfig is the installation SecurityConfiguration
     * @param cmpCtx is the ComponentContext
     * @param envCtx is the SecurityService EnvironmentContext.
     * @param secSvc is a handle to the Security Service that created this instance.
     * @param authLayer is the authentication layer ex. SOAP.
     * @throws IllegalStateException if the SecurityHandler cannot be initialized
     * successfully.
     * 
     */
    public void initialize(SecurityConfiguration secConfig, ComponentContext cmpCtx,
        EnvironmentContext envCtx, SecurityService secSvc, String authLayer)
        throws IllegalStateException
    {
        mSecSvc = secSvc;
        mEnvCtx = new BindingComponentContext(cmpCtx, envCtx);
        sLogger = Logger.getLogger(com.sun.jbi.internal.security.Constants.PACKAGE);

        mSSLHelper = new SSLHelper();
        mInstallSecConfig = secConfig;
        mSecurityProcessor = 
            new com.sun.jbi.internal.security.msg.SecurityProcessor(mEnvCtx, authLayer);
        mDeploymentListener = new DeploymentListenerImpl(secSvc.getSchemaDir());
        mDeploymentListener.addDeploymentObserver(mSSLHelper);
        mDeploymentListener.addDeploymentObserver(mSecurityProcessor);
        
        initStringTranslator();
        sLogger.info(mTranslator.getString(
            LocalStringConstants.BC_INFO_SECHANDLER_INITIALIZED, 
            getComponentName()));
    }
   
    /**
     * Initialize the String translator
     *
     */
    private void initStringTranslator()
    {
        mTranslator = mEnvCtx.getStringTranslator(Constants.PACKAGE);
    }
    
    /**
     * Get an Instance of the Deployment Listener.
     *
     * @return an Instance of the Deployment Listener
     */
    public DeploymentListener getDeploymentListener()
    {
        return mDeploymentListener;
    }

    /**
     * Get an Instance of the Interceptor.
     *
     * @return an Instance of the Deployment Listener
     */
    public Interceptor getInterceptor()
    {
        return mSecurityProcessor;
    }
    
    /**
     *
     * Get the HttpSecurityHandler.
     *
     * @return a new Instance of the HttpSecurityHandler
     */
    public com.sun.jbi.binding.security.HttpSecurityHandler getHttpSecurityHandler()
    {
        return new com.sun.jbi.internal.security.https.HttpSecurityHandler(mSSLHelper);
    }
     
    /**
     * Get the ComponentContext of the Component associated with the
     * handler. The Environment Context is useful in getting the ComponentId and 
     * the StringTranslator
     *
     * @return the ComponentContext instance.
     */
    public com.sun.jbi.internal.security.ComponentContext getComponentContext ()
    {
        return mEnvCtx;
    }
    
    /** 
     * Get the Name of the Component associated with the handler.
     *
     * @return the String Component Id.
     */   
    public String getComponentName()
    {
        return mEnvCtx.getComponentName();
    }
    
}
