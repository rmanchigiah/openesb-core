/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointInfoImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.config;


import java.net.URI;

/**
 * Endpoint interface provides a means to define the three attributes that
 * uniquely identify an endpoint.
 * TODO: Replace this with Endpoint ( do away with redundancy ).
 *
 * @author Sun Microsystems, Inc.
 */
public class EndpointInfoImpl
    implements EndpointInfo
{
    /** Local Endpoint Name. */
    private String mName = null;
    
    /** Service Local Name. */
    private String mServiceName = null;
    
    /** Target Namespace. */
    private URI mTns = null;
    
    /** Component Id */
    private String mComponentId;
    
    /**
     * @param endpt is the Endpoint to extract the EndpointInfo from
     * @throws java.net.URISyntaxException if the Namespace URI is in the incorrect 
     * format.
     */
    public EndpointInfoImpl(com.sun.jbi.binding.security.Endpoint endpt)
        throws java.net.URISyntaxException
    {
        mName = endpt.getEndpointName();
        
        mServiceName = endpt.getServiceName().getLocalPart();
     
        mTns = new java.net.URI( endpt.getServiceName().getNamespaceURI() );    
        
        mComponentId = endpt.getComponentId();
    }
    
    /** 
     * Creates a new instance of EndpointInfo.
     * 
     * @param name is the local name of the Endpoint
     * @param service is the local name of the parent Service
     * @param tns is the TargetNamespace for the Endpoint
     * @param compId is the name of the Component Id to which this Endpoint is deployed
     */
    public EndpointInfoImpl(String name, String service, URI tns, String compId)
    {
        
        this(name, service, tns);
        mComponentId = compId;
    }
    
    /** 
     * Creates a new instance of EndpointInfo.
     * 
     * @param name is the local name of the Endpoint
     * @param service is the local name of the parent Service
     * @param tns is the TargetNamespace for the Endpoint
     */
    public EndpointInfoImpl(String name, String service, URI tns)
    {
        mName = name;
        mServiceName = service;
        mTns = tns;
        mComponentId = "";
    }
    
    /**
     * Get the Local name of the endpoint.
     *
     * @return the Local Name of the Endpoint
     */
    public String getName()
    {
        return mName;
    }
    
    /**
     * Get the Local Name of the parent Service that contains this Endpoint.
     *
     * @return the local name of the Parenter Service
     */
    public String getServiceName()
    {
        return mServiceName;
    }
     
   /**
    * Get the targetNamespace URI for the Endpoint.
    * @return the targetNamespace URI for the Endpoint
    */
    public java.net.URI getTargetNamespace()
    {
        return mTns;
    }
    
    /**
     *
     * Get the Component Id associated with the Endpoint.
     *
     * @return the Component Id
     * @deprecated
     */
    public String getComponentId()
    {
        return mComponentId;
    }
    
    /**
     * 
     * @param obj to compare 
     * @return true if the other Endpoint is equal to this one.
     */
    public boolean equals(Object obj)
    {
        if ( obj  instanceof EndpointInfo)
        {
            EndpointInfo otherEndpoint = (EndpointInfo) obj;
            
            if ( getName().equals(otherEndpoint.getName()) &&
                getServiceName().equals(otherEndpoint.getServiceName()) &&
                getTargetNamespace().equals(otherEndpoint.getTargetNamespace()))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * hashCode.
     *
     * @return the hashcode
     */
    public int hashCode()
    {
        return ( getName().hashCode() + getServiceName().hashCode() 
            + getTargetNamespace().hashCode());
    }
    
}
