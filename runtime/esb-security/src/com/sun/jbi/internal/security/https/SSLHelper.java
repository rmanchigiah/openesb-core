/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SSLHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SSLHelper.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 11, 2004, 7:15 PM
 */

package com.sun.jbi.internal.security.https;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.security.Context;
import com.sun.jbi.binding.security.Endpoint; 
import com.sun.jbi.internal.security.KeyStoreManager;
import com.sun.jbi.internal.security.ContextImpl;
import com.sun.jbi.internal.security.SecurityService;

import com.sun.jbi.internal.security.config.EndpointSecurityConfig;
import com.sun.jbi.internal.security.config.SecurityContext;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509KeyManager;

import java.util.logging.Logger;
import java.util.HashMap;
import java.util.Properties;

/**
 * This is a internal helper class which is created by the SecurityHandler. The 
 * The SecurityHandler creates this class during its initialization, this class gets
 * a handle to the SecurityHandler, which can be used to get the Installtime Security
 * configuration, UserManager, KeyStoreManager etc.
 *
 * This class is used by the HttpSecurityHandler for creation of the SSL Contexts. The 
 * DeploymentListener notifies this class of Endpoint deployments and Undeployments, the
 * SSL helper gets the deployment time Security Context from these deployments.
 *
 * @author Sun Microsystems, Inc.
 */
public class SSLHelper
    implements com.sun.jbi.internal.security.DeploymentObserver
{
    
    /** The Logger. */
    private Logger mLogger;
    
    /** The map which contains endpointKey -> TlsContexts. */
    private HashMap mEndptTlsCtxs;
    
    /** String Translator.
     */
    private StringTranslator mTranslator;
    
    /** 
     * Creates a new instance of SSLHelper. 
     *
     */
    public SSLHelper()
    {
        mTranslator = SecurityService.getStringTranslator(HttpConstants.PACKAGE);
        mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
        mEndptTlsCtxs = new HashMap();
    }
    
    
    /**
     * Add an endpoint deployment. The deployment time security configuration is 
     * combined with the install time security configuration and based on the result a
     * SSL Context is created for the endpoint. The endpoint deployment configuration
     * overrides the installtime security configuration.
     *
     *
     * @param endpoint is the Endpoint being deployed
     * @param epSecConfig is the Security Configuration for the deployed endpoint.
     * @throws Exception on errors
     */
    public void registerEndpointDeployment(Endpoint endpoint, 
        EndpointSecurityConfig epSecConfig)
        throws Exception
    {
        // -- Get the Security Context used by the endpoint
        SecurityContext deplSecCtx = epSecConfig.getSecurityContext();

        // -- System Transport Security Ctx
        Properties instCtx = SecurityService.getTransportSecurityContext(); 
        
        // -- Get the KeyStoreManager configured for the Endpoint
        KeyStoreManager ksMgr = SecurityService.getKeyStoreManager(
            deplSecCtx.getKeyStoreManagerName());
        
        
        if ( ksMgr == null )
        {
            mLogger.info(mTranslator.getString(HttpConstants.BC_INFO_KSMGR_USED,    
                endpoint.getEndpointName(), 
                SecurityService.getDefaultKeyStoreManagerName()));
            ksMgr = SecurityService.getDefaultKeyStoreManager();
        }
        else
        {
            mLogger.info(mTranslator.getString(HttpConstants.BC_INFO_KSMGR_USED,    
                endpoint.getEndpointName(), deplSecCtx.getKeyStoreManagerName()));
        }
        
        // -- Merge with the deplSecCtx
        TlsContext tlsCtx = createTlsContext(mergeContexts(instCtx, deplSecCtx), 
            ksMgr);
        
        // -- Add to registry
        mLogger.info(mTranslator.getString( HttpConstants.BC_INFO_CREATE_TLS_CTX,
            endpoint.getEndpointName(), 
            endpoint.getServiceName().getLocalPart (),
            endpoint.getServiceName().getNamespaceURI() ));
        mEndptTlsCtxs.put(getKey(endpoint),  tlsCtx);  
    }
    
    
    /**
     * Unregister an Endpoint deployment.
     *
     * @param endpoint is the Endpoint being undeployed
     */
    public void unregisterEndpointDeployment(Endpoint endpoint)
    {
        mEndptTlsCtxs.remove(getKey(endpoint));
    }
    
    /**
     * Merge the Contexts
     *
     * @param instCtx is the Installation time Security Context.
     * @param deplCtx is the Deployment time Security Context
     * @return the merged contexts, deployment context overrides install context. 
     */
    private Context mergeContexts(Properties instCtx, Context deplCtx)
    {
        Context newCtx = new ContextImpl(
            SecurityService.getStringTranslator(HttpConstants.MAIN_PACKAGE));
        
        newCtx.setValue (HttpConstants.PARAM_SSL_USE_DEFAULT, 
            new Boolean(
                getValue(HttpConstants.PARAM_SSL_USE_DEFAULT, instCtx, deplCtx, true)
                ));
        
        newCtx.setValue (HttpConstants.PARAM_SSL_PROTOCOL,
            getValue(HttpConstants.PARAM_SSL_PROTOCOL, instCtx, deplCtx,
                HttpConstants.DEFAULT_SSL_PROTOCOL) );   
         
        newCtx.setValue (HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH,
            new Boolean(
                getValue(HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH, instCtx, deplCtx,
                    HttpConstants.DEFAULT_SSL_REQ_CLIENT_AUTH) ));

        newCtx.setValue (HttpConstants.PARAM_SSL_CLIENT_ALIAS,
            getValue(HttpConstants.PARAM_SSL_CLIENT_ALIAS, instCtx, deplCtx,
                HttpConstants.DEFAULT_CLIENT_ALIAS) );

        newCtx.setValue (HttpConstants.PARAM_SSL_CLIENT_ALIAS_PASSWD,
            getValue(HttpConstants.PARAM_SSL_CLIENT_ALIAS_PASSWD, instCtx, deplCtx,
                HttpConstants.DEFAULT_CLIENT_ALIAS_PWD) );

        newCtx.print(mLogger, java.util.logging.Level.FINEST);
        return newCtx;
    }
    
    /**
     * Get the value for a particular key from the depl ctx, if not specified
     * then get it from the install ctx, if not specified use default.
     *
     * @param key is the Key whose value is required.
     * @param instCtx is the Installation time Security Context.
     * @param deplCtx is the Deployment time Security Context
     * @param defVal is the default value that applies if the key is not found
     * @return the required value for the key.
     */
    private boolean getValue(String key, Properties instCtx, Context deplCtx, 
        boolean defVal )
    {
        if ( deplCtx.containsKey(key) )
        {
            return new Boolean(
                (String) deplCtx.getValue(key)).booleanValue();
        }
        
        if ( instCtx.containsKey(key) )
        {
            return new Boolean(
                instCtx.getProperty(key)).booleanValue();
        }
        
        return defVal;
        
    }
    
    /**
     * Get the value for a particular key from the depl ctx, if not specified
     * then get it from the install ctx, if not specified use default.
     *
     * @param key is the Key whose value is required.
     * @param instCtx is the Installation time Security Context.
     * @param deplCtx is the Deployment time Security Context
     * @param defVal is the Default value that applies
     * @return the required value for the key.
     */
    private String getValue(String key, Properties instCtx, Context deplCtx, 
        String defVal )
    {
        if ( deplCtx.containsKey(key) )
        {
            return (String) deplCtx.getValue(key);
        }
        
        if ( instCtx.containsKey(key) )
        {
            return instCtx.getProperty(key);
        }
        
        return defVal;
        
    }
    
    /**
     * Get a Key based on the endpoint
     * @return the Key generated from the Endpoint. The Key is of the
     * form TargetNamespace:Service:Endpoint
     * @param endpt is the Endpoint
     */
    private String getKey (Endpoint endpt)
    {
        StringBuffer strBuffer = new StringBuffer();
        String separator = ":";
        if ( endpt != null )
        {
            strBuffer.append (endpt.getServiceName().getNamespaceURI());
            strBuffer.append (separator);
            strBuffer.append (endpt.getServiceName().getLocalPart().trim());
            strBuffer.append (separator);
            strBuffer.append (endpt.getEndpointName().trim());
        }

        return strBuffer.toString ();
    }
    
    /**
     * Create and initialize an SSL Context for the Endpoint
     * @return a TlsContext initialized with the appropriate SSLContext 
     * @param secCtx is the merged Security Context for the endpoint
     * @param ksMgr is the KeyStoreManager
     */
    private TlsContext createTlsContext (Context secCtx, KeyStoreManager ksMgr)
    {
        boolean useDefaultSSLCtx = 
            ( (Boolean) secCtx.getValue(HttpConstants.PARAM_SSL_USE_DEFAULT)).
                booleanValue();
        
        SSLContext sslc = null;
        if (!useDefaultSSLCtx) 
        {
            try
            {
                sslc = createSSLContext(secCtx, ksMgr);
            }
            catch (Exception ex)
            {
                // -- If any problem occurs log a warning and use the default
                mLogger.warning(mTranslator.getString(
                    HttpConstants.BC_ERR_SSL_CTX_FAIL_USE_DEFAULT, ex.toString()));
                // -- ex.printStackTrace();   
                sslc = null;
            }
        }       
        return new TlsContext(sslc, 
            ((Boolean) secCtx.getValue(HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH)).
                booleanValue(), ksMgr);
    }    
    
    
    /**
     * Create a SSL Context as required by the Endpoint
     *
     * @param ksMgr is the KeyStoreManager
     * @param secCtx is the Security Context to be used for creating the SSLContext.
     * @throws NoSuchAlgorithmException if algorithm incorrect
     * @throws KeyManagementException on key management problems
     * @throws KeyStoreException on KeyStore related errors.
     * @throws UnrecoverableKeyException if a key cannot be obtained from any
     * of the KeyStores.
     * @return the newly created SSLContext.
     */
    private SSLContext createSSLContext(Context secCtx, KeyStoreManager ksMgr)
        throws NoSuchAlgorithmException, 
            KeyManagementException, 
            KeyStoreException, 
            UnrecoverableKeyException
    {
        KeyManagerFactory kmf = null;
        TrustManagerFactory tmf = null;
        try
        {
            kmf = KeyManagerFactory.getInstance (
                HttpConstants.DEFAULT_KEY_MANAGEMENT_ALGO);
            tmf = TrustManagerFactory.getInstance(
                HttpConstants.DEFAULT_KEY_MANAGEMENT_ALGO);
        }
        catch (NoSuchAlgorithmException ex)
        {
            mLogger.severe (mTranslator.getString (
                HttpConstants.BC_ERR_KEYMGMT_ALGO_NOT_SUPPORTED,
                HttpConstants.DEFAULT_KEY_MANAGEMENT_ALGO));
            throw ex;
        }
        SSLContext sslc = null;    
        try
        {
            
            tmf.init (getTrustStore(ksMgr));

            if ( HttpConstants.DEFAULT_CLIENT_ALIAS.equals(
                secCtx.getValue(HttpConstants.PARAM_SSL_CLIENT_ALIAS)) )
            {
                // -- The Client SSL socket uses any client key pair for Client auth.
                
                // -- If the Key Store Manager is directly hooked into the App Srv. Env
                // -- use the default SSL context.
                if ( ksMgr.getType() == null ? KeyStoreManager.SJSAS == null : ksMgr.getType().equals(KeyStoreManager.SJSAS) )
                {
                    return sslc;
                }
                else // -- Create a new SSL Context and init it with the key/trust info
                {
                    sslc = SSLContext.getInstance((String) secCtx.getValue(
                        HttpConstants.PARAM_SSL_PROTOCOL));
                    kmf.init (ksMgr.getKeyStore (), 
                        ksMgr.getKeyStorePassword().toCharArray ());
                    sslc.init (kmf.getKeyManagers(), tmf.getTrustManagers (), null);
                }
            }
            else
            {
                String clientAlias = 
                    (String) secCtx.getValue(HttpConstants.PARAM_SSL_CLIENT_ALIAS);
                mLogger.fine(mTranslator.getString(
                    HttpConstants.BC_INFO_CLIENT_AUTH_DETAILS, clientAlias, 
                        ksMgr.getName(), ksMgr.getType()));
                sslc = SSLContext.getInstance((String) secCtx.getValue(
                    HttpConstants.PARAM_SSL_PROTOCOL));
                X509KeyManager[] keyManagers = 
                {new SSLClientKeyManager(ksMgr, clientAlias, mTranslator), };
                sslc.init (keyManagers, tmf.getTrustManagers(), null);
            }
        }
        catch (KeyStoreException ksex)
        {
            mLogger.warning (mTranslator.getString (
                HttpConstants.BC_ERR_SSL_CONTEXT_CREATION_FAILED,
                ksex.toString ()));
            throw ksex;
        }
        catch (KeyManagementException kmex)
        {
            mLogger.warning (mTranslator.getString (
                HttpConstants.BC_ERR_SSL_CONTEXT_CREATION_FAILED,
                kmex.toString ()));
            throw kmex;
        }
        
        return sslc;   
    }
    
    /**
     * Get the TlsContext for a Endpoint.
     *
     * @param endpoint is the Endpoint whose TLS Context is to be obtained.
     * @return a TlsContext for an Endpoint, returns null if the Endpoint has not been
     * registered.
     */
    public TlsContext getTlsContext(Endpoint endpoint)
    {
        return (TlsContext) mEndptTlsCtxs.get(getKey(endpoint));
    }
    
    /**
     * Get the TrustStore for initializing the TrustStoreManager.
     *
     * @param ksMgr is the KeyStoreManager
     * @return the TrustStore handle.
     * @throws KeyStoreException if the truststore cannot be obtained.
     */
    private java.security.KeyStore getTrustStore(KeyStoreManager ksMgr)
        throws KeyStoreException
    {
        try
        {
            // -- Create a TrustStore Callback
            // -- Ask the ksMgr to handle it
            com.sun.enterprise.security.jauth.callback.TrustStoreCallback tsCB =
                new com.sun.enterprise.security.jauth.callback.TrustStoreCallback();
            ksMgr.handle( new javax.security.auth.callback.Callback[] {tsCB} );

            // -- Get back the Trust Store and return it
            return tsCB.getTrustStore();
            
        }
        catch ( Exception ex )
        {
            throw new KeyStoreException(ex.toString());
        }     
    }
   
}
