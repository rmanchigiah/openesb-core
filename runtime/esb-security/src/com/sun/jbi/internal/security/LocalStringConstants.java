/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  LocalStringConstants.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 1, 2004, 4:44 PM
 */

package com.sun.jbi.internal.security;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringConstants
{
    /*-------------------------------------------------------------------------------*\
     *                   Constant Substrings appearing in  Messages                  *
    \*-------------------------------------------------------------------------------*/
    
    /** Context. */
    String CONST_CONTEXT = "CONST_CONTEXT";
    
    /** Parameter. */
    String CONST_PARAM = "CONST_PARAM";
    
    /** Value. */
    String CONST_VALUE = "CONST_VALUE";
    
    /** Endpoint Context. */
    String CONST_ENDPT_CONTEXT = "CONST_ENDPT_CONTEXT";
    
    /** User Domain. */
    String CONST_UD = "CONST_UD";
    
    /** KeyStore Manager. */
    String CONST_KM = "CONST_KM";
    
    /** Security Context. */
    String CONST_SC = "CONST_SC";
    
    /** Security Context Name. */
    String CONST_SEC_CTX_NAME = "CONST_SEC_CTX_NAME";

    /*-------------------------------------------------------------------------------*\
     *                          INFORMATIONAL Messages                               *
    \*-------------------------------------------------------------------------------*/
    /** INFO: Security Handler Init msg. */
    String BC_INFO_SECHANDLER_INITIALIZED = 
        "BC_INFO_SECHANDLER_INITIALIZED";
    
    /** INFO: Initializing Key Store Manager. */
    String BC_INFO_INITIALIZING_KM = "BC_INFO_INITIALIZING_KM";
    
    /** INFO: Registered KeyStore Manager. */
    String BC_INFO_UD_REGISTERED = "BC_INFO_UD_REGISTERED";
    
    /** INFO: Registered the User Domain. */
    String BC_INFO_KM_REGISTERED = "BC_INFO_KM_REGISTERED";
    
    /** INFO: Information on the Endpoint Security Configuration file. */
    String BC_INFO_SEC_CONFIG_FILE = "BC_INFO_SEC_CONFIG_FILE";
    
    /** INFO: Operation not configured for security. */
    String BC_INFO_OPERATION_MISSING_SECURITY_CONFIGURATION = 
        "BC_INFO_OPERATION_MISSING_SECURITY_CONFIGURATION";
    
    /** INFO: Endpoint not configured for security. */
    String BC_INFO_ENDPOINT_MISSING_SECURITY_CONFIGURATION = 
        "BC_INFO_ENDPOINT_MISSING_SECURITY_CONFIGURATION";
    
    /** INFO: resolved external entity ref. */
    String  BC_INFO_RESOLVED_EXTERNAL_ENTITY = 
        "BC_INFO_RESOLVED_EXTERNAL_ENTITY";
    
     /** INFO: Endpoint Operation policy not defined in the jbi deployment. */
    String  BC_INFO_NO_EP_OP_SEC_POLICY =
        "BC_INFO_NO_EP_OP_SEC_POLICY";
    
    /** INFO: Endpoint has security overrides. */
    String BC_INFO_ENDPT_SEC_SPEC = "BC_INFO_ENDPT_SEC_SPEC";
    
    /** INFO: Security Service Initialized Successfully. */
    String SS_INFO_INIT = "SS_INFO_INIT";
        
    /** INFO: Security Service Started Successfully. */
    String SS_INFO_START = "SS_INFO_START";
    
    /** INFO: Security Service Stopped Successfully. */
    String SS_INFO_STOP = "SS_INFO_STOP";
    
    /*-------------------------------------------------------------------------------*\
     *                          ERROR Messages                                       *
    \*-------------------------------------------------------------------------------*/
    /** Error: Default UD Creation failed. */
    String BC_ERR_DEFAULT_UD_CREATION_FAILED =
        "BC_ERR_DEFAULT_UD_CREATION_FAILED";
    
    /** Error: Default KeyStoreManager Creation failed. */
    String BC_ERR_DEFAULT_KM_CREATION_FAILED =
        "BC_ERR_DEFAULT_KM_CREATION_FAILED";

    
    /** ERR: User Domain missing parameter. */
    String BC_ERR_UD_MISSING_PROPERTY = "BC_ERR_UD_MISSING_PROPERTY";
    
    /** ERR: UserDomain Creation failed. */
    String BC_ERR_UD_CREATION_FAILED = "BC_ERR_UD_CREATION_FAILED";
    
    /** ERR: User Domain Initialization failed. */
    String BC_ERR_UD_INITIALIZATION_FAILED = 
        "BC_ERR_UD_INITIALIZATION_FAILED";
    
    /** ERR: Invalid User Domain Instance. */
    String BC_ERR_UD_CLASS_INVALID = "BC_ERR_UD_CLASS_INVALID";
    
    
    /** ERR: No KMgrs Specified. */
    String BC_ERR_NO_KM = "BC_ERR_NO_KM";
    
    /** ERR: Creation of KeyStore Manager failed. */
    String BC_ERR_KM_CREATION_FAILED = "BC_ERR_KM_CREATION_FAILED";
    
    /** ERR: Initialization of KeyStore Manager failed. */
    String BC_ERR_KM_INITIALIZATION_FAILED = 
        "BC_ERR_KM_INITIALIZATION_FAILED";
    
    /** ERR: Invalid KeyStore Manager Instance. */
    String BC_ERR_KM_CLASS_INVALID = "BC_ERR_KM_CLASS_INVALID";
    
     /** ERR: Key Store Manager missing parameter. */
    String BC_ERR_KM_MISSING_PROPERTY = "BC_ERR_KM_MISSING_PROPERTY";
    
    /** ERR: Invalid Namespace URI. */
    String BC_ERR_INVALID_NS_URI = "BC_ERR_INVALID_NS_URI";
    
    /** ERR: A Message Handler creation failed. */
    String BC_ERR_MSGHANDLER_CREATION_FAILED = 
        "BC_ERR_MSGHANDLER_CREATION_FAILED";
    
    /** ERR: In reading the deployment Security Configuration. */
    String BC_ERR_READING_DEPLOYMENT_CONFIG = 
        "BC_ERR_READING_DEPLOYMENT_CONFIG";
    
    /** ERR: Error reading XWSS Config. */
    String BC_ERR_READING_XWSS_SEC_CONFIG = 
        "BC_ERR_READING_XWSS_SEC_CONFIG";
    
    /** ERR: SAX Parsing error. */
    String BC_ERR_SAX_PARSING_EX = "BC_ERR_SAX_PARSING_EX";

    /** ERR: Missing element. */
    String BC_ERR_MISSING_CONFIG_ELEMENT = 
        "BC_ERR_MISSING_CONFIG_ELEMENT";
    
    /** ERR: Defualt Element Missing from Security Service Configuration. */
    String  BC_ERR_DEFAULT_CONFIG_ELEMENT_MISSING =
        "BC_ERR_DEFAULT_CONFIG_ELEMENT_MISSING";
    
    /** ERR: Error occured in processing ( securing/validating ) message. */
    String  ERR_PROCESSING_OUTGOING_MESSAGE = 
        "ERR_PROCESSING_OUTGOING_MESSAGE";
    
    /** ERR: Error occured in processing ( securing/validating ) message. */
    String  ERR_PROCESSING_INCOMING_MESSAGE = 
        "ERR_PROCESSING_INCOMING_MESSAGE";   
    
    /** ERR: The Endpoint deploy config and the deployed endpoint mismatch. */
    String  BC_ERR_INVALID_DEPL_CONFIG = 
        "BC_ERR_INVALID_DEPL_CONFIG";
    
    /*-------------------------------------------------------------------------------*\
     *                          WARNING Messages                                     *
    \*-------------------------------------------------------------------------------*/
    
    /** WRN: NO User Domains specified. */
    String BC_WRN_NO_UD = "BC_WRN_NO_UD";
    
    /** WRN: No message handler for outbound Endpoint/Operation. */
    String BC_WRN_NO_OUT_MSG_HNDLR = "BC_WRN_NO_OUT_MSG_HNDLR";
    
    /** WRN: No message handler for inbound Endpoint/Operation. */
    String BC_WRN_NO_IN_MSG_HNDLR = "BC_WRN_NO_IN_MSG_HNDLR";
    
    /** WRN: Endpoint is missing in deployment configuration. */
    String BC_WRN_ENDPT_MISSING = "BC_WRN_ENDPT_MISSING";
    
    /** WRN: Parameter could not be added to Security Configuration. */
    String BC_WRN_FAILED_TO_ADD_PARAM = "BC_WRN_FAILED_TO_ADD_PARAM";
}
