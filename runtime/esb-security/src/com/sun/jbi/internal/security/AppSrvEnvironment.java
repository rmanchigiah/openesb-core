/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AppSrvEnvironment.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  AppSrvEnvironment.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 24, 2005, 1:47 PM
 */

package com.sun.jbi.internal.security;

import javax.security.auth.callback.CallbackHandler;

/**
 * AppSrvEnvironment provides clients with a hook into the 
 * Application Server environment.
 *
 * @author Sun Microsystems, Inc.
 */
public final class AppSrvEnvironment
{
    /** The Callback Handler from the AS' Web/EJB Container. */
    private static CallbackHandler sEjbServletCBHndlr = null;
    
    /** Creates a new instance of AppSrvEnvironment */
    private AppSrvEnvironment ()
    {
    }
    
    /**
     * Get the CallbackHandler used by the EJB's and Servlets.
     *
     * @return the CallbackHandler instance.
     */
    public static CallbackHandler getCallbackHandler()
    {
        if ( sEjbServletCBHndlr == null )
        {
            initCallbackHandler();
        }
        return sEjbServletCBHndlr;
    }
    
    /**
     * Initialize the Callback Handler 
     */
    private static void initCallbackHandler()
    {
        try
        {
            // -- This is a hack for now, we need to define a private contract and get 
            // -- a handle to the handler.
            Class clazz = Class.forName(
                "com.sun.enterprise.webservice.EjbServletWSSCallbackHandler");
            java.lang.reflect.Method niMtd = clazz.getDeclaredMethod("newInstance", null);

            if ( niMtd != null )
            {
                niMtd.setAccessible(true);
                sEjbServletCBHndlr =  (CallbackHandler) niMtd.invoke(null, null);

            }
        }
        catch ( Exception ex)
        {
            ex.printStackTrace();
        }
 
    } 
}
