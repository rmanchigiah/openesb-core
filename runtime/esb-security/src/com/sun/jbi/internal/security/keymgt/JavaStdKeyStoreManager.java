/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JavaStdKeyStoreManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  JavaStdKeyStoreManager.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on October 24, 2004, 12:35 AM
 */

package com.sun.jbi.internal.security.keymgt;

import com.sun.enterprise.security.jauth.callback.*;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.internal.security.Constants;

import java.security.cert.CertStore;
import java.security.cert.CollectionCertStoreParameters;
import java.security.KeyStore;
import java.util.Properties;
import java.util.logging.Logger;
import java.io.FileInputStream;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class JavaStdKeyStoreManager
    implements com.sun.jbi.internal.security.KeyStoreManager
{
    /** The KeyStore. */
    private KeyStore mKeyStore;
    
    /** The KeyStore Passphrase. */
    private String mKeyStorePass;
    
    /** The TrustStore Passphrase. */
    private String mTrustStorePass;
    
    /** The TrustStore. */
    private KeyStore mTrustStore;
    
    /** The Certificate Store. */
    private CertStore mCertStore;
    
    /** The context. */
    private Properties mProps;
    
    /** KeyStoreManager Name. */
    private String mName;
    
    /** The StringTranslator. */
    private StringTranslator mTranslator;
    
    /** Key Store Init flag. */
    private boolean mIsKeyStoreInitialized;
    
    /** Trust Store Init flag. */
    private boolean mIsTrustStoreInitialized;
    
    /** Cert Store Init flag. */
    private boolean mIsCertStoreInitialized;
    
    /** Type. */
    private String mType;
    
    /** Constructor. */
    public JavaStdKeyStoreManager()
    {
        Logger mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
        mIsCertStoreInitialized = false;
        mIsTrustStoreInitialized = false;
        mIsKeyStoreInitialized = false;
    }
    
    
    /**
     * Get the Type String. 
     * In Shasta 1.0 we support "JavaStandard" which encompasses : JKS, PKCS12, JCEKS
     * and "SJSAS" which implies the Application Server environment.
     *
     * @return manager type
     */
    public String getType()
    {
        return mType;
    }
    
    /**
     * Set the type string.
     *
     * @param type is the type.
     */
    public void setType(String type)
    {
        mType = type;
    }
    
    /**
     * Get the name of the KeyStoreManager.
     * @return the name of KeyStoreManager
     */
    public String getName()
    {
        return mName;
    }
    
    /**
     * Set the User KeyStoreManager.
     * @param name is the name of the KeyStoreManager
     */
    public void setName(String name)
    {
        mName = name;
    }
    
    /**
     * Set the StringTranslator on the Plugin. The StringTranslator
     * is created for the package the plugin belongs to.
     *
     * @param translator is the StringTranslator to use.
     */
    public void setStringTranslator(StringTranslator translator)
    {
        mTranslator = translator;
    }
    
    /**
     * @return a KeyStore instance. This KeyStore has the Private Keys.
     */
    public KeyStore getKeyStore()
    {
        if ( !mIsKeyStoreInitialized )
        {
            initializeKeyStore(mProps);
        }
        return mKeyStore;
    }
    
    /**
     * @return a TrustStore instance. This KeyStore has the Public Key Certificates and
     * Trusted CA Certificates.
     */
    public KeyStore getTrustStore()
    {
        if ( !mIsTrustStoreInitialized )
        {
            initializeTrustStore(mProps);
        }
        return mTrustStore;
    }
    
    
    /**
     * @return the password required for accessing the Trust Store
     */
    /*public String geyTrustStorePassword()
    {
        return mTrustStorePass;
    }*/
    
    /**
     * Initialize this KeyStore Service.
     *
     * @param initProperties are the Properties specified for the Service
     * @throws IllegalStateException if the domain cannot be initialized
     */
    public void initialize(Properties initProperties)
        throws IllegalStateException
    {
        mProps = initProperties;
    }
    
    /**
     * Initialize the Keystore.
     *
     * @param props the Properties which are required for initialization. This 
     * particualr instance of the service looks for the properties keystore.url, 
     * keystore.type and keystore.password
     */
    public synchronized void initializeKeyStore(Properties props)
    {
        // -- This is to take care of the condition where there
        // -- are two threads which check if key store is initialized,
        // -- one thread would then initialize the store, then the other
        // -- would attempt to initialize as well, this check avoids re-initialization.
        // -- The init status check could be synchronized too but that would slow things.
        if ( mIsKeyStoreInitialized )
        {
            return;
        }
        String keyStoreURL = (String) 
            props.getProperty(Constants.PARAM_KEYSTORE_LOCATION);
        String keyStoreType = (String) 
            props.getProperty(Constants.PARAM_KEYSTORE_TYPE);
        String keyStorePass = (String) 
            props.getProperty(Constants.PARAM_KEYSTORE_PASS);
        
        if ( (keyStoreURL == null) || (keyStoreType == null) || (keyStorePass == null) )
        {
            String missingParam = 
                new String (Constants.PARAM_KEYSTORE_LOCATION +  " / " +
                        Constants.PARAM_KEYSTORE_TYPE + " / " +
                        Constants.PARAM_KEYSTORE_PASS);
            String errMsg = null;
            if ( mTranslator != null )
            {
                errMsg = mTranslator.getString(
                    LocalStringConstants.BC_ERR_KS_MISSING_PARAM, 
                    missingParam,
                    getName());
            }
            else
            {
                errMsg = "One of the parameters " + missingParam 
                    + " required to initialize the KeyStore for JavaStdKeyStoreManager " 
                    + getName() + " is missing, JavaStdKeyStoreManager " +
                    "is in invalid state.";
            }
            throw new IllegalStateException(errMsg);
        }

        try 
        {
            mKeyStore = KeyStore.getInstance(keyStoreType);
            mKeyStore.load(new FileInputStream(keyStoreURL), keyStorePass.toCharArray());
            mKeyStorePass = keyStorePass;
            mIsKeyStoreInitialized = true;
        } 
        catch (Exception ex) 
        {
            String errMsg = null;
            if ( mTranslator != null )
            {
                errMsg = mTranslator.getString(
                    LocalStringConstants.BC_ERR_KS_INIT_FAILED, 
                        getName(), ex.toString());
            }
            else
            {
                errMsg = "A problem occured in initializing the" +
                    " KeyStore for JavaStdKeyStoreManager " + getName() 
                    + " error details: " + ex.toString();
            }
            throw new IllegalStateException(errMsg);
        }
    }
    
    /**
     * Initialize the TrustStore.
     *
     * @param props the Properties which are required for initialization. This 
     * particualr instance of the service looks for the properties keystore.url, 
     * keystore.type and keystore.password
     */
    public synchronized void initializeTrustStore(Properties props)
    {
        // -- This is to take care of the condition where there
        // -- are two threads which check if cert store is initialized,
        // -- one thread would then initialize the store, then the other
        // -- would attempt to initialize as well, this check avoids re-initialization.
        // -- The init status cjeck could be synchronized too but that would slow things.
        if ( mIsTrustStoreInitialized )
        {
            return;
        }
        String trustStoreURL = (String) 
            props.getProperty(Constants.PARAM_TRUSTSTORE_LOCATION);
        String trustStoreType = (String) 
            props.getProperty(Constants.PARAM_TRUSTSTORE_TYPE);
        String trustStorePass = (String) 
            props.getProperty(Constants.PARAM_TRUSTSTORE_PASS);
        
        if ((trustStoreURL == null) || (trustStoreType == null) || 
            (trustStorePass == null))
        {
            String missingParam = 
                new String (Constants.PARAM_TRUSTSTORE_LOCATION +  " / " +
                        Constants.PARAM_TRUSTSTORE_TYPE + " / " +
                        Constants.PARAM_TRUSTSTORE_PASS);
            String errMsg = null;
            if ( mTranslator != null )
            {
                errMsg = mTranslator.getString(
                    LocalStringConstants.BC_ERR_TS_MISSING_PARAM, 
                    missingParam,
                    getName());
            }
            else
            {
                errMsg = "One of the parameters " + missingParam 
                    + " required to initialize the TrustStore for JavaStdKeyStoreManager" 
                    + " " + getName() 
                    + " is missing, JavaStdKeyStoreManager is in invalid state.";
            }
            throw new IllegalStateException(errMsg);
        }

        try 
        {
            mTrustStore = KeyStore.getInstance(trustStoreType);
            mTrustStore.load(new FileInputStream(trustStoreURL), 
                trustStorePass.toCharArray());
            mTrustStorePass = trustStorePass;
            mIsTrustStoreInitialized = true;
        } 
        catch (Exception ex) 
        {
            String errMsg = null;
            if ( mTranslator != null )
            {
                errMsg = mTranslator.getString(
                    LocalStringConstants.BC_ERR_TS_INIT_FAILED, 
                        getName(), ex.toString());
            }
            else
            {
                errMsg = "A problem occured in initializing the" +
                    " TrustStore for JavaStdKeyStoreManager " + getName() 
                    + " error details: " + ex.toString();
            }
            throw new IllegalStateException(errMsg);
        }
    }
    
    /**
     * Get the Certificate Store. This gets all the client certificates from the Trust
     * Store and returns the Certificate store.
     *
     * @return a CertificateStore instance, this store has the client certificates which
     * will be used in verifying a certification path.
     */
    public CertStore getCertStore()
    {
        if ( !mIsCertStoreInitialized )
        {
            initializeCertStore();
        }
        return mCertStore;
    }
    
    
    /**
     * Initialize Certificate Store.
     */
    private synchronized void initializeCertStore()
    {
        // -- This is to take care of the condition where there
        // -- are two threads which check if cert store is initialized,
        // -- one thread would then initialize the store, then the other
        // -- would attempt to initialize as well, this check avoids re-initialization.
        // -- The init status cjeck could be synchronized too but that would slow things.
        if ( mIsCertStoreInitialized )
        {
            return;
        }
        
        KeyStore ts = getTrustStore();
        java.util.List list = new java.util.ArrayList();
        try
        {
            java.util.Enumeration aliases = ts.aliases();
            while (aliases.hasMoreElements())
            {
                String alias = (String) aliases.nextElement();
                if ( ts.isCertificateEntry(alias) )
                {
                    list.add(ts.getCertificate(alias));
                }
            }
            
            // -- Create the CertStore
            mCertStore = CertStore.getInstance("Collection", 
                new CollectionCertStoreParameters(list));
            mIsCertStoreInitialized = true;
        }
        catch (Exception ex)
        {
            String errMsg = null;
            if ( mTranslator != null )
            {
                errMsg = mTranslator.getString(
                    LocalStringConstants.BC_ERR_CS_INIT_FAILED, 
                        getName(), ex.toString());
            }
            else
            {
                errMsg = "A problem occured in initializing the" +
                    " CertStore for JavaStdKeyStoreManager " + getName() 
                    + " error details: " + ex.toString();
            }
            throw new IllegalStateException(errMsg);
        }
    }
    /**
     * Reload the Key / Trust Stores.
     *
     * @throws IllegalStateException if the information cannot be reloaded
     */
    public void reload()
        throws IllegalStateException
    {
        initialize(mProps);
    }
    
    /**
     * The implementation on the CallbackHandlerInterface. This class supports
     * CertStoreCallback, PrivateKeyCallback, SecretKeyCallback & TrustStoreCallback
     *
     * @param callbacks - array of Callbacks to be handled.
     * @throws java.io.IOException - if an input or output error occurs. 
     * @throws UnsupportedCallbackException - if the implementation of this method
     * does not support one or more of the Callbacks specified in the callbacks 
     * parameter.
     */
    public void handle(Callback[] callbacks)
        throws java.io.IOException, UnsupportedCallbackException
    {
        for (int i = 0; i < callbacks.length; i++) 
        {
            CallbackHandler handler = null;
            
            if  (callbacks[i] instanceof CertStoreCallback) 
            {
                handler = 
                    new com.sun.jbi.internal.security.callback.
                        CertStoreCallbackHandler(this);
            }
            else if (callbacks[i] instanceof PrivateKeyCallback)  
            {
                handler = 
                    new com.sun.jbi.internal.security.callback.
                        PrivateKeyCallbackHandler(this);
            }
            else if (callbacks[i] instanceof SecretKeyCallback)
            {
                handler = 
                    new com.sun.jbi.internal.security.callback.
                        SecretKeyCallbackHandler(this);
            }
            else if (callbacks[i] instanceof TrustStoreCallback) 
            {
                handler = 
                    new com.sun.jbi.internal.security.callback.
                        TrustStoreCallbackHandler(this);
            } 
            else
            {
                throw new UnsupportedCallbackException(callbacks[i]);
            }
            
            // -- Handle the request
            handler.handle(new Callback[] {callbacks[i]});
        }
    }
    
    /**
     * @return the password required for accessing the KeyStore
     */
    public String getKeyStorePassword ()
    {
        return mKeyStorePass;
    }    
    
    

    
}
