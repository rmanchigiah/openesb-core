/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)X500PrincipalWrapper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  X500PrincipalWrapper.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 11, 2005, 3:12 PM
 */

package com.sun.jbi.security;

import javax.security.auth.x500.X500Principal;

/**
 * This is a wrapper around X500 Principal, since X500 Principal cannot be extended.
 *
 * @author Sun Microsystems, Inc.
 */
class X500PrincipalWrapper 
    implements java.security.Principal
{
    
    /**
     * The actual X500 Principal Implementation.
     */
    protected X500Principal mX500Principal;
    
    /**
     *
     * Creates a new instance of SSLClientPrincipal
     *
     * @param name an X.500 distinguished name in RFC 1779 or RFC 2253 format.
     * @see javax.security.auth.x500.X500Principal
     */
    public X500PrincipalWrapper (String name)
    {
        mX500Principal = new X500Principal (name);
    }
    
    /**
     * Creates a new instance of SSLClientPrincipal
     *
     * @param name a byte array containing the distinguished name in ASN.1 DER
     * encoded form.
     * @see javax.security.auth.x500.X500Principal
     */
    public X500PrincipalWrapper (byte[] name)
    {
        mX500Principal = new X500Principal (name);
    }
    
    /**
     * Creates a new instance of SSLClientPrincipal
     *
     * @param fis an InputStream containing the distinguished name in ASN.1 DER
     * encoded form.
     *
     * @see javax.security.auth.x500.X500Principal
     */
    public X500PrincipalWrapper (java.io.FileInputStream fis)
    {
        mX500Principal = new X500Principal (fis);
    }
    
    /**
     * Creates a new instance of SSLClientPrincipal
     *
     * @param x500Principal is the X500Principal this class
     * encapsulates.
     *
     * @see javax.security.auth.x500.X500Principal
     */
    public X500PrincipalWrapper (X500Principal x500Principal)
    {
        mX500Principal = x500Principal;
    }
    
    /**
     *
     * @return a string representation of the X.500 distinguished name
     * using the format defined in RFC 2253.
     */
    public String getName ()
    {
        return mX500Principal.getName ();
    }
    
    /**
     * Returns a string representation of the X.500 distinguished name using the
     * specified format. The acceptable formats are
     * javax.security.auth.x500.X500Principal.CANONICAL,
     * javax.security.auth.x500.X500Principal.RFC2253 ans
     * javax.security.auth.x500.X500Principal.RFC1779.
     *
     * @param format the naming format.
     * @return a string representation of the X.500 distinguished name using the
     * specified format.
     * @see javax.security.auth.x500.X500Principal.getName(String format)
     */
    public String getName (String format)
    {
        return mX500Principal.getName (format);
    }
    
    /**
     * Returns the distinguished name in ASN.1 DER encoded form. The ASN.1 notation for
     * this structure is supplied in the documentation for X500Principal(byte[] name).
     *
     * @return the distinguished name in ASN.1 DER encoded form
     */
    public byte[] getEncoded ()
    {
        return mX500Principal.getEncoded ();
    }
    
    /**
     * Return a hash code for this X500Principal.
     *
     * The hash code is calculated via: getName(X500Principal.CANONICAL).hashCode()
     *
     * @return a hash code for this X500Principal.
     */
    public int hashCode ()
    {
        return mX500Principal.getName (X500Principal.CANONICAL).hashCode ();
    }
    
    /**
     * Compares the specified Object with this X500Principal for equality.
     * Specifically, this method returns true if the Object o is an X500Principal
     * and if the respective canonical string representations (obtained via the
     * getName(X500Principal.CANONICAL) method) of this object and o are equal.
     *
     * @param o obj to check for comparison
     * @return true if the specified Object is equal to this X500Principal,
     * false otherwise
     */
    public boolean equals (Object o)
    {
        if ( o == null )
        {
            return false;
        }
        
        if ( o instanceof SSLClientPrincipal)
        {
            return getName (X500Principal.CANONICAL).equals (
                ( (SSLClientPrincipal) o).getName (X500Principal.CANONICAL));
        }
        
        return false;
    }
    
}
