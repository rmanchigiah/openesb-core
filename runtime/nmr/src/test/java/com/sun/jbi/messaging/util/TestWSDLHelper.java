/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestWSDLHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging.util;

import com.sun.jbi.messaging.WsdlDocument;

import java.util.HashMap;
import javax.xml.namespace.QName;

import org.w3c.dom.Document;

/**
 * Tests for the TestWSDLHelper class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestWSDLHelper extends junit.framework.TestCase
{
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestWSDLHelper(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
        
    }

// =============================  test methods ================================

    /**
     *  Test single operation case for service
     */
    public void testGetOperationForServiceSingle()
        throws Exception
    {
        Document desc;
        HashMap  map;
        
        desc = WsdlDocument.readDefaultDocument();
        map  = WSDLHelper.getOperationsForService(desc, WsdlDocument.STOCK_SERVICE_Q);
        
        // only one operation for this service
        assertTrue(map.size() == 1);
        // make sure the operation name is correct
        assertTrue(map.containsKey(WsdlDocument.STOCK_OPERATION_Q.toString()));
    }
    
    /**
     *  Test multiple operation case for service
     */
    public void testGetOperationsForServiceMultiple()
        throws Exception
    {
        Document desc;
        HashMap  map;
        
        desc = WsdlDocument.readDefaultDocument();
        map  = WSDLHelper.getOperationsForService(desc, WsdlDocument.HELLO_SERVICE_Q);
        
        // three operation for this service
        assertTrue(map.size() == 3);
        // make sure the operation name is correct
        assertTrue(map.containsKey(WsdlDocument.HELLO_C_OPERATION_1_Q.toString()));
        assertTrue(map.containsKey(WsdlDocument.HELLO_W_OPERATION_Q.toString()));
    }
    
    /**
     *  Test single operation case for service
     */
    public void testGetInterfacesForServiceSingle()
        throws Exception
    {
        Document desc;
        QName[]  interfaces;
        
        desc = WsdlDocument.readDefaultDocument();
        interfaces = WSDLHelper.getInterfacesForService(desc, WsdlDocument.STOCK_SERVICE_Q);
        
        assertTrue(interfaces.length == 1);
        assertTrue(interfaces[0].equals(WsdlDocument.STOCK_INTERFACE_Q));
    }
    
    /**
     *  Test multiple operation case for service
     */
    public void testGetInterfacesForServiceMultiple()
        throws Exception
    {
        Document desc;
        QName[]  interfaces;
        
        desc = WsdlDocument.readDefaultDocument();
        interfaces = WSDLHelper.getInterfacesForService(desc, WsdlDocument.HELLO_SERVICE_Q);
        
        assertTrue(interfaces.length == 2);
    }   
}
