/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import com.sun.jbi.messaging.util.RouteType;
import com.sun.jbi.messaging.util.Translator;
import java.util.ArrayList;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import java.util.concurrent.ConcurrentHashMap;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.xml.namespace.QName;

import javax.jbi.servicedesc.ServiceEndpoint;

/** In-memory store of all endpoints that have been registered with the NMS.
 * @author Sun Microsystems, Inc.
 */
public class EndpointRegistry implements ConnectionManager, Interface2Manager
{
    public static final String DIRECT_ROUTE_TYPE="direct";
    public static final String INDIRECT_ROUTE_TYPE="indirect";
    public static final String REDIRECT_ROUTE_TYPE="redirect";
    
    public static final String LOCAL_MODE="local";
    public static final String DISTRIBUTED_MODE="distributed";
    
    /** Me. */
    private static EndpointRegistry                             mMe;
    private long                                                mTotalEndpoints;
    
    /** List of internal endpoints. */
    private ConcurrentHashMap<Endpoint, RegisteredEndpoint>     mInternalEndpoints;
    private ConcurrentHashMap<QName, VectorArray>               mIEbyService;
    private ConcurrentHashMap<QName, VectorArray>               mIEbyInterface;
    private ConcurrentHashMap<String, RegisteredEndpoint>       mInternalEndpointNames;
    private LinkedList<RegisteredEndpoint>                      mPendingInternalInterfaces;
    private long                                                mTotalInternalEndpoints;
    
    /** List of external endpoints. */
    private ConcurrentHashMap<Endpoint, RegisteredEndpoint>     mExternalEndpoints;
    private ConcurrentHashMap<QName, VectorArray>               mEEbyService;
    private ConcurrentHashMap<QName, VectorArray>               mEEbyInterface;
    private LinkedList<RegisteredEndpoint>                      mPendingExternalInterfaces;
    private long                                                mTotalExternalEndpoints;
    
    /** List of mapped endpoints. */
    private ConcurrentHashMap<Endpoint, LinkedEndpoint>         mLinkedEndpoints;
    private ConcurrentHashMap<String, RegisteredEndpoint>       mLinkedEndpointNames;
    private long                                                mTotalLinkedEndpoints;
      
    /** Map of interface (QName) to endpoint (Endpoint) connections. */
    private ConcurrentHashMap<QName, Endpoint>                  mInterfaceConnections;
    
    //added byDavidD
    /** Map endPoint -> nonConnectedConsumer */
    private ConcurrentHashMap<Endpoint, RegisteredEndpoint> mNonConnectedConsumers;
    /** Map service -> non-connected Consumer */
    private ConcurrentHashMap<QName, VectorArray> mNonConnectedConsumersbyService;
    /** Map of consumers -> route type */
    private ConcurrentHashMap<Endpoint, RouteType> mRouteTypes;
    /** Map of consumer -> routing rule */
    private ConcurrentHashMap<Endpoint, String> mRoutingRule;
    /** Map of consumer -> mode */
    private ConcurrentHashMap<Endpoint, String> mModes;
    /** Map consumer -> consumed interface */
    private ConcurrentHashMap<Endpoint, QName> mConsumedInterfaces;
    /** Map interfaces -> providers implementing the interface */
    private ConcurrentHashMap<QName, ArrayList<Endpoint>> mImplementedInterfaces;
    
    
    private MessageService                                      mMsgSvc;
    
    private Logger mLog = Logger.getLogger(this.getClass().getPackage().getName());
    
    /** No public access. */
    private EndpointRegistry()
    {
        mInternalEndpoints      = new ConcurrentHashMap();
        mIEbyService            = new ConcurrentHashMap();
        mIEbyInterface          = new ConcurrentHashMap();
        mInternalEndpointNames  = new ConcurrentHashMap();
        mPendingInternalInterfaces = new LinkedList();
        mExternalEndpoints      = new ConcurrentHashMap();
        mEEbyService            = new ConcurrentHashMap();
        mEEbyInterface          = new ConcurrentHashMap();
        mPendingExternalInterfaces = new LinkedList();
        mLinkedEndpoints        = new ConcurrentHashMap();
        mLinkedEndpointNames  = new ConcurrentHashMap();
        mInterfaceConnections   = new ConcurrentHashMap();
        mNonConnectedConsumers = new ConcurrentHashMap();
        mNonConnectedConsumersbyService = new ConcurrentHashMap();
        mRouteTypes=new ConcurrentHashMap();
        mModes=new ConcurrentHashMap();
        mRoutingRule=new ConcurrentHashMap();
        mConsumedInterfaces=new ConcurrentHashMap();      
        mImplementedInterfaces=new ConcurrentHashMap();   
    }
    
    /** Get an instance of the registry. */
    static final synchronized EndpointRegistry getInstance()
    {
        if (mMe == null)
        {
            mMe = new EndpointRegistry();
        }        
        return mMe;
    }
    
    synchronized void setMessageService(MessageService ms)
    {
        mMsgSvc = ms;
    }
    
    boolean statisticsEnabled()
    {
        return (mMsgSvc.areStatisticsEnabled());
    }
    
    void zeroStatistics()
    {
        for (RegisteredEndpoint re : mInternalEndpoints.values())
        {
            re.zeroStatistics();
        }
        for (LinkedEndpoint le : mLinkedEndpoints.values())
        {
            le.zeroStatistics();
        }
    }
    
    /**######################################################################
     * ####################### EXTERNAL ENDPOINTS ###########################
     * ####################################################################*/    
    
    public synchronized RegisteredEndpoint registerExternalEndpoint(ServiceEndpoint endpoint, String ownerId)
        throws javax.jbi.messaging.MessagingException
    {        
        RegisteredEndpoint re;
        Endpoint           ep = new Endpoint(endpoint.getServiceName(), endpoint.getEndpointName());
        
        re = (RegisteredEndpoint)mExternalEndpoints.get(ep);        
        if (re != null)
        {
            // Duplicate registrations are not permitted for external endpoints
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.DUPLICATE_ENDPOINT,
                        new Object[] {re.toExternalName(), re.getOwnerId()}));
        }
        
        re = new ExternalEndpoint(endpoint, ownerId);
        mEEbyService.put(endpoint.getServiceName(), new VectorArray(mEEbyService.get(endpoint.getServiceName()), re));
        mExternalEndpoints.put(ep, re);
        mPendingExternalInterfaces.add(re);
        mTotalEndpoints++;

        if (mLog.isLoggable(Level.FINE))
        {
            mLog.log(Level.FINE, "Registered External endpoint: {0}", re.toExternalName());
        }
        return re;
    }
    
    public RegisteredEndpoint getExternalEndpoint(QName service, String endpoint)
    {
        return ((RegisteredEndpoint)mExternalEndpoints.get(new Endpoint(service, endpoint)));        
    }
    
    public RegisteredEndpoint[] getExternalEndpointsForService(QName service)
    {
        VectorArray va = mEEbyService.get(service);
        return (va == null ? new RegisteredEndpoint[0] : va.mArray);
    }
    
    public RegisteredEndpoint[] getExternalEndpointsForInterface(QName interfaceName)
    {
        RegisteredEndpoint[]    endpoints;
        Endpoint                link;
        
        //
        //  Check if the caller wants all of the endpoints of just ones that implement the named interface.
        //
        if (interfaceName != null)
        {
            if ((link = (Endpoint)mInterfaceConnections.get(interfaceName)) != null)
            {
                RegisteredEndpoint  ep;

                ep = (RegisteredEndpoint)mExternalEndpoints.get(new Endpoint(link.getServiceName(), link.getEndpointName()));        

                if (ep == null)
                {
                     endpoints = new RegisteredEndpoint[0];
                }
                else
                {            
                    endpoints = new RegisteredEndpoint[] {ep};
                }
            }
            else
            {
                // no connection, search through all of the available endpoints
                endpoints = getEndpointsForInterface(
                    interfaceName, RegisteredEndpoint.EXTERNAL);
            }
        }
        else
        {
            //
            //  Don't allow updates so that the size() doesn't change concurrently.
            //'
            synchronized (this)
            {
                endpoints = new RegisteredEndpoint[mExternalEndpoints.size()];
                mExternalEndpoints.values().toArray(endpoints);
            }
        }
       
        return endpoints;
    }
        
    /**######################################################################
     * ####################### INTERNAL ENDPOINTS ###########################
     * ####################################################################*/
    
    public synchronized RegisteredEndpoint registerInternalEndpoint(
        QName service, String endpoint, String ownerId)
        throws javax.jbi.messaging.MessagingException
    {        
        RegisteredEndpoint re;
        Endpoint           ep = new Endpoint(service, endpoint);
        
        // check to see if it already exists
        re = (RegisteredEndpoint)mInternalEndpoints.get(ep);        
        
        if (re != null)
        {
            // Duplicate registrations are permitted if it's the same component.
            if (!re.getOwnerId().equals(ownerId))
            {
                throw new javax.jbi.messaging.MessagingException(
                     Translator.translate(LocalStringKeys.DUPLICATE_ENDPOINT,
                        new Object[] {re.toExternalName(), re.getOwnerId()}));
            }
            return re;
        }
        
        re = new InternalEndpoint(service, endpoint, ownerId);
        mIEbyService.put(service, new VectorArray(mIEbyService.get(service), re));
        mInternalEndpoints.put(ep, re);
        mPendingInternalInterfaces.add(re);
        mInternalEndpointNames.put(re.toExternalName(), re);
        mTotalEndpoints++;
        
        if (mLog.isLoggable(Level.FINE))
        {
            mLog.log(Level.FINE, "Registered Internal endpoint: {0}", re.toExternalName());
        }
        return re;
    }    
    
    /** Return a registered endpoint with the specified name, if one exists. */
    public RegisteredEndpoint getInternalEndpoint(QName service, String endpoint)
    {
        RegisteredEndpoint re;
        Endpoint           e = new Endpoint(service, endpoint);
        
        // check for a service connection first
        re = (RegisteredEndpoint)mLinkedEndpoints.get(e);
        
        if (re == null)
        {
            // no mapping, see if an internal endpoint exists
            re = (RegisteredEndpoint)mInternalEndpoints.get(e);
        }
        
        return re;
    } 
    
    /** Return a registered endpoint with the specified name, if one exists. */
    public RegisteredEndpoint getInternalEndpointByName(String epName)
    {
        return ((RegisteredEndpoint)mInternalEndpointNames.get(epName));
    }   
    
    /** Return a linked endpoint with the specified name, if one exists. */
    public RegisteredEndpoint getLinkedEndpointByName(String epName)
    {
        return ((RegisteredEndpoint)mLinkedEndpointNames.get(epName));
    }    
        
        
    /** Return a list of linked endpoint owned by the specified channel. */
    public String[]  getLinkedEndpointsByChannel(String dcName)
    {
        LinkedEndpoint[]        linkedEndpoints;
        LinkedList<String>      ll = new LinkedList();
        
        
        synchronized (this)
        {
            linkedEndpoints = new LinkedEndpoint[mLinkedEndpoints.size()];
            mLinkedEndpoints.values().toArray(linkedEndpoints);
        }
        for (LinkedEndpoint linkedEndpoint : linkedEndpoints) {
            if (linkedEndpoint.getOwnerId().equals(dcName)) {
                ll.add(linkedEndpoint.toExternalName());
            }
        }
        return (ll.toArray(new String[ll.size()]));
    }    
        
    /** Retrieves all internal endpoints, including linked endpoints established
     *  through service connections.
     *  @param service the service QName
     *  @param convertLinks true if a service connection link should be converted to 
     *   its corresponding internal (e.g. hard) endpoint.  False if the linked
     *   endpoint should be returned directly.
     */
    RegisteredEndpoint[] getInternalEndpointsForService(QName service, boolean convertLinks)
    {
        RegisteredEndpoint  endpoints[];
        boolean            cloned = false;
        
        //
        //  Check if the called wants all endpoints or just then ones that implemented the named service.
        //
        if (service != null)
        {
            VectorArray va = mIEbyService.get(service);
            endpoints =  va == null ? new RegisteredEndpoint[0] : va.mArray;
        }
        else
        {
            //
            //  Don't allow updates so that the size() doesn't change concurrently.
            //
            synchronized (this)
            {
                endpoints = new RegisteredEndpoint[mInternalEndpoints.size()];
                mInternalEndpoints.values().toArray(endpoints);
            }
        }
        
        if (convertLinks)
        {          
            for (int i = 0; i < endpoints.length; i++)
            {    
                LinkedEndpoint      le;
                RegisteredEndpoint  re = endpoints[i];
                
                //
                //  All linked endpoints appear at the front of the list.
                //
                if (re instanceof LinkedEndpoint)
                {
                    //
                    //  If we find a Linked endpoint, clone the array since we need to
                    //  change the contents.
                    //
                    if (!cloned)
                    {
                        endpoints = endpoints.clone();
                        cloned = true;
                    }

                    le = (LinkedEndpoint)re;
                    re = (RegisteredEndpoint)mInternalEndpoints.get(new Endpoint(le.getServiceLink(), le.getEndpointLink()));
                    if (re == null)
                    {
                         continue;
                    }
                    endpoints[i] = re;
                    continue;
                }
                break;
            }
        }
        
        return (endpoints);
    }
    
    /** Find internal endpoints that implement the specified interface.  If
     *  a service connection exists for the interface, that connection is 
     *  used exclusive of any other services provided in JBI.
     */
    public RegisteredEndpoint[] getInternalEndpointsForInterface(
        QName interfaceName)
    {
        RegisteredEndpoint[]    endpoints;
        Endpoint                link;
        
        //
        //  Check if the caller wants all of the endpoints of just ones that implement the named interface.
        //
        if (interfaceName != null)
        {
            if ((link = (Endpoint)mInterfaceConnections.get(interfaceName)) != null)
            {
                RegisteredEndpoint  ep;

                ep = (RegisteredEndpoint)mInternalEndpoints.get(new Endpoint(link.getServiceName(), link.getEndpointName()));        

                if (ep == null)
                {
                     endpoints = new RegisteredEndpoint[0];
                }
                else
                {            
                    endpoints = new RegisteredEndpoint[] {ep};
                }
            }
            else
            {
                // no connection, search through all of the available endpoints
                endpoints = getEndpointsForInterface(
                    interfaceName, RegisteredEndpoint.INTERNAL);
            }
        }
        else
        {
            //
            //  Don't allow updates so that the size() doesn't change concurrently.
            //'
            synchronized (this)
            {
                endpoints = new RegisteredEndpoint[mInternalEndpoints.size()];
                mInternalEndpoints.values().toArray(endpoints);
            }
        }
       
        return endpoints;
    }
    
    /**######################################################################
     * ########################## ALL ENDPOINTS #############################
     * ####################################################################*/
    
    /** Remove an endpoint reference from the registry.  Invoking this method
     *  will also set the reference to inactive status.
     */
    public synchronized void removeEndpoint(ServiceEndpoint ref)
    {
       RegisteredEndpoint re;

       if (ref instanceof RegisteredEndpoint)
       {
           re = (RegisteredEndpoint)ref;
       }
       else
       {
           re = (RegisteredEndpoint)mExternalEndpoints.get(
                   new Endpoint(ref.getServiceName(), ref.getEndpointName()));        
       }
       
       if (re != null && re.isActive())
       {
            re.setActive(false);

            if (re.isInternal())
            {
                removeInternalEndpoint(re);           
            }
            else if (re.isExternal())
            {
                removeExternalEndpoint(re);           
            }
            else if (re.isLinked())
            {
                removeLinkedEndpoint(re);           
            }
            if (mLog.isLoggable(Level.FINE))
            {
                mLog.log(Level.FINE, "Removed endpoint: {0}", re.toExternalName());
            }
        }
    }    
    
    
    /**######################################################################
     * ##################### CONNECTION MANAGEMENT ##########################
     * ####################################################################*/    
    
    public void addEndpointConnection(
        QName fromService, String fromEndpoint, 
        QName toService, String toEndpoint, Link linkType) 
        throws javax.jbi.messaging.MessagingException
    {
        if (addEndpointConnectionInternal(fromService, fromEndpoint,
                toService, toEndpoint, linkType))
        {
	    EndpointListener	el = mMsgSvc.getEndpointListener();

	    if (el != null)
	    {
	        el.addServiceConnection(fromService, fromEndpoint, toService, toEndpoint, linkType.toString());
	    }
        }
    }
    
    public boolean addEndpointConnectionInternal(
        QName fromService, String fromEndpoint, 
        QName toService, String toEndpoint, Link linkType) 
        throws javax.jbi.messaging.MessagingException
    {
        RegisteredEndpoint  re;
        LinkedEndpoint      link;
        Endpoint            ep = new Endpoint(fromService, fromEndpoint);
        VectorArray         va;
        boolean             added = true;

        mLog.log(Level.FINE, "Add service connection for endpoint {0} {1}", new Object[]{fromService, fromEndpoint});
        re = (LinkedEndpoint)mLinkedEndpoints.get(ep);
        link = new LinkedEndpoint(fromService, fromEndpoint, toService, toEndpoint, linkType);
        
        // If we find an existing link, see if it's identical to the one we
        // are trying to add.  If it's identical, we consider the operation a
        // NOP, otherwise it's an error.
        if (re != null)
        {
            if (!re.equals(link))
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.ENDPOINT_CONNECTION_EXISTS,
                    new Object[] {fromService, fromEndpoint, re.getOwnerId()}));
            }
            added = false;
        }
        else
        {
            va = new VectorArray(mIEbyService.get(fromService), link);   
            mIEbyService.put(fromService, va);
            mLinkedEndpoints.put(ep, link);
            mLinkedEndpointNames.put(link.toExternalName(), link);
            mTotalLinkedEndpoints++;
        }
        return (added);
    }
    
    public  boolean removeEndpointConnection(
        QName fromService, String fromEndpoint, 
        QName toService, String toEndpoint) 
    {
        boolean removed;
        
       if (removed = removeEndpointConnectionInternal(fromService, fromEndpoint,
            toService, toEndpoint))
       {
	    EndpointListener	el = mMsgSvc.getEndpointListener();

	    if (el != null)
	    {
               el.removeServiceConnection(fromService, fromEndpoint, toService, toEndpoint);
	    }
       }
        return (removed);
    }
    
    public synchronized boolean removeEndpointConnectionInternal(
        QName fromService, String fromEndpoint, 
        QName toService, String toEndpoint) 
    {
        boolean         isRemoved = false;
        LinkedEndpoint  link;
        
        mLog.log(Level.FINE, "Remove service connection for endpoint {0} {1}", new Object[]{fromService, fromEndpoint});
            
        // look for the linked endpoint
        link = (LinkedEndpoint)mLinkedEndpoints.get(new Endpoint(fromService, fromEndpoint));
        
        if (link != null)
        {
            removeEndpoint(link);
            isRemoved = true;
            mTotalLinkedEndpoints--;
        }
        
        return isRemoved;
    }
        
    public synchronized void addInterfaceConnection(
        QName fromInterface, QName toService, String toEndpoint)
        throws javax.jbi.messaging.MessagingException 
    {
        Endpoint        ep = new Endpoint(toService, toEndpoint);
        
        if (mInterfaceConnections.containsKey(fromInterface) &&
 	             !ep.equals(mInterfaceConnections.get(fromInterface)))
        {            
            throw new javax.jbi.messaging.MessagingException(
                Translator.translate(LocalStringKeys.INTERFACE_CONNECTION_EXISTS,
                new Object[] {fromInterface}));
        }

        mLog.log(Level.FINE, "Adding service connection for interface {0}", fromInterface);
        mInterfaceConnections.put(fromInterface, ep);
    }
    
    public synchronized boolean removeInterfaceConnection(
        QName fromInterface, QName toService, String toEndpoint)
    {
        mLog.log(Level.FINE, "Removing service connection for interface {0}", fromInterface);
        return (mInterfaceConnections.remove(fromInterface) != null);
    }
    
    /** Translates a linked endpoint to its internal counterpart based on 
     *  a service connection specification.
     */
    public RegisteredEndpoint resolveLinkedEndpoint(LinkedEndpoint link)
    {
        return ((RegisteredEndpoint)mInternalEndpoints.get(
            new Endpoint(link.getServiceLink(), link.getEndpointLink())));
    }
    
    /* ######################################################################
     * #################### NON-JBI INTERFACE SYSTEM ########################
     * ####################################################################*/
    //by DavidD, new interface routing system
    //as independant as possible from the old system
    
    @Override
    /**
     * Register a new NonLinkedConsumer. 
     * Also create the route type entry if necessary
     * @param service  -> the service of the endpoint to register
     * @param endpointName  -> the name of the endpoint to register
     * @param consumedInterface  -> the interface for the service consumed by the consumer
     * @return true if a new entry has been created, false if the endpoint already exists
     */
    public synchronized boolean registerNonLinkedConsumer(QName service, String endpointName, QName consumedInterface){

        // check to see if it already exists
        Endpoint endpoint = new Endpoint(service, endpointName);
        RegisteredEndpoint re = (RegisteredEndpoint)mNonConnectedConsumers.get(endpoint);        
        
        if (re != null){  
            //the consumer is already registered
            return false;
        }
        
        re = new NonLinkedConsumerInternalEndpoint(service, endpointName);
        mNonConnectedConsumersbyService.put(service, new VectorArray(mNonConnectedConsumersbyService.get(service), re));
        mNonConnectedConsumers.put(endpoint, re);
        //a non connected consumer is always indirect
        mRouteTypes.put(endpoint, RouteType.indirect);
        mConsumedInterfaces.put(endpoint, consumedInterface);
        
        if (mLog.isLoggable(Level.FINE)){
            mLog.log(Level.FINE, "Registered non connected Internal endpoint: {0}", re.toExternalName());
        }
        return true;
    }
    
    @Override
    /**
    * Unregister a NonLinkedConsumer :
    * remove the endpoint from the maps and also remove the interface consumed
    * remove the route type if still set
    * @param service  -> the service name of the endpoint to remove
    * @param endpointName  -> the name of the endpoint to remove
    * @return true if a consumer has been unregistered
    */
    public synchronized boolean unregisterNonLinkedConsumer(QName service, String endpointName){
        
        Endpoint endpoint = new Endpoint(service, endpointName);
        RegisteredEndpoint re = (RegisteredEndpoint)mNonConnectedConsumers.get(endpoint); 
        if(re==null){
            //the endpoint to unregister is not registered !
            return false;
        }
        //remove the entry in the mNonConnectedConsumers map
        mNonConnectedConsumers.remove(endpoint);
        
        //remove the entry in the mNonConnectedConsumersbyService map
        VectorArray endpointsForService = mNonConnectedConsumersbyService.get(service);
        if(endpointsForService.remove(re)){
            //the vector is now empty so we remove the map entry
            mNonConnectedConsumersbyService.remove(service);
        }else{
            //update the map with the removed (or not if not found) entry 
            mNonConnectedConsumersbyService.put(service,endpointsForService);
        }
        
        //remove the consumed interface
        mConsumedInterfaces.remove(endpoint);
        
        //remove the route type if still set
        mRouteTypes.remove(endpoint);
        
        return true;
    }
    
    @Override
    /**
     * Set the route type for a consumer. ALL consumers must have a route type.  
     * (does not check if the consumer exists) 
     * @param service -> the service of the consumer to set.
     * @param endpointName  -> the name of the consumer to set.
     * @param routeType -> the route type to set. 'direct', 'indirect', 'redirect' are supported.
     * @return false if the endpoint has already a route type set
     * @throws IllegalArgumentException if the route type is not supported.
     */
    public synchronized boolean setRouteType(QName service, String endpointName, String routeType){
        Endpoint endpoint = new Endpoint(service, endpointName);
        if(mRouteTypes.get(endpoint)!=null){
            return false;
        }
        RouteType type;
        if(routeType.equalsIgnoreCase("direct")){
            type=RouteType.direct;
        }else if(routeType.equalsIgnoreCase("indirect")){
            type=RouteType.indirect;
        }else if(routeType.equalsIgnoreCase("redirect")){
            type=RouteType.redirect;
        }else{
            throw new IllegalArgumentException("the routeType should be 'direct', 'indirect' or 'redirect'.");
        }
        mRouteTypes.put(endpoint, type);
        return true;
    }
    
    @Override
    /**
     * Set the way that the provider is chosen among all of those implementing the consumed interface
     * Overrides the existing rule if any.
     * @param service   -> the service of the consumer 
     * @param endpointName  -> the name of the consumer
     * @param routingRule   -> the applied rule. 'default','random' are supported ('load-balancing' is random)
     * @return false if a rule has been overridden, true if not.
     */
    public boolean setRoutingRule(QName service, String endpointName, String routingRule){
        if(routingRule!=null && 
                (routingRule.equals("load-balancing") || routingRule.equals("load balancing"))){
            routingRule="random";
        }
        Endpoint endpoint = new Endpoint(service, endpointName);
        return mRoutingRule.put(endpoint, routingRule)==null;
    };
    
    @Override
    /**
     * Specifies if the consumer can consume a provider outside the JVM or not.
     * Overrides the existing mode if any.
     * @param service   -> the service of the consumer 
     * @param endpointName  -> the name of the consumer
     * @param mode   -> the applied rule. 'local','distributed' are supported
     * @return false if a mode has been overridden, true if not.
     */
    public boolean setMode(QName service, String endpointName, String mode){
        Endpoint endpoint = new Endpoint(service, endpointName);
        return mModes.put(endpoint, mode)==null;
    }
    
    @Override
    /**
     * Unset the route type of a consumer (for deactivation).
     * Also remove the routing rule and the mode if set.
     * @param service   -> the service of the consumer
     * @param endpointName  -> the name of the consumer
     * @return true if a route type has been removed, false if not.
     */
    public synchronized boolean removeRouteType(QName service, String endpointName){
        Endpoint endpoint = new Endpoint(service,endpointName);
        mModes.remove(endpoint);
        mRoutingRule.remove(endpoint);
        return (mRouteTypes.remove(endpoint)!=null);
    }
    
    @Override
    /**
     * Specifies that the given provider is implementing the given interface.
     * @param providerName  -> the name of the provider
     * @param providerService   -> the service of the provider
     * @param implementedInterface  -> the interface implemented by the provider
     * @return false if the provider already implements the given interface, true if not.
     */
    public synchronized boolean addImplementedInterface(String providerName, QName providerService, QName implementedInterface){
        Endpoint endpoint = new Endpoint(providerService, providerName);
        ArrayList<Endpoint> providers = mImplementedInterfaces.get(implementedInterface);
        if(providers!= null && providers.contains(endpoint)){
            return false;
        }else{
            if(providers==null){
                providers=new ArrayList<Endpoint>();
            }
            providers.add(endpoint);
            mImplementedInterfaces.put(implementedInterface,providers);
            return true;
        }        
    }
    
    @Override
    /**
     * Specifies that the given provider is no longer implementing the given interface (for deactivation).
     * @param providerName  -> the name of the provider
     * @param providerService   -> the service of the provider
     * @param implementedInterface  -> the interface implemented by the provider
     * @return true if the implemented interface by the provider has been removed, false if not.
     */
    public synchronized boolean removeImplementedInterface(String providerName, QName providerService, QName implementedInterface){
        Endpoint endpoint = new Endpoint(providerService, providerName);
        ArrayList<Endpoint> providers = mImplementedInterfaces.get(implementedInterface);
        if(providers==null){
            //no providers for the given interface
            return false;
        }
        boolean removed = providers.remove(endpoint);
        if(removed && providers.isEmpty()){
            //No providers implements the interface anymore => remove the map entry
            mImplementedInterfaces.remove(implementedInterface);
        }else{
            //update in the map the providers implementing the interface
            mImplementedInterfaces.put(implementedInterface, providers);
        }    
        return removed;
    }
    
    @Override
    /**
     * Specifies the interface of the service consumed by a consumer. 
     * Should be called on for redirect linked endpoint.
     * (does not check if the consumer exists)
     * @param consumerService   -> the service of the consumer
     * @param consumerName  -> the name of the consumer
     * @param consumedInterface -> the interface of the service consumed by the consumer
     * @return true if a new entry has been created, 
     * false if the entry has overridden an already existing interface for this consumer
     */
    public synchronized boolean addConsumedInterface(QName consumerService, String consumerName, QName consumedInterface){
        Endpoint endpoint = new Endpoint(consumerService,consumerName);
        return (mConsumedInterfaces.put(endpoint, consumedInterface)==null);
    }
    
    @Override
    /**
     * Specifies that the given consumer no longer consumes its interface-defined service.
     * @param consumerService   -> the service of the consumer
     * @param consumerName  -> the name of the consumer
     * @return true if an entry has been remove, false if the given consumer had no consumed interface.
     */
    public synchronized boolean removeConsumedInterface(QName consumerService, String consumerName){
        Endpoint endpoint = new Endpoint(consumerService,consumerName);
        return (mConsumedInterfaces.remove(endpoint)!=null);
    }
    
    /**
     * get the route type for the given endpoint
     * @param endpoint -> the LinkedEndpoint or NonLinkedEndpoint object
     * @return the route type, 'direct' if not found (default). Not null.
     */
    public RouteType getRouteType(RegisteredEndpoint endpoint){
        Endpoint ep = new Endpoint(endpoint.getServiceName(),endpoint.getEndpointName());
        RouteType type = mRouteTypes.get(ep);
        if(type==null){
            return RouteType.direct;
        }
        return type;
    }
    
    /**
     * get the routint rule for the given endpoint
     * @param endpoint -> the LinkedEndpoint or NonLinkedEndpoint object
     * @return the routing rule. 'default' if not found.
     */
    public String getRoutingRule(RegisteredEndpoint endpoint){
        Endpoint ep = new Endpoint(endpoint.getServiceName(),endpoint.getEndpointName());
        String rule = mRoutingRule.get(ep);
        if(rule==null || rule.equals("")){
            return "default";
        }
        return rule;
    }
    
    /**
     * get the mode for the given endpoint
     * @param endpoint -> the LinkedEndpoint or NonLinkedEndpoint object
     * @return the mode. 'local' if not found.
     */
    public String getMode(RegisteredEndpoint endpoint){
        Endpoint ep = new Endpoint(endpoint.getServiceName(),endpoint.getEndpointName());
        String mode = mModes.get(ep);
        if(mode==null || mode.equals("")){
            return "local";
        }
        return mode;
    }
    
    /**
     * get all the NonLinkedConsumerInternalEndpoint for the given service name
     * @param service   -> the name of the service
     * @return All NonLinkedConsumerInternalEndpoint with the given service as service name.
     *      empty if none. Not null.
     */
    public ServiceEndpoint[] getNonLinkedConsumerForService(QName service){
        VectorArray consumers = mNonConnectedConsumersbyService.get(service);
        return consumers==null ? new RegisteredEndpoint[0] : consumers.mArray;
    }
    
    /**
     * retrieve all providers implementing the given interface.
     * @param mInterface -> the interface name
     * @return The providers that implements the interface, empty if none. Not null.
     */
    public ArrayList<Endpoint> getInternalProvidersForInterface(QName mInterface){
        ArrayList<Endpoint> providers = mImplementedInterfaces.get(mInterface);
        return providers==null ? new ArrayList<Endpoint>() : providers;
    }
    
    /**
     * retrieve all the providers implementing the interface consumed by the given consumer.
     * @param consumerService   -> the service of the consumer
     * @param consumerName  -> the name of the consumer
     * @return All the providers that can provide the service to the consumer through the interface. empty if none. null if the consumer is not registered.
     */
    public ArrayList<Endpoint> findProvidersForConsumerByInterface(QName consumerService, String consumerName){
        Endpoint endpoint = new Endpoint(consumerService, consumerName);
        QName mInterface = mConsumedInterfaces.get(endpoint);
        if(mInterface==null){
            return null;//the consumer is not registered
        }
        return getInternalProvidersForInterface(mInterface);        
    }
    
    public QName getConsumedInterface(QName consumerService, String consumerName){
        Endpoint endpoint = new Endpoint(consumerService, consumerName);
        return mConsumedInterfaces.get(endpoint);
    }
    
    /**######################################################################
     * #################### IMPLEMENTATION PRIVATE ##########################
     * ####################################################################*/
    
    /** List all the endpoints for a given interface.
     */
   private RegisteredEndpoint[] getEndpointsForInterface(
        QName interfaceName, int type)
    {
        LinkedList<RegisteredEndpoint>          interfaces = type == RegisteredEndpoint.INTERNAL ?
                                                    mPendingInternalInterfaces : mPendingExternalInterfaces;
        ConcurrentHashMap<QName, VectorArray>   interfaceMap = type == RegisteredEndpoint.INTERNAL ?
                                                    mIEbyInterface : mEEbyInterface;
    
        for (;;)
        {
            RegisteredEndpoint  ep;
           
            //
            //  Drain the pending queue of endpoints to query for interface information.
            //
            synchronized (this)
            {
                ep = interfaces.poll();
            }
            if (ep == null)
            {
                break;
            }
            getInterfacesForEndpoint(ep);
        }
        VectorArray va = interfaceMap.get(interfaceName);
        return (va == null ? new RegisteredEndpoint[0] : va.mArray);
    }
    
    void getInterfacesForEndpoint(RegisteredEndpoint re)
    {
        LinkedList<RegisteredEndpoint>          interfaces = re.getType() == RegisteredEndpoint.INTERNAL ?
                                                    mPendingInternalInterfaces : mPendingExternalInterfaces;
        ConcurrentHashMap<QName, VectorArray>   interfaceMap = re.getType() == RegisteredEndpoint.INTERNAL ?
                                                    mIEbyInterface : mEEbyInterface;

        synchronized (this)
        {
            //
            //  Remove from the work list if present.
            //
            interfaces.remove(re);
            
            //
            //  Query component for WSDL and extract supported interfaces.
            //
            try
            {
                if (re.getInterfacesInternal() == null)
                {
                    re.parseDescriptor(mMsgSvc.queryDescriptor(re));
                }
                for (QName in : re.getInterfaces())
                {
                    interfaceMap.put(in, new VectorArray(interfaceMap.get(in), re));                           
                }
            }
            catch (javax.jbi.messaging.MessagingException msgEx)
            {
                mLog.warning(msgEx.toString());
            }
        }

    }

    private void removeInternalEndpoint(RegisteredEndpoint endpoint)
    {
        VectorArray va;
        
        va = mIEbyService.get(endpoint.getServiceName());
        if (va != null)
        {
            if (va.remove(endpoint))
            {
                mIEbyService.remove(endpoint.getServiceName());
            }
            else
            {
                mIEbyService.put(endpoint.getServiceName(), va);
            }
            QName[] interfaceNames = endpoint.getInterfacesInternal();
            if (interfaceNames != null)
            {
                for (QName i : interfaceNames)
                {
                    va = mIEbyInterface.get(i);
                    if (va != null)
                    {
                        if (va.remove(endpoint))
                        {
                            mIEbyInterface.remove(i);
                        }
                        else
                        {
                            mIEbyInterface.put(i, va);
                        }
                    }
                }
            }
            mInternalEndpoints.remove(new Endpoint(endpoint.getServiceName(), endpoint.getEndpointName()));        
            mInternalEndpointNames.remove(endpoint.toExternalName());
            mPendingInternalInterfaces.remove(endpoint);
	}
    }
    
    private void removeExternalEndpoint(RegisteredEndpoint endpoint)
    {
        VectorArray         va;
        
        va = mEEbyService.get(endpoint.getServiceName());
	if (va != null)
        {
            if (va.remove(endpoint))
            {
                mEEbyService.remove(endpoint.getServiceName());
            }
            else
            {
                mEEbyService.put(endpoint.getServiceName(), va);
            }
            QName[] interfaceNames = endpoint.getInterfacesInternal();
            if (interfaceNames != null)
            {
                for (QName i : interfaceNames)
                {
                    va = mEEbyInterface.get(i);
                    if (va != null)
                    {
                        if (va.remove(endpoint))
                        {
                            mEEbyInterface.remove(i);
                        }
                        else
                        {
                            mEEbyInterface.put(i, va);
                        }
                    }
                }
            }
            mExternalEndpoints.remove(new Endpoint(endpoint.getServiceName(), endpoint.getEndpointName()));            
            mPendingExternalInterfaces.remove(endpoint);
        }
    }

    private void removeLinkedEndpoint(RegisteredEndpoint endpoint)
    {
        VectorArray         va;
        
        va = mIEbyService.get(endpoint.getServiceName());
	if (va != null)
        {
            if (va.remove(endpoint))
            {
                mIEbyService.remove(endpoint.getServiceName());
            }
            else
            {
                mIEbyService.put(endpoint.getServiceName(), va);
            }
            mLinkedEndpoints.remove(new Endpoint(endpoint.getServiceName(), endpoint.getEndpointName()));
            mLinkedEndpointNames.remove(endpoint.toExternalName());        
        }
    }
            
    int countEndpoints(int type)
    { 
        int     count = 0;
        
        if (type == RegisteredEndpoint.INTERNAL)
        {
            count = mInternalEndpoints.size();
        }
        else if (type == RegisteredEndpoint.EXTERNAL)
        {
            count = mExternalEndpoints.size();
        }
        else
        {
            count = mLinkedEndpoints.size();
        }
        return (count);
    }
    
    ServiceEndpoint[] listEndpoints(int type)
    {
        ServiceEndpoint[]   endpoints;
        Collection          values;
        
        if (type == RegisteredEndpoint.INTERNAL)
        {
            values = mInternalEndpoints.values();
        }
        else if (type == RegisteredEndpoint.EXTERNAL)
        {
            values = mExternalEndpoints.values();
        }
        else
        {
            values = mLinkedEndpoints.values();
        }
        
        endpoints = new ServiceEndpoint[values.size()];
        values.toArray(endpoints);
        return endpoints;
    }
        
    void clear()
    {
        mLinkedEndpoints.clear();
        mInternalEndpoints.clear();
        mExternalEndpoints.clear();
        mInterfaceConnections.clear();
        mIEbyService.clear();
        mEEbyService.clear();
        mIEbyInterface.clear();
        mEEbyInterface.clear();
        mPendingInternalInterfaces.clear();
        mPendingExternalInterfaces.clear();
     }


    
    public String toString()
    {
        StringBuilder       sb = new StringBuilder();
        
        sb.append("  EndpointRegistry\n");
        sb.append("    InternalEndpoints Count: ");
        sb.append(mInternalEndpoints.size());
        sb.append("\n");
        for (Iterator i = mInternalEndpoints.entrySet().iterator(); i.hasNext(); )
        {
            sb.append(((Map.Entry<Endpoint, RegisteredEndpoint>)i.next()).getValue().toString());
        }
        sb.append("    InternalEndpointsByService Count: ");
        sb.append(mIEbyService.size());
        sb.append("\n");
        for (Map.Entry<QName, VectorArray> m : mIEbyService.entrySet())
        {
            VectorArray     va = m.getValue();

            sb.append("      Service: " + m.getKey());
            sb.append("\n");
            sb.append(va.toString());
        }
        sb.append("    InternalEndpointsByInterface Count: ");
        sb.append(mIEbyInterface.size());
        sb.append("\n");
        for (Map.Entry<QName, VectorArray> m : mIEbyInterface.entrySet())
        {
            VectorArray     va = m.getValue();

            sb.append("      Interface: " + m.getKey());
            sb.append("\n");
            sb.append(va.toString());
        }
        sb.append("    ExternalEndpoints Count: ");
        sb.append(mExternalEndpoints.size());
        sb.append("\n");
        for (Iterator i = mExternalEndpoints.entrySet().iterator(); i.hasNext(); )
        {
            sb.append(((Map.Entry<Endpoint, RegisteredEndpoint>)i.next()).getValue().toString());
        }
        sb.append("    ExternalEndpointsByService Count: ");
        sb.append(mEEbyService.size());
        sb.append("\n");
        for (Map.Entry<QName, VectorArray> m : mEEbyService.entrySet())
        {
            VectorArray     va = m.getValue();

            sb.append("      Service: " + m.getKey());
            sb.append("\n");
            sb.append(va.toString());
        }
        sb.append("    ExternalEndpointsByInterface Count: ");
        sb.append(mEEbyInterface.size());
        sb.append("\n");
        for (Map.Entry<QName, VectorArray> m : mEEbyInterface.entrySet())
        {
            VectorArray     va = m.getValue();

            sb.append("      Interface: " + m.getKey());
            sb.append("\n");
            sb.append(va.toString());
	}
        sb.append("    LinkedEndpoints Count: ");
        sb.append(mLinkedEndpoints.size());
        sb.append("\n");
        for (Iterator i = mLinkedEndpoints.entrySet().iterator(); i.hasNext(); )
        {
            sb.append(((Map.Entry<Endpoint, LinkedEndpoint>)i.next()).getValue().toString());
        }
        sb.append("    InterfaceConnections Count: ");
        sb.append(mInterfaceConnections.size());
        sb.append("\n");
        for (Map.Entry<QName, Endpoint> e : mInterfaceConnections.entrySet()) {
            sb.append("      InterfaceName: " + e.getKey());
            sb.append("\n");
            sb.append(e.getValue().toString());
        }
        return (sb.toString());
    }
    
    private class VectorArray
    {
        Vector                  mVector;
        RegisteredEndpoint      mArray[];
        
        
        VectorArray(VectorArray va, RegisteredEndpoint o)
        {
            if (va == null)
            {
                mVector = new Vector();
            }
            else
            {
                mVector = va.mVector;
            }
            if (o instanceof LinkedEndpoint)
            {
                mVector.add(0, o);
            }
            else
            {
                mVector.add(o);
            }
            mArray = new RegisteredEndpoint[mVector.size()];
            mVector.toArray(mArray);
        }
        
        boolean remove(Object o)
        {
            mVector.remove(o);
            if (mVector.size() != 0)
            {
                mArray = new RegisteredEndpoint[mVector.size()];
                mVector.toArray(mArray);
                return (false);
            }
            return (true);
        }

        public String toString()
        {
            StringBuilder   sb = new StringBuilder();

            for (RegisteredEndpoint re : mArray)
            {
                sb.append(re.toString());
            }

            return (sb.toString());
        }
    }
    
}
