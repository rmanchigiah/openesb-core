/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.messaging.util;

/**
 *
 * @author DavidD
 */
public enum RouteType {
    direct, indirect, redirect
}
