/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Value.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging.stats;


/** Simple value sampler that computes basic statistics.
 * @author Sun Microsystems, Inc.
 */
public class Value
{
    private double  sum;
    private double  squaredsum;
    private long    min;
    private long    max;
    private long    count;
    
    public Value()
    {        
    }
    
    public void zero()
    {
        sum = 0.0;
        squaredsum = 0.0;
        min = 0;
        max = 0;
        count = 0;
    }
    
    public void addSample(long sample)
    {
        if (sample < min || count == 0)
        {
            min = sample;
        }
        if (sample > max || count == 0)
        {
            max = sample;
        }
        count++;
        sum += sample;
        squaredsum += ((double)sample)*((double)sample);
    }
    
    public double getAverage()
    {
        return (sum / count);
    }
    
    public long getMin()
    {
        return (min);
    }
    
    public long getMax()
    {
        return (max);
    }
    
    public long getCount()
    {
        return (count);
    }
    
    public double getSd()
    {
        return (Math.sqrt((1.0 / (count - 1)) * (squaredsum - ((sum * sum) / count))));
    }
    
    public String toString()
    {
        StringBuilder        sb = new StringBuilder();
        
        sb.append("Count("+count+")");
        sb.append("Min("+min+")");
        sb.append("Avg("+getAverage()+")");
        sb.append("Max("+max+")");
        sb.append("Std("+getSd()+")");
        
        return (sb.toString());
    }
        

}

    
