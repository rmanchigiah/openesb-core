/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExternalEndpoint.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.w3c.dom.DocumentFragment;

/** Represents an external-facing endpoint activated by a JBI component.  External
 *  endpoint references are *never* used to address message exchanges.
 * @author Sun Microsystems, Inc.
 */
public class ExternalEndpoint extends RegisteredEndpoint
{
    private ServiceEndpoint mDelegate;
    
    /** Creates a new ExternalEndpoint.
     *  @param endpoint endpoint implementation provided by component
     *  @param ownerId id of component which is registering the service
     */
    public ExternalEndpoint(ServiceEndpoint endpoint, String ownerId)
    {
        super(endpoint.getServiceName(), endpoint.getEndpointName(), ownerId);
	mDelegate = endpoint;
    }
    
    public int getType() 
    {
        return EXTERNAL;
    }
    
    public ServiceEndpoint getDelegate()
    {
        return mDelegate;
    }
        
    public DocumentFragment getAsReference(QName operationName)
    {
        return mDelegate.getAsReference(operationName);
    }
    
    /** Compares two ExternalEndpoint instances for equality. */
    public boolean equals(Object obj)
    {
        ExternalEndpoint ee;
        boolean isEqual = false;

        if (obj != null && this.getClass() == obj.getClass())
        {
            ee = (ExternalEndpoint)obj;
            isEqual = (getEndpointName().equals(ee.getEndpointName())) && 
                    (getServiceName().equals(ee.getServiceName()));
        }
        return isEqual;

    }

    public String toString()
    {
        StringBuilder   sb = new StringBuilder();
        
        sb.append("        Endpoint Type: External(Delegate=");
        sb.append(mDelegate == null ? "Null" : mDelegate.toString());
        sb.append(")\n");
        sb.append(super.toString());
        return (sb.toString());
    }

}
