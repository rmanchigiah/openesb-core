package com.sun.jbi.messaging;

import java.net.URI;

import javax.jbi.messaging.NormalizedMessage;


/**
 *
 * Used to pass copied Message Exchanges to all known Observers of
 * the NMR.
 *
 */
public class Observer
    extends MessageExchangeProxy 
{
    /** Simple state machine for the SOURCE side of the pattern */
    private static final int[][] SOURCE = 
    {
        { DO_ACCEPT | SET_PROPERTY, -1, 1, 0, 0 },
        { MARK_DONE, -1, -1, -1, -1 },
    };

    /** Simple state machine for the TARGET side of the pattern */
    private static final int[][] TARGET = 
    {
        { DO_ACCEPT, -1, -1, -1, -1 },
        { MARK_DONE, -1, -1, -1, -1 },
    };

    private Role    mRole;
    private URI     mPattern;
    
    /**
     * Default Constructor
     *
     */
    public Observer(MessageExchangeProxy me) 
    {
        this(SOURCE);
        mRole = me.getRole();
        mPattern = me.getPattern();
    }

    /**
     * Create a new Observer in the forward or reverse direction
     * @param state
     */
    Observer(int[][] state) 
    {
        super(state);
    }

    public Role getRole()
    {
        return (mRole);
    }
    
    /**
     * Return a new instance of ourselves in the target role
     */
    public MessageExchangeProxy newTwin() 
    {
        return (new Observer(TARGET));
    }

    /**
     * Get the pattern
     */
    public URI getPattern() 
    {
        return mPattern;
    }

    /**
     * Retrieve the message with reference id "in" from this exchange.
     * @return the in message, or null if it is not present in the exchange
     */
    public NormalizedMessage getInMessage() 
    {
        return getMessage(IN_MSG);
    }

    /**
     * Retrieve the message with the reference id "out" from this exchange.
     * @return the out message, or null if it is not present in the exchange
     */
    public NormalizedMessage getOutMessage() 
    {
        return getMessage(OUT_MSG);
    }

    /**
     * Specifies the "in" message reference for this exchange
     * @param msg in message
     * @throws javax.jbi.messaging.MessagingException invalid message or the
     * curent state of the exchange does not permit this operation.
     */
    public void setInMessage(NormalizedMessage msg)
        throws javax.jbi.messaging.MessagingException 
    {
        setMessage(msg, IN_MSG);
    }

    /**
     * Specifies the "out" message reference for this exchange
     * @param msg out message
     * @throws javax.jbi.messaging.MessagingException nvalid message or the
     * curent state of the exchange does not permit this operation.
     */
    public void setOutMessage(NormalizedMessage msg)
        throws javax.jbi.messaging.MessagingException 
    {
        setMessage(msg, OUT_MSG);
    }
}


