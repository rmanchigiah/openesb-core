/*
  * BEGIN_HEADER - DO NOT EDIT

  * The contents of this file are subject to the terms
  * of the Common Development and Distribution License
  * (the "License").  You may not use this file except
  * in compliance with the License.

  * You can obtain a copy of the license at
  * http://opensource.org/licenses/cddl1.php.
  * See the License for the specific language governing
  * permissions and limitations under the License.

  * When distributing Covered Code, include this CDDL
  * HEADER in each file and include the License file at
  * http://opensource.org/licenses/cddl1.php.
  * If applicable add the following below this CDDL HEADER,
  * with the fields enclosed by brackets "[]" replaced with
  * your own identifying information: Portions Copyright
  * [year] [name of copyright owner]
 */

package org.openesb.runtime.tracking.tasks;

/**
 *
 * Copyright 2011 Alexander Lomov.
 */

/**
 * Ideal class: doesn nothing - neither bugs nor works
 * 
 */

public abstract class MessageTrackingTaskRunnable 
        extends MessageTrackingTask
        implements Runnable {
    
    private String exName;

    public String getPreferredExecutorName(){
        return exName;
    }

    public void setPreferredExecutorName(String preferredExecutorName){
        exName = preferredExecutorName;
    }

    @Override
    public void track(){
        run();
    }

}
