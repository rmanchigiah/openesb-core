/*
 * An XML document type.
 * Localname: service
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.ServiceDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one service(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class ServiceDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.ServiceDocument
{
    
    public ServiceDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName SERVICE$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "service");
    
    
    /**
     * Gets the "service" element
     */
    public org.w3.ns.wsdl.ServiceType getService()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ServiceType target = null;
            target = (org.w3.ns.wsdl.ServiceType)get_store().find_element_user(SERVICE$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "service" element
     */
    public void setService(org.w3.ns.wsdl.ServiceType service)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ServiceType target = null;
            target = (org.w3.ns.wsdl.ServiceType)get_store().find_element_user(SERVICE$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.ServiceType)get_store().add_element_user(SERVICE$0);
            }
            target.set(service);
        }
    }
    
    /**
     * Appends and returns a new empty "service" element
     */
    public org.w3.ns.wsdl.ServiceType addNewService()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ServiceType target = null;
            target = (org.w3.ns.wsdl.ServiceType)get_store().add_element_user(SERVICE$0);
            return target;
        }
    }
}
