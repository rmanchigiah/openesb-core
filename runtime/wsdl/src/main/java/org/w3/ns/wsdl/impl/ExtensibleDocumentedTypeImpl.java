/*
 * XML Type:  ExtensibleDocumentedType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.ExtensibleDocumentedType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML ExtensibleDocumentedType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class ExtensibleDocumentedTypeImpl extends org.w3.ns.wsdl.impl.DocumentedTypeImpl implements org.w3.ns.wsdl.ExtensibleDocumentedType
{
    
    public ExtensibleDocumentedTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    
}
