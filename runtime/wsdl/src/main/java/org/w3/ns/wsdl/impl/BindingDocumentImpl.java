/*
 * An XML document type.
 * Localname: binding
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.BindingDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one binding(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class BindingDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.BindingDocument
{
    
    public BindingDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName BINDING$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "binding");
    
    
    /**
     * Gets the "binding" element
     */
    public org.w3.ns.wsdl.BindingType getBinding()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingType target = null;
            target = (org.w3.ns.wsdl.BindingType)get_store().find_element_user(BINDING$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "binding" element
     */
    public void setBinding(org.w3.ns.wsdl.BindingType binding)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingType target = null;
            target = (org.w3.ns.wsdl.BindingType)get_store().find_element_user(BINDING$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.BindingType)get_store().add_element_user(BINDING$0);
            }
            target.set(binding);
        }
    }
    
    /**
     * Appends and returns a new empty "binding" element
     */
    public org.w3.ns.wsdl.BindingType addNewBinding()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingType target = null;
            target = (org.w3.ns.wsdl.BindingType)get_store().add_element_user(BINDING$0);
            return target;
        }
    }
}
