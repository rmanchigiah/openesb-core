/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Import.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.w3.ns.wsdl.ImportType;

/**
 * Abstract implementation of
 * WSDL 2.0 Import component.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class Import extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.Import
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final ImportType getBean()
    {
        return (ImportType) this.mXmlObject;
    }

    /**
     * Construct an abstract Import implementation base component.
     * 
     * @param bean      The XML bean for this Import component
     */
    Import(ImportType bean)
    {
        super(bean);
    }

    /**
     * Get the description from this import component
     *
     * @return Description from this import component, if any
     */
    public abstract com.sun.jbi.wsdl2.Description getDescription();

    /**
     * Get the definitions from this import component.
     *
     * @deprecated - replaced by getDescription
     * @return Definitions n from this import component, if any
     */
     public abstract com.sun.jbi.wsdl2.Definitions getDefinitions();

}

// End-of-file: Import.java
