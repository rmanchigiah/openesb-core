/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtensibleDocumentedComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.apache.xmlbeans.XmlObject;
import org.w3.ns.wsdl.ExtensibleDocumentedType;

/**
 * Abstract implementation of
 * Base for extensible, documented components
 *
 * @author Sun Microsystems, Inc.
 */
abstract class ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.ExtensibleDocumentedComponent
{
    /** 
     * The base XML object we are dealing with. This can be downcast to 
     * ExtensibleDocumentedType safely. 
     */
    protected XmlObject mXmlObject;

    /**
     * Get the extensible, documented XML bean for this (abstract) component.
     *
     * @return The extensible, documented XML bean for this component.
     */
    protected final ExtensibleDocumentedType getExBean()
    {
        return (ExtensibleDocumentedType) this.mXmlObject;
    }

     /** The extensions for this component, if any. */
    private ExtensionsImpl mExtensions = null;

    /**
     * Construct a new extensible, documented component whose contents
     * are within the given XML bean.
     *
     * @param xmlObject The XML bean containing this component's contents.
     */
    ExtensibleDocumentedComponent(XmlObject xmlObject)
    {
        this.mXmlObject = xmlObject;
    }

    /**
     * Get the container for this component.
     *
     * @return The container for this component.
     */
    protected abstract DescriptionImpl getContainer();

    /**
     * Get documentation for component, if any.
     * @param i index by which this document is obtained
     *
     * @return documentation for component, if any
     */
    public com.sun.jbi.wsdl2.Document getDocument(int i)
    {
        return DocumentImpl.Factory.getInstance(
            getExBean().getDocumentationArray(i), 
            this, 
            getContainer());
    }

    /**
     * Get documentation for component, if any.
     * This method is for backward compatibility, it always returns
     * the first document (i.e., index = 0)
     *
     * @deprecated - replaced by getDocument(int i)
     * @return documentation for component, if any
     */
    public com.sun.jbi.wsdl2.Document getDocument()
    {
	return getDocument(0);
    }

    /**
     * Set documentation for component, if any.
     * @param i index by which this document is set
     *
     * @param theDocument documentation for component, if any
     */
    public void setDocument(int i, com.sun.jbi.wsdl2.Document theDocument)
    {
        getExBean().setDocumentationArray(i,
            theDocument != null ? ((DocumentImpl) theDocument).getBean()
                                : null);  
    }

    /**
     * Set documentation for component, if any.
     * This method is for backward compatibility, it always set the
     * the document which has the index = 0
     *
     * @deprecated - replaced by setDocument(int i, Document theDocument);
     * @param theDocument documentation for component, if any
     */
    public void setDocument(com.sun.jbi.wsdl2.Document theDocument)
    {
	setDocument(0, theDocument);
    }

    /**
     * Get extensions for component, if any.
     *
     * @return extensions for component, if any
     */
    public com.sun.jbi.wsdl2.Extensions getExtensions()
    {
        return this.mExtensions;
    }

    /**
     * Set extensions for component, if any.
     *
     * @param theExtensions extensions for component, if any
     */
    public void setExtensions(com.sun.jbi.wsdl2.Extensions theExtensions)
    {
        return; // $$TODO
    }

}

// End-of-file: ExtensibleDocumentedComponent.java
