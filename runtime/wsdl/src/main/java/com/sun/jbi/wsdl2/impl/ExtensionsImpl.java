/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtensionsImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.w3c.dom.Attr;
import org.w3c.dom.Element;

/**
 * Implementation of Extension elements and attributes for a WSDL component.
 * 
 * @author Sun Microsystems, Inc.
 */
final class ExtensionsImpl extends Extensions
{
    /**
     * Get the number of Element items in elements.
     *
     * @return The number of Element items in elements
     */
    public int getElementsLength()
    {
        return 0;
    }

    /**
     * Get extension elements by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Extension elements at given <code>index</code> position.
     */
    public Element getElement(int index)
    {
        return null; // $$TODO
    }

    /**
     * Set extension elements by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theElement Item to add at position <code>index</code>.
     */
    public void setElement(int index, Element theElement)
    {
        return; // $$TODO
    }

    /**
     * Append an item to extension elements.
     *
     * @param theElement Item to append to elements
     */
    public void appendElement(Element theElement)
    {
        return; // $$TODO
    }

    /**
     * Remove extension elements by index position.
     *
     * @param index The index position of the element to remove
     * @return The Element removed, if any.
     */
    public Element removeElement(int index)
    {
        return null; // $$TODO
    }

    /**
     * Get the number of Attr items in attributes.
     *
     * @return The number of Attr items in attributes
     */
    public int getAttributesLength()
    {
        return 0; // $$TODO
    }

    /**
     * Get extension attributes by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Extension attributes at given <code>index</code> position.
     */
    public Attr getAttribute(int index)
    {
        return null; // $$TODO
    }

    /**
     * Set extension attributes by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theAttribute Item to add at position <code>index</code>.
     */
    public void setAttribute(int index, Attr theAttribute)
    {
        return; // $$TODO
    }

    /**
     * Append an item to extension attributes.
     *
     * @param theAttribute Item to append to attributes
     */
    public void appendAttribute(Attr theAttribute)
    {
        return; // $$TODO
    }

    /**
     * Remove extension attributes by index position.
     *
     * @param index The index position of the attribute to remove
     * @return The Attr removed, if any.
     */
    public Attr removeAttribute(int index)
    {
        return null; // $$TODO
    }

}

// End-of-file: ExtensionsImpl.java
