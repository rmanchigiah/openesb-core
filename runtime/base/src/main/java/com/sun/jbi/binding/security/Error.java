/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Error.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.security;

/**
 * Defines the Error interface for the ErrorHandler processError() callback method.
 *
 * @author Sun Microsystems, Inc.
 */
public interface Error
{
    /**
     * @return true if this Error is fatal
     */
    boolean isFatal();
    
    /**
     * @return the ErrorCode associated with this error
     */
    String getCode();
    
    /**
     * @return the short description associated with the error
     */
    String getShortDescription();
    
    /**
     * @return the long description associated with the error
     */
    String getLongDescription();
    
    /**
     * @return the Error Context ( as name value pairs )
     */
    Context getContext();
    
    /**
     * @return the Cause of this error
     */
    Throwable getCause();
}
