/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeliveryChannel.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.messaging;

import java.net.URI;

import javax.jbi.messaging.MessageExchange;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public interface DeliveryChannel
        extends javax.jbi.messaging.DeliveryChannel
{
    public boolean              activeReference(MessageExchange me);
    public void                 addServiceConnection(QName serviceLink, String endpointLink,
                                    QName service, String endpoint, String type)
        throws javax.jbi.messaging.MessagingException;
    public void                 removeServiceConnection(QName serviceLink, String endpointLink,
                                    QName service, String endpoint);
    
    public ServiceEndpoint      createEndpoint(QName service, String endpoint)
        throws javax.jbi.messaging.MessagingException;
    
    public MessageExchange      createExchange(URI pattern, String id)
        throws javax.jbi.messaging.MessagingException;
    
    public void                 setEndpointListener(EndpointListener listener);
    public void                 setTimeoutListener(TimeoutListener listener);
    public void                 setExchangeIdGenerator(ExchangeIdGenerator generator);
    public ClassLoader           getClassLoader();
    public boolean		sendLast(MessageExchange me)
         throws javax.jbi.messaging.MessagingException;
    public boolean             isExchangeOkay(MessageExchange me);
}
