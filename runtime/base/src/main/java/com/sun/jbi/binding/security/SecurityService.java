/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityService.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */

package com.sun.jbi.binding.security;

import com.sun.jbi.component.ComponentContext;

/**
 * The Security service is the factory for Security Handlers. A Security handler is 
 * is created for each binding component based on its installation configuration.
 *
 * @author Sun Microsystems, Inc.
 */
public interface SecurityService
{
    
    /**
     * Create a SecurityHandler based on the SecurityConfiguration.
     *
     * @param envCtx is the ComponentContext of the Binding Component associated
     * with the Securityhandler
     * @return a instance of the newly created SecurityHandler
     * @throws IllegalStateException if the SecurityHandler is in a invalid
     * state on creation.
     */
    SecurityHandler createSecurityHandler(ComponentContext envCtx)
        throws IllegalStateException;
    
    /**
     * Create a SecurityHandler based on the SecurityConfiguration.
     *
     * @param envCtx is the ComponentContext of the Binding Component associated
     * with the Securityhandler
     * @param authLayer is the type of layer that requires to use the security services.
     * @return a instance of the newly created SecurityHandler
     * @throws IllegalStateException if the SecurityHandler is in a invalid
     * state on creation.
     */
    SecurityHandler createSecurityHandler(ComponentContext envCtx, String authLayer)
        throws IllegalStateException;
    
    /**
     * Get the SecurityHandler for a Component.
     *
     * @param componentId is the Component Id
     * @return a instance of the components SecurityHandler, if it exists null otherwise.
     */
    SecurityHandler getSecurityHandler(String componentId);
   
    /**
     * Remove the SecurityHandler for the component, this method should be called when 
     * the component is shutdown or uninstalled.
     *
     * @param componentId is the Component Id
     */
    void removeSecurityHandler(String componentId);
    
}
