/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package com.sun.jbi.management;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

/**
 * Starting from 2.4, there is a unique version of the OpenESB Bus. So we are
 * now using a concrete VersionInfo class without the use of template.
 *
 * TODO: Retrieving information from MANIFEST.mf
 *
 * @author David BRASSELY
 * @since 2.4
 */
public class VersionInfo implements com.sun.jbi.VersionInfo {

    /**
     * @return the full product name.
     */
    public String fullProductName() {
        return "Open Enterprise Service Bus";
    }

    /**
     * @return the short product name.
     */
    public String shortProductName() {
        return "Open_ESB";
    }

    /**
     * @return the major version number.
     */
    public String majorVersion() {
        return getManifestImplementationMajorVersion();
    }

    /**
     * @return the minor version number.
     */
    public String minorVersion() {
        return getManifestImplementationMinorVersion();
    }

    /**
     * @return the build number for this version.
     */
    public String buildNumber() {
        return getManifestImplementationBuildNumber();
    }

    /**
     * @return the Copyright for this version.
     */
    public String copyright() {
        return ("Under Common Development and Distribution License (CDDL). For more information, please visit www.open-esb.net");
    }

    private static String getManifestImplementationMajorVersion() {
        Attributes attrs = getManifestAttributes();
        if (attrs != null) {
            String version = attrs.getValue("Implementation-MajorVersion");
            if (version != null) {
                return version;
            }
        }
        
        return null;
    }
    
    private static String getManifestImplementationMinorVersion() {
        Attributes attrs = getManifestAttributes();
        if (attrs != null) {
            String version = attrs.getValue("Implementation-MinorVersion");
            if (version != null) {
                return version;
            }
        }
        
        return null;
    }
    
    private static String getManifestImplementationBuildNumber() {
        Attributes attrs = getManifestAttributes();
        if (attrs != null) {
            String version = attrs.getValue("Implementation-Build");
            if (version != null) {
                return version;
            }
        }
        
        return null;
    }

    private static Attributes getManifestAttributes() {
        try {
            URL url = VersionInfo.class.getProtectionDomain().getCodeSource().getLocation();
            JarFile jar = new JarFile(url.getFile());
            
            return jar.getManifest().getMainAttributes();
        } catch (IOException e1) {
            // Silently ignore wrong manifests on classpath?
        }

        return null;
    }
}