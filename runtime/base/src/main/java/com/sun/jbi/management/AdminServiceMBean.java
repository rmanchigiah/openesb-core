/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AdminServiceMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import javax.management.ObjectName;

/**
 * This interface extends the JBI AdminService interface. The runtime facade 
 * AdminService implements this interface. This interface has additional 
 * operations : 
 * 
 * <ul>
 *   <li>Find component extension facade MBean name for a component </li>
 * </ul>
 *
 * @author JSR208 Expert Group
 */
public interface AdminServiceMBean
        extends javax.jbi.management.AdminServiceMBean
{
    /**
     * Query the component extension facade MBeanName.
     * 
     * @param componentName - component identification
     * @return JMX object name of the components Extension facade MBean, or 
     *         <code>null</code> if there is no such MBean registered.
     */
    ObjectName getComponentExtensionFacadeMBean(String componentName);
}
