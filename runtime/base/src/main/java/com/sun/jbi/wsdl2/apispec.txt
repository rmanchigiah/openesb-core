#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)apispec.txt
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
// Copyright (C) 2004, Sun Microsystems Inc. All rights reserved.
/*
 * apispec.txt
 *
 * SUN PROPRIETARY/CONFIDENTIAL.
 * This software is the proprietary information of Sun Microsystems, Inc.
 * Use is subject to license terms.
 *
 */

// API spec for WSDL 2.0 Components. This is used to feed the API code generator,
// which produces the interfaces and abstract implementation base classes for the
// component-view of WSDL exposed by the WSDL 2.0 API.
//
// HISTORY
//  2004.10.26 rtenhove
//    Revised for August 2004 "Last Call" Draft of WSDL 2.0 Spec.
//
//  2004.04.12 rtenhove
//    Revised for March 2004 Draft of WSDL 2.0 Spec.
//
//  2004.03.08 rtenhove
//    Implemented documentation and extensibility support.
//
//  2004.03.03 rtenhove
//    Cleaned up some checkstyle errors.
//
//  2004.02.27 rtenhove
//    Initial version based on draft WSDL 2.0 schema from December, 2003.
//

API [desc = "WSDL 2.0 API" package = "com.sun.jbi.wsdl2"
                      impl-package = "com.sun.jbi.wsdl2.impl"]


////////////////////////////////////////////////////////////////////////////////
component ExtensibleDocumentedComponent 
    [desc        = "Base for extensible, documented components"
     impl-import = "org.apache.xmlbeans.XmlObject"
     impl-import = "org.w3.x2004.x08.wsdl.ExtensibleDocumentedType"]
(
  { 
    /** 
     * The base XML object we are dealing with. This can be downcast to 
     * ExtensibleDocumentedType safely. 
     */
    protected XmlObject mXmlObject;

    /**
     * Get the extensible, documented XML bean for this (abstract) component.
     *
     * @return The extensible, documented XML bean for this component.
     */
    protected final ExtensibleDocumentedType getExBean()
    {
        return (ExtensibleDocumentedType) this.mXmlObject;
    }

     /** The extensions for this component, if any. */
    private ExtensionsImpl mExtensions = null;

    /**
     * Construct a new extensible, documented component whose contents
     * are within the given XML bean.
     *
     * @param xmlObject The XML bean containing this component's contents.
     */
    ExtensibleDocumentedComponent(XmlObject xmlObject)
    {
        this.mXmlObject = xmlObject;
    }

    /**
     * Get the container for this component.
     *
     * @return The container for this component.
     */
    protected abstract DefinitionsImpl getContainer();
  }

  prop java.util.Map wsdlAttributeNameMap [desc = "Map of WSDL-defined attribute QNames for this component, indexed by QName.toString()" read-only = "true"];

  prop Document   document   [desc = "documentation for component, if any"]
  get {
        return DocumentImpl.Factory.getInstance(
            getExBean().getDocumentation(), 
            this, 
            getContainer());
  }
  set {
        getExBean().setDocumentation(
            theDocument != null ? ((DocumentImpl) theDocument).getBean()
                                : null);  
  };

  prop Extensions extensions [desc = "extensions for component, if any"]
  get {
        return this.mExtensions;
  }
  set {
        return; // $$TODO
  };
)


////////////////////////////////////////////////////////////////////////////////
component Binding subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 Binding component." 
     import      = "javax.xml.namespace.QName"
     import      = "org.w3c.dom.DocumentFragment"
     impl-import = "org.w3.x2004.x08.wsdl.BindingType"]
(
{
    /**
     * Get the BindingType XML bean behind this Binding component.
     *
     * @return The BindingType XML bean behind this component.
     */
    protected final BindingType getBean()
    {
        return (BindingType) this.mXmlObject;
    }

    /**
     * Construct an abstract Binding implementation base component.
     * 
     * @param bean      The XML bean for this Binding component
     */
    Binding(BindingType bean)
    {
        super(bean);
    }
}
  prop String             targetNamespace [desc = "Target namespace of this binding" read-only="true"];

  prop String             name       [desc = "Local name of this binding component"]
    get 
    {
        return getBean().getName(); 
    } 
    set 
    {
        getBean().setName(theName); 
    };

  prop QName              qName      [desc = "Qualified name of this component" read-only = "true"];
  prop Interface          interface  [desc = "Interface being bound by this binding definition, if any"];
  prop BindingFault[]     faults     [desc = "Faults bound by this binding definition" access = "indexed"];
  prop BindingOperation[] operations [desc = "Named binding operations, if any" access="indexed"];
  prop Feature[]          features   [desc = "Features of this binding, if any"   access = "indexed"];
  prop Property[]         properties [desc = "Properties of this binding, if any" access = "indexed" 
                                     singularName = "property"];

  method BindingOperation [desc = "Newly created operation, appended to this binding's operation list."]
  addNewOperation() 
      [desc = "Create a new operation, appending it to this binding's operations list."];

  method BindingFault [desc = "Newly created binding fault, appended to the faults list."]
  addNewBindingFault(
    InterfaceFault ref [desc = "Interface fault to which the new binding fault adds binding information"])
      [desc = "Create a new binding fault, appending it to this binding's faults list."];
      
  method Feature [desc = "Newly created feature, appended to the features list."]
  addNewFeature(
    QName   uri        [desc = "Identifier for this feature."]  
    boolean isRequired [desc = "True if this feature is to be marked as 'required.'"]
    ) [desc = "Create a new feature, and append it to this binding's features list."];

  method Property [desc = "Newly created property, appended to the properties list."]
  addNewProperty(
    QName   uri        [desc = "Identifier for this property."]  
    boolean isRequired [desc = "True if this property is to be marked as 'required.'"]
    ) [desc = "Create a new property, and append it to this binding's properties list."];

  method String [desc = "This binding, serialized as an XML string."]
  toXmlString() [desc = "Return this WSDL binding as an XML string."];

  method DocumentFragment [desc = "This binding, as a DOM document fragment."]
  toXmlDocumentFragment() [desc = "Return this binding as a DOM document fragment. The DOM subtree is a copy; altering it will not affect this binding."];
)


////////////////////////////////////////////////////////////////////////////////
component BindingOperation subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 Binding Operation Component."
     import      = "javax.xml.namespace.QName"
     impl-import = "org.w3.x2004.x08.wsdl.BindingOperationType"]
(
{
    /**
     * Get the BindingOperationType XML bean behind this BindingOperation component.
     *
     * @return The BindingOperationType XML bean behind this component.
     */
    protected final BindingOperationType getBean()
    {
        return (BindingOperationType) this.mXmlObject;
    }

    /**
     * Construct an abstract Binding operation implementation base component.
     * 
     * @param bean      The XML bean for this Binding Operation component
     */
    BindingOperation(BindingOperationType bean)
    {
        super(bean);
    }
}
  prop QName      interfaceOperation [desc = "Qualified name of Interface Operation being bound"];
  prop Feature[]  features           [desc = "Features of this interface operation, if any"   access = "indexed"];
  prop Property[] properties         [desc = "Properties of this interface operation, if any" access = "indexed" 
                                      singularName = "property"];

  prop BindingMessageReference[] inputs     [desc = "Input binding message references"     access = "indexed"];
  prop BindingMessageReference[] outputs    [desc = "Output binding message references"    access = "indexed"];

  method String [desc = "Target namespace of this operation"]
  getTargetNamespace() [desc = "Get the target namespace of this operation"];

  method Feature [desc = "Newly created feature, appended to the features list."]
  addNewFeature(
    QName   uri        [desc = "Identifier for this feature."]  
    boolean isRequired [desc = "True if this feature is to be marked as 'required.'"]
    ) [desc = "Create a new feature, and append it to this operation's features list."];

  // Factory methods:
  method Property [desc = "Newly created property, appended to the properties list."]
  addNewProperty(
    QName   uri        [desc = "Identifier for this property."]  
    boolean isRequired [desc = "True if this property is to be marked as 'required.'"]
    ) [desc = "Create a new property, and append it to this operation's properties list."];

  method BindingMessageReference [desc = "The newly created input message reference, appended to this operation."]
  addNewInput(
    String messageLabel [desc = "NC role name in the {message exchange pattern} of the message for which binding details are provided"]
    ) [desc = "Create a new message reference for this operation, appending it to this operation's input list."];

  method BindingMessageReference [desc = "The newly created output message reference, appended to this operation."]
  addNewOutput(
    String messageLabel [desc = "NC name of the message for which binding details are provided."]
    ) [desc = "Create a new message reference for this operation, appending it this operation's output list."];
)


////////////////////////////////////////////////////////////////////////////////
component BindingMessageReference subclasses ExtensibleDocumentedComponent 
    [desc       = "WSDL 2.0 Binding message reference component." 
     impl-import = "org.w3.x2004.x08.wsdl.BindingOperationMessageType"]
(
{
    /**
     * The the Xml bean for this component.
     *
     * @return Xml Bean for this component.
     */
    BindingOperationMessageType getBean()
    {
        return (BindingOperationMessageType) this.mXmlObject;
    }

    /**
     * Construct an abstract Binding operation message implementation base 
     * component.
     * 
     * @param bean The XML bean for this Binding operation message component.
     */
    BindingMessageReference(BindingOperationMessageType bean)
    {
        super(bean);
    }
}
  prop String    messageLabel [desc = "Message exchange pattern role identifier"];
  prop Direction direction    [desc = "Direction of this message in the exchange"];
)

////////////////////////////////////////////////////////////////////////////////
component BindingFault subclasses ExtensibleDocumentedComponent
    [desc = "WSDL 2.0 Binding Fault container"
     import      = "javax.xml.namespace.QName"
     impl-import = "org.w3.x2004.x08.wsdl.BindingFaultType"]
(
{
    /**
     * Get the XML bean for this binding fault component.
     *
     * @return The definitions XML bean for this component.
     */
    protected final BindingFaultType getBean()
    {
        return (BindingFaultType) this.mXmlObject;
    }

    /**
     * Construct an abstract Binding fault implementation base component.
     * 
     * @param bean The XML bean for this Binding Fault component
     */
    BindingFault(BindingFaultType bean)
    {
        super(bean);
    }
}

  prop QName ref [desc = "QName of Interface Fault that is bound to this binding fault"];
)


////////////////////////////////////////////////////////////////////////////////
component Definitions subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 Definitions container" 
     import      = "javax.xml.namespace.QName"
     impl-import = "org.w3.x2004.x08.wsdl.DefinitionsType"]
(
{
    /**
     * Get the XML bean for this definitions component.
     *
     * @return The definitions XML bean for this component.
     */
    protected final DefinitionsType getBean()
    {
        return (DefinitionsType) this.mXmlObject;
    }

    /**
     * Construct an abstract Definitions fault reference implementation base component.
     * 
     * @param bean The XML bean for this Definitions component
     */
    Definitions(DefinitionsType bean)
    {
        super(bean);
    }
}

  prop Binding[]   bindings        [desc = "Bindings for this component"   access="indexed"];
  prop String      documentBaseUri [desc = "Base URI for the document for this wsdl:definitions component"];
  prop Import[]    imports         [desc = "Definitions imported into this container" access="indexed"];
  prop Include[]   includes        [desc = "Definitions included in this container" access="indexed"];
  prop Interface[] interfaces      [desc = "Interfaces for this component" access="indexed"];
  prop Service[]   services        [desc = "Services for this component"   access="indexed"];
  prop String      targetNamespace [desc = "Namespace for components in this container"];
  prop Types[]     types           [desc = "Types (in schema or whatever)" access="indexed"];

  method Binding [desc="Named Binding; null if none found."] 
  findBinding(QName name [desc = "Name of binding to find."]) 
	[desc = "Find named binding in this definition or the imported/included bindings."];

  method Interface [desc="Named Interface; null if none found."] 
  findInterface(QName name [desc = "Name of interface to find."]) 
	[desc = "Find named interface in this definition or the imported/included interfaces."];

  method Service [desc="Named Service; null if none found."] 
  findService(QName name [desc = "Name of service to find."]) 
	[desc = "Find named service in this definition or the imported/included services."];

  // Factory methods
  method Binding [desc = "Newly created binding, appended to the bindings list."]
  addNewBinding(String name [desc = "Name of binding to create."])
        [desc = "Create a new binding component, appended to this definition's binding list."];

  method Import [desc = "Newly created import, appended to the imports list."]
  addNewImport()
        [desc = "Create a new import component, appended to this definition's import list."];

  method Include [desc = "Newly created include, appended to the includes list."]
  addNewInclude()
        [desc = "Create a new include, appended to this definition's include list."];

  method Interface [desc="Newly created interface, appended to interfaces list."]
  addNewInterface(String name [desc = "Name of interface to create."])
        [desc = "Create a new interface component, appended to this definition's interface list."];

  method Service [desc = "Newly created service, appended to the services list."]
  addNewService(String name [desc = "Name of service to create."])
        [desc = "Create a new service component, appended to this definition's service list."];

  method Types [desc = "Newly created Types component."]
  newTypes()
        [desc = "Create a new types component, replacing the existing types component of this definition, if necessary."];

  method String [desc = "This definition, serialized as an XML string."]
  toXmlString() [desc = "Return this WSDL definition as an XML string."];

  method org.w3c.dom.Document [desc = "This definition, as a DOM document."]
  toXmlDocument() [desc = "Return this document as a DOM document. The DOM tree is a copy; altering it will not affect this definitions component."];
)


////////////////////////////////////////////////////////////////////////////////
component Document 
    [desc   = "Documentation for a WSDL, component"
     import = "org.w3c.dom.DocumentFragment"]
(
  prop DocumentFragment documentation [desc = "DOM document fragment containing documentation"];
)


////////////////////////////////////////////////////////////////////////////////
component Endpoint subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 Endpoint component" 
     import      = "org.w3c.dom.DocumentFragment"
     impl-import = "org.w3.x2004.x08.wsdl.EndpointType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return Xml Bean for this component.
     */
    EndpointType getBean()
    {
        return (EndpointType) this.mXmlObject;
    }

    /**
     * Construct an abstract endpoint implementation base component.
     * 
     * @param bean      The XML bean for this endpoint component
     */
    Endpoint(EndpointType bean)
    {
        super(bean);
    }
}

  prop String  name    [desc = "Name of this endpoint component"];
  prop Binding binding [desc = "Binding for this endpoint"];

  method String [desc = "This endpoint, serialized as an XML string."]
  toXmlString() [desc = "Return this WSDL endpoint as an XML string."];

  method DocumentFragment [desc = "This endpoint, as a DOM document fragment."]
  toXmlDocumentFragment() [desc = "Return this endpoint as a DOM document fragment. The DOM subtree is a copy; altering it will not affect this endpoint."];
)


////////////////////////////////////////////////////////////////////////////////
component Extensions 
    [desc   = "Extension elements and attributes for a WSDL component."
     import = "org.w3c.dom.Attr" 
     import = "org.w3c.dom.Element"]
(
  prop Element[] elements   [desc = "Extension elements" access="indexed"];
  prop Attr[]    attributes [desc = "Extension attributes" access="indexed"];
)


////////////////////////////////////////////////////////////////////////////////
component Feature subclasses ExtensibleDocumentedComponent
    [desc       = "WSDL 2.0 Feature component" 
     import      = "javax.xml.namespace.QName"
     impl-import = "org.w3.x2004.x08.wsdl.FeatureType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return Xml Bean for this component.
     */
    protected final FeatureType getBean()
    {
        return (FeatureType) this.mXmlObject;
    }

    /**
     * Construct an abstract feature implementation base component.
     * 
     * @param bean      The XML bean for this feature component
     */
    Feature(FeatureType bean)
    {
        super(bean);
    }
}
  prop QName   uri      [desc = "URI of this feature"];
  prop boolean required [desc = "True if this feature is mandatory"];
)


////////////////////////////////////////////////////////////////////////////////
component Import subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 Import component." 
     impl-import = "org.w3.x2004.x08.wsdl.ImportType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final ImportType getBean()
    {
        return (ImportType) this.mXmlObject;
    }

    /**
     * Construct an abstract Import implementation base component.
     * 
     * @param bean      The XML bean for this Import component
     */
    Import(ImportType bean)
    {
        super(bean);
    }
}

  prop String namespace [desc = "Target namespace of import (must be different that this Definitions TNS)"];
  prop String location  [desc = "Location hint, if any"];

  method Definitions [desc = "Definitions from this import component, if any"]
  getDefinitions() [desc = "Get the definitions from this import component"];
)


////////////////////////////////////////////////////////////////////////////////
component Include subclasses ExtensibleDocumentedComponent 
    [desc        = "WSDL 2.0 Include component." 
     impl-import = "org.w3.x2004.x08.wsdl.IncludeType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final IncludeType getBean()
    {
        return (IncludeType) this.mXmlObject;
    }

    /**
     * Construct an abstract Include implementation base component.
     * 
     * @param bean      The XML bean for this Include component
     */
    Include(IncludeType bean)
    {
        super(bean);
    }
}
  prop String location  [desc = "Location hint for the included definitions"];

  method Definitions [desc = "Definitions from included by this component, if any"]
  getDefinitions() [desc = "Get the definitions included by this component"];
)


////////////////////////////////////////////////////////////////////////////////
component Interface subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 Interface Component." 
     import      = "javax.xml.namespace.QName"
     import      = "java.net.URI"
     impl-import = "org.w3.x2004.x08.wsdl.InterfaceType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final InterfaceType getBean()
    {
        return (InterfaceType) this.mXmlObject;
    }

    /**
     * Construct an abstract Interface implementation base component.
     * 
     * @param bean      The XML bean for this Interface component
     */
    Interface(InterfaceType bean)
    {
        super(bean);
    }
}

  prop String      targetNamespace [desc = "Target namespace of this binding" read-only="true"];
  prop String      name            [desc = "Local name of this interface component"];
  prop QName       qName           [desc = "Qualified name of this component" read-only = "true"];
 
  prop Interface[] interfaces 
       [desc = "Interfaces this interface extends, if any" access = "indexed"];
  prop InterfaceFault[] faults
       [desc = "Faults that may occur executing operations of this interface" access = "indexed"];
  prop URI[]       styleDefault
       [desc = "The default styles used to construct the {element} properties of {message references} of all operations contained in this interface"];
  prop Feature[]   features   
       [desc = "Features of this interface, if any"   access = "indexed"];
  prop Property[]  properties 
       [desc = "Properties of this interface, if any" access = "indexed" singularName = "property"];
  prop InterfaceOperation[] operations 
       [desc = "operations defined by this interface only" access = "indexed"];

  method Interface[] [desc = "Extended interfaces, including those extended by this interface's super-interfaces"]
  getExtendedInterfaces() [desc = "Get the interfaces extended by this interface, including those extended by this interface's super-interfaces."];
 
  method InterfaceOperation[] [desc = "Operations defined by this interface, and all super-interfaces"]
  getExtendedOperations() [desc = "Get the operations defined by this interface, and all its super-interfaces"];

  method InterfaceOperation [desc = "Newly created operation, appended to the operations list."]
  addNewOperation() 
      [desc = "Create a new operation, appending it to this interface's operations list."];

  method InterfaceFault [desc = "Newly created interface, appended to the faults list."]
  addNewFault(
    String name [desc = "Name of the new fault"]
  ) [desc = "Create a new fault, and append it to this interface's faults list."];

  method Feature [desc = "Newly created feature, appended to the features list."]
  addNewFeature(
    QName   uri        [desc = "Identifier for this feature."]  
    boolean isRequired [desc = "True if this feature is to be marked as 'required.'"]
    ) [desc = "Create a new feature, and append it to this interface's features list."];

  method Property [desc = "Newly created property, appended to the properties list."]
  addNewProperty(
    QName   uri        [desc = "Identifier for this property."]  
    boolean isRequired [desc = "True if this property is to be marked as 'required.'"]
    ) [desc = "Create a new property, and append it to this interface's properties list."];
)


////////////////////////////////////////////////////////////////////////////////
component InterfaceFault subclasses ExtensibleDocumentedComponent
    [desc = "WSDL 2.0 Interface Fault component."
     import      = "javax.xml.namespace.QName"
     impl-import = "org.w3.x2004.x08.wsdl.InterfaceFaultType"]
(
{
    /**
     * Get the XML bean for this interface fault component.
     *
     * @return The definitions XML bean for this component.
     */
    protected final InterfaceFaultType getBean()
    {
        return (InterfaceFaultType) this.mXmlObject;
    }

    /**
     * Construct an abstract Interface Fault implementation base component.
     * 
     * @param bean The XML bean for this Interface Fault component
     */
    InterfaceFault(InterfaceFaultType bean)
    {
        super(bean);
    }
}
  prop String name    [desc = "The name of this component"];
  prop QName  element [desc = "Qualified name referring to the element declaration of the payload of the fault"];
  prop QName  qualifiedName [desc = "Qualified name of this component" read-only="true"];
)


////////////////////////////////////////////////////////////////////////////////
component InterfaceOperation subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 interface operation component." 
     import      = "javax.xml.namespace.QName"
     import      = "org.w3c.dom.DocumentFragment"
     impl-import = "org.w3.x2004.x08.wsdl.InterfaceOperationType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final InterfaceOperationType getBean()
    {
        return (InterfaceOperationType) this.mXmlObject;
    }

    /**
     * Construct an abstract Interface operation implementation base component.
     * 
     * @param bean      The XML bean for this Interface operation component
     */
    InterfaceOperation(InterfaceOperationType bean)
    {
        super(bean);
    }
}
  prop String      name       [desc = "Name of this interface operation component"];
  prop String      pattern    [desc = "Message exchange pattern URI for this operation"];
  prop String      style      [desc = "A URI identifying the message construction rules for a message reference"];
  prop Feature[]   features   [desc = "Features of this interface operation, if any"   access = "indexed"];
  prop Property[]  properties [desc = "Properties of this interface operation, if any" access = "indexed" 
                               singularName = "property"];
  prop boolean     safe       [desc = "True if the operation is safe, as defined by the W3C Web Services Architecture document"];

  prop MessageReference[]      inputs     [desc = "Input message references"     access = "indexed"];
  prop MessageReference[]      outputs    [desc = "Output message references"    access = "indexed"];
  prop MessageFaultReference[] inFaults   [desc = "In fault message references"  access = "indexed"];
  prop MessageFaultReference[] outFaults  [desc = "Out fault message references" access = "indexed"];
  
  prop QName qualifiedName [desc = "Qualified name of this interface operation" read-only="true"];

  // Factory methods
  method Feature [desc = "Newly created feature, appended to the features list."]
  addNewFeature(
    QName   uri        [desc = "Identifier for this feature."]  
    boolean isRequired [desc = "True if this feature is to be marked as 'required.'"]
    ) [desc = "Create a new feature, and append it to this operations's features list."];

  method Property [desc = "Newly created property, appended to the properties list."]
  addNewProperty(
    QName   uri        [desc = "Identifier for this property."]  
    boolean isRequired [desc = "True if this property is to be marked as 'required.'"]
    ) [desc = "Create a new property, and append it to this operation's properties list."];

  method MessageReference [desc = "The newly created input message reference."]
  addNewInput() [desc = "Create a new input message reference for this operation, and append it to this operation's input list."];

  method MessageReference [desc = "The newly created input message reference."]
  addNewOutput() [desc = "Create a new output message reference for this operation, and append it to this operation's output list."];

  method MessageFaultReference [desc = "The newly created in-fault reference."]
  addNewInFault(
    InterfaceFault fault [desc = "Fault referenced by the new in-fault"]
    ) [desc = "Create a new messsage in-fault reference for this operation, and append it to the operation's in-fault list."];

  method MessageFaultReference [desc = "The newly created out-fault reference."]
  addNewOutFault(
    InterfaceFault fault [desc = "Fault referenced by the new out-fault"]
    ) [desc = "Create a new messsage out-fault reference for this operation, and append it to the operation's out-fault list."];
    
  method String [desc = "This operation, serialized as an XML string."]
  toXmlString() [desc = "Return this WSDL interface operation as an XML string."];

  method DocumentFragment [desc = "This operation, as a DOM document fragment."]
  toXmlDocumentFragment() [desc = "Return this interface operation as a DOM document fragment. The DOM subtree is a copy; altering it will not affect this service."];
)


////////////////////////////////////////////////////////////////////////////////
component MessageFaultReference subclasses ExtensibleDocumentedComponent
     [desc        = "WSDL 2.0 Fault Message Reference Component." 
      import      = "javax.xml.namespace.QName"
      impl-import = "org.w3.x2004.x08.wsdl.MessageRefFaultType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final MessageRefFaultType getBean()
    {
        return (MessageRefFaultType) this.mXmlObject;
    }

    /**
     * Construct an abstract Message Fault Reference implementation base component.
     * 
     * @param bean      The XML bean for this Message Fault Reference component
     */
    MessageFaultReference(MessageRefFaultType bean)
    {
        super(bean);
    }
}
  prop QName      ref          [desc = "Fault component describing the fault referred to by the component"];
  prop String     messageLabel [desc = "Message exchange pattern role label"];
  prop Direction  direction    [desc = "Direction of this message in the exchange"];
)


////////////////////////////////////////////////////////////////////////////////
component MessageReference subclasses ExtensibleDocumentedComponent
     [desc        = "WSDL 2.0 Message Reference Component." 
      import      = "javax.xml.namespace.QName"
      impl-import = "org.w3.x2004.x08.wsdl.MessageRefType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final MessageRefType getBean()
    {
        return (MessageRefType) this.mXmlObject;
    }

    /**
     * Construct an abstract Message Reference implementation base component.
     * 
     * @param bean      The XML bean for this Message Reference component
     */
    MessageReference(MessageRefType bean)
    {
        super(bean);
    }
}
  prop String    messageLabel [desc = "Message exchange pattern role label"];
  prop Object    element      [desc = "Content (String) or payload element type declaration (QName)"];
  prop Direction direction    [desc = "Direction of this message in the exchange"];
  prop MessageContentModel messageContentModel [desc = "Content model type: #ANY, #NONE, or #ELEMENT" read-only="true"];
)


////////////////////////////////////////////////////////////////////////////////
component Property subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 Property component" 
     import      = "javax.xml.namespace.QName"
     impl-import = "org.w3.x2004.x08.wsdl.PropertyType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final PropertyType getBean()
    {
        return (PropertyType) this.mXmlObject;
    }

    /**
     * Construct an abstract Property implementation base component.
     * 
     * @param bean      The XML bean for this Property component
     */
    Property(PropertyType bean)
    {
        super(bean);
    }
}
  prop QName   uri         [desc = "URI of this property"];
  prop boolean required    [desc = "True if this property is mandatory"];
  prop String  stringValue [desc = "Value of this property, as a string"];
  prop QName   constraint  [desc = "Constraint for this property"];
)


////////////////////////////////////////////////////////////////////////////////
component Service subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 Service component." 
     import      = "javax.xml.namespace.QName"
     import      = "org.w3c.dom.DocumentFragment"
     impl-import = "org.w3.x2004.x08.wsdl.ServiceType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final ServiceType getBean()
    {
        return (ServiceType) this.mXmlObject;
    }

    /**
     * Construct an abstract Service implementation base component.
     * 
     * @param bean      The XML bean for this Service component
     */
    Service(ServiceType bean)
    {
        super(bean);
    }
}

  prop String     targetNamespace [desc = "Target namespace of this binding" read-only="true"];
  prop String     name            [desc = "Local name of this service component"];
  prop QName      qName           [desc = "Qualified name of this component" read-only = "true"];
  prop Interface  interface       [desc = "Interface provided by this service"];
  prop Endpoint[] endpoints       [desc = "Endpoints for this service" access = "indexed"];

  method Endpoint [desc = "The newly created endpoint, appended to this service's endpoint list."]
  addNewEndpoint(
    String  name    [desc = "NC name for the new endpoint."]
    Binding binding [desc = "Binding to which the endpoint refers."]
    ) [desc = "Create a new end point component, appending it to this service's endpoint list."];

  method String [desc = "This service, serialized as an XML string."]
  toXmlString() [desc = "Return this WSDL service as an XML string."];

  method DocumentFragment [desc = "This service, as a DOM document fragment."]
  toXmlDocumentFragment() [desc = "Return this service as a DOM document fragment. The DOM subtree is a copy; altering it will not affect this service."];
)


////////////////////////////////////////////////////////////////////////////////
component Types subclasses ExtensibleDocumentedComponent
    [desc        = "WSDL 2.0 type definitions component." 
     impl-import = "org.w3.x2004.x08.wsdl.TypesType"]
(
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final TypesType getBean()
    {
        return (TypesType) this.mXmlObject;
    }

    /**
     * Construct an abstract Types implementation base component.
     * 
     * @param bean      The XML bean for this Types component
     */
    Types(TypesType bean)
    {
        super(bean);
    }
}
)
