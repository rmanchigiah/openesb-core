/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceLifecycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi;

/**
 * ServiceLifecycle is an interface implemented by all internal services.
 * This interface defines the lifecycle contract between the JBI framework
 * and the service.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ServiceLifecycle
{
    /**
     * Initialize a service. This performs any initialization tasks
     * required by the service but does not make the service ready
     * to process requests.
     * @param aContext the JBI environment context created
     * by the JBI framework
     * @throws javax.jbi.JBIException if an error occurs
     */
    void initService(EnvironmentContext aContext)
        throws javax.jbi.JBIException;

    /**
     * Start a service. This makes the service ready to process requests.
     * @throws javax.jbi.JBIException if an error occurs
     */
    void startService()
        throws javax.jbi.JBIException;

    /**
     * Stop a service. This makes the service stop processing requests.
     * @throws javax.jbi.JBIException if an error occurs
     */
    void stopService()
        throws javax.jbi.JBIException;

}
