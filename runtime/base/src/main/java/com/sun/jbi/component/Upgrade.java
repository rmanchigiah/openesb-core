/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Upgrade.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.component;

import java.util.List;
import javax.jbi.JBIException;

/**
 * This is an optional interface that can be implemented by a JBI component to
 * provide a way to upgrade to a new version of the component. It is called by
 * the JBI runtime during the upgrade processing initiated by a client, which
 * can be either an ant task, an asadmin command, or an action in the admin
 * console.
 *
 * @author Mark S White
 */
public interface Upgrade
{
    /**
     * This method is called by the JBI runtime to allow a component to upgrade
     * its workspace and any existing Service Units to match the new version of
     * the component. The JBI runtime copies the existing workspace root to the
     * new workspace root prior to calling this method, so that the component
     * can examine the contents of the workspace root to determine the version
     * of the component from which the upgrade is being made. All updates to the
     * workspace root and Service Unit roots are done in-place; in the event of
     * a failure, the JBI runtime reverts back to the original workspace root
     * and the Service Unit roots.
     * <p>
     * Note that the component must ensure that it never holds open references
     * to any files in the workspace root or any of the Service Unit roots upon
     * returning from this method. Failure to do so will cause problems when
     * the runtime attempts to complete the upgrade processing.
     * 
     * @param workspaceRoot the workspace root for the new version of the
     * component that will replace the currently installed version. This is
     * populated with the contents of the original workspace root and the
     * component must update it to match the new version of the component.
     * @param serviceUnitRoots a list of directory paths to all of the Service
     * Units currently deployed to the component. The component must update all
     * of these to match the new version of the component.
     * @exception JBIException when there is an error requiring that the upgrade
     * be terminated.
     */
    void upgrade(String workspaceRoot, List<String> serviceUnitRoots)
        throws JBIException;
}
