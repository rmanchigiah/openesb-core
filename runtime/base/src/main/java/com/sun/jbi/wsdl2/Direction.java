/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Direction.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * This class enumerates the legal values for a {message reference} direction
 * attribute.
 * 
 * @author Sun Microsystems, Inc.
 */
public final class Direction 
{
    /** The "in" direction. */
    public static final Direction IN  = new Direction("in");

    /** The "out" direction. */
    public static final Direction OUT = new Direction("out");

    /**
     * Get this direction's value as a String value.
     *
     * @return this direction's string value.
     */
    public String toString()
    {
        return this.mValue;
    }
  
    /** The value of this direction */
    private String    mValue;

    /**
     * Private constructor for a direction instance. This is private to 
     * guarantee there are no other Direction instances possible other than the
     * class (static) fields of this class. (This is a standard pattern for
     * creating a type-safe enumeration in Java 1.4 and earlier.)
     * 
     * @param value The value of the direction.
     */
    private Direction(String value)
    {
        this.mValue = value;
    }
}
