/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MBeanNames.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import java.util.logging.Logger;

import com.sun.jbi.ComponentType;
import javax.management.ObjectName;

/**
 * Extends the functionality of the public javax.jbi.management.MBeanNames
 * interface, by defining constants used in MBean ObjectName attributes, and by
 * providing additional operations to support multiple named logger MBeans.
 * 
 * @author Sun Microsystems, Inc.
 */
public interface MBeanNames extends javax.jbi.management.MBeanNames {

    
    /**
     * System services' names. This will be used in the MBean
     * Object names as
     * com.sun.jbi:Target={target},ServiceName={serviceName},ServiceType={serviceType}
     * com.sun.jbi:Target=domain,ServiceName=InstallationService,ServiceType=Installation
     */
    public enum ServiceName {
        AdminService,
        ConfigurationService,
        DeploymentService,
        FileTransferService, 
        Framework, 
        InstallationService
    };

    /**
     * System services' types. This will be used in the MBean
     * Object names as
     * com.sun.jbi:Target={target},ServiceName={serviceName},ServiceType={serviceType}
     * com.sun.jbi:Target=domain,ServiceName=InstallationService,ServiceType=Installation
     */
    public enum ServiceType 
    {
        Admin,
        Deployment,
        Download,
        Installation,
        Logger,
        Notification,
        System,
        Upload
    };

    public enum ComponentServiceType 
    {
        Installer, ComponentLifeCycle, InstallerConfiguration, Configuration, Extension
    };

    /**
     * The Objectname attribute key for the JBI runtime instance name. Value
     * is "JbiName".
     */
    String INSTANCE_NAME_KEY = "JbiName";

    /**
     * The name of the JBI runtime AdminService management service. Value is
     * "AdminService".
     */
    String SERVICE_NAME_ADMIN_SERVICE = "AdminService";

    /**
     * The name of the JBI runtime ConfigurationService management service.
     * Value is "ConfigurationService".
     */
    String SERVICE_NAME_CONFIG_SERVICE = "ConfigurationService";

    /**
     * The name of the JBI runtime DeploymentService management service. Value
     * is "DeploymentService".
     */
    String SERVICE_NAME_DEPLOY_SERVICE = "DeploymentService";

    /**
     * The name of the JBI runtime InstallationService management service.
     * Value is "InstallationService".
     */
    String SERVICE_NAME_INSTALL_SERVICE = "InstallationService";

    /**
     * The name of the JBI runtime LoggingService management service. Value is
     * "LoggingService".
     */
    String SERVICE_NAME_LOGGING_SERVICE = "LoggingService";

    /**
     * The name of the JBI runtime MessageService service. Value is
     * "MessageService".
     */
    String SERVICE_NAME_MESSAGE_SERVICE = "MessageService";

    /**
     * The name of the JBI runtime Framework service. Value is "Framework".
     */
    String SERVICE_NAME_FRAMEWORK = "Framework";

    /**
     * The name of the JBI runtime top-level service. Value is "JBI".
     */
    String SERVICE_NAME_JBI = "JBI";

    /**
     * The property name key for the MBean type. Value is "ControlType".
     */
    String CONTROL_TYPE_KEY = "ControlType";

    /**
     * The JBI runtime AdministrationService MBean. Value is
     * "AdministrationService".
     */
    String CONTROL_TYPE_ADMIN_SERVICE = "AdministrationService";

    /**
     * The JBI runtime ConfigurationService MBean. Value is
     * "ConfigurationService".
     */
    String CONTROL_TYPE_CONFIG_SERVICE = "ConfigurationService";

    /**
     * The JBI runtime DeploymentService MBean. Value is "DeploymentService".
     */
    String CONTROL_TYPE_DEPLOY_SERVICE = "DeploymentService";

    /**
     * The JBI runtime InstallationService MBean. Value is
     * "InstallationService".
     */
    String CONTROL_TYPE_INSTALL_SERVICE = "InstallationService";

    /**
     * The JBI runtime LoggingService MBean. Value is "LoggingService".
     */
    String CONTROL_TYPE_LOGGING_SERVICE = "LoggingService";

    /**
     * The JBI runtime MessageService MBean. Value is "MessageService".
     */
    String CONTROL_TYPE_MESSAGE_SERVICE = "MessageService";

    /**
     * A Lifecycle MBean. Value is "Lifecycle".
     */
    String CONTROL_TYPE_LIFECYCLE = "Lifecycle";

    /**
     * A Logger MBean. Value is "Logger".
     */
    String CONTROL_TYPE_LOGGER = "Logger";

    /**
     * A Configuration MBean. Value is "Configuration".
     */
    String CONTROL_TYPE_CONFIGURATION = "Configuration";

    /**
     * A Controller MBean. Value is "Controller".
     */
    String CONTROL_TYPE_CONTROLLER = "Controller";

    /**
     * A Deployer MBean. Value is "Deployer".
     */
    String CONTROL_TYPE_DEPLOYER = "Deployer";

    /**
     * An Installer MBean. Value is "Installer".
     */
    String CONTROL_TYPE_INSTALLER = "Installer";

    /**
     * A Notification MBean. Value is "Notification".
     */
    String CONTROL_TYPE_NOTIFICATION = "Notification";

    /**
     * A Statistics MBean. Value is "Statistics".
     */
    String CONTROL_TYPE_STATISTICS = "Statistics";

    /**
     * A custom vendor-installed MBean. Value is "Custom".
     */
    String CONTROL_TYPE_CUSTOM = "Custom";

    /**
     * A System MBean.
     */
    String CONTROL_TYPE_SYSTEM = "System";
    
    /**
     * The type of component represented by this MBean: either System or
     * Installed Value is "ComponentType".
     */
    String COMPONENT_TYPE_KEY = "ComponentType";

    /**
     * A schemaorg_apache_xmlbeans.system component MBean. Value is "System".
     */
    String COMPONENT_TYPE_SYSTEM = "System";

    /**
     * An Installed component MBean. Value is "Installed".
     */
    String COMPONENT_TYPE_INSTALLED = "Installed";

    /**
     * The type of installation: either Engine or Binding. Value is
     * "InstalledType".
     */
    String INSTALLED_TYPE_KEY = "InstalledType";

    /**
     * Installed type for an engine component. Value is "Engine".
     */
    String INSTALLED_TYPE_ENGINE = "Engine";

    /**
     * Installed type for a binding component. Value is "Binding".
     */
    String INSTALLED_TYPE_BINDING = "Binding";

    /**
     * ServiceName is the name of the service where ComponentType is System.
     * Value is "ServiceName".
     */
    String SERVICE_NAME_KEY = "ServiceName";

    /**
     * ComponentName is defined to be the unique name assigned where
     * ComponentType is Installed. Value is "ComponentName".
     */
    String COMPONENT_ID_KEY = "ComponentName";

    /**
     * LoggerName is the name of the Logger instance when ControlType is Logger.
     * Value is "LoggerName".
     */
    String LOGGER_NAME_KEY = "LoggerName";

    /**
     * CustomControlName is the custom name of a vendor control where
     * ControlType is Custom. Value is "CustomControlName".
     */
    String CUSTOM_CONTROL_NAME_KEY = "CustomControlName";

    /**
     * The event management forwarder mbean is used to forward event from components
     *  and application to interested clients that registered for them
     */
    String EVENT_MANAGEMENT_MBEAN_NAME =   "EventManagement:name=EventForwarderMBean";
    
    /**
     * The event management controller mbean is used to configure and manage the event forwarding
     * mbean.
     */
    String EVENTMANAGEMENT_CONTROLLER_MBEAN_NAME = "EventManagement:name=EventManagementControllerMBean";    

    /**
     * Formulate and return the MBean ObjectName of a control MBean for a
     * Binding Component.
     * 
     * @param bindingName
     *            the name of the Binding Component.
     * @param controlType
     *            the type of control (MBean type).
     * @return the JMX ObjectName of the MBean, or <code>null</code> if
     *         <code>controlType</code> is invalid.
     */
    ObjectName getBindingMBeanName(String bindingName, String controlType);

    /**
     * Formulate and return the MBean ObjectName of custom control MBean for a
     * Binding Component.
     * 
     * @param customName
     *            the name of the custom control.
     * @param bindingName
     *            the name of the Binding Component.
     * @return the JMX ObjectName or <code>null</code> if illegal name.
     */
    ObjectName getCustomBindingMBeanName(String customName, String bindingName);

    /**
     * Formulate and return the MBean ObjectName of custom control MBean for a
     * Service Engine.
     * 
     * @param customName
     *            the name of the custom control.
     * @param engineName
     *            the name of the Service Engine.
     * @return the JMX ObjectName or <code>null</code> if illegal name.
     */
    ObjectName getCustomEngineMBeanName(String customName, String engineName);

    /**
     * Formulate and return the MBean ObjectName of a control MBean for a
     * Service Engine.
     * 
     * @param engineName
     *            the name of the Service Engine.
     * @param controlType
     *            the type of control (MBean type).
     * @return the JMX ObjectName of the MBean, or <code>null</code> if
     *         <code>controlType</code> is invalid.
     */
    ObjectName getEngineMBeanName(String engineName, String controlType);

    /**
     * Formulate and return the MBean ObjectName of control MBean for a JBI
     * schemaorg_apache_xmlbeans.system service.
     * 
     * @param serviceName
     *            the name of the schemaorg_apache_xmlbeans.system service.
     * @param type
     *            the type of the MBean.
     * @return the JMX ObjectName of the specified MBean, or null if the
     *         serviceName or type is illegal.
     */
    ObjectName getSystemServiceMBeanName(String serviceName, String type);

    /**
     * Formulate and return the MBean ObjectName of control MBean for a JBI
     * schemaorg_apache_xmlbeans.system service for a specific instance.
     * 
     * @param serviceName
     *            the name of the schemaorg_apache_xmlbeans.system service.
     * @param type
     *            the type of the MBean.
     * @param instanceName
     *            the name of the server instance.
     * @return the JMX ObjectName of the specified MBean, or null if the
     *         serviceName or type is illegal.
     */
    ObjectName getSystemServiceMBeanName(String serviceName, String type,
        String instanceName);

    /**
     * Formulate and return the LoggerMBean ObjectName of a JBI Framework schemaorg_apache_xmlbeans.system
     * service.
     * 
     * @param name -
     *            the name of the schemaorg_apache_xmlbeans.system service.
     * @param logger -
     *            the Logger instance.
     * @return the JMX ObjectName of the service, or null if illegal name.
     */
    ObjectName getSystemServiceLoggerMBeanName(String name, Logger logger);

    /**
     * Return the name of this JBI Framework runtime
     * 
     * @return the instance name of this runtime.
     */
    String getJbiInstanceName();

    /**
     * Formulate and return the MBean ObjectName of a local JBI Framework schemaorg_apache_xmlbeans.system
     * service. A local services does not include the jbi runtime instance name.
     * 
     * @param name -
     *            the name of the schemaorg_apache_xmlbeans.system service
     * @param type -
     *            the type of the MBean
     * @return the JMX ObjectName of the service, or null if illegal name
     */
    ObjectName getLocalSystemServiceMBeanName(String name, String type);
    
    /**
     * Utility method to get a component's MBean name based on the component's
     * type (binding or engine).  This saves clients from coding the same
     * conditional (e.g. if type == binding call getBindingMBeanName) over
     * and over again.
     * @param componentName component name
     * @param serviceType type of MBean
     * @param target administration target
     * @return ObjectName in the component's namespace for the given service
     */
    ObjectName getComponentMBeanName(String compName, ComponentType compType,
            ComponentServiceType serviceType, String target);

    //////////////////////////////
    // -- Overloaded methods --
    //////////////////////////////
    /**
     * Returns object name of type a Binding Object Name:
     * com.sun.jbi:Target={target}, ServiceType={serviceType}, ComponentName="bindingName"
     * 
     * @param bindingName -
     *            binding component name
     * @param serviceType
     * @param target -
     *            administration target
     * @return the Object name of the facade AdminServiceMBean for the desired
     *         target.
     */
    ObjectName getBindingMBeanName(String bindingName, ComponentServiceType serviceType, String target);
    

    /**
     * Returns object name of type a Engine Object Name:
     * com.sun.jbi:Target={target}, ServiceType={serviceType}, ComponentName="engineName"
     *  
     * @param engineName -
     *            service engine name
     * @param serviceType
     * @param target -
     *            administration target
     * @return the Object name of the facade AdminServiceMBean for the desired
     *         target.
     */
    ObjectName  getEngineMBeanName(String engineName,   ComponentServiceType serviceType, String target);
    
    /**
     * Returns object name of type a SystemService Object Name: 
     * com.sun.jbi:Target={target}, ServiceName={serviceName}, ServiceType={serviceType}
     * @param serviceName the name of the schemaorg_apache_xmlbeans.system service, e.g, InstallationService
     * @param serviceType the type of the schemaorg_apache_xmlbeans.system service, e.g, Installation
     * @param target -
     *            administration target
     * @return the Object name of the facade AdminServiceMBean for the desired
     *         target.
     */
    ObjectName  getSystemServiceMBeanName(ServiceName serviceName, ServiceType serviceType, String target);    
    
    /**
     * Returns the ObjectName to be used to filter component registered custom MBeans : 
     * com.sun.jbi:JbiName=instanceName,CustomControlName=customName,ComponentName=mComponentName,ControlType=Custom,*
     *
     * @param instanceName - target instance name
     * @param customName - target custom control name
     * @param componentName - target component name
     * @return the ObjectName to be used to filter component registered custom MBeans
     */
    ObjectName getCustomComponentMBeanNameFilter(String instanceName, String customName, String componentName);
    
    /**
     * Returns the ObjectName to be used to filter Logger MBeans registered for a component : 
     * com.sun.jbi:JbiName=instanceName,CustomControlName=logName,ComponentName=mComponentName,ControlType=Logger,*
     *
     * @param instanceName - target instance name
     * @param componentName - target component name
     * @return the ObjectName to be used to filter component registered custom MBeans
     */
    ObjectName getComponentLoggerMBeanNameFilter(String instanceName, String componentName);
    
    /**
     * Returns the ObjectName of the ComponentExtension facade MBean registered for
     * the component.
     *
     * @param target - target name
     * @param componentName - target component name
     * @return the ObjectName of the component extension facade MBean.
     */
    ObjectName getComponentExtensionFacadeMBeanName(String componentName, String target);
}
