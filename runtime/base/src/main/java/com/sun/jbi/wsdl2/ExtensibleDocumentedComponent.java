/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtensibleDocumentedComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * Base API for all extensible, documented components.
 *
 * @author ApiGen AX.00
 */
public interface ExtensibleDocumentedComponent
{
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed
     * by QName.toString().
     *
     * @return Map of WSDL-defined attribute QNames for this component,
     * indexed by QName.toString()
     */
    java.util.Map getWsdlAttributeNameMap();

    /**
     * Get documentation for component, if any.
     * @param i the index by which the document is going to obtained
     *
     * @return documentation for component, if any
     */
    Document getDocument(int i);

    /**
     * Get documentation for component, if any.
     * This method is for backward compatibility, it always returns
     * the first document (i.e., index = 0)
     *
     * @deprecated - replaced by getDocument(int i)
     * @return documentation for component, if any
     */
    Document getDocument();

    /**
     * Set documentation for component, if any.
     * @param i the index by which the document is going to be set
     *
     * @param theDocument documentation for component, if any
     */
    void setDocument(int i, Document theDocument);

    /**
     * Set documentation for component, if any.
     * This method is for backward compatibility, it always set the
     * the document which has the index = 0
     *
     * @deprecated - replaced by setDocument(int i, Document theDocument);
     * @param theDocument documentation for component, if any
     */
    void setDocument(Document theDocument);

    /**
     * Get extensions for component, if any.
     *
     * @return extensions for component, if any
     */
    Extensions getExtensions();

    /**
     * Set extensions for component, if any.
     *
     * @param theExtensions extensions for component, if any
     */
    void setExtensions(Extensions theExtensions);

}

// End-of-file: ExtensibleDocumentedComponent.java
