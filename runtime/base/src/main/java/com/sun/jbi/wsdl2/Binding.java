/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Binding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;
import org.w3c.dom.DocumentFragment;

/**
 * API for  WSDL 2.0 Binding component.
 *
 * @author ApiGen AX.00
 */
public interface Binding extends ExtensibleDocumentedComponent
{
    /**
     * Get target namespace of this binding.
     *
     * @return Target namespace of this binding
     */
    String getTargetNamespace();

    /**
     * Get local name of this binding component.
     *
     * @return Local name of this binding component
     */
    String getName();

    /**
     * Set local name of this binding component.
     *
     * @param theName Local name of this binding component
     */
    void setName(String theName);

    /**
     * Get qualified name of this component.
     *
     * @return Qualified name of this component
     */
    QName getQName();

    /**
     * Get interface being bound by this binding definition, if any.
     *
     * @return Interface being bound by this binding definition, if any
     */
    Interface getInterface();

    /**
     * Set interface being bound by this binding definition, if any.
     *
     * @param theInterface Interface being bound by this binding definition,
     * if any
     */
    void setInterface(Interface theInterface);

    /**
     * Get the number of BindingFault items in faults.
     *
     * @return The number of BindingFault items in faults
     */
    int getFaultsLength();

    /**
     * Get faults bound by this binding definition by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Faults bound by this binding definition at given
     * <code>index</code> position.
     */
    BindingFault getFault(int index);

    /**
     * Set faults bound by this binding definition by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theFault Item to add at position <code>index</code>.
     */
    void setFault(int index, BindingFault theFault);

    /**
     * Append an item to faults bound by this binding definition.
     *
     * @param theFault Item to append to faults
     */
    void appendFault(BindingFault theFault);

    /**
     * Remove faults bound by this binding definition by index position.
     *
     * @param index The index position of the fault to remove
     * @return The BindingFault removed, if any.
     */
    BindingFault removeFault(int index);

    /**
     * Get the number of BindingOperation items in operations.
     *
     * @return The number of BindingOperation items in operations
     */
    int getOperationsLength();

    /**
     * Get named binding operations, if any by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Named binding operations, if any at given <code>index</code>
     * position.
     */
    BindingOperation getOperation(int index);

    /**
     * Set named binding operations, if any by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theOperation Item to add at position <code>index</code>.
     */
    void setOperation(int index, BindingOperation theOperation);

    /**
     * Append an item to named binding operations, if any.
     *
     * @param theOperation Item to append to operations
     */
    void appendOperation(BindingOperation theOperation);

    /**
     * Remove named binding operations, if any by index position.
     *
     * @param index The index position of the operation to remove
     * @return The BindingOperation removed, if any.
     */
    BindingOperation removeOperation(int index);

    /**
     * Create a new operation, appending it to this binding's operations
     * list.
     *
     * @return Newly created operation, appended to this binding's operation
     * list.
     */
    BindingOperation addNewOperation();

    /**
     * Create a new binding fault, appending it to this binding's faults
     * list.
     *
     * @param ref Interface fault to which the new binding fault adds binding
     * information
     * @return Newly created binding fault, appended to the faults list.
     */
    BindingFault addNewBindingFault(
        InterfaceFault ref);

    /**
     * Get URI indicating the type of this binding.
     *
     * @return URI indicating the type of this binding
     */
    String getType();

    /**
     * Set URI indicating the type of this binding
     *
     * @param theType URI indicating the type of this binding
     */
    void setType(String theType);

    /**
     * Return this WSDL binding as an XML string.
     *
     * @return This binding, serialized as an XML string.
     */
    String toXmlString();

    /**
     * Return this binding as a DOM document fragment. The DOM subtree is a
     * copy; altering it will not affect this binding.
     *
     * @return This binding, as a DOM document fragment.
     */
    DocumentFragment toXmlDocumentFragment();

}

// End-of-file: Binding.java
