/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingOperation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import javax.xml.namespace.QName;

/**
 * API for  WSDL 2.0 Binding Operation Component.
 *
 * @author ApiGen AX.00
 */
public interface BindingOperation extends ExtensibleDocumentedComponent
{
    /**
     * Get qualified name of Interface Operation being bound.
     *
     * @return Qualified name of Interface Operation being bound
     */
    QName getInterfaceOperation();

    /**
     * Set qualified name of Interface Operation being bound.
     *
     * @param theInterfaceOperation Qualified name of Interface Operation
     * being bound
     */
    void setInterfaceOperation(QName theInterfaceOperation);

    /**
     * Get the number of BindingMessageReference items in inputs.
     *
     * @return The number of BindingMessageReference items in inputs
     */
    int getInputsLength();

    /**
     * Get input binding message references by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Input binding message references at given <code>index</code>
     * position.
     */
    BindingMessageReference getInput(int index);

    /**
     * Set input binding message references by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theInput Item to add at position <code>index</code>.
     */
    void setInput(int index, BindingMessageReference theInput);

    /**
     * Append an item to input binding message references.
     *
     * @param theInput Item to append to inputs
     */
    void appendInput(BindingMessageReference theInput);

    /**
     * Remove input binding message references by index position.
     *
     * @param index The index position of the input to remove
     * @return The BindingMessageReference removed, if any.
     */
    BindingMessageReference removeInput(int index);

    /**
     * Get the number of BindingMessageReference items in outputs.
     *
     * @return The number of BindingMessageReference items in outputs
     */
    int getOutputsLength();

    /**
     * Get output binding message references by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Output binding message references at given <code>index</code>
     * position.
     */
    BindingMessageReference getOutput(int index);

    /**
     * Set output binding message references by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theOutput Item to add at position <code>index</code>.
     */
    void setOutput(int index, BindingMessageReference theOutput);

    /**
     * Append an item to output binding message references.
     *
     * @param theOutput Item to append to outputs
     */
    void appendOutput(BindingMessageReference theOutput);

    /**
     * Remove output binding message references by index position.
     *
     * @param index The index position of the output to remove
     * @return The BindingMessageReference removed, if any.
     */
    BindingMessageReference removeOutput(int index);

    /**
     * Get the target namespace of this operation.
     *
     * @return Target namespace of this operation
     */
    String getTargetNamespace();

    /**
     * Create a new message reference for this operation, appending it to
     * this operation's input list.
     *
     * @param messageLabel NC role name in the {message exchange pattern} of
     * the message for which binding details are provided
     * @return The newly created input message reference, appended to this
     * operation.
     */
    BindingMessageReference addNewInput(
        String messageLabel);

    /**
     * Create a new message reference for this operation, appending it this
     * operation's output list.
     *
     * @param messageLabel NC name of the message for which binding details
     * are provided.
     * @return The newly created output message reference, appended to this
     * operation.
     */
    BindingMessageReference addNewOutput(
        String messageLabel);

}

// End-of-file: BindingOperation.java
