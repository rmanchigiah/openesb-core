/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)KeyStoreUtil.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.security;

import java.security.KeyStoreException;

/**
 * KeyStoreUtil provides helper methods for encrypting and decrypting
 * Strings and/or byte arrays.  An implementation is generally
 * expected to provide at least one default key for use in encrypting
 * and decrypting.  The algorithm used for encrypting and decrypting
 * messages is chosen by the implementation; keys must match the
 * algorithm used by the implementation, so it's important that the
 * implementation also be the generator of the keys!  The current
 * interface purposely tries to limit flexibility by not allowing the
 * user to use any key or any algorithm when invoking the encrypt and
 * decrypt methods.
 *
 * @version      $Revision: 1.5 $
 *
 */
public interface KeyStoreUtil {

    /**
     * Encrypts a message using a default key. 
     *
     * @param        clearText the byte array that will be encrypted
     * @return       the encrypted byte array
     * @exception    KeyStoreException if any error occurs retrieving the
     * key to be used
     */
    public byte[] encrypt(byte[] clearText) throws KeyStoreException;

    /**
     * Decrypts a message using a default key
     *
     * @param        cipherText the byte array with the encrypted data
     * @return       the unencrypted byte array
     * @exception    KeyStoreException if any error occurs retrieving the
     * key to be used
     */
    public byte[] decrypt(byte[] cipherText) throws KeyStoreException;

    /**
     * Encrypts a message using a default key.  The result
     * is a Base64-encoded string.
     *
     * @param        clearText a String representing the message to be encrypted
     * @return       a Base64-encoded string representing the encrypted message
     * @exception    KeyStoreException if any error occurs retrieving the
     * key to be used
     */
    public String encrypt(String clearText) throws KeyStoreException;

    /**
     * Decrypts a message using the key identified by keyName.  The second
     * argument must be a Base-64 encoded string
     *
     * @param        base64EncodedCipherText a Base-64 Encoded string
     * @return       the decrypted message as a String
     * @exception    KeyStoreException if any error occurs retrieving the
     * key to be used
     */
    public String decrypt(String base64EncodedCipherText) throws KeyStoreException;
}

