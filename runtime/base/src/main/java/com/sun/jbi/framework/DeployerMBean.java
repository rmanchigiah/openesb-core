/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployerMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ServiceUnitState;

/**
 * This is the framework-provided version of the DeployerMBean for use with
 * the ServiceUnitManager SPI of a component.
 *
 * @author Sun Microsystems, Inc.
 */
public interface DeployerMBean
{
    /**
     * Deploy a Service Unit to the component. This is called to deploy the
     * specified artifact to the component.
     * @param serviceUnitName the name of the Service Unit being deployed.
     * @param serviceUnitRootPath the full path to the Service Unit artifact
     * root directory.
     * @throws javax.jbi.management.DeploymentException if the deployment
     * operation is unsuccessful.
     * @return a deployment status message.
     */
    String deploy(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException;

    /**
     * Return a list of all Service Units currently deployed to the component.
     * @return array of Service Unit name strings.
     */
    String[] getDeployments();

    /**
     * Get the current state of a Service Unit.
     * @param serviceUnitName The unique name of the Service Unit.
     * @return The service unit state.
     */
    ServiceUnitState getServiceUnitState(String serviceUnitName);

    /**
     * Return a boolean value indicating whether the Service Unit is currently
     * deployed.
     * @param serviceUnitName the name of the Service Unit.
     * @return true if the Service Unit is deployed, false if not.
     */
    boolean isDeployed(String serviceUnitName);

    /**
     * Undeploy a Service Unit from the component. The Service Unit must be
     * shut down before it can be undeployed.
     * @param serviceUnitName the name of the Service Unit being undeployed.
     * @param serviceAssemblyName the name of the parent Service Assembly 
     * of the Service Unit.
     * @throws javax.jbi.management.DeploymentException if the undeploy
     * operation is unsuccessful.
     * @return A status message.
     */
    String undeploy(String serviceUnitName, String serviceAssemblyName)
        throws javax.jbi.management.DeploymentException;

    /**
     * Undeploy a Service Unit from the component. The Service Unit must be
     * shut down before it can be undeployed, unless the force options is set,
     * in which case all state checks are skipped.
     * @param serviceUnitName the name of the Service Unit being undeployed.
     * @param serviceAssemblyName the name of the parent Service Assembly 
     * of the Service Unit.
     * @param force when set to true, indicates a forced undeploy, which
     * causes undeployment to be completed regardless of any errors which
     * may occur in the component's Service Unit Manager.
     * @throws javax.jbi.management.DeploymentException if the undeploy
     * operation is unsuccessful.
     * @return A status message.
     */
    String undeploy(String serviceUnitName, String serviceAssemblyName,
        boolean force)
        throws javax.jbi.management.DeploymentException;

    /**
     * Initialize a Service Unit. This is the first phase of a two-phase
     * start, where the component must prepare to receive service requests
     * related to the Service Unit (if any).
     * @param serviceUnitName the name of the Service Unit being initialized.
     * @param serviceUnitRootPath the absolute path to the directory, which
     * has the extracted service unit contents.
     * @throws javax.jbi.management.DeploymentException if the Service Unit is
     * not deployed, or is in an incorrect state.
     */
    void init(String serviceUnitName, String serviceUnitRootPath)
        throws javax.jbi.management.DeploymentException;

    /**
     * Shut down a Service Unit. This causes the Service Unit to return to the
     * state it was in after <code>deploy()</code> and before <code>init()</code>.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    void shutDown(String serviceUnitName)
        throws javax.jbi.management.DeploymentException;

    /**
     * Shut down a Service Unit. This causes the Service Unit to return to the
     * state it was in after <code>deploy()</code> and before <code>init()</code>.
     * If the force option is set, then the operation proceeds regardless of
     * of any errors in state or in the component's Service Unit Manager.
     * @param serviceUnitName the name of the Service Unit being shut down.
     * @param force when set to true, indicates a forced shutdown, which
     * causes the shutdown to be completed regardless of any errors which
     * may occur in the component's Service Unit Manager.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    void shutDown(String serviceUnitName, boolean force)
        throws javax.jbi.management.DeploymentException;

    /**
     * Start a Service Unit. This is the second phase of a two-phase start,
     * where the component can now initiate service requests related to the
     * Service Unit.
     * @param serviceUnitName the name of the Service Unit being started.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    void start(String serviceUnitName)
        throws javax.jbi.management.DeploymentException;

    /**
     * Stop a Service Unit. This causes the component to cease generating
     * service requests related to the Service Unit. This returns the Service
     * Unit to a state equivalent to after <code>init()</code> was called.
     * @param serviceUnitName the name of the Service Unit being stopped.
     * @throws javax.jbi.management.DeploymentException if the Service Unit
     * is not deployed, or is in an incorrect state.
     */
    void stop(String serviceUnitName)
        throws javax.jbi.management.DeploymentException;
}
