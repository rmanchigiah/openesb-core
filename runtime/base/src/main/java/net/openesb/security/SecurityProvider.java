package net.openesb.security;

import java.util.Collection;
import javax.security.auth.Subject;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface SecurityProvider {
    
    String MANAGEMENT_REALM = "management";
    
    /**
     * Returns a collection of user realms. These realms can be used in
     * components to use authentication system.
     * 
     * @return A collection of user realms.
     */
    Collection<String> getRealms();
    
    /**
     * This method is trying to authenticate a user by using the provided 
     * {@link AuthenticationToken} and the given realm.
     * 
     * @param realmName The realm which have to be used to authenticate user.
     * @param authenticationToken The token which represents the way to authenticate
     *  the user.
     * @return A security subject.
     * @throws AuthenticationException If the system is not able to do authentication.
     */
    Subject login(String realmName, AuthenticationToken authenticationToken)
            throws AuthenticationException;
    
    /**
     * This method is trying to authenticate a user by using the provided 
     * {@link AuthenticationToken} and using the main administration realm.
     * 
     * @param authenticationToken
     * @return A security subject.
     * @throws AuthenticationException If the system is not able to do authentication.
     */
    Subject login(AuthenticationToken authenticationToken)
            throws AuthenticationException;
}
