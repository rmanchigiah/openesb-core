package net.openesb.security;

import javax.security.auth.Subject;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class SecurityHandlerImpl implements SecurityHandler {

    private final SecurityProvider securityProvider;
    
    public SecurityHandlerImpl(final SecurityProvider securityProvider) {
        this.securityProvider = securityProvider;
    }
    
    @Override
    public Subject authenticate(String realmName, AuthenticationToken authenticationToken) throws AuthenticationException {
        return securityProvider.login(realmName, authenticationToken);
    }
}
