package net.openesb.security;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class UsernamePasswordToken implements AuthenticationToken {

    /**
     * The username
     */
    private String username;

    /**
     * The password, in char[] format
     */
    private char[] password;
    
    public UsernamePasswordToken(final String username, final String password) {
        this(username, password.toCharArray());
    }
    
    public UsernamePasswordToken(final String username, final char[] password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }
    
    @Override
    public Object getPrincipal() {
        return getUsername();
    }

    @Override
    public Object getCredentials() {
        return getPassword();
    }
}
