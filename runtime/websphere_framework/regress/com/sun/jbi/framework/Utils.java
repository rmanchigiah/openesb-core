/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Utils.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentType;

import java.io.ByteArrayInputStream;
import java.io.File;

import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Utility class used by multiple test classes.
 *
 * @author Sun Microsystems, Inc.
 */
public class Utils
{
    /**
     * DocumentBuilder used by the class.
     */
    private static DocumentBuilder sDocBuilder;

    /**
     * SAXErrorHandler used by the class.
     */
    private static SAXErrorHandler sErrorHandler;

    /**
     * Constructor.
     */
    public Utils()
        throws Exception
    {
        if ( null == sDocBuilder )
        {
            sDocBuilder =
                DocumentBuilderFactory.newInstance().newDocumentBuilder();
            sDocBuilder.setErrorHandler(new SAXErrorHandler());
        }
    }

    /**
     * Utility method to return the last level of a qualified name.
     * @param name the qualified name (such as a logger name).
     * @return the last level of the name (i.e. the part following the last
     * period, or the whole name if it has no period).
     */
    public String getLastLevel(String name)
    {
        String lastLevel = null;
        int lastDot = name.lastIndexOf(".");
        if ( -1 < lastDot )
        {
            lastLevel = name.substring(lastDot+1);
        }
        else
        {
            lastLevel = name;
        }
        return lastLevel;
    }

    /**
     * Utility method to create a DOM document from an XML string provided by
     * the caller.
     * @param xmlString an XML fragment in a String.
     * @return the DOM document representing the XML string.
     * @throws Exception if an unexpected error occurs.
     */
    public Document createDocument(String xmlString)
        throws Exception
    {
        Document doc = null;

        // Add a standard header to the XML string.

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" + xmlString;

        // Create a DOM Document from the XML string.

        ByteArrayInputStream xmlStream =
            new ByteArrayInputStream(xml.getBytes());
        try
        {
            doc = sDocBuilder.parse(xmlStream);
        }
        catch (org.xml.sax.SAXParseException spEx)
        {
            System.err.println("Unexpected exception received: " +
                spEx.toString());
        }
        catch (org.xml.sax.SAXException saxEx)
        {
            System.err.println("Unexpected exception received: " +
            saxEx.toString());
        }

        return doc;
    }


    /**
     * Utility method to create a DOM Element from an XML string provided by
     * the caller.
     * @param xmlString an XML node in a String.
     * @return the provided node as an Element.
     * @throws Exception if an unexpected error occurs.
     */
    public Element createElement(String xmlString)
        throws Exception
    {
        Document doc = createDocument(xmlString);
        Element elem = (Element) doc.getFirstChild();
        return elem;
    }

    /**
     * This class provides The error handler for the SAX parser.
     * @author Sun Microsystems, Inc.
     */
    class SAXErrorHandler
        implements org.xml.sax.ErrorHandler
    {
        /**
         * This is a callback from the XML parser used to handle warnings.
         * @param aSaxException SAXParseException is the warning.
         * @throws org.xml.sax.SAXException when finished logging.
         */
        public void warning(org.xml.sax.SAXParseException aSaxException)
            throws org.xml.sax.SAXException
        {
            throw aSaxException;
        }

        /**
         * This is a callback from the XML parser used to handle errors.
         * @param aSaxException SAXParseException is the error.
         * @throws org.xml.sax.SAXException when finished logging.
         */
        public void error(org.xml.sax.SAXParseException aSaxException)
            throws org.xml.sax.SAXException
        {
            throw aSaxException;
        }

        /**
         * This is a callback from the XML parser used to handle fatal errors.
         * @param aSaxException SAXParseException is the error.
         * @throws org.xml.sax.SAXException when finished logging.
         */
        public void fatalError(org.xml.sax.SAXParseException aSaxException)
            throws org.xml.sax.SAXException
        {
            throw aSaxException;
        }
    }

}
