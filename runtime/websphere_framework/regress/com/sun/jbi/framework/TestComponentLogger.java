/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestComponentLogger.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentType;

import java.io.File;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Element;

/**
 * Tests for the ComponentLogger class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestComponentLogger
    extends junit.framework.TestCase
{
    /**
     * Current test name.
     */
    private String mTestName;

    /**
     * Current SRCROOT path.
     */
    private String mSrcroot;

    /**
     * EnvironmentContext
     */
    private EnvironmentContext mEnvironmentContext;

    /**
     * Scaffolded registry used to set up test data
     */
    private ScaffoldRegistry mScaffoldRegistry;

    /**
     * Component Root directory
     */
    private String mInstallRoot;

    /**
     * Component instance created during setup.
     */
    private Component mComponent;

    /**
     * ComponentLogger instance created during setup.
     */
    private ComponentLogger mComponentLogger;

    /**
     * Utility class instance.
     */
    private Utils mUtils;

    /**
     * Component logger.
     */
    private Logger mLogger1 = Logger.getLogger(LOGGER_NAME_1);

    /**
     * Component logger.
     */
    private Logger mLogger2 = Logger.getLogger(LOGGER_NAME_2);

    /**
     * Component logger.
     */
    private Logger mLogger3 = Logger.getLogger(LOGGER_NAME_3);

    /**
     * Parent logger.
     */
    private Logger mParentLogger;

    /**
     * Array of component loggers.
     */
    private Logger[] mLoggers = {mLogger1, mLogger2, mLogger3};

    /**
     * Component name for the tests.
     */
    private static final String COMPONENT_NAME = "Engine1";

    /**
     * Component logger display name for the tests.
     */
    private static final String DISPLAY_NAME_1 = "Logger1";

    /**
     * Component logger display name for the tests.
     */
    private static final String DISPLAY_NAME_2 = "Logger2";

    /**
     * Component logger display name for the tests.
     */
    private static final String DISPLAY_NAME_3 = "Logger3";

    /**
     * Component logger name for the tests.
     */
    private static final String LOGGER_NAME_1 = "Engine1.logger1";

    /**
     * Component logger name for the tests.
     */
    private static final String LOGGER_NAME_2 = "Engine1.logger2";

    /**
     * Component logger name for the tests.
     */
    private static final String LOGGER_NAME_3 = "Engine1.logger3";

    /**
     * Component parent logger name for the tests.
     */
    private static final String PARENT_LOGGER_NAME = "com.sun.test.parent";

    /**
     * Array of component logger names and display names.
     */
    private static final String[][] NAMES = {
        { LOGGER_NAME_1, DISPLAY_NAME_1 },
        { LOGGER_NAME_2, DISPLAY_NAME_2 },
        { LOGGER_NAME_3, DISPLAY_NAME_3 } };

    /**
     * Index of logger name.
     */
    private static final int LOGGER_NAME_INDEX = 0;

    /**
     * Index of display name.
     */
    private static final int DISPLAY_NAME_INDEX = 1;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestComponentLogger(String aTestName)
    {
        super(aTestName);
        mTestName = aTestName;
    }

    /**
     * Setup for the test. This creates the ComponentLoggerMBean instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        System.err.println("***** START of test " + mTestName);
        mSrcroot = System.getProperty("junit.srcroot") + "/";
        mInstallRoot = mSrcroot + "runtime/framework/bld/Engine1/install_root";

        mEnvironmentContext = new EnvironmentContext(
            new ScaffoldPlatformContext(), 
            new JBIFramework(), new Properties());
        mScaffoldRegistry = new ScaffoldRegistry(mEnvironmentContext);
        mEnvironmentContext.setRegistry(mScaffoldRegistry);
        mUtils = new Utils();

        // Create loggers for the test.

        mParentLogger = Logger.getLogger(PARENT_LOGGER_NAME);
        mParentLogger.setLevel(Level.FINEST);

        for ( int i = 0; i < mLoggers.length; i++ )
        {
            mLoggers[i].setLevel(null);
            mLoggers[i].setParent(mParentLogger);
        }

        // Create and initialize a Component instance and a ComponentLogger
        // instance.

        mComponent = new Component();
        mComponent.setName(COMPONENT_NAME);
        mComponent.setComponentType(ComponentType.ENGINE);
        mComponent.setInstallRoot(mInstallRoot);
        mComponentLogger = new ComponentLogger(mComponent);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        File installRoot = new File(mInstallRoot);
        File f = new File(installRoot.getParent() +
            ComponentLogger.CONFIG_DIRECTORY + ComponentLogger.LOGGER_SETTINGS);
        f.delete();
        System.err.println("***** END of test " + mTestName);
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Test the addLogger method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testAddLogger()
        throws Exception
    {
        String displayName = "Test Engine";
        String name = "com.sun.jbi.component.TestEngine";
        Logger log = Logger.getLogger(name);

        assertTrue("Failure adding new logger entry",
            mComponentLogger.addLogger(log, displayName));
        assertFalse("Failure adding existing logger entry",
            mComponentLogger.addLogger(log, displayName));
    }

    /**
     * Test the addLoggerInfo method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testAddLoggerInfo()
        throws Exception
    {
        // This will retrieve the top-level logger entry, created by the
        // constructor.

        String[] topName = mComponentLogger.getLoggerNames();

        assertEquals("Failure getting logger names: ",
            1, topName.length);
        assertEquals("Failure getting logger names: ",
            COMPONENT_NAME, topName[0]);

        // Add some more logger names to the list.

        int i;
        for ( i = 0; i < mLoggers.length; i++ )
        {
            mComponentLogger.addLoggerInfo(NAMES[i][LOGGER_NAME_INDEX],
                NAMES[i][DISPLAY_NAME_INDEX], true);
        }

        // Verification. Note that the top-level component logger is added
        // to the count.

        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Failure getting logger names: ",
            mLoggers.length+1, names.length);
        assertEquals("Failure getting logger names: ",
            COMPONENT_NAME, names[0]);

        // Note that the table is a TreeMap, so the names come out in
        // alphabetical order, so skip the first entry for the top-level logger.

        for ( i = 1; i < names.length; i++ )
        {
            assertEquals("Logger info entry name mismatch: ",
                NAMES[i-1][LOGGER_NAME_INDEX], names[i]);
        }
    }

    /**
     * Test the addLoggerInfo method with a default display name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testAddLoggerInfoDefault()
        throws Exception
    {
        // This will retrieve the top-level logger entry, created by the
        // constructor.

        String[] topName = mComponentLogger.getLoggerNames();

        assertEquals("Failure getting logger names: ",
            1, topName.length);
        assertEquals("Failure getting logger names: ",
            COMPONENT_NAME, topName[0]);

        // Add some more logger names to the list.

        int i;
        for ( i = 0; i < mLoggers.length; i++ )
        {
            mComponentLogger.addLoggerInfo(NAMES[i][LOGGER_NAME_INDEX],
                null, true);
        }

        // Verification. Note that the top-level component logger is added
        // to the count.

        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Failure getting logger names: ",
            mLoggers.length+1, names.length);
        assertEquals("Failure getting logger names: ",
            COMPONENT_NAME, names[0]);

        // Note that the table is a TreeMap, so the names come out in
        // alphabetical order, so skip the first entry for the top-level logger.

        for ( i = 1; i < names.length; i++ )
        {
            assertEquals("Logger info entry name mismatch: ",
                names[i], NAMES[i-1][LOGGER_NAME_INDEX]);
            String displayName = mUtils.getLastLevel(names[i]);
            assertEquals("Logger info display name mismatch: ",
                displayName, mComponentLogger.getDisplayName(names[i]));
        }
    }

    /**
     * Test the addLoggerInfo method with a prefix that doesn't match the
     * component name.
     * @throws Exception if an unexpected error occurs.
     */
    public void testAddLoggerInfoWithPrefix()
        throws Exception
    {
        // This will retrieve the top-level logger entry, created by the
        // constructor.

        String[] topName = mComponentLogger.getLoggerNames();

        assertEquals("Failure getting logger names: ",
            1, topName.length);
        assertEquals("Failure getting logger names: ",
            COMPONENT_NAME, topName[0]);

        // Add some more logger names to the list, but with a prefix set.

        String prefix = "Tests.";
        mComponentLogger.setLoggerNamePrefix(prefix);
        int i;
        for ( i = 0; i < mLoggers.length; i++ )
        {
            mComponentLogger.addLoggerInfo(NAMES[i][LOGGER_NAME_INDEX],
                NAMES[i][DISPLAY_NAME_INDEX], true);
        }

        // Verification. Note that the top-level component logger is added
        // to the count.

        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Failure getting logger names: ",
            mLoggers.length+1, names.length);
        assertEquals("Failure getting logger names: ",
            COMPONENT_NAME, names[0]);

        // Note that the table is a TreeMap, so the names come out in
        // alphabetical order, so skip the first entry for the top-level logger.

        for ( i = 1; i < names.length; i++ )
        {
            assertEquals("Logger info entry name mismatch: ",
                prefix + NAMES[i-1][LOGGER_NAME_INDEX], names[i]);
        }
    }

    /**
     * Test the getLoggerNames method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerNames()
        throws Exception
    {
        // Because the constructor always creates the top-level logger, at
        // least one name is always returned.

        String[] oneName = mComponentLogger.getLoggerNames();
        assertEquals("Failure getting logger names: ",
            1, oneName.length);

        int i;
        for ( i = 0; i < mLoggers.length; i++ ) 
        {
            mComponentLogger.addLogger(mLoggers[i], NAMES[i][DISPLAY_NAME_INDEX]);
        }

        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Failure getting logger names: ",
            mLoggers.length+1, names.length);

        // Note that the table is a TreeMap, so the names come out in
        // alphabetical order, so skip the first entry (which is "Engine1").

        for ( i = 1; i < names.length; i++ )
        {
            assertEquals("Logger info entry name mismatch: ",
                NAMES[i-1][LOGGER_NAME_INDEX], names[i]);
        }
    }

    /**
     * Test the get method for the display name for a logger that has been
     * added dynamically.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetDisplayNameGood()
        throws Exception
    {
        for ( int i = 0; i < mLoggers.length; i++ )
        {
            mComponentLogger.addLogger(mLoggers[i], NAMES[i][DISPLAY_NAME_INDEX]);
            assertEquals("Failure getting display name at index " + i + ": ",
                NAMES[i][DISPLAY_NAME_INDEX],
                mComponentLogger.getDisplayName(NAMES[i][LOGGER_NAME_INDEX]));
        }
    }

    /**
     * Test the get method for the display name for a logger that was not added
     * dynamically and was not pre-defined.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetDisplayNameDefault()
        throws Exception
    {
        for ( int i = 0; i < mLoggers.length; i++ )
        {
            String displayName = null;
            String logName = NAMES[i][LOGGER_NAME_INDEX];
            displayName = mUtils.getLastLevel(logName);
            assertEquals("Failure getting display name at index " + i + ": ",
                displayName,
                mComponentLogger.getDisplayName(NAMES[i][LOGGER_NAME_INDEX]));
        }
    }

    /**
     * Test the get method for the display name for a logger with a null
     * argument.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetDisplayNameBadNullLoggerName()
        throws Exception
    {
        try
        {
            mComponentLogger.getDisplayName(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("logName")));
        }
    }

    /**
     * Test the get method for the logger level.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLevelGood()
        throws Exception
    {
        // Now register the logger with no level set
        mComponentLogger.addLogger(mLogger1, DISPLAY_NAME_1);
        assertEquals("Failure getting log level with no level set: ",
            mLogger1.getParent().getLevel().getLocalizedName(),
            mComponentLogger.getLevel(LOGGER_NAME_1));

        // Now test with a level set
        mLogger1.setLevel(Level.SEVERE);
        assertEquals("Failure getting log level: ",
            mLogger1.getLevel().getLocalizedName(),
            mComponentLogger.getLevel(LOGGER_NAME_1));
    }

    /**
     * Test the get method for the logger level with a null argument.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLevelBadNullLoggerName()
        throws Exception
    {
        try
        {
            mComponentLogger.getLevel(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("logName")));
        }
    }

    /**
     * Test the get method for the logger level with an unregistered logger.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLevelBadNotRegistered()
        throws Exception
    {
        try
        {
            mComponentLogger.getLevel(LOGGER_NAME_1);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2016")));
        }
    }

    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - no root attribute
     * - no displayName attribute
     * - no addPrefix attribute
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs1()
        throws Exception
    {
        // Create an XML document for the test.
        String logName = "com.sun.jbi.testbinding.LifeCycle";
        String xmlString = "<logging:Logging>" +
                           "<logging:logger>" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix defaulted to the component name.
        assertEquals("Prefix failed to default correctly",
            COMPONENT_NAME + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            mComponentLogger.getLoggerNamePrefix() + logName, names[1]);

        // Verify that the display name defaulted correctly
        assertEquals("Display name did not default correctly",
            "LifeCycle", mComponentLogger.getDisplayName(names[1]));
    }

    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - root attribute specified
     * - no displayName attribute
     * - no addPrefix attribute
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs2()
        throws Exception
    {
        // Create an XML document for the test.
        String prefix = "com.sun.jbi";
        String logName = "testbinding.LifeCycle";
        String xmlString = "<logging:Logging root=\"" +
                           prefix +
                           "\">" +
                           "<logging:logger>" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix was picked up
        assertEquals("Prefix failed to be picked up correctly",
            prefix + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            mComponentLogger.getLoggerNamePrefix() + logName, names[1]);

        // Verify that the display name defaulted correctly
        assertEquals("Display name did not default correctly",
            "LifeCycle", mComponentLogger.getDisplayName(names[1]));
    }
    
    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - root attribute specified
     * - displayName attribute specified
     * - no addPrefix attribute
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs3()
        throws Exception
    {
        // Create an XML document for the test.
        String prefix = "com.sun.jbi";
        String logName = "testbinding.LifeCycle";
        String displayName = "Binding Life Cycle";
        String xmlString = "<logging:Logging root=\"" +
                           prefix +
                           "\">" +
                           "<logging:logger displayName=\"" +
                           displayName +
                           "\">" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix was picked up
        assertEquals("Prefix failed to be picked up correctly",
            prefix + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            mComponentLogger.getLoggerNamePrefix() + logName, names[1]);

        // Verify that the display name was picked up correctly
        assertEquals("Display name did not get picked up correctly",
            displayName, mComponentLogger.getDisplayName(names[1]));
    }
    
    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - root attribute specified
     * - displayName attribute specified
     * - addPrefix attribute specified
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs4()
        throws Exception
    {
        // Create an XML document for the test.
        String prefix = "com.sun.jbi";
        String logName = "com.sun.jbi.testbinding.LifeCycle";
        String displayName = "Binding Life Cycle";
        String xmlString = "<logging:Logging root=\"" +
                           prefix +
                           "\">" +
                           "<logging:logger displayName=\"" +
                           displayName +
                           "\" addPrefix=\"false\">" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix was picked up
        assertEquals("Prefix failed to be picked up correctly",
            prefix + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            logName, names[1]);

        // Verify that the display name was picked up correctly
        assertEquals("Display name did not get picked up correctly",
            displayName, mComponentLogger.getDisplayName(names[1]));
    }
    
    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - no root attribute
     * - displayName attribute specified
     * - addPrefix attribute specified
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs5()
        throws Exception
    {
        // Create an XML document for the test.
        String logName = "com.sun.jbi.testbinding.LifeCycle";
        String displayName = "Binding Life Cycle";
        String xmlString = "<logging:Logging>" +
                           "<logging:logger displayName=\"" +
                           displayName +
                           "\" addPrefix=\"false\">" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix defaulted to the component name.
        assertEquals("Prefix failed to default correctly",
            COMPONENT_NAME + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            logName, names[1]);

        // Verify that the display name was picked up correctly
        assertEquals("Display name did not get picked up correctly",
            displayName, mComponentLogger.getDisplayName(names[1]));
    }
    
    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - no root attribute
     * - no displayName attribute
     * - addPrefix attribute specified
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs6()
        throws Exception
    {
        // Create an XML document for the test.
        String logName = "com.sun.jbi.testbinding.LifeCycle";
        String xmlString = "<logging:Logging>" +
                           "<logging:logger addPrefix=\"false\">" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix defaulted to the component name.
        assertEquals("Prefix failed to default correctly",
            COMPONENT_NAME + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            logName, names[1]);

        // Verify that the display name was picked up correctly
        assertEquals("Display name did not default correctly",
            "LifeCycle", mComponentLogger.getDisplayName(names[1]));
    }
    
    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - no root attribute
     * - displayName attribute specified
     * - no addPrefix attribute
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs7()
        throws Exception
    {
        // Create an XML document for the test.
        String logName = "com.sun.jbi.testbinding.LifeCycle";
        String displayName = "Binding Life Cycle";
        String xmlString = "<logging:Logging>" +
                           "<logging:logger displayName=\"" +
                           displayName +
                           "\">" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix defaulted to the component name.
        assertEquals("Prefix failed to default correctly",
            COMPONENT_NAME + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            mComponentLogger.getLoggerNamePrefix() + logName, names[1]);

        // Verify that the display name was picked up correctly
        assertEquals("Display name did not get picked up correctly",
            displayName, mComponentLogger.getDisplayName(names[1]));
    }
    
    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - root attribute specified
     * - no displayName attribute
     * - addPrefix attribute specified
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs8()
        throws Exception
    {
        // Create an XML document for the test.
        String prefix = "com.sun.jbi";
        String logName = "com.sun.jbi.testbinding.LifeCycle";
        String xmlString = "<logging:Logging root=\"" +
                           prefix +
                           "\">" +
                           "<logging:logger addPrefix=\"false\">" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix was picked up
        assertEquals("Prefix failed to be picked up correctly",
            prefix + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            logName, names[1]);

        // Verify that the display name defaulted correctly
        assertEquals("Display name did not default correctly",
            "LifeCycle", mComponentLogger.getDisplayName(names[1]));
    }
    
    /**
     * Tests the getLoggerDefs() method with a logging extension that has:
     * - root attribute specified
     * - no displayName attribute
     * - addPrefix attribute specified
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefs9()
        throws Exception
    {
        // Create an XML document for the test.
        String prefix = "com.sun.jbi";
        String logName1 = "com.sun.jbi.testbinding.LifeCycle";
        String logName2 = "testbinding.ServiceUnitManager";
        String xmlString = "<logging:Logging root=\"" +
                           prefix +
                           "\">" +
                           "<logging:logger addPrefix=\"true\">" +
                           logName1 + 
                           "</logging:logger>" +
                           "<logging:logger addPrefix=\"true\">" +
                           logName2 + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix was picked up
        assertEquals("Prefix failed to be picked up correctly",
            prefix + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger names were picked up. In this case, the first
        // logger name already begins with the prefix, so it is left unchanged,
        // while the second logger name has the prefix added to it. Skip the
        // top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            logName1, names[1]);
        assertEquals("Pre-defined logger not registered with correct name",
            mComponentLogger.getLoggerNamePrefix() + logName2, names[2]);

        // Verify that the display name defaulted correctly
        assertEquals("Display name did not default correctly",
            "LifeCycle", mComponentLogger.getDisplayName(names[1]));
        assertEquals("Display name did not default correctly",
            "ServiceUnitManager", mComponentLogger.getDisplayName(names[2]));
    }
    
    /**
     * Tests the getLoggerDefs() method to make sure that loggers added in an
     * update of a component are picked up properly. This test first calls
     * getLoggerDefs() as though a component has just been installed, then
     * calls it again as though the component has been updated with a new set
     * of logger definitions.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetLoggerDefsUpdate()
        throws Exception
    {
        // Create an XML document for the test.
        String logName = "com.sun.jbi.testbinding.LifeCycle";
        String xmlString = "<logging:Logging>" +
                           "<logging:logger>" +
                           logName + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix defaulted to the component name.
        assertEquals("Prefix failed to default correctly",
            COMPONENT_NAME + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the logger name was picked up. Skip the top-level logger.
        String[] names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            mComponentLogger.getLoggerNamePrefix() + logName, names[1]);

        // Verify that the display name defaulted correctly
        assertEquals("Display name did not default correctly",
            "LifeCycle", mComponentLogger.getDisplayName(names[1]));

        // Create a new XML document for the test with an additional logger
        // defined.
        String logName1 = "com.sun.jbi.testbinding.LifeCycle";
        String logName2 = "com.sun.jbi.testbinding.Messaging";
        String displayName1 = "Binding Life Cycle";
        String displayName2 = "Message Handler";
        xmlString = "<logging:Logging>" +
                    "<logging:logger displayName=\"" +
                    displayName1 +
                    "\">" +
                    logName1 + 
                    "</logging:logger>" +
                    "<logging:logger displayName=\"" +
                    displayName2 +
                    "\">" +
                    logName2 + 
                    "</logging:logger>" +
                    "</logging:Logging>";
        extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // test the method
        mComponentLogger.getLoggerDefs();

        // Verify that the prefix defaulted to the component name.
        assertEquals("Prefix failed to default correctly",
            COMPONENT_NAME + ".", mComponentLogger.getLoggerNamePrefix());

        // Verify that the new logger name was picked up. Skip the top-level
        // logger.
        names = mComponentLogger.getLoggerNames();
        assertEquals("Pre-defined logger not registered with correct name",
            mComponentLogger.getLoggerNamePrefix() + logName1, names[1]);
        assertEquals("Pre-defined logger not registered with correct name",
            mComponentLogger.getLoggerNamePrefix() + logName2, names[2]);

        // Verify that the display names were picked up correctly
        assertEquals("Display name did not get picked up correctly",
            displayName1, mComponentLogger.getDisplayName(names[1]));
        assertEquals("Display name did not get picked up correctly",
            displayName2, mComponentLogger.getDisplayName(names[2]));
    }

    /**
     * Test the get method for the saved logger level with a null logger name.
     * An exception is expected
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSavedLevelBadNull()
        throws Exception
    {
        try
        {
            mComponentLogger.getSavedLevel(null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("Null argument")));
        }
    }

    /**
     * Test the get method for a saved logger level when no level has been
     * set.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSavedLevelGoodNotSet()
        throws Exception
    {
        Level l = mComponentLogger.getSavedLevel("com.sun.jbi.test.logger");
        assertNull("getLoggerLevel with no prior setLoggerLevel should return" +
            " a null value, got " + l, l);
    }

    /**
     * Test the get and set methods for the logger name prefix.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetSetLoggerNamePrefix()
        throws Exception
    {
        String prefix = "com.sun.jbi.binding.test.";
        mComponentLogger.setLoggerNamePrefix(prefix);
        assertEquals("Failure setting/getting logger name prefix: ",
                prefix, mComponentLogger.getLoggerNamePrefix());

        prefix = "com.acme.jbi.engine";
        mComponentLogger.setLoggerNamePrefix(prefix);
        assertEquals("Failure setting/getting logger name prefix: ",
                prefix + ".", mComponentLogger.getLoggerNamePrefix());
    }

    /**
     * Test the isLoggerRegistered method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testIsLoggerRegistered()
        throws Exception
    {
        String displayName = "Test Binding";
        String name = "com.sun.jbi.component.TestBinding";
        Logger log = Logger.getLogger(name);

        assertFalse("Failure checking for logger",
            mComponentLogger.isLoggerRegistered(name));

        assertTrue("Failure adding new logger entry",
            mComponentLogger.addLogger(log, displayName));

        assertTrue("Failure checking for logger",
            mComponentLogger.isLoggerRegistered(name));
    }

    /**
     * Test the load/save methods for component logger levels. This test
     * uses the get/set methods for verification. It also uses the
     * setInstallRoot() method to set up the directory used by the load/save
     * methods.
     * @throws Exception if an unexpected error occurs.
     */
    public void testLoadSaveLoggerSettings()
        throws Exception
    {
        File installRoot = new File(mInstallRoot);
        installRoot.mkdirs();

        int i;

        for ( i = 0; i < mLoggers.length; i++ )
        {
            mComponentLogger.addLogger(mLoggers[i], NAMES[i][DISPLAY_NAME_INDEX]);
        }

        // Save with no levels, then load. All levels should be null.

        mComponentLogger.saveLoggerSettings();
        mComponentLogger.loadLoggerSettings();
        for ( i = 0; i < mLoggers.length; i++ )
        {
            Level level =
                mComponentLogger.getSavedLevel(NAMES[i][LOGGER_NAME_INDEX]);
            assertNull("Logger " + NAMES[i][LOGGER_NAME_INDEX] +
                " level should have been null, not " + level,
                level);
        }

        // Set up some logger levels to persist

        Level[] levels = {Level.FINEST, Level.WARNING, Level.ALL};
        for ( i = 0; i < mLoggers.length; i++ )
        {
            mComponentLogger.setLevel(NAMES[i][LOGGER_NAME_INDEX], levels[i]);
        }

        // Now save logger levels and load again. All levels should be what
        // was set.

        mComponentLogger.saveLoggerSettings();
        mComponentLogger.loadLoggerSettings();
        for ( i = 0; i < mLoggers.length; i++ )
        {
            assertEquals("setLoggerLevel failed to set level for logger " +
                NAMES[i][LOGGER_NAME_INDEX],
                levels[i],
                mComponentLogger.getSavedLevel(NAMES[i][LOGGER_NAME_INDEX]));
        }
    }

    /**
     * Test the set method for the logger level with a null argument. This
     * test covers all of the set<level> methods because they all delegate to
     * this method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetLevelBadNullLoggerName()
        throws Exception
    {
        try
        {
            mComponentLogger.setLevel(null, null);
            fail("Expected exception not received");
        }
        catch (java.lang.IllegalArgumentException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("logName")));
        }
    }

    /**
     * Test the set method for the logger level with an unregistered logger.
     * This test covers all of the set<level> methods because they all delegate
     * to this method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testSetLevelBadNotRegistered()
        throws Exception
    {
        try
        {
            mComponentLogger.setLevel(LOGGER_NAME_1, null);
            fail("Expected exception not received");
        }
        catch (javax.jbi.JBIException ex)
        {
            // Verification
            assertTrue("Incorrect exception received: " + ex.toString(),
                       (-1 < ex.getMessage().indexOf("JBIFW2016")));
        }
    }

    /**
     * Tests the setAll method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetAll()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger1, DISPLAY_NAME_1);
        mComponentLogger.setAll(LOGGER_NAME_1);
        assertEquals("setAll failed: ",
            Level.ALL, mLogger1.getLevel());
    }

    /**
     * Tests the setConfig method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetConfig()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger1, DISPLAY_NAME_1);
        mComponentLogger.setConfig(LOGGER_NAME_1);
        assertEquals("setConfig failed: ",
            Level.CONFIG, mLogger1.getLevel());
    }

    /**
     * Tests the setDefault method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetDefault()
        throws Exception
    {
        // Set the child and parent loggers so that we can validate the results
        mParentLogger.setLevel(Level.FINEST);
        mComponentLogger.addLogger(mLogger1, DISPLAY_NAME_1);
        mLogger1.setLevel(Level.INFO);
        assertEquals("getLevel failed to get correct level: ",
            Level.INFO.getLocalizedName(),
            mComponentLogger.getLevel(LOGGER_NAME_1));

        mComponentLogger.setDefault(LOGGER_NAME_1);
        assertNull("setDefault failed: ",
            mLogger1.getLevel());
        assertEquals("setDefault failed: ",
            mParentLogger.getLevel().getLocalizedName(),
            mComponentLogger.getLevel(LOGGER_NAME_1));
    }

    /**
     * Tests the setFine method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetFine()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger1, DISPLAY_NAME_1);
        mComponentLogger.setFine(LOGGER_NAME_1);
        assertEquals("setFine failed: ",
            Level.FINE, mLogger1.getLevel());
    }

    /**
     * Tests the setFiner method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetFiner()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger2, DISPLAY_NAME_2);
        mComponentLogger.setFiner(LOGGER_NAME_2);
        assertEquals("setFiner failed: ",
            Level.FINER, mLogger2.getLevel());
    }

    /**
     * Tests the setFinest method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetFinest()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger3, DISPLAY_NAME_3);
        mComponentLogger.setFinest(LOGGER_NAME_3);
        assertEquals("setFinest failed: ",
            Level.FINEST, mLogger3.getLevel());
    }

    /**
     * Tests the setInfo method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetInfo()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger1, DISPLAY_NAME_1);
        mComponentLogger.setInfo(LOGGER_NAME_1);
        assertEquals("setInfo failed: ",
            Level.INFO, mLogger1.getLevel());
    }

    /**
     * Tests the setOff method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetOff()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger2, DISPLAY_NAME_2);
        mComponentLogger.setOff(LOGGER_NAME_2);
        assertEquals("setOff failed: ",
            Level.OFF, mLogger2.getLevel());
    }

    /**
     * Tests the setSevere method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetSevere()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger3, DISPLAY_NAME_3);
        mComponentLogger.setSevere(LOGGER_NAME_3);
        assertEquals("setSevere failed: ",
            Level.SEVERE, mLogger3.getLevel());
    }

    /**
     * Tests the setWarning method.
     * @throws Exception if an unexpected error occurs.
     */

    public void testSetWarning()
        throws Exception
    {
        mComponentLogger.addLogger(mLogger1, DISPLAY_NAME_1);
        mComponentLogger.setWarning(LOGGER_NAME_1);
        assertEquals("setWarning failed: ",
            Level.WARNING, mLogger1.getLevel());
    }

    /**
     * Tests the constructor to make sure that it is unaffected by persisted
     * logger properties and still re-reads the logging definitions from the
     * component descriptor in jbi.xml.
     * @throws Exception if an unexpected error occurs.
     */
    public void testConstructor()
        throws Exception
    {
        String logName1 = "com.sun.jbi.testengine.LifeCycle";
        String logName2 = "com.sun.jbi.testengine.Messaging";
        String displayName1 = "Engine Life Cycle";
        String displayName2 = "Message Handler";

        // Create a new ComponentLogger instance and persist one logger
        // setting. 
        ComponentLogger cl = new ComponentLogger(mComponent);
        cl.addLoggerInfo(logName1, displayName1, false);
        cl.saveLoggerSettings();
        
        // Create a new XML document for the test with loggers defined
        String xmlString = "<logging:Logging root=\"com.sun.jbi\">" +
                           "<logging:logger displayName=\"" +
                           displayName1 +
                           "\">" +
                           logName1 + 
                           "</logging:logger>" +
                           "<logging:logger displayName=\"" +
                           displayName2 +
                           "\">" +
                           logName2 + 
                           "</logging:logger>" +
                           "</logging:Logging>";
        Element extension = mUtils.createElement(xmlString);
        mScaffoldRegistry.setXmlElement(extension);

        // Create a new ComponentLogger instance and make sure the persisted
        // settings did not affect the pre-defined settings.
        cl = new ComponentLogger(mComponent);

        // Verify that both pre-defined loggers were picked up.
        String[] names = cl.getLoggerNames();
        assertEquals("Pre-defined logger not picked up correctly",
            logName1, names[1]);
        assertEquals("Pre-defined logger not picked up correctly",
            logName2, names[2]);

        // Verify that the display names were picked up
        assertEquals("Display name not picked up correctly",
            displayName1, cl.getDisplayName(logName1));
        assertEquals("Display name not picked up correctly",
            displayName2, cl.getDisplayName(logName2));
    }

}
