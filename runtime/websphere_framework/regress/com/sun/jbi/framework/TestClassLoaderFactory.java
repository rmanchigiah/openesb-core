/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestClassLoaderFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.io.File;
import java.io.InputStream;

import java.net.URLClassLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.jbi.JBIException;

/**
 * Tests for the various methods on the ClassLoaderFactory class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestClassLoaderFactory
    extends junit.framework.TestCase
{
    /**
     * Value of the $SRCROOT environment variable
     */
    private String mSrcroot;

    /**
     * ClassLoaderFactory
     */
     private ClassLoaderFactory mClassLoaderFactory ;

    /**
     * EnvironmentContext
     */
     private EnvironmentContext mEnvironmentContext ;

    /**
     * Component
     */
     private Component mComponent ;
     
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestClassLoaderFactory(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the ClassLoaderFactory instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mSrcroot = System.getProperty("junit.srcroot"); 
	mEnvironmentContext = new EnvironmentContext(new ScaffoldPlatformContext(), 
                new JBIFramework(), new Properties());
	mClassLoaderFactory = ClassLoaderFactory.getInstance() ;
	mComponent = new Component() ;
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  Bootstrap classloader test methods ================================

    /**
     * testCreateBootstrapClassLoader
     * tests the creation of a Bootstrap classloadera by
     * 1.creating the class loader with a specified class path containing 2  jar files (a.jar,b.jar)
     * 2.loading 1 class from each JAR file using reflection
     * 3.creating an instance of the classpath
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateBootstrapClassLoader()
           throws Exception
    {
	final String testName = "testCreateBootstrapClassLoader";
        try
        {
            start( testName ); 
            // set up the class paths
            List cpList = new ArrayList() ;
            String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
            String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
            String path3 = getTestClassPath() ;
            String path4 = getTestResourcePath() ;
            cpList.add (path1) ;
            cpList.add (path2) ;
            cpList.add (path3) ;
            cpList.add (path4) ;

	    mComponent.setName("bootcomponent0") ;
            mComponent.setBootstrapClassPathElements(cpList);
	    log ("created component") ;

	    // create a bootstrap classloader
            ClassLoader bootCL = mClassLoaderFactory.createBootstrapClassLoader(mComponent) ;
            URLClassLoader booturlCL = (URLClassLoader) bootCL ;
	    // load a class using the classloader
	    
            Class testAJarClass = bootCL.loadClass("binding1.Bootstrap1") ;
            Class testBJarClass = bootCL.loadClass("engine1.Bootstrap1") ;
            Class testLoadClass = bootCL.loadClass("com.sun.workflow.classes.WFEngine") ;
            //InputStream testLoadResource = bootCL.getResourceAsStream("com/sun/workflow/resources/resources.properties") ;
            InputStream testLoadResource = bootCL.getResourceAsStream("resources.properties") ;
	    
	    // create classes to make sure classloading succeeded
            Object binding1CL   = testAJarClass.newInstance() ;
            Object engine1CL    = testBJarClass.newInstance() ;
            Object wfEngineCL   = testLoadClass.newInstance() ;

	    //
	    // MOVE THIS CODE TO A UTILS CLASS
	    // load class resource to make sure classloading succeeded
	    Properties p = new Properties();
	    p.load(testLoadResource); 

	    // if we get here, the test has passed
            log ("testCreateBootstrapClassLoader Passed ") ;
	    endOK(testName);
        }
        catch (JBIException e)
        {
	    // test failure
	    endException(testName , e);

	    fail ("Failure getting/setting BootstrapClass") ;
        } 
	catch (ClassNotFoundException cnfe)
	{
	    // test failure
	    endException(testName , cnfe);

	    fail ("Failure getting/setting BootstrapClass") ;
	}
    }

    /**
     * testCreateBootstrapClassLoaderLoadClass
     * tests loading of a class by the bootstrap class loader based
     * on the values passed in 
     * @throws Exception if an unexpected error occurs
     */
    public void testBootstrapClassLoaderLoadClass(String testName, boolean selfFirst , String expectedClassLoader , String className , List libraries)
           throws Exception
    {
        try
        {
            start( testName ); 
            // set up the class paths
            List cpList = new ArrayList() ;
            String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
            String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
            String path3 = getTestClassPath() ;
            String path4 = getTestResourcePath() ;
            cpList.add (path1) ;
            cpList.add (path2) ;
            cpList.add (path3) ;
            cpList.add (path4) ;

	    mComponent.setName("bootcomponent0") ;
            mComponent.setBootstrapClassPathElements(cpList);
	    log ("created component") ;

	    // create a bootstrap classloader
            ClassLoader bootCL = mClassLoaderFactory.createBootstrapClassLoader(mComponent) ;
            URLClassLoader booturlCL = (URLClassLoader) bootCL ;
	    // load a class using the classloader
	    
            Class testAJarClass = bootCL.loadClass("binding1.Bootstrap1") ;
            Class testBJarClass = bootCL.loadClass("engine1.Bootstrap1") ;
            Class testLoadClass = bootCL.loadClass("com.sun.workflow.classes.WFEngine") ;
            //InputStream testLoadResource = bootCL.getResourceAsStream("com/sun/workflow/resources/resources.properties") ;
            InputStream testLoadResource = bootCL.getResourceAsStream("resources.properties") ;
	    
	    // create classes to make sure classloading succeeded
            Object binding1CL   = testAJarClass.newInstance() ;
            Object engine1CL    = testBJarClass.newInstance() ;
            Object wfEngineCL   = testLoadClass.newInstance() ;

	    //
	    // MOVE THIS CODE TO A UTILS CLASS
	    // load class resource to make sure classloading succeeded
	    Properties p = new Properties();
	    p.load(testLoadResource); 

	    // if we get here, the test has passed
            log ("testCreateBootstrapClassLoader Passed ") ;
	    endOK(testName);
        }
        catch (JBIException e)
        {
	    // test failure
	    endException(testName , e);

	    fail ("Failure getting/setting BootstrapClass") ;
        } 
	catch (ClassNotFoundException cnfe)
	{
	    // test failure
	    endException(testName , cnfe);

	    fail ("Failure getting/setting BootstrapClass") ;
	}
    }
    
    /**
     * testCreateBootstrapClassLoaderBadEmptyPaths
     * tests the creation of a Bootstrap classloadera by
     * 1.creating the class loader with a specified class path containing 2  jar files (a.jar,b.jar)
     * 2.loading 1 class from each JAR file using reflection
     * 3.creating an instance of the classpath
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateBootstrapClassLoaderBadEmptyPaths()
           throws Exception
    {
        try
        {
            // set up the class paths
            List cpList = new ArrayList() ;

	    mComponent.setName("bootcomponent1") ;
            mComponent.setBootstrapClassPathElements(cpList);
	    log ("created component") ;

	    // create a bootstrap classloader
            ClassLoader bootCL = mClassLoaderFactory.createBootstrapClassLoader(mComponent) ;
	    
	    // test fails if we get here
	    fail("testCreateBootstrapClassLoaderBadEmptyPaths:JBIException not caught") ;
        }
        catch (IllegalArgumentException e)
        {
	    // test passes if we catch an exception
            log("Test Passed with Exception:" + e.toString()) ;
	    // Verification
            assertTrue("Incorrect exception received: " + e.toString(),
		                                   (-1 < e.getMessage().indexOf("Empty list")));
	}
    }

    /**
     * testCreateBootstrapClassLoaderBadNullPaths
     * tests the creation of a Bootstrap classloadera by
     * 1.creating the class loader with a specified class path containing 2  jar files (a.jar,b.jar)
     * 2.loading 1 class from each JAR file using reflection
     * 3.creating an instance of the classpath
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateBootstrapClassLoaderBadNullPaths()
           throws Exception
    {
        try
        {
	    // create a bootstrap classloader
            ClassLoader bootCL = mClassLoaderFactory.createBootstrapClassLoader(null) ;
	    
	    // test fails if we get here
	    fail("testCreateBootstrapClassLoaderBadNullPaths:JBIException not caught") ;
        }
        catch (IllegalArgumentException e)
        {
	    // test passes if we catch an exception
            log("Test Passed with Exception:" + e.toString()) ;
	    // Verification
            assertTrue("Incorrect exception received: " + e.toString(),
		                                   (-1 < e.getMessage().indexOf("Null")));
	}
    }


// =============================  Component classloader test methods ================================

    /**
     * tests the successful creation of a component class loader 
     * by loading a class in its classpath
     *
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateComponentClassLoader()
	    throws Exception
    {
        final String testName = "testCreateComponentClassLoader" ;
	try
	{	
            start (testName); 
            // create a component
            List cpList = new ArrayList() ;
            String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
            String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
            String path3 = getTestClassPath() ;
            String path4 = getTestResourcePath() ;
            cpList.add (path1) ;
            cpList.add (path2) ;
            cpList.add (path3) ;
            cpList.add (path4) ;
	    mComponent.setName("component1") ;
            mComponent.setComponentClassPathElements(cpList);
	    log ("created component") ;
	
	    //create a valid shared library list 
	    List sharedLibList = new ArrayList() ;
	    List sharedLibElements = new ArrayList() ;
	    String path5 = getTestJarsPath() + File.separator +  "quotes.jar" ;
	    sharedLibElements.add(path5) ;
	    SharedLibrary sharedLib3= new SharedLibrary("sharedLib3" ,
                                                        "jar file c",
                                                        "/sharedLib3",
                                                        sharedLibElements );	
            sharedLibList.add ("sharedLib3");
            mComponent.setSharedLibraryNames(sharedLibList);
	    log ("created shared library") ;

            //create and install the shared classloader before
	    //installing the component classloader that references it.
	    ClassLoader sharedLoader =mClassLoaderFactory.createSharedClassLoader( sharedLib3) ;

	    // create the component classloader
	    ClassLoader compClassLoader =
                mClassLoaderFactory.createComponentClassLoader(mComponent) ;
	                                                    

	    if (compClassLoader == null)
	      fail (testName + ":create classloader unsucessful-NULL") ;

	    // attempt to load a component class from a JAR using this classloader
            Class testAJarClass = compClassLoader.loadClass("binding1.Bootstrap1") ;
	    Object testAJarObj  = testAJarClass.newInstance() ;

	    // attempt to load a component class from a classpath using this classloader
            Class testClassFromPath = compClassLoader.loadClass("com.sun.workflow.classes.WFEngine") ;
	    Object testClassFromPathObj  = testClassFromPath.newInstance() ;
	    
	    // attempt to load a component class from a classpath using this classloader
            //InputStream testResourceFromPath = compClassLoader.getResourceAsStream("com/sun/workflow/resources/resources.properties") ;
            InputStream testResourceFromPath = compClassLoader.getResourceAsStream("resources.properties") ;
	    // MOVE THIS CODE TO A UTILS CLASS
	    //
            Properties p = new Properties();
	    p.load(testResourceFromPath);

	    
	    // also attempt to load a shared class using the same component classloader
	    Class testCJarClass  = compClassLoader.loadClass("library1.QuoteEngine");
	    Object testCJatObj   = testCJarClass.newInstance();
	    //double sunStockPrice = QuoteEngine.getQuote("SUNW");
	    log (testName + ":component classloader successfully created-PASSED ") ;
	    endOK(testName);
	}
	catch(JBIException je)
	{
           endException( testName,je);
           je.printStackTrace() ;
           // exception means failure
	   fail (testName + ":unable to create a component classloader" + je.toString() ) ;
	}	
	catch(ClassNotFoundException cnfe) 
	{
            endException( testName,cnfe);
            fail(testName + ":classloader unable to load component class" + cnfe.toString()) ;
	}
    }


    /**
     * testCreateComponentClassLoaderBadNullComponent
     * tests the creation of a Component classloader by
     * creating the class loader with a null Component reference and a valid
     * Shared Library List
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateComponentClassLoaderBadNullComponent()
           throws Exception
    {
        try
        {
	    //create a valid shared library list - currently null
	    // TO DO after implementing corresponding functionality in 
	    // ClassLoaderFactory.createComponentClassLoader()
	    
	    // create a component classloader
            ClassLoader compCL = mClassLoaderFactory.createComponentClassLoader(null);
	    
	    // test fails if we get here
	    fail("testCreateComponentClassLoaderBadNullComponent:JBIException not caught") ;
        }
        catch (IllegalArgumentException e)
        {
	    // test passes if we catch an exception
            log("Test Passed with Exception:" + e.toString()) ;
	    // Verification
            assertTrue("Incorrect exception received: " + e.toString(),
		                                   (-1 < e.getMessage().indexOf("Null")));
	}
    }

    /**
     * testCreateComponentClassLoaderBadNullNamespaces
     * tests the creation of a Component classloader by
     * creating the class loader with valid Component reference and a null
     * Shared Library List
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateComponentClassLoaderBadNullSNs()
           throws Exception
    {
	final String testName = "testCreateComponentClassLoaderBadNullSNs";
        try
        {
            // create a component
            List cpList = new ArrayList() ;
            String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
            String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
            cpList.add (path1) ;
            cpList.add (path2) ;
	    mComponent.setName("component1") ;
            mComponent.setComponentClassPathElements(cpList);
	    log ("created component") ;

	    // create a component classloader
            ClassLoader compCL =
                mClassLoaderFactory.createComponentClassLoader(mComponent);
	    
	    // attempt to load a component class using this classloader
            Class testAJarClass = compCL.loadClass("binding1.Bootstrap1") ;
	    Object testAJarObj  = testAJarClass.newInstance() ;
	    
	    // test succeeds if we get here
	    log (testName + ":" + "passed") ; 
        }
        catch (Exception e)
        {
	    // test fails if we catch an exception
            log("Test Failed with Exception:" + e.toString()) ;
	    // Verification
	    fail(testName + ":JBIException not caught") ;
	}
    }

    /**
     * testCreateComponentClassLoaderParent
     * tests if the created ClassLoader has the correct parent
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateComponentClassLoaderCorrectParent ()
           throws Exception
    {
        try
        {
            // create a component
	    Component comp = new Component() ; 
            List cpList = new ArrayList() ;
            String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
            String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
            cpList.add (path1) ;
            cpList.add (path2) ;
	    comp.setName("component1") ;
            comp.setComponentClassPathElements(cpList);
	    log ("created component") ;

	    //create a valid shared library list - currently null
	    // TO DO after implementing corresponding functionality in 
	    // ClassLoaderFactory.createComponentClassLoader()
	    List sharedLibList = new ArrayList() ;
	    List sharedLibElements = new ArrayList() ;
	    String path = getTestJarsPath() + File.separator +  "quotes.jar" ;
	    sharedLibElements.add(path) ;
            SharedLibrary sharedLib = new SharedLibrary("dummy", "dummy",
                                                        "dummy",
                                                        sharedLibElements );	
            sharedLibList.add ("dummy") ;
            comp.setSharedLibraryNames(sharedLibList);
	    log ("created shared library") ;

	    // create a shared library classloader 
	    ClassLoader sharedCL = mClassLoaderFactory.createSharedClassLoader( sharedLib ) ;

	    // create a component classloader for the same shared library
            ClassLoader compCL =
                mClassLoaderFactory.createComponentClassLoader(comp) ;
            ClassLoader compCLParent= compCL.getParent();
	    log ("Component ClassLoader parent is :" + compCLParent) ;
	    
	    // test that the parent is the Delegating Classloader
	    log ("COMPCL:" +  new Integer(compCLParent.toString().indexOf("DelegatingClassLoader")).toString());

	    assertFalse("Correct Parent of Component ClassLoader" + compCL.toString() ,
			(-1 == compCLParent.toString().indexOf("DelegatingClassLoader")));
            log("testCreateComponentClassLoaderCorrectParent Passed") ;
        }
        catch (JBIException e)
        {
	    // test passes if we catch an exception
            log("Test Failed with Exception:" + e.toString()) ;
	    fail ("Unable to get ClassLoader Chain Of Component Classloader") ;
	}
    }

    /**
     * tests the successful creation of a component class loader 
     * with a class path extension by loading a class from the extra
     * class path library
     *
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateComponentClassLoaderWithExtension()
	    throws Exception
    {
        final String testName = "testCreateComponentClassLoaderWithExtension" ;
	try
	{	
            start (testName); 
            // create a component
            List cpList = new ArrayList() ;
            String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
            String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
            String path3 = getTestClassPath() ;
            String path4 = getTestResourcePath() ;
	    String path5 = getTestJarsPath() + File.separator +  "quotes.jar" ;
            cpList.add (path1) ;
            cpList.add (path2) ;
            cpList.add (path3) ;
            cpList.add (path4) ;
	    mComponent.setName("component1") ;
            mComponent.setComponentClassPathElements(cpList);
	    log ("created component") ;

            // create the class loading extension directory, which is
            // JBI_INSTALL_ROOT/libext/component1 for this test
            try
            {
                String extdir = mEnvironmentContext.getJbiInstallRoot()
                    + File.separator + "libext" + File.separator + mComponent.getName();;
	        File extFile = new File(extdir);
                extFile.mkdirs();
                log ("create extension directory: " + extdir);
                log ("copying " + path5 + " to extension directory");
                String copy = "cp -f " + path5 + " " + extdir;
                Process p = Runtime.getRuntime().exec(copy);
                p.waitFor();
                if ( p.exitValue() != 0 )
                {
                    fail(testName + ": failed to copy extension jar");
                }
                log ("copy complete");
            }
            catch ( Exception ex )
            {
               endException( testName,ex);
               ex.printStackTrace() ;
	       fail (testName + ":unable to create extension files" + ex.toString() ) ;
            }

	    // create the component classloader
	    ClassLoader compClassLoader =
                mClassLoaderFactory.createComponentClassLoader(mComponent) ;

	    if (compClassLoader == null)
	      fail (testName + ":create classloader unsucessful-NULL") ;

	    // attempt to load a component class from a JAR using this classloader
            Class testAJarClass = compClassLoader.loadClass("binding1.Bootstrap1") ;
	    Object testAJarObj  = testAJarClass.newInstance() ;

	    // attempt to load a component class from a classpath using this classloader
            Class testClassFromPath = compClassLoader.loadClass("com.sun.workflow.classes.WFEngine") ;
	    Object testClassFromPathObj  = testClassFromPath.newInstance() ;
	    
	    // attempt to load a component class from a classpath using this classloader
            //InputStream testResourceFromPath = compClassLoader.getResourceAsStream("com/sun/workflow/resources/resources.properties") ;
            InputStream testResourceFromPath = compClassLoader.getResourceAsStream("resources.properties") ;
            Properties p = new Properties();
	    p.load(testResourceFromPath);

	    // Now attempt to load a class from the extension using the same
            // component classloader
	    Class testCJarClass  = compClassLoader.loadClass("library1.QuoteEngine");
	    Object testCJatObj   = testCJarClass.newInstance();
	    log (testName + ":component classloader successfully created-PASSED ") ;
	    endOK(testName);
	}
	catch(JBIException je)
	{
           endException( testName,je);
           je.printStackTrace() ;
           // exception means failure
	   fail (testName + ":unable to create a component classloader" + je.toString() ) ;
	}	
	catch(ClassNotFoundException cnfe) 
	{
            endException( testName,cnfe);
            fail(testName + ":classloader unable to load component class" + cnfe.toString()) ;
	}
    }

// ======================  Shared classloader test methods ====================

    /**
     * tests the successful creation of a shared library class loader 
     * by loading a class in its classpath
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateSharedClassLoader()
	    throws Exception
    {
        final String testName = "testCreateSharedClassLoader";
	try
	{	
	    //create a valid shared library list
	    List sharedLibElements = new ArrayList() ;
	    String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
	    String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
	    sharedLibElements.add(path1) ;
	    sharedLibElements.add(path2) ;
	    SharedLibrary sharedLib = new SharedLibrary("sharedLib2" ,
                                                        "Jar files a and b",
                                                        "/sharedLib2",
                                                        sharedLibElements );	
	    log ("created shared library") ;

	    // create the classloader
	    ClassLoader sharedClassLoader = mClassLoaderFactory.createSharedClassLoader
	                                                    (sharedLib) ;
	    if (sharedClassLoader == null)
	      fail (testName + ":create shared classloader unsucessful-NULL") ;

	    // attempt to load a component class using this classloader
            Class testAJarClass = sharedClassLoader.loadClass("binding1.Bootstrap1") ;
	    Object testAJarObj  = testAJarClass.newInstance() ;

            Class testBJarClass = sharedClassLoader.loadClass("engine1.Bootstrap1") ;
	    Object testBJarObj  = testBJarClass.newInstance() ;

            if ( (testAJarObj == null) || (testBJarObj == null) )
	      fail (testName + ":unable to create object for shared class -NULL") ;
	    log (testName + ":shared library classloader,objects test passed") ;
	}
	catch(JBIException je)
	{
           // exception means failure
	   fail (testName + ":unable to create a shared classloader" + je.toString() ) ;
	}	
	catch(ClassNotFoundException cnfe) 
	{
            fail(testName + ":classloader unable to load shared class" + cnfe.toString()) ;
	}
    }

    /**
     * tests that an exception is thrown if a null shared library 
     * is passed to it. 
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateSharedClassLoaderNullNamespace()
	    throws Exception
    {
        final String testName = "testCreateSharedClassLoaderNullNamespace";
	try
	{	
	    // create the classloader
	    ClassLoader sharedClassLoader = mClassLoaderFactory.createSharedClassLoader
	                                                    (null) ;
	    // if we reach here , we've failed
	    fail (testName + ":" + "failed by missing to catch an exception");
	}
	catch(IllegalArgumentException je)
	{
           // exception means failure
	   assertTrue(testName + ":" + "failed by missing to catch an exception" ,
	              -1 < je.toString().indexOf("Null argument"));
	   log (testName +  ": passed") ;
	}	
    }

    /**
     * tests that repeated calls to createSharedClassLoader do not throw
     * an exception
     * @throws Exception if an unexpected error occurs
     */
    public void testCreateSharedClassLoaderTwice()
	    throws Exception
    {
        final String testName = "testCreateSharedClassLoaderTwice";
	try
	{	
	    //create a valid shared library list
	    List sharedLibElements = new ArrayList() ;
	    String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
	    String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
	    sharedLibElements.add(path1) ;
	    sharedLibElements.add(path2) ;
	    SharedLibrary sharedLib = new SharedLibrary("sharedLib1" ,
                                                        "Jar files a and b",
                                                        "/sharedLib1",
                                                        sharedLibElements );	
	    log ("created shared library") ;

	    // create the classloader
	    ClassLoader sharedClassLoader  = mClassLoaderFactory.createSharedClassLoader
	                                                    (sharedLib) ;
	    ClassLoader sharedClassLoader1 = mClassLoaderFactory.createSharedClassLoader
	                                                    (sharedLib) ;
	    
	    assertTrue(testName + ":" + "shared  classloader instances are different" ,
                                      (sharedClassLoader == sharedClassLoader1)) ;
	    // if we reach here , we're ok
	    log (testName + ":passed");
	}
	catch(JBIException je)
	{
           // exception means failure
	   fail (testName + ":failed with exception:" + je.toString());
	}	
    }

    /**
     * tests the successful removal of a shared library classloader w/o 
     * an error
     * @throws Exception if an unexpected error occurs
     */
    public void testRemoveSharedClassLoader()
	    throws Exception
    {
        final String testName = "testRemoveSharedClassLoader";
	try
	{	
	    //create a valid shared library list
	    List sharedLibElements = new ArrayList() ;
	    String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
	    String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
	    sharedLibElements.add(path1) ;
	    sharedLibElements.add(path2) ;
	    SharedLibrary sharedLib = new SharedLibrary("sharedLib4" ,
                                                        "Jar files a and b",
                                                        "/sharedLib4",
                                                        sharedLibElements );	
	    log ("created shared library") ;

	    // create the classloader
	    ClassLoader sharedClassLoader  = mClassLoaderFactory.createSharedClassLoader( sharedLib );
	    // remove the classloader
	    mClassLoaderFactory.removeSharedClassLoader(sharedLib.getName()) ;

	    // test if classloader exists.
	    //
	    mClassLoaderFactory.getSharedClassLoader (sharedLib.getName());
	    fail (testName + " failed--- did not remove shared classloader") ;
	}
	catch(JBIException je)
	{
           // correct exception means failure
	   log (testName +  " passed OK" + je.toString().indexOf(" no shared")); 
	   assertTrue(testName + ":" + "failed by not removing classloader" ,
	              -1 < je.toString().indexOf("No shared classloader found"));
	}	
    }

    /**
     * tests that null argument passed to removeSharedClassLoader
     * throws the correct exception.
     * @throws Exception if an unexpected error occurs
     */
    public void testRemoveSharedClassLoaderNullSL()
	    throws Exception
    {
        final String testName = "testRemoveSharedClassLoaderNullSL";
	try
	{	
	    // remove the classloader
	    mClassLoaderFactory.removeSharedClassLoader(null) ;

	    fail (testName + " failed no exception thrown") ;
	}
	catch(IllegalArgumentException e)
	{
           // correct exception means failure
	   assertTrue(testName + ":" + "failed by not removing classloader" ,
	              -1 < e.toString().indexOf("Null argument"));
	   log (testName +  " passed OK" ); 
	}	
    }

    /**
     * tests that an invalid shared library passed to removeSharedClassLoader
     * throws the correct exception.
     * @throws Exception if an unexpected error occurs
     */
    public void testRemoveSharedClassLoaderInvalidSL()
	    throws Exception
    {
        final String testName = "testRemoveSharedClassLoaderInvalidSL";
	try
	{	
	    //create a valid shared library list
	    List sharedLibElements = new ArrayList() ;
	    String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
	    String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
	    sharedLibElements.add(path1) ;
	    sharedLibElements.add(path2) ;
	    SharedLibrary sharedLib = new SharedLibrary("sharedLib5" ,
                                                        "Jar files a and b",
                                                        "/sharedLib5",
                                                        sharedLibElements );	
	    log ("created shared library") ;
	    
	    // remove the classloader
	    mClassLoaderFactory.removeSharedClassLoader(sharedLib.getName()) ;

	    fail (testName + " failed no exception thrown") ;
	}
	catch(JBIException je)
	{
           // correct exception means failure
	   assertTrue(testName + ":" + "failed with wrong exception" + je.toString() ,
	              -1 < je.toString().indexOf("No shared classloader"));
	   log (testName +  " passed OK" ); 
	}	
    }

    /**
     * tests the successful removal of a component classloader 
     * w/o an error
     * @throws Exception if an unexpected error occurs
     */
    public void testRemoveComponentClassLoader()
	    throws Exception
    {
        final String testName = "testRemoveComponentClassLoader";
	try
	{	
            List cpList = new ArrayList() ;
            String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
            String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
            String path3 = getTestClassPath() ;
            String path4 = getTestResourcePath() ;
            cpList.add (path1) ;
            cpList.add (path2) ;
            cpList.add (path3) ;
            cpList.add (path4) ;
	    Component comp = new Component();
	    comp.setName(testName) ;
            comp.setComponentClassPathElements(cpList);
	    log ("created component") ;

	    // create a component classloader
	    ClassLoader compClassLoader = mClassLoaderFactory.createComponentClassLoader( comp ) ;
	    // remove the classloader
	    mClassLoaderFactory.removeComponentClassLoader(comp.getName()) ;

	    // test if classloader exists.
	    //
	    mClassLoaderFactory.getComponentClassLoader (comp.getName());
	    fail (testName + " failed--- did not remove component classloader") ;
	}
	catch(JBIException je)
	{
           // correct exception means failure
	   assertTrue(testName + ":" + "failed by not removing classloader" ,
	              -1 < je.toString().indexOf("No component classloader found"));
	   log (testName +  " passed OK");
	}	
    }

    /**
     * tests the successful removal of a component classloader containing
     * shared libraries  w/o an error
     * @throws Exception if an unexpected error occurs
     */
    public void testRemoveComponentClassLoaderWithSharedLib()
	    throws Exception
    {
        final String testName = "testRemoveComponentClassLoaderWithSharedLib";
	try
	{	
            List cpList = new ArrayList() ;
            String path1 = getTestJarsPath() + File.separator +  "a.jar" ;
            String path2 = getTestJarsPath() + File.separator +  "b.jar" ;
            String path3 = getTestClassPath() ;
            String path4 = getTestResourcePath() ;
            cpList.add (path1) ;
            cpList.add (path2) ;
            cpList.add (path3) ;
            cpList.add (path4) ;
	    Component comp = new Component();
	    comp.setName(testName) ;
            comp.setComponentClassPathElements(cpList);
	    log ("created component") ;

	    //create a valid shared library list
	    List sharedLibElements = new ArrayList() ;
	    String path5 = getTestJarsPath() + File.separator +  "a.jar" ;
	    String path6 = getTestJarsPath() + File.separator +  "b.jar" ;
	    sharedLibElements.add(path5) ;
	    sharedLibElements.add(path6) ;

	    log ("created shared library element list") ;

	    //create a valid shared library id list
	    List sharedLibIds = new ArrayList() ;
	    String slid1 = "sharedLib6" ;
	    sharedLibIds.add(slid1) ;
	    // link namspace to component
	    comp.setSharedLibraryNames( sharedLibIds );

	    // create shared library
	    SharedLibrary sharedLib = new SharedLibrary("sharedLib6" ,
                                                        "Jar files a and b",
                                                        "/sharedLib6",
                                                        sharedLibElements );	
	    
	    // create Shared Classloader 
	    // as this needs to exist prior to a component that
	    // references it
	    
	    ClassLoader sharedClassLoader =
                mClassLoaderFactory.createSharedClassLoader( sharedLib ) ;
	    ClassLoader compClassLoader =
                mClassLoaderFactory.createComponentClassLoader( comp ) ;
	    // remove the classloader
	    mClassLoaderFactory.removeComponentClassLoader(comp.getName()) ;

	    // test if classloader exists.
	    //
	    mClassLoaderFactory.getComponentClassLoader (comp.getName());
	    fail (testName + " failed--- did not remove component classloader") ;
	}
	catch(JBIException je)
	{
           // correct exception means failure
	   assertTrue(testName + ":" + "failed by not removing classloader" ,
	              -1 < je.toString().indexOf("No component classloader found"));
	   log (testName +  " passed OK");
	}	
    }

    /**
     * tests that null argument passed to removeComponentClassLoader
     * throws the correct exception.
     * @throws Exception if an unexpected error occurs
     */
    public void testComponentClassLoaderNullComponent()
	    throws Exception
    {
        final String testName = "testComponentClassLoaderNullComponent";
	try
	{	
	    // remove the classloader
	    mClassLoaderFactory.removeComponentClassLoader(null) ;

	    fail (testName + " failed no exception thrown") ;
	}
	catch(IllegalArgumentException e)
	{
           // correct exception means failure
	   assertTrue(testName + ":" + "failed by not removing classloader" ,
	              -1 < e.toString().indexOf("Null argument received for componentId"));
	   log (testName +  " passed OK" ); 
	}	
    }

    /**
     * tests that an invalid component id passed to removeSharedClassLoader
     * throws the correct exception.
     * @throws Exception if an unexpected error occurs
     */
    public void testRemoveComponentClassLoaderInvalidComponent()
	    throws Exception
    {
        final String testName = "testRemoveComponentClassLoaderInvalidComponent";
	try
	{	
            Component c = new Component () ;
	    c.setName("invalidcomponent");
	    
	    log ("created component") ;
	    
	    // remove the classloader
	    mClassLoaderFactory.removeComponentClassLoader(c.getName()) ;

	    fail (testName + " failed no exception thrown") ;
	}
	catch(JBIException je)
	{
           // correct exception means failure
	   assertTrue(testName + ":" + "failed with wrong exception" + je.toString() ,
	              -1 < je.toString().indexOf("No component classloader found for"));
	   log (testName +  " passed OK" ); 
	}	
    }
// ========================  general classloader factory utility test methods ==========================

    /**
     * testgetInstance
     * tests that only a single instance of the ClassLoaderFactory exists at a point in time
     * @throws Exception if an unexpected error occurs
     */
    public void testgetInstance()
	    throws Exception
    {
        // create a new instance and compare it to the one already 
	// created in setup 
	ClassLoaderFactory cf1 = ClassLoaderFactory.getInstance() ;
	ClassLoaderFactory cf2 = ClassLoaderFactory.getInstance() ;

	// java.lang.Object guarantees that 2 references that refer to the same object
	// will always return true when Object.equals() is invoked on any of them with the other
	// as the target.
	assertEquals (cf1 , cf2) ;
	log("Test testgetInstance() passed:" + (cf1==cf2)) ;
    }

// =============================  private utility test methods ================================

    /*  
     * private logging method
     */ 
    private static void log ( String msg )
    {
        System.out.println ("[TestClassLoaderFactory]-" + msg) ;
    }

    /*  
     * location of the path to the unit test data JAR files
     */ 
    private String getTestJarsPath ()
    {
        return (CLUtils.getTestJarsPath(mSrcroot) ) ;
    }

    /*  
     * location of the path to the unit test data class files
     */ 
    private String getTestClassPath ()
    {
        return (CLUtils.getTestClassPath( mSrcroot )) ;
    }

    /*  
     * location of the path to the unit test data resource files
     */ 
    private String getTestResourcePath ()
    {
        return (CLUtils.getTestResourcePath( mSrcroot )) ;
    }

    /*  
     * private logging method to indicate start of test
     */ 
    private void start (String testName)
    {
        log (testName + "-" + "Started") ;
    }

    /*  
     * private logging method to indicate normal start of test
     */ 
    private void endOK (String testName)
    {
        log (testName + "-" + "Ended OK") ;
    }

    /*  
     * private logging method to indicate end of test due to an exception
     */ 
    private void endException (String testName , Exception e)
    {
        log (testName + "-" + "Ended With Following Exception") ;
        log (e.toString()) ;
    }

}
