/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestStringTranslator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;

/**
 * Tests for the StringTranslator class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestStringTranslator
    extends junit.framework.TestCase
{
    /**
     * Value of the $SRCROOT environment variable
     */
    private String mSrcroot;

    /**
     * Locale of the JVM
     */
    private Locale mLocale;

    /**
     * Package name for the test resource bundles.
     */
    private static final String PACKAGE_NAME = "testI18n";

    /**
     * Message key for message not found in any bundle
     */
    private static final String KEY_NOT_FOUND = "not_found";

    /**
     * Message key for message in english bundle only
     */
    private static final String KEY_ENGLISH_ONLY = "english_only";

    /**
     * Message key for message with no inserts
     */
    private static final String KEY_NO_INSERTS = "no_inserts";

    /**
     * Message key for message with one insert
     */
    private static final String KEY_ONE_INSERT = "one_insert";

    /**
     * Message key for message with two inserts
     */
    private static final String KEY_TWO_INSERTS = "two_inserts";

    /**
     * Message key for message with three inserts
     */
    private static final String KEY_THREE_INSERTS = "three_inserts";

    /**
     * Message key for message with four inserts
     */
    private static final String KEY_FOUR_INSERTS = "four_inserts";

    /**
     * Message key for message with five inserts
     */
    private static final String KEY_FIVE_INSERTS = "five_inserts";

    /**
     * Message text for message not found
     */
    private static final String MSG_NOT_FOUND =
        "No translation available for message with key=not_found " +
        "and inserts=[x,y,z].";

    /**
     * Message text for message not found
     */
    private static final String MSG_NO_TRANSLATION =
        "No translation available for message with key=english_only " +
        "and inserts=[].";

    /**
     * Message text for message in english only
     */
    private static final String MSG_ENGLISH_ONLY =
        "This message is found only in the English bundle.";

    /**
     * Message text for message with no inserts
     */
    private static final String MSG_NO_INSERTS =
        "This message has no inserts.";

    /**
     * Message text for message with no inserts in German
     */
    private static final String MSG_NO_INSERTS_DE =
        "Diese Nachricht haben kein Einlage.";

    /**
     * Message text for message with one insert
     */
    private static final String MSG_ONE_INSERT =
        "This is a message with one insert here: insert1.";

    /**
     * Message text for message with one insert in German
     */
     
    private static final String MSG_ONE_INSERT_DE =
        "Diese ist ein Nachricht mit ein Einlage hier: insert1.";

    /**
     * Message text for message with two inserts
     */
    private static final String MSG_TWO_INSERTS =
        "This message contains two inserts, one here: insert1 " +
        "and one over there: insert2.";

    /**
     * Message text for message with two inserts
     */
    private static final String MSG_TWO_INSERTS_BAD =
        "This message contains two inserts, one here: insert1 " +
        "and one over there: {1}.";

    /**
     * Message text for message with two inserts in German
     */
     
    private static final String MSG_TWO_INSERTS_DE =
        "Diese Nachricht halten zwei Einlage, ein hier: insert1 " +
        "und ein uber da: insert2.";

    /**
     * Message text for message with two inserts in German
     */
     
    private static final String MSG_TWO_INSERTS_DE_BAD =
        "Diese Nachricht halten zwei Einlage, ein hier: insert1 " +
        "und ein uber da: {1}.";

    /**
     * Message text for message with three inserts
     */
    private static final String MSG_THREE_INSERTS =
        "Here is a message with three inserts: insert1, insert2, and insert3.";

    /**
     * Message text for message with three inserts in German
     */
    private static final String MSG_THREE_INSERTS_DE =
        "Hier ist ein Nachricht mit drei Einlage: insert1, insert2, und insert3.";

    /**
     * Message text for message with four inserts
     */
    private static final String MSG_FOUR_INSERTS =
        "Some messages have four inserts, the first is insert1, the second is" +
        " insert2, the third is insert3, and the fourth is insert4.";

    /**
     * Message text for message with four inserts in German
     */
    private static final String MSG_FOUR_INSERTS_DE =
        "Etwas Nachricht haben vier Einlage, erste ist insert1, zweite " +
        "ist insert2, dritte ist insert3, und vierte ist insert4.";

    /**
     * Message text for message with five inserts
     */
    private static final String MSG_FIVE_INSERTS =
        "I have a message with five inserts displayed together: " +
        "insert1+insert2+insert3+insert4+insert5.";

    /**
     * Message text for message with five inserts in German
     */
    private static final String MSG_FIVE_INSERTS_DE =
        "Ich habe ein Nachricht mit funf Einlage entfalten zusammen: " +
        "insert1+insert2+insert3+insert4+insert5.";

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestStringTranslator(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mSrcroot = System.getProperty("junit.srcroot") + "/";
        mLocale = Locale.getDefault();
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        Locale.setDefault(mLocale);
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Test the constructor with a null class loader specified.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStringTranslatorNoClassLoader()
        throws Exception
    {
        StringTranslator st;

        // Create an instance for a valid resource bundle. This should
        // return a non-null value.

        st = new StringTranslator(PACKAGE_NAME, null);
        assertNotNull("Failure on new StringTranslator(): " +
                      "expected a non-null value, got a null",
                      st);

        // Create an instance for a non-existent resource bundle. This
        // should return a non-null value.

        st = new StringTranslator(this.getClass().getPackage().getName(), null);
        assertNotNull("Failure on new StringTranslator(): " +
                      "expected a non-null value, got a null",
                      st);
    }

    /**
     * Test the constructor with a class loader specified.
     * @throws Exception if an unexpected error occurs.
     */
    public void testStringTranslatorClassLoader()
        throws Exception
    {
        // Create a class loader to pass to the constructor

        String path;
        File dir;
        URL[] urls;
        URLClassLoader cl;
        path = mSrcroot + "framework/regress/" + PACKAGE_NAME;
        dir = new File(path);
        urls = new URL[1];
        urls[0] = dir.toURL();
        cl = new URLClassLoader(urls);

        StringTranslator st;
        String msg;

        // Create an instance for a valid resource bundle. This should
        // return a non-null value.

        st = new StringTranslator(PACKAGE_NAME, cl);
        assertNotNull("Failure on new StringTranslator(): " +
                      "expected a non-null value, got a null",
                      st);

        // Verify that the instance is using the correct bundle.

        msg = st.getString(KEY_ENGLISH_ONLY);
        assertTrue("Failure on getString(" + KEY_ENGLISH_ONLY + "): " +
                   "expected \"" + MSG_ENGLISH_ONLY +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_ENGLISH_ONLY));

        // Create an instance for a non-existent resource bundle. This
        // should return a non-null value.

        st = new StringTranslator(this.getClass().getPackage().getName(), cl);
        assertNotNull("Failure on new StringTranslator(): " +
                      "expected a non-null value, got a null",
                      st);

        // Verify that the instance has no resource bundle.

        msg = st.getString(KEY_ENGLISH_ONLY);
        assertTrue("Failure on getString(" + KEY_ENGLISH_ONLY + "): " +
                   "expected \"" + MSG_NO_TRANSLATION +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_NO_TRANSLATION));

    }
    /**
     * Test getString() with a message key that is not found in any resource
     * bundle.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringNotFound()
        throws Exception
    {
        StringTranslator st;
        String msg;

        // First test the English locale. There is no fallback resource
        // bundle in this case.

        st = new StringTranslator(PACKAGE_NAME, null);

        msg = st.getString(KEY_NOT_FOUND, "x", "y", "z");
        assertTrue("Failure on getString(" + KEY_NOT_FOUND + "): " +
                   "expected \"" + MSG_NOT_FOUND +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_NOT_FOUND));

        // Next test the German locale. There is a fallback resource bundle
        // (English) in this case, but the message is not found there either.

        Locale.setDefault(Locale.GERMANY);
        st = new StringTranslator(PACKAGE_NAME, null);

        msg = st.getString(KEY_NOT_FOUND, "x", "y", "z");
        assertTrue("Failure on getString(" + KEY_NOT_FOUND + "): " +
                   "expected \"" + MSG_NOT_FOUND +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_NOT_FOUND));

    }

    /**
     * Test getString() with a message key only.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringNoInserts()
        throws Exception
    {
        StringTranslator st;
        String msg;

        // Test in English locale.

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_NO_INSERTS);   
        assertTrue("Failure on getString(" + KEY_NO_INSERTS + "): " +
                   "expected \"" + MSG_NO_INSERTS +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_NO_INSERTS));

        // Test in German locale.

        Locale.setDefault(Locale.GERMANY);

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_NO_INSERTS);   
        assertTrue("Failure on getString(" + KEY_NO_INSERTS + "): " +
                   "expected \"" + MSG_NO_INSERTS_DE +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_NO_INSERTS_DE));

    }

    /**
     * Test getString() with a single insert.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringOneInsert()
        throws Exception
    {
        StringTranslator st;
        String msg;

        // Test in English locale.

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_ONE_INSERT, "insert1");
        assertTrue("Failure on getString(" + KEY_ONE_INSERT + "): " +
                   "expected \"" + MSG_ONE_INSERT +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_ONE_INSERT));

        // Test in German locale.

        Locale.setDefault(Locale.GERMANY);

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_ONE_INSERT, "insert1");
        assertTrue("Failure on getString(" + KEY_ONE_INSERT + "): " +
                   "expected \"" + MSG_ONE_INSERT_DE +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_ONE_INSERT_DE));

    }

    /**
     * Test getString() with two inserts.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringTwoInserts()
        throws Exception
    {
        StringTranslator st;
        String msg;

        // Test in English locale.

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_TWO_INSERTS, "insert1", "insert2");
        assertTrue("Failure on getString(" + KEY_TWO_INSERTS + "): " +
                   "expected \"" + MSG_TWO_INSERTS +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_TWO_INSERTS));

        // Test in German locale.

        Locale.setDefault(Locale.GERMANY);

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_TWO_INSERTS, "insert1", "insert2");
        assertTrue("Failure on getString(" + KEY_TWO_INSERTS + "): " +
                   "expected \"" + MSG_TWO_INSERTS_DE +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_TWO_INSERTS_DE));

    }

    /**
     * Test getString() with two inserts.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringTwoInsertsMissingOne()
        throws Exception
    {
        StringTranslator st;
        String msg;

        // Test in English locale.

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_TWO_INSERTS, "insert1");
        assertTrue("Failure on getString(" + KEY_TWO_INSERTS + "): " +
                   "expected \"" + MSG_TWO_INSERTS_BAD +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_TWO_INSERTS_BAD));

        // Test in German locale.

        Locale.setDefault(Locale.GERMANY);

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_TWO_INSERTS, "insert1");
        assertTrue("Failure on getString(" + KEY_TWO_INSERTS + "): " +
                   "expected \"" + MSG_TWO_INSERTS_DE_BAD +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_TWO_INSERTS_DE_BAD));

    }

    /**
     * Test getString() with three inserts.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringThreeInserts()
        throws Exception
    {
        StringTranslator st;
        String msg;

        // Test in English locale.

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_THREE_INSERTS, "insert1", "insert2", "insert3");
        assertTrue("Failure on getString(" + KEY_THREE_INSERTS + "): " +
                   "expected \"" + MSG_THREE_INSERTS +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_THREE_INSERTS));

        // Test in German locale.

        Locale.setDefault(Locale.GERMANY);

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_THREE_INSERTS, "insert1", "insert2", "insert3");
        assertTrue("Failure on getString(" + KEY_THREE_INSERTS + "): " +
                   "expected \"" + MSG_THREE_INSERTS_DE +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_THREE_INSERTS_DE));

    }

    /**
     * Test getString() with four inserts.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringFourInserts()
        throws Exception
    {
        StringTranslator st;
        String msg;

        // Test in English locale.

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_FOUR_INSERTS, "insert1", "insert2",
                           "insert3", "insert4");
        assertTrue("Failure on getString(" + KEY_FOUR_INSERTS + "): " +
                   "expected \"" + MSG_FOUR_INSERTS +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_FOUR_INSERTS));

        // Test in German locale.

        Locale.setDefault(Locale.GERMANY);

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_FOUR_INSERTS, "insert1", "insert2",
                           "insert3", "insert4");
        assertTrue("Failure on getString(" + KEY_FOUR_INSERTS + "): " +
                   "expected \"" + MSG_FOUR_INSERTS_DE +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_FOUR_INSERTS_DE));

    }

    /**
     * Test getString() with five inserts.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetStringFiveInserts()
        throws Exception
    {
        StringTranslator st;
        String msg;
        String inserts[] = {"insert1", "insert2", "insert3", "insert4", "insert5"};

        // Test in English locale.

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_FIVE_INSERTS, inserts);
        assertTrue("Failure on getString(" + KEY_FIVE_INSERTS + "): " +
                   "expected \"" + MSG_FIVE_INSERTS +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_FIVE_INSERTS));

        // Test in German locale.

        Locale.setDefault(Locale.GERMANY);

        st = new StringTranslator(PACKAGE_NAME, null);
        msg = st.getString(KEY_FIVE_INSERTS, inserts);
        assertTrue("Failure on getString(" + KEY_FIVE_INSERTS + "): " +
                   "expected \"" + MSG_FIVE_INSERTS_DE +
                   "\", got \"" + msg + "\"",
                   msg.equals(MSG_FIVE_INSERTS_DE));

    }

}
