#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)framework00003.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#regress setup
. ./regress_defs.ksh

echo testing shared library invocation for engines/bindings

# package components
ant -emacs -q -f $JV_FRAMEWORK_REGRESS_DIR/scripts/build-framework00003-components.ant build-components

# install shared library 
$JBI_ANT -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/StockQuotesSharedLibrary.jar install-shared-library

# install binding 
$JBI_ANT -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/SharedLibraryTestBinding.jar install-component

# install engine
$JBI_ANT -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/SharedLibraryTestEngine.jar install-component

# start binding (this invokes the shared lib API)
$JBI_ANT -Djbi.component.name="SharedLibraryTestBinding" start-component

# start engine (this invokes the shared lib API)
$JBI_ANT -Djbi.component.name="SharedLibraryTestEngine" start-component

# shutdown binding
$JBI_ANT -Djbi.component.name="SharedLibraryTestBinding" shut-down-component

# shutdown engine
$JBI_ANT -Djbi.component.name="SharedLibraryTestEngine" shut-down-component

# uninstall binding
$JBI_ANT -Djbi.component.name="SharedLibraryTestBinding" uninstall-component

# uninstall engine
$JBI_ANT -Djbi.component.name="SharedLibraryTestEngine" uninstall-component

# uninstall shared library
$JBI_ANT -Djbi.shared.library.name="StockQuotesSharedLibrary" uninstall-shared-library

echo Engines Shared Library Invocation
grep "MSFT'S STOCK VALUE" $JBI_DOMAIN_ROOT/logs/classloaderregresstests.sharedlibtest.engine.rt.log | wc -l
echo Bindings Shared Library Invocation
grep "MSFT'S STOCK VALUE" $JBI_DOMAIN_ROOT/logs/classloaderregresstests.sharedlibtest.binding.rt.log | wc -l
# ### END
