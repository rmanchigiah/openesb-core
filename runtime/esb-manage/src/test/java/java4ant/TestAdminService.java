/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestAdminService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import javax.management.remote.*;
import javax.management.*;
import org.junit.Ignore;

/**
 */
@Ignore
public class TestAdminService
{
     
    private MBeanServerConnection mbns;
    private static final String RESULT_PREFIX = "##### Result of ";
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String TARGET   = "target";
    
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    public ObjectName getAdminServiceName()
        throws Exception
    {
        String admin = "com.sun.jbi:Target=" + System.getProperty(TARGET, "domain") + ",ServiceName=AdminService,ServiceType=Admin";
        return new ObjectName(admin);
    }
    
    
    public void getEngineComponents()
        throws Exception
    {
        ObjectName[] objNames = (ObjectName[]) mbns.invoke(getAdminServiceName(), "getEngineComponents", new Object[0], new String[0]);
        
        System.out.println(RESULT_PREFIX + " getEngineComponents =" );
        for ( ObjectName mbnName : objNames )
        {
            System.out.println(mbnName.toString());
        }
    }
    
    public void getBindingComponents()
            throws Exception
    {
        ObjectName[] objNames = (ObjectName[]) mbns.invoke(getAdminServiceName(), "getBindingComponents", new Object[0], new String[0]);
        
        System.out.println(RESULT_PREFIX + " getBindingComponents =" );
        for ( ObjectName mbnName : objNames )
        {
            System.out.println(mbnName.toString());
        }
    }
    
    public static void main (String[] params)
        throws Exception 
    {
        TestAdminService test = new TestAdminService();
        
        test.initMBeanServerConnection();
        test.getEngineComponents();
        test.getBindingComponents();
    }
    
}
