/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JavaEEApplicationInterceptor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.ee;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.message.MessageBuilder;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.ObjectName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import org.xml.sax.InputSource;

/**
 * Implementation of ApplicationInterceptor contract, which takes care of 
 * Glassfish-specific Java EE processing concerns.  This implementation just
 * delegates to the JavaEEDeployerMBean registered by the Java EE Service Engine.
 *
 * @author Sun Microsystems, Inc.
 */
public class JavaEEApplicationInterceptor implements ApplicationInterceptor
{
    /** Name of the Java EE SE */
    private static final String JAVA_EE_SE = "sun-javaee-engine";
    /** Name of the JavaEEDeployerMBean registered by the Java EE SE*/
    private static final String JAVA_EE_DEPLOYER_MBEAN= "JavaEEDeployer";
    /** Not success ;-) */
    private static final String FAILED = "FAILED";
    
    private EnvironmentContext  mCtx;
    private ObjectName          mJavaEEDeployerMBean;
    private Logger              mLog = Logger.getLogger("com.sun.jbi.management.ee");
    private StringTranslator    mTranslator;
    private XPath               mXPath;
    
    /**
     * Create a new instance of JavaEEApplicationInterceptor.
     */
    public JavaEEApplicationInterceptor(EnvironmentContext ctx)
    {
        mCtx = ctx;
        mTranslator = ctx.getStringTranslator(this.getClass().getPackage().getName());
        mJavaEEDeployerMBean = mCtx.getMBeanNames().getCustomEngineMBeanName(
                JAVA_EE_DEPLOYER_MBEAN, JAVA_EE_SE);
        
        // initialize XPath processor
        mXPath = XPathFactory.newInstance().newXPath();
        mXPath.setNamespaceContext(new JBINSContext());
    }
    
    /** Performs platform-specific application processing on a service unit
     *  for a given operation.
     *  @param operation name of the operation
     *  @serviceAssemblyName name of the service assembly targeted by the operation
     *  @serviceUnitName name of the service unit targeted by the operation
     *  @serviceUnitPath path to the deployable service unit archive
     *  @target the target (cluster/instance/domain) of the operation
     */
    public String performAction(String operation,
        String serviceAssemblyName, 
        String serviceUnitName, 
        String serviceUnitRootPath,
        String target)
        throws Exception
    {
        String result = null;
        
        // Deployment ops on JavaEEDeployerMBean require the rootPath, while
        // lifecycle ops do not.
        if ("deploy".equals(operation) || "undeploy".equals(operation))
        {
            result = invokeJavaEEMBean(operation, new String[] {
                serviceAssemblyName, serviceUnitName, serviceUnitRootPath, target});
        }
        else if ("start".equals(operation) || "stop".equals(operation) || 
                 "shutDown".equals(operation))
        {
            result = invokeJavaEEMBean(operation, new String[] {
                serviceAssemblyName, serviceUnitName, target});
        }
        else
        {
            mLog.log(Level.FINE, "ApplicationInterceptor: unknown operation {0}for service unit {1}", new Object[]{operation, serviceUnitName});
        }
        
        // We need to throw an exception if the interceptor encounters a fatal
        // error.
        if (result != null)
        {
            verifyResult(operation, result);
        }
                
        return result;
    }    
    
    /** Utility method used to invoke a method on the JavaEEDeployerMBean
     *  registered by the Java EE Service Engine.
     */
    private String invokeJavaEEMBean(String operation, String[] params)
        throws Exception
    {
        String response = null;
        
        // build the sig
        String[] methodSig = new String[params.length];
        java.util.Arrays.fill(methodSig, "java.lang.String");
        
        if (mCtx.getMBeanServer().isRegistered(mJavaEEDeployerMBean))
        {
            try
            {
                response = (String)mCtx.getMBeanServer().invoke(
                    mJavaEEDeployerMBean, operation, params, methodSig);
                
                mLog.log(Level.FINER, "Response from JavaEEDeployer MBean: {0}", response);
            }
            catch (javax.management.MBeanException ex)
            {
                // throw the original exception
                response = ex.getTargetException().getMessage();
                
                // if it's not an XML result message, we need to throw again
                if (!MessageBuilder.isXmlString(response))
                {
                    throw ex.getTargetException();
                }
            }
            catch (Exception ex)
            {
                // Something really nasty happened.
                ex.printStackTrace();
            }
        }
        else
        {
            mLog.warning(mTranslator.getString(
                    LocalStringKeys.JBI_ADMIN_NO_JAVAEEDEPLOYER_MBEAN, 
                    mJavaEEDeployerMBean));
        }
        
        return response;
    }
    
    /** Verify the response returned from the Java EE Deployer MBean.  If
     *  it's task-result value is FAILED, throw an exception containing the error
     *  loc-message.
     *  @param operation operation being invoked
     *  @param result result of the invocation
     *  @throws Exception if the task result is FAILED
     */
    private void verifyResult(String operation, String result)
        throws Exception
    {
        // Look for an error in the result message
        synchronized (mXPath)
        {
            if (FAILED.equals(mXPath.evaluate("//jbi:task-result", 
                    new InputSource(new StringReader(result)))))
            {
                throw new Exception(mXPath.evaluate("//jbi:loc-message", 
                    new InputSource(new StringReader(result))));
            }
        }
    }
    
    /** 
     *  Provides namespace processing help for the JBI management message NS.
     */
    class JBINSContext implements javax.xml.namespace.NamespaceContext
    {
        String JBI_NS_PREFIX = "jbi";
        String JBI_NS_URI = "http://java.sun.com/xml/ns/jbi/management-message";
        
        public Iterator getPrefixes(String namespaceURI)
        {
            ArrayList p = new ArrayList();
            if (namespaceURI.equals(JBI_NS_PREFIX))
            {
                p.add(JBI_NS_URI);
            }
            
            return p.iterator();
        }

        public String getPrefix(String namespaceURI) 
        {
            return JBI_NS_PREFIX;
        }

        public String getNamespaceURI(String prefix) 
        {
            return JBI_NS_URI;
        }
        
    }
}
