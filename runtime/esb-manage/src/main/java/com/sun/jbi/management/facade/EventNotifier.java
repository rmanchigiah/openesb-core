/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventNotifier.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.EventNotifierBase;
import com.sun.jbi.management.EventNotifierMBean;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.platform.PlatformContext;

import java.util.Hashtable;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.NotificationListener;
import javax.management.ObjectName;


/**
 * This is the DAS Event Notifier MBean that collects notifications from each
 * instance Event Notifier MBean in the domain, and re-emits them. This allows a
 * client to register as a notification listener with this single MBean instead
 * of having to register with every instance Event Notifier MBean.
 *
 * In order for this to function correctly, this MBean keeps track of all active
 * instances. This is accomplished in two ways. First, when this MBean is first
 * created by the Management Runtime Service, the <code>start()</code> method
 * is called to determine what instances in the domain are currently active and
 * register with each of them as a notification listener. This is accomplished
 * by invoking the <code>addNotificationListener</code> operation on the
 * instance Event Notifier MBeans. Second, when a new instance Event Notifier
 * MBean is registered after this MBean is already active, the new instance
 * MBean invokes the <code>instanceStarted</code> operation on this MBean to
 * inform it to register as a notification listener with the instance MBean.
 * The <code>instanceStarted</code> operation in turn invokes the
 * <code>addNotificationListener</code> operation on the instance MBean to
 * complete the registration.
 *
 * For both cases, an entry is added to a table of instance names and MBean
 * object names for every instance Event Notifier MBean with which this MBean
 * is registered as a notification listener.
 *
 * When an instance is terminating, its Event Notifier MBean invokes the
 * <code>instanceStopped</code> operation on this MBean to inform it to
 * unregister as a notification listener with the instance MBean. The
 * <code>instanceStopped</code> operation in turn invokes the 
 * <code>removeNotificationListener</code> operation on the instance MBean to
 * complete the unregistration. Likewise, when the Management Runtime Service
 * is stopping, this MBean unregisters as a notification listener from each
 * instance Event Notifier MBean still found in the table of instance names.
 * The entry for an instance is removed as soon as the unregistration has
 * completed.
 *
 * @author Mark S White
 */
public class EventNotifier
    extends NotificationBroadcasterSupport
    implements EventNotifierBase, EventNotifierMBean, NotificationListener
{
    /**
     * Flag indicating whether or not notifications are enabled.
     */
    private boolean mNotificationsEnabled;

    /**
     * Logger for messages.
     */
    private Logger mLog;

    /**
     * The MBeanNames instance used for constructing JMX ObjecNames.
     */
    private MBeanNames mMBeanNames;

    /**
     * JMX ObjectName of this MBean.
     */
    private ObjectName mObjName;

    /**
     * Platform Context for the runtime environment.
     */
    private PlatformContext mPlatformContext;

    /**
     * List of instances with which this MBean has registered as a notification
     * listener. The key is the instance name and the value is the MBean object
     * name of the EventNotifierMBean for the instance. Note that Hashtable
     * is synchronized.
     */
    private Hashtable<String, ObjectName> mInstanceList;

    /**
     * Constructor to initialize local variables.
     */
    EventNotifier(EnvironmentContext envContext, String target)
    {
        mLog = Logger.getLogger("com.sun.jbi.management");
        mMBeanNames = envContext.getMBeanNames();
        mPlatformContext = envContext.getPlatformContext();
        mNotificationsEnabled = true; // default to enabled for now
        mInstanceList = new Hashtable();
    }

//
// Overridden methods of the NotificationBroadcasterSupport class
//

    /**
     * Returns an array indicating, for each notification this MBean may send,
     * the notification type, the name of the Java class of the notification,
     * and a description of the notification.
     *
     * @return an array of <code>javax.managagement.MBeanNotificationInfo</code>
     * objects which describe the notifications.
     */
    public MBeanNotificationInfo[] getNotificationInfo()
    {
        MBeanNotificationInfo[] notifs = {
            new MBeanNotificationInfo(NOTIFICATION_TYPES,
                NOTIFICATION_CLASS_NAME, NOTIFICATION_DESCRIPTION)
        };
        return notifs;
    }

//
// Methods from the EventNotifierMBean interface
//

    /**
     * This method is called to disable event notifications.
     *
     * @return true if notifications were previously enabled, false if they
     * were already disabled.
     */
    public boolean disableNotifications()
    {
        boolean ret = mNotificationsEnabled;
        mNotificationsEnabled = false;
        mLog.fine("Event notifications disabled for domain");
        return ret;
    }

    /**
     * This method is called to enable event notifications.
     *
     * @return true if notifications were previously disabled, false if they
     * were already enabled.
     */
    public boolean enableNotifications()
    {
        boolean ret = !mNotificationsEnabled;
        mNotificationsEnabled = true;
        mLog.fine("Event notifications enabled for domain");
        return ret;
    }

    /**
     * Inform this MBean that an instance has started and this MBean needs to
     * register itself as a notification listener for the instance Event
     * Notifier MBean on that instance. This calls a method to invoke the
     * <code>addNotificationListener</code> operation on the Event Notifier
     * MBean for the specified instance using the specified JMX object name.
     *
     * @param instanceName the name of the instance that has started.
     * @param objectName the JMX object name of the Event Notifier MBean for
     * the instance.
     * @return true if registration was successful, false if not.
     */
    public boolean instanceStarted(String instanceName, ObjectName objectName)
    {
        boolean ret = false;

        // Although the instance list table is itself synchronized, this
        // check must be synchronized to avoid a race condition that could
        // occur if an instance is trying to register at the same time that
        // this MBean is trying to register with the instance MBean.

        synchronized ( mInstanceList )
        {
            if ( !mInstanceList.containsKey(instanceName) )
            {
                boolean added = invokeInstanceMBean("addNotificationListener",
                    instanceName, objectName);
 
                // If we invoked the operation, then we are now registered as
                // a notification listener with the instance so we add that
                // instance to our list.

                if ( added )
                {
                    mInstanceList.put(instanceName, objectName);
                    ret = true;
                }
            }
        }
        return ret;
    }

    /**
     * Inform this MBean that an instance has stopped and this MBean needs to
     * unregister itself as a notification listener for the instance Event
     * Notifier MBean on that instance. This calls a method to invoke the
     * <code>removeNotificationListener</code> operation on the Event Notifier
     * MBean for the specified instance using the specified JMX object name.
     *
     * @param instanceName the name of the instance that has stopped.
     * @param objectName the JMX object name of the Event Notifier MBean for
     * the instance.
     * @return true if unregistration was successful, false if not.
     */
    public boolean instanceStopped(String instanceName, ObjectName objectName)
    {
        boolean ret = false;

        // Although the instance list table is itself synchronized, this
        // check must be synchronized to avoid a race condition that could
        // occur if an instance is trying to unregister at the same time that
        // this MBean is trying to unregister with the instance MBean.

        synchronized ( mInstanceList )
        {
            if ( mInstanceList.containsKey(instanceName) )
            {
                invokeInstanceMBean("removeNotificationListener",
                    instanceName, objectName);

                // Here we ignore the return value because regardless of whether
                // or not we were able to invoke the operation, we should remove
                // the instance list entry. If we could not invoke the operation
                // it means the instance or MBean was already gone so we are
                // no longer registered as a notification listener.

                mInstanceList.remove(instanceName);
                ret = true;
            }
        }
        return ret;
    }

//
// Methods from the NotificationListener interface
//

    /**
     * This is invoked when a JMX notification is received from an instance
     * Event Notifier MBean with which this MBean has registered as a
     * notification listener. If notifications are enabled, this resends the
     * notification without modification; otherwise, this does nothing.
     *
     * @param notification The notification object.
     * @param handback An object reference that was used when this MBean
     * registered as a listener with the instance Event Notifier MBean. In
     * this case, it is the instance name.
     */
    public void handleNotification(Notification notification, Object handback)
    {
        mLog.log(Level.FINER, "Received notification from instance {0}: {1}", new Object[]{(String) handback, notification.toString()});
        if ( mNotificationsEnabled )
        {
            mLog.finer("Forwarding notification");
            sendNotification(notification);
        }
    }

//
// Package-visible methods
//

    /**
     * Register this MBean as a notification listener with the Event Notifier
     * MBean on each active instance in the domain. This is called when the
     * Management Runtime Service starts. 
     */
    void start()
    {
        // Get lists of all standalone and clustered instances in the domain

        Set<String> saServers = mPlatformContext.getStandaloneServerNames();
        Set<String> clServers = mPlatformContext.getClusteredServerNames();

        // For every instance, check to see if the instance is up, and if so,
        // call instanceStarted() to register with that instance's Event
        // Notifier MBean as a notification listener.

        ObjectName mbName;

        for ( String server : saServers )
        {
            if ( mPlatformContext.isInstanceUp(server) )
            {
                mbName = mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_FRAMEWORK,
                    MBeanNames.CONTROL_TYPE_NOTIFICATION, server);
                instanceStarted(server, mbName);
            }
        }

        for ( String server : clServers )
        {
            if ( mPlatformContext.isInstanceUp(server) )
            {
                mbName = mMBeanNames.getSystemServiceMBeanName(
                    MBeanNames.SERVICE_NAME_FRAMEWORK,
                    MBeanNames.CONTROL_TYPE_NOTIFICATION, server);
                instanceStarted(server, mbName);
            }
        }
    }

    /**
     * Unregister this MBean as a notification listener from each instance
     * Event Notifier MBean with which it was previously registered. This is
     * called when the Management Runtime Service stops.
     */
    void stop()
    {
        Set<String> instanceNames = mInstanceList.keySet();
        ObjectName mbName;

        for ( String instanceName : instanceNames )
        {
            mbName = mInstanceList.get(instanceName);
            if ( null != mbName )
            {
                instanceStopped(instanceName, mbName);
            }
        }
    }

//
// Private methods
//

    /**
     * Invoke an operation on an instance Event Notifier MBean.
     *
     * @param operation the operation to be invoked. This can be either
     * "addNotificationListener" or "removeNotificationListener".
     * @param instanceName the name of the instance on which the operation is
     * to be invoked.
     * @param objectName the JMX object name of the event notifier MBean for
     * the instance.
     * @return true if the operation was invoke successfully, false if not.
     */
    private boolean invokeInstanceMBean(String operation, String instanceName,
        ObjectName objectName)
    {
        boolean ret = false;

        // Get an MBean server connection with the instance

        MBeanServerConnection mbsConn = null;
        try
        {
            mbsConn =
                mPlatformContext.getMBeanServerConnection(instanceName);
        }
        catch ( Throwable ex )
        {
            // This means that the instance is not currently running. This
            // can be ignored. Log this at the FINE level for debugging
            // purposes.
            mLog.log(Level.FINE, "Unable to get MBean server connection " +
                " for instance " + instanceName + ", exception follows.", ex);
        }

        // If we have an MBean server connection, invoke the operation.

        if ( null != mbsConn )
        {
            // Set up parameters for calling the MBean
            String[] signature = {
                "javax.management.NotificationListener",
                "javax.management.NotificationFilter",
                "java.lang.Object"
            };
            Object[] params = {this, null, instanceName};

            // Invoke the specified operation on the instance Event
            // Notifier MBean
            try
            {
                mLog.log(Level.FINE, "Invoking {0} on MBean {1} on instance {2}", new Object[]{operation, objectName, instanceName});
                mbsConn.invoke(objectName, operation, params, signature);
                ret = true;
            }
            catch ( Throwable ex )
            {
                Throwable actualEx = stripJmxException(ex);
                mLog.log(Level.FINE, "Failure invoking MBean operation, " +
                    "exception follows.", actualEx);
            }
        }
        return ret;
    }

    /**
     * Strip JMX Exceptions from the exception chain and return the actual
     * embedded Exception thrown by the remote instance.
     *
     * @param ex the JMX exception caught.
     * @return the actual exception thrown by the remote instance.
     */
    private static Throwable stripJmxException(Throwable ex)
    {
        Throwable currEx = ex;

        while ( null != currEx.getCause() )
        {
            if ( !(currEx instanceof javax.management.JMException)
                && !(currEx instanceof javax.management.JMRuntimeException) )
            {
                return currEx;
            }
            currEx = currEx.getCause();
        }
        return currEx;
    }

}
