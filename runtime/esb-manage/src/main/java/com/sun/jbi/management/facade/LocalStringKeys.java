/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LocalStringKeys.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

/**
 * This interface contains the property keys used for looking up message
 * text in the LocalStrings resource bundle.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LocalStringKeys
{
    
    /**
     * General Admin Messages.
     */
    static final String JBI_ADMIN_INVALID_ARCHIVE_TYPE = 
        "JBI_ADMIN_INVALID_ARCHIVE_TYPE";

    static final String JBI_ADMIN_UNSUPPORTED_OPERATION =
        "JBI_ADMIN_UNSUPPORTED_OPERATION";
    
    static final String JBI_ADMIN_MBEAN_NOT_REGISTERED =
        "JBI_ADMIN_MBEAN_NOT_REGISTERED";
    
    static final String JBI_ADMIN_LOCAL_MBEAN_NOT_REGISTERED =
        "JBI_ADMIN_LOCAL_MBEAN_NOT_REGISTERED";
    
    static final String JBI_ADMIN_INSTANCE_DOWN = 
        "JBI_ADMIN_INSTANCE_DOWN";
    
    static final String JBI_ADMIN_INVOKING_REMOTE_OPERATION =
        "JBI_ADMIN_INVOKING_REMOTE_OPERATION";
    
    static final String JBI_ADMIN_DONE_INVOKING_REMOTE_OPERATION =
        "JBI_ADMIN_DONE_INVOKING_REMOTE_OPERATION";
    
    static final String JBI_ADMIN_INVOKING_LOCAL_OPERATION =
        "JBI_ADMIN_INVOKING_LOCAL_OPERATION";
    
    static final String JBI_ADMIN_DONE_INVOKING_LOCAL_OPERATION =
        "JBI_ADMIN_DONE_INVOKING_LOCAL_OPERATION";
    
    static final String JB_ADMIN_TARGET_NOT_UP =
        "JB_ADMIN_TARGET_NOT_UP";
    
    /**
     * Installation Service Message Keys
     */
    static final String JBI_ADMIN_FAILED_LOAD_NEW_INSTALLER = 
        "JBI_ADMIN_FAILED_LOAD_NEW_INSTALLER";
    
    static final String JBI_ADMIN_UNKNOWN_COMPONENT = 
        "JBI_ADMIN_UNKNOWN_COMPONENT";
    
    static final String JBI_ADMIN_KNOWN_COMPONENT = 
        "JBI_ADMIN_KNOWN_COMPONENT";    
    
    static final String JBI_ADMIN_UNKNOWN_SHARED_LIBRARY = 
        "JBI_ADMIN_UNKNOWN_SHARED_LIBRARY";
    
    static final String JBI_ADMIN_KNOWN_SHARED_LIBRARY =
        "JBI_ADMIN_KNOWN_SHARED_LIBRARY";
    
    static final String JBI_ADMIN_FAILED_ADD_COMPONENT  =
        "JBI_ADMIN_FAILED_ADD_COMPONENT";
    
    static final String JBI_ADMIN_FAILED_REMOVE_COMPONENT = 
        "JBI_ADMIN_FAILED_REMOVE_COMPONENT";
    
    static final String JBI_ADMIN_FAILED_ADD_SHARED_LIBRARY =
        "JBI_ADMIN_FAILED_ADD_SHARED_LIBRARY";
    
    static final String JBI_ADMIN_FAILED_REMOVE_SHARED_LIBRARY =
        "JBI_ADMIN_FAILED_REMOVE_SHARED_LIBRARY";
    
    static final String JBI_ADMIN_FAILED_REGISTER_COMPONENT_MBEAN =
        "JBI_ADMIN_FAILED_REGISTER_COMPONENT_MBEAN";
    
    static final String JBI_ADMIN_BAD_INSTALL_URL = 
        "JBI_ADMIN_BAD_INSTALL_URL";
    
    static final String JBI_ADMIN_SHARED_LIBRARY_NOT_INSTALLED_ON_TARGET =
        "JBI_ADMIN_SHARED_LIBRARY_NOT_INSTALLED_ON_TARGET";
    
    static final String JBI_ADMIN_SHARED_LIBRARY_INSTALLED_ON_TARGET =
        "JBI_ADMIN_SHARED_LIBRARY_INSTALLED_ON_TARGET";
    
    static final String JBI_ADMIN_DEPENDENT_COMPONENT_ACTIVE =
        "JBI_ADMIN_DEPENDENT_COMPONENT_ACTIVE";
    
    static final String JBI_ADMIN_FAILED_INSTALL_SHARED_LIBRARY_TO_INSTANCE =
        "JBI_ADMIN_FAILED_INSTALL_SHARED_LIBRARY_TO_INSTANCE";
    
    static final String JBI_ADMIN_FAILED_UNINSTALL_SHARED_LIBRARY_FROM_INSTANCE =
        "JBI_ADMIN_FAILED_UNINSTALL_SHARED_LIBRARY_FROM_INSTANCE";
    
    static final String JBI_ADMIN_INSTALLING_SHARED_LIBRARY_TO_TARGET =
        "JBI_ADMIN_INSTALLING_SHARED_LIBRARY_TO_TARGET";
    
    static final String JBI_ADMIN_UNINSTALLING_SHARED_LIBRARY_FROM_TARGET =
        "JBI_ADMIN_UNINSTALLING_SHARED_LIBRARY_FROM_TARGET";
    
    static final String JBI_ADMIN_FAILED_INSTALL_COMPONENT_TO_INSTANCE =
        "JBI_ADMIN_FAILED_INSTALL_COMPONENT_TO_INSTANCE";
    
    static final String JBI_ADMIN_FAILED_UNINSTALL_COMPONENT_FROM_INSTANCE =
        "JBI_ADMIN_FAILED_UNINSTALL_COMPONENT_FROM_INSTANCE";
    
    static final String JBI_ADMIN_FAILED_LOAD_NEW_INSTALLER_ON_INSTANCE =
        "JBI_ADMIN_FAILED_LOAD_NEW_INSTALLER_ON_INSTANCE";
    
    static final String JBI_ADMIN_FAILED_UNLOAD_INSTALLER_FROM_INSTANCE = 
        "JBI_ADMIN_FAILED_UNLOAD_INSTALLER_FROM_INSTANCE";
    
    static final String JBI_ADMIN_INSTALLING_COMPONENT_TO_TARGET =
        "JBI_ADMIN_INSTALLING_COMPONENT_TO_TARGET";
    
    static final String JBI_ADMIN_UNINSTALLING_COMPONENT_FROM_TARGET =
        "JBI_ADMIN_UNINSTALLING_COMPONENT_FROM_TARGET";
    
    static final String JBI_ADMIN_LOADING_NEW_INSTALLER_ON_TARGET =
        "JBI_ADMIN_LOADING_NEW_INSTALLER_ON_TARGET";
    
    static final String JBI_ADMIN_LOADING_INSTALLER_ON_TARGET =
        "JBI_ADMIN_LOADING_INSTALLER_ON_TARGET";
    
    static final String JBI_ADMIN_UNLOADING_INSTALLER_FROM_TARGET =
        "JBI_ADMIN_UNLOADING_INSTALLER_FROM_TARGET";
    
    static final String JBI_ADMIN_CANNOT_PERFORM_COMPONENT_INSTALLATION_OP =
        "JBI_ADMIN_CANNOT_PERFORM_COMPONENT_INSTALLATION_OP";
    
    static final String JBI_ADMIN_CANNOT_PERFORM_COMPONENT_LIFECYCLE_OP =
        "JBI_ADMIN_CANNOT_PERFORM_COMPONENT_LIFECYCLE_OP";
    
    static final String JBI_ADMIN_FAILED_START_COMPONENT_ON_INSTANCE =
        "JBI_ADMIN_FAILED_START_COMPONENT_ON_INSTANCE";
    
    static final String JBI_ADMIN_FAILED_STOP_COMPONENT_ON_INSTANCE =
        "JBI_ADMIN_FAILED_STOP_COMPONENT_ON_INSTANCE";
    
    static final String JBI_ADMIN_FAILED_SHUTDOWN_COMPONENT_ON_INSTANCE = 
        "JBI_ADMIN_FAILED_SHUTDOWN_COMPONENT_ON_INSTANCE";
    
    static final String JBI_ADMIN_GETTING_COMPONENT_STATE_ON_TARGET =
        "JBI_ADMIN_GETTING_COMPONENT_STATE_ON_TARGET";

    static final String JBI_ADMIN_GETTING_CUSTOM_MBEANS_FOR_COMPONENT =
            "JBI_ADMIN_GETTING_CUSTOM_MBEANS_FOR_COMPONENT";
    
    static final String JBI_ADMIN_CHANGING_COMPONENT_STATE_ON_INSTANCE = 
        "JBI_ADMIN_CHANGING_COMPONENT_STATE_ON_INSTANCE";
    
    static final String JBI_ADMIN_CURRENT_STATE_ON_INSTANCE =
        "JBI_ADMIN_CURRENT_STATE_ON_INSTANCE";
    
    static final String JBI_ADMIN_COMPONENT_STATES_ON_CLUSTER =
        "JBI_ADMIN_COMPONENT_STATES_ON_CLUSTER";
    
    static final String JBI_ADMIN_COMPONENT_INSTALLED_ON_TARGET =
		"JBI_ADMIN_COMPONENT_INSTALLED_ON_TARGET";

    static final String JBI_ADMIN_COMPONENT_NOT_INSTALLED_ON_TARGET =
		"JBI_ADMIN_COMPONENT_NOT_INSTALLED_ON_TARGET";
    
    static final String JBI_ADMIN_SERVICE_UNIT_NOT_DEPLOYED_ON_TARGET =
        "JBI_ADMIN_SERVICE_UNIT_NOT_DEPLOYED_ON_TARGET";
    
    static final String JBI_ADMIN_CANNOT_UNINSTALL_COMPONENT_WITH_DEPLOYED_SUS = 
        "JBI_ADMIN_CANNOT_UNINSTALL_COMPONENT_WITH_DEPLOYED_SUS";
    
    static final String JBI_ADMIN_REQUIRED_SHARED_LIBRARIES_NOT_INSTALLLED =
        "JBI_ADMIN_REQUIRED_SHARED_LIBRARIES_NOT_INSTALLLED";
    
    static final String JBI_ADMIN_DIFFERENT_COMPONENT_ARCHIVE_EXISTS =
        "JBI_ADMIN_DIFFERENT_COMPONENT_ARCHIVE_EXISTS";
    
    static final String JBI_ADMIN_DIFFERENT_SHARED_LIBRARY_ARCHIVE_EXISTS =
        "JBI_ADMIN_DIFFERENT_SHARED_LIBRARY_ARCHIVE_EXISTS";
    
    static final String JBI_ADMIN_SHARED_LIBRARY_INSTALLED_ON_TARGETS = 
        "JBI_ADMIN_SHARED_LIBRARY_INSTALLED_ON_TARGETS";
    
    static final String JBI_ADMIN_COMPONENT_INSTALLED_ON_TARGETS = 
        "JBI_ADMIN_COMPONENT_INSTALLED_ON_TARGETS";
    
    static final String JBI_ADMIN_COMPONENT_STARTED = 
        "JBI_ADMIN_COMPONENT_STARTED";
      
    static final String JBI_ADMIN_COMPONENT_INSTALLER_CONFIG_MBEAN_NOT_REGISTERED =
        "JBI_ADMIN_COMPONENT_INSTALLER_CONFIG_MBEAN_NOT_REGISTERED";
    
    static final String JBI_ADMIN_COMPONENT_NOT_INSTALLED_ON_INSTANCE =
        "JBI_ADMIN_COMPONENT_NOT_INSTALLED_ON_INSTANCE";
    
    /**
     * Deployment Service Messages
     */
    static final String JBI_ADMIN_SERVICE_ASSEMBLY_IS_DEPLOYED =
        "JBI_ADMIN_SERVICE_ASSEMBLY_IS_DEPLOYED";
    
    static final String JBI_ADMIN_FAILED_GET_SERVICE_ASSEMBLY_DESCRIPTOR =
        "JBI_ADMIN_FAILED_GET_SERVICE_ASSEMBLY_DESCRIPTOR";

    static final String JBI_ADMIN_FAILED_GET_SERVICE_UNIT_DESCRIPTOR =
        "JBI_ADMIN_FAILED_GET_SERVICE_UNIT_DESCRIPTOR";
    
    static final String JBI_ADMIN_KNOWN_SERVICE_ASSEMBLY  =
        "JBI_ADMIN_KNOWN_SERVICE_ASSEMBLY";
    
    static final String JBI_ADMIN_UNKNOWN_SERVICE_ASSEMBLY  =
        "JBI_ADMIN_UNKNOWN_SERVICE_ASSEMBLY";
    
    static final String JBI_ADMIN_FAILED_ADD_SERVICE_ASSEMBLY =
        "JBI_ADMIN_FAILED_ADD_SERVICE_ASSEMBLY";
    
    static final String JBI_ADMIN_FAILED_REMOVE_SERVICE_ASSEMBLY =
        "JBI_ADMIN_FAILED_REMOVE_SERVICE_ASSEMBLY";
    
    static final String JBI_ADMIN_SERVICE_ASSEMBLY_NOT_DEPLOYED_ON_TARGET =
        "JBI_ADMIN_SERVICE_ASSEMBLY_NOT_DEPLOYED_ON_TARGET";
    
    static final String JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_NOT_INSTALLED =
        "JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_NOT_INSTALLED";
    
    static final String JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_NOT_STARTED =
        "JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_NOT_STARTED";
    
    static final String JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_INCORRECT_STATE =
        "JBI_ADMIN_SERVICE_ASSEMBLY_TARGET_INCORRECT_STATE";
    
    static final String JBI_ADMIN_SERVICE_ASSEMBLY_NOT_SHUTDOWN =
        "JBI_ADMIN_SERVICE_ASSEMBLY_NOT_SHUTDOWN";
    
    static final String JBI_ADMIN_DEPLOYING_SERVICE_ASSEMBLY_TO_TARGET =
        "JBI_ADMIN_DEPLOYING_SERVICE_ASSEMBLY_TO_TARGET";
    
    static final String JBI_ADMIN_UNDEPLOYING_SERVICE_ASSEMBLY_FROM_TARGET =
        "JBI_ADMIN_UNDEPLOYING_SERVICE_ASSEMBLY_FROM_TARGET";
    
    static final String JBI_ADMIN_STARTING_SERVICE_ASSEMBLY_ON_TARGET = 
        "JBI_ADMIN_STARTING_SERVICE_ASSEMBLY_ON_TARGET";
    
    static final String JBI_ADMIN_STOPPING_SERVICE_ASSEMBLY_ON_TARGET =
        "JBI_ADMIN_STOPPING_SERVICE_ASSEMBLY_ON_TARGET";
    
    static final String JBI_ADMIN_SHUTTING_DOWN_SERVICE_ASSEMBLY_ON_TARGET =
        "JBI_ADMIN_SHUTTING_DOWN_SERVICE_ASSEMBLY_ON_TARGET";
    
    static final String JBI_ADMIN_STATE_OF_SERVICE_ASSEMBLY_IN_CLUSTER =
        "JBI_ADMIN_STATE_OF_SERVICE_ASSEMBLY_IN_CLUSTER";
    
    static final String JBI_ADMIN_STATE_OF_SERVICE_UNIT_IN_CLUSTER =
        "JBI_ADMIN_STATE_OF_SERVICE_UNIT_IN_CLUSTER";

    static final String JBI_ADMIN_DIFFERENT_SERVICE_ASSEMBLY_ARCHIVE_EXISTS =
        "JBI_ADMIN_DIFFERENT_SERVICE_ASSEMBLY_ARCHIVE_EXISTS";

    static final String JBI_ADMIN_AUTO_START_COMPONENT =
        "JBI_ADMIN_AUTO_START_COMPONENT";
    
    static final String JBI_ADMIN_APPLICATION_INTERCEPTOR_ERROR =
        "JBI_ADMIN_APPLICATION_INTERCEPTOR_ERROR";
    
    static final String JBI_ADMIN_APPLICATION_INTERCEPTOR_ROLLBACK_ERROR =
        "JBI_ADMIN_APPLICATION_INTERCEPTOR_ROLLBACK_ERROR";
            
    static final String JBI_ADMIN_CREATE_INSTANCE_EVENT_ERROR =
        "JBI_ADMIN_CREATE_INSTANCE_EVENT_ERROR";

    static final String JBI_ADMIN_CREATE_CLUSTER_EVENT_ERROR =
        "JBI_ADMIN_CREATE_CLUSTER_EVENT_ERROR";
        
    static final String JBI_ADMIN_DELETE_INSTANCE_EVENT_ERROR =
        "JBI_ADMIN_DELETE_INSTANCE_EVENT_ERROR";
    
    static final String JBI_ADMIN_DELETE_CLUSTER_EVENT_ERROR =
        "JBI_ADMIN_DELETE_CLUSTER_EVENT_ERROR";    
    

    /**
     * SA is not deployed in instance
     */
    static final String JBI_ADMIN_SA_NOT_DEPLOYED_IN_INSTANCE =
          "JBI_ADMIN_SA_NOT_DEPLOYED_IN_INSTANCE";
    
    /**
     * the component name provided for upgrade does not exist in the schemaorg_apache_xmlbeans.system
     */
    static final String JBI_ADMIN_UPGRADE_COMPONENT_NOT_INSTALLED =
            "JBI_ADMIN_UPGRADE_COMPONENT_NOT_INSTALLED";
    
    /**
     * the component selected for upgrade is not shutdown 
     */
    static final String JBI_ADMIN_UPGRADE_COMPONENT_NOT_SHUTDOWN =
            "JBI_ADMIN_UPGRADE_COMPONENT_NOT_SHUTDOWN";
    
    /**
     * given component descriptor is not identical with existing component
     * descriptor
     */
    static final String JBI_ADMIN_UPGRADE_COMPONENT_NAME_NOT_SAME =
            "JBI_ADMIN_UPGRADE_COMPONENT_NAME_NOT_SAME";
    
    /**
     * upgradeComponent method should be called only in the domain facade mbean
     */    
    static final String JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_TARGET_ONLY =
            "JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_TARGET_ONLY";
    
    /**
     * component install root was not updated in domain repository
     */    
    static final String JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_UPDATED =
            "JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_UPDATED";
    
    /**
     * component install root was not restored in domain repository
     */    
    static final String JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_RESTORED =
            "JBI_ADMIN_COMPONENT_UPGRADE_DOMAIN_INSTALL_ROOT_NOT_RESTORED";
    
    /**
     * component upgrade was successful
     */    
    static final String JBI_ADMIN_COMPONENT_UPGRADE_SUCCESSFUL =
            "JBI_ADMIN_COMPONENT_UPGRADE_SUCCESSFUL";
    
    /**
     * component upgrade failed
     */    
    static final String JBI_ADMIN_COMPONENT_UPGRADE_FAILED =
            "JBI_ADMIN_COMPONENT_UPGRADE_FAILED";    
    
    /**
     * component logger settings were not restored
     */
    static final String JBI_ADMIN_COMPONENT_UPGRADE_LOGGER_SETTINGS_NOT_RESTORED =
            "JBI_ADMIN_COMPONENT_UPGRADE_LOGGER_SETTINGS_NOT_RESTORED";
    
    /**
     * No attributes for configuration
     */
    static final String JBI_CCFG_NO_CONFIG_ATTRIBUTES = 
            "JBI_CCFG_NO_CONFIG_ATTRIBUTES";
    
    /**
     * No registered configuration MBean is found
     */
    static final String JBI_CCFG_CONFIG_MBEAN_NOT_FOUND = 
            "JBI_CCFG_CONFIG_MBEAN_NOT_FOUND";
    
    /** Attribute not found */
    static final String JBI_CCFG_ATTRIBUTE_NOT_FOUND =
        "JBI_CCFG_ATTRIBUTE_NOT_FOUND";
    
    /** Attribute value invalid */
    static final String JBI_CCFG_ATTRIBUTE_VALUE_INVALID = 
        "JBI_CCFG_ATTRIBUTE_VALUE_INVALID";
    
    /** Cannot configure a component which is not started or not stopped */
    static final String JBI_CCFG_COMPONENT_NOT_STARTED_NOT_STOPPED_CFG =
        "JBI_CCFG_COMPONENT_NOT_STARTED_NOT_STOPPED_CFG";
    
    /** Cannot list application configuration for a component which is not
      * started or not stopped
      */
    static final String JBI_CCFG_COMPONENT_NOT_STARTED_NOT_STOPPED_LST =
        "JBI_CCFG_COMPONENT_NOT_STARTED_NOT_STOPPED_LST";
    
    /** Component Instance down */
    static final String JBI_CCFG_COMPONENT_INSTANCE_DOWN =
        "JBI_CCFG_COMPONENT_INSTANCE_DOWN";
    
    /** Set attributes successfully.*/
    static final String JBI_CCFG_SET_ATTRIBUTES_SUCCESS =
        "JBI_CCFG_SET_ATTRIBUTES_SUCCESS";
    
    static final String JBI_CCFG_SET_ATTRIBUTE_SUCCESS =
        "JBI_CCFG_SET_ATTRIBUTE_SUCCESS";
    
    static final String JBI_CCFG_PERSIST_ATTRIBUTE_FAILURE =
        "JBI_CCFG_PERSIST_ATTRIBUTE_FAILURE";
    
    static final String JBI_CCFG_INVALID_APP_VAR =
        "JBI_CCFG_INVALID_APP_VAR";
    
    static final String JBI_CCFG_APP_VAR_EXISTS =
        "JBI_CCFG_APP_VAR_EXISTS";
    
    static final String JBI_CCFG_APP_VAR_DOES_NOT_EXIST =
        "JBI_CCFG_APP_VAR_DOES_NOT_EXIST";
    
    static final String JBI_CCFG_INCOMPLETE_APP_VAR =
        "JBI_CCFG_INCOMPLETE_APP_VAR";
    
    static final String JBI_CCFG_ADD_APP_VAR_SUCCESS =
        "JBI_CCFG_ADD_APP_VAR_SUCCESS";
    
    static final String JBI_CCFG_UPDATE_APP_VAR_SUCCESS =
        "JBI_CCFG_UPDATE_APP_VAR_SUCCESS";
    
    static final String JBI_CCFG_DELETE_APP_VAR_SUCCESS =
        "JBI_CCFG_DELETE_APP_VAR_SUCCESS";
    
    static final String JBI_CCFG_FAILED_ADD_VAR_REG =
        "JBI_CCFG_FAILED_ADD_VAR_REG";
    
    static final String JBI_CCFG_FAILED_UPDATE_VAR_REG =
        "JBI_CCFG_FAILED_UPDATE_VAR_REG";
        
    static final String JBI_CCFG_FAILED_DELETE_VAR_REG =
        "JBI_CCFG_FAILED_DELETE_VAR_REG";
    
    static final String JBI_CCFG_FAILED_ADD_CFG_REG =
        "JBI_CCFG_FAILED_ADD_CFG_REG";
    
    static final String JBI_CCFG_FAILED_UPDATE_CFG_REG =
        "JBI_CCFG_FAILED_UPDATE_CFG_REG";
        
    static final String JBI_CCFG_FAILED_DELETE_CFG_REG =
        "JBI_CCFG_FAILED_DELETE_CFG_REG";
    
    static final String JBI_CCFG_INVALID_APP_CFG =
        "JBI_CCFG_INVALID_APP_CFG";
    
    static final String JBI_CCFG_APP_CFG_TYPE_NULL =
        "JBI_CCFG_APP_CFG_TYPE_NULL";
    
    static final String JBI_CCFG_APP_CFG_DOES_NOT_EXIST =
        "JBI_CCFG_APP_CFG_DOES_NOT_EXIST";
        
    static final String JBI_CCFG_APP_CFG_EXISTS =
        "JBI_CCFG_APP_CFG_EXISTS";
    
    static final String JBI_CCFG_INVALID_APP_CFG_NAME =
        "JBI_CCFG_INVALID_APP_CFG_NAME";
    
    static final String JBI_CCFG_INVALID_APP_CFG_DATA =
        "JBI_CCFG_INVALID_APP_CFG_DATA";
    
    static final String JBI_CCFG_INVALID_CLUSTER_INST_NAME =
            "JBI_CCFG_INVALID_CLUSTER_INST_NAME";
    
    static final String JBI_CCFG_MISSING_CONFIG_XSD_FILE =
            "JBI_CCFG_MISSING_CONFIG_XSD_FILE";
    
    static final String JBI_CCFG_APP_VARS_NOT_SUPPORTED =
            "JBI_CCFG_APP_VARS_NOT_SUPPORTED";
    
    static final String JBI_CCFG_APP_CFG_NOT_SUPPORTED =
            "JBI_CCFG_APP_CFG_NOT_SUPPORTED";
    
    static final String JBI_CCFG_APP_CFG_MISSING_FIELD =
            "JBI_CCFG_APP_CFG_MISSING_FIELD";
    
    static final String JBI_CCFG_CANNOT_UPDATE_TYPE =
            "JBI_CCFG_CANNOT_UPDATE_TYPE";
    
    static final String JBI_CCFG_SET_ATTRIBUTES_FAILURE =
            "JBI_CCFG_SET_ATTRIBUTES_FAILURE";
    
    static final String JBI_CCFG_INVALID_APP_VAR_VALUE =
            "JBI_CCFG_INVALID_APP_VAR_VALUE";
    
    static final String JBI_CCFG_CLUSTER_NOT_COMPLETELY_UP =
            "JBI_CCFG_CLUSTER_NOT_COMPLETELY_UP";
}
