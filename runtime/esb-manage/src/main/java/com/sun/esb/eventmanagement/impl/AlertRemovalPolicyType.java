/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AlertRemovalPolicyType.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.eventmanagement.impl;

public enum AlertRemovalPolicyType {
    ALERTS_AGE("ALERTS_AGE"),ALERTS_COUNT("ALERTS_COUNT"),ALERTS_LEVEL("ALERTS_LEVEL");
    
    String policy;

    /** @param protocolString */
    private AlertRemovalPolicyType(String policyType) {
        this.policy = policyType;
    }

    /** @return the protocol description */
    public String getDescription() {
        switch (this) {
            case ALERTS_AGE:
                return "Take alert age when deciding to remove alerts from persistence";
            case ALERTS_COUNT:
                return "Take alerts count when deciding to remove alerts from persistence";
            case ALERTS_LEVEL:
                return "Take alerts level when deciding to remove alerts from persistence";
        }
        return "";
    }

    /** @return the protocol */
    public String getPolicyType() {
        return policy;
    }

}
