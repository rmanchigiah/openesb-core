#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01000.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01000 - Negative component installation test cases

echo testname is jbiadmin01000
. ./regress_defs.ksh

# Try to install a component using a shared library archive
$JBI_ANT_NEG -Djbi.install.file=$MANAGE_TEST_SL install-component

# Try to install a component twice
$JBI_ANT -Djbi.install.file=$MANAGE_TEST_SL install-shared-library
$JBI_ANT -Djbi.install.file=$JBI_KIT_FB install-component
$JBI_ANT_NEG -Djbi.install.file=$JBI_KIT_FB install-component

# Clean up
$JBI_ANT -Djbi.component.name=SunFileBinding uninstall-component
$JBI_ANT -Djbi.shared.library.name=TestSharedLibrary uninstall-shared-library
