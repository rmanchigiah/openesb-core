#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01310.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin01310 - Negative test for component installation with target=cluster ( Fix for CR 6514223 )"
. ./regress_defs.ksh

##########################################################################################################
#     Test installing a component to a cluster, the component throws an exception on install             #
#                                                                                                        #
#  The response management XML message should have the marker instance task-status-msg and               #  
#  exception-info.                                                                                       # 
##########################################################################################################

COMPONENT_ARCHIVE_1=$JV_SVC_TEST_CLASSES/dist/bad-install-binding.jar
COMPONENT_NAME_1=bad-install-binding

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01300.xml pkg.test.component

# Test : Install the test component
$JBI_ANT_NEG -Djbi.target="cluster1" -Djbi.install.file=$COMPONENT_ARCHIVE_1  install-component
