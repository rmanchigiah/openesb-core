#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01600.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                     Tests the facade Runtime Logger Configuration                         #
#############################################################################################

echo "jbiadmin01603 : Tests the facade Runtime Logger Configuration for target=cluster and target=cluster-instance"

. ./regress_defs.ksh

# Cluster and Member Instance Setup
asadmin create-cluster  --port $ASADMIN_PORT $ASADMIN_PW_OPTS rtcfg-cluster
createClusterDelay
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster rtcfg-cluster --nodeagent agent1 rtcfg-cluster-instance1
createInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS rtcfg-cluster-instance1
startInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS instance1
startInstanceDelay

# Test :

# Target = cluster
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster -Djbi.logger.name=com.sun.jbi.management.AdminService -Djbi.target=rtcfg-cluster -Djbi.logger.level=WARNING set-runtime-logger
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster list-runtime-loggers

# Clustered instance loggers should be same as the cluster loggers
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster-instance1 list-runtime-loggers
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster -Djbi.logger.name=com.sun.jbi.management.AdminService -Djbi.logger.level=INFO set-runtime-logger
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster list-runtime-loggers

# Target = clustered-instance
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster-instance1 -Djbi.logger.name=com.sun.jbi.framework -Djbi.logger.level=SEVERE set-runtime-logger
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster-instance1 list-runtime-loggers
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster-instance1 -Djbi.logger.name=com.sun.jbi.framework -Djbi.logger.level=INFO set-runtime-logger
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster-instance1 list-runtime-loggers

# ---
# Test the JBI Runtimes support for setting the log level to "DEFAULT", i.e. set the log level to "null" internally to inherit from the parent logger
# ---

echo "Test setting the log level to DEFAULT for target=cluster"
# --- target = cluster
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster list-runtime-loggers
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster -Djbi.logger.name=com.sun.jbi.framework -Djbi.logger.level=FINE set-runtime-logger
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster list-runtime-loggers
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster -Djbi.logger.name=com.sun.jbi.framework -Djbi.logger.level=DEFAULT set-runtime-logger
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster list-runtime-loggers

# --- target = clustered instance
echo "Test setting the log level to DEFAULT for target=cluster-instance1"
$JBI_ANT_NEG  -Djbi.target=rtcfg-cluster-instance1 list-runtime-loggers
$JBI_ANT_NEG  -Djbi.target=rtcfg-cluster-instance1 -Djbi.logger.name=com.sun.jbi.messaging -Djbi.logger.level=FINE set-runtime-logger
$JBI_ANT_NEG  -Djbi.target=rtcfg-cluster-instance1 list-runtime-loggers
$JBI_ANT_NEG  -Djbi.target=rtcfg-cluster-instance1 -Djbi.logger.name=com.sun.jbi.messaging -Djbi.logger.level=DEFAULT set-runtime-logger
$JBI_ANT_NEG  -Djbi.target=rtcfg-cluster-instance1 list-runtime-loggers

# --- target = server instance
echo "Test setting the log level to DEFAULT for target=server"
$JBI_ANT_NEG  list-runtime-loggers
$JBI_ANT_NEG  -Djbi.logger.name=com.sun.jbi.management -Djbi.logger.level=FINE set-runtime-logger
$JBI_ANT_NEG  list-runtime-loggers
$JBI_ANT_NEG  -Djbi.logger.name=com.sun.jbi.management -Djbi.logger.level=DEFAULT set-runtime-logger
$JBI_ANT_NEG  list-runtime-loggers

echo "Test setting a logger which is not defined in the JBI Runtime"
$JBI_ANT_NEG  -Djbi.logger.name=com.sun.jbi.i.dont.exist -Djbi.logger.level=FINE set-runtime-logger

echo "Test setting the com.sun.jbi logger in the JBI Runtime and getting the value from Glassfish"

# -- target = server
asadmin set-jbi-runtime-logger $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=server com.sun.jbi=WARNING  
asadmin get $ASADMIN_PW_OPTS --port $ASADMIN_PORT  "server-config.log-service.module-log-levels.property.com\.sun\.jbi"

# -- target = instance1
asadmin set-jbi-runtime-logger $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=instance1 com.sun.jbi=SEVERE  
asadmin get $ASADMIN_PW_OPTS --port $ASADMIN_PORT "instance1-config.log-service.module-log-levels.property.com\.sun\.jbi"

# -- target = cluster
asadmin set-jbi-runtime-logger $ASADMIN_PW_OPTS --port $ASADMIN_PORT --target=rtcfg-cluster com.sun.jbi=FINER  
asadmin get $ASADMIN_PW_OPTS --port $ASADMIN_PORT "rtcfg-cluster-config.log-service.module-log-levels.property.com\.sun\.jbi"

echo "Test setting the com.sun.jbi logger in Glassfish and getting it from the JBI Runtime"

echo "Listing all the loggers for reference before the test."
$JBI_ANT_NEG -Djbi.target=server list-runtime-loggers
$JBI_ANT_NEG -Djbi.target=instance1 list-runtime-loggers
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster list-runtime-loggers

# -- target = server
asadmin set $ASADMIN_PW_OPTS --port $ASADMIN_PORT "server-config.log-service.module-log-levels.property.com\.sun\.jbi"=FINE
$JBI_ANT_NEG -Djbi.target=server list-runtime-loggers

# -- target = instance1
asadmin set $ASADMIN_PW_OPTS --port $ASADMIN_PORT "instance1-config.log-service.module-log-levels.property.com\.sun\.jbi"=FINE
$JBI_ANT_NEG -Djbi.target=instance1 list-runtime-loggers

# -- target = cluster
asadmin set $ASADMIN_PW_OPTS --port $ASADMIN_PORT "rtcfg-cluster-config.log-service.module-log-levels.property.com\.sun\.jbi"=FINE
$JBI_ANT_NEG -Djbi.target=rtcfg-cluster list-runtime-loggers

# Cleanup
# Cluster and Member Instance Cleanup
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT instance1
stopInstanceDelay
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT rtcfg-cluster-instance1
stopInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS rtcfg-cluster-instance1
deleteInstanceDelay
asadmin delete-cluster $ASADMIN_PW_OPTS --port $ASADMIN_PORT rtcfg-cluster
deleteClusterDelay
