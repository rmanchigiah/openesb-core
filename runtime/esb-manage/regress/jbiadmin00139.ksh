#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)framework00007.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin00139 : Test fix for Issue 29 - Deploying a service assembly with missing target components should fail with a proper response."

#regress setup
. ./regress_defs.ksh

# start the framework
start_jbise -Dcom.sun.jbi.registry.readonly=true &
startInstanceDelay

# deploy a service assembly targeted to components which are not installed ( target component : sun-javaee-engine )
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.deploy.file=$JV_SVC_TEST_CLASSES/testdata/SoapRpcInOnly.zip deploy-service-assembly
deploySaDelay

# undeploy the service assembly w/o force
#$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="OrphanSA" undeploy-service-assembly
#undeploySaDelay

# undeploy the orphan service assembly forcefully
#$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.force.task=true -Djbi.service.assembly.name="OrphanSA" undeploy-service-assembly
#undeploySaDelay

shutdown_jbise
stopInstanceDelay

# ### END
