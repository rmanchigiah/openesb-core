#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01503.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# This is a negative test for component upgrade.
# This tests fix for issue 67
# A component binding1 is installed and we attempt to upgrade that component
# with another component binding2
# upgrade component should fail
####
echo "jbiadmin01503 : Test Component Upgrade."


. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01503.xml 1>&2

echo install test-component
asadmin install-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT $JV_SVC_TEST_CLASSES/dist/component-binding1.jar

echo try to upgrade with a wrong archive
$JBI_ANT -Djbi.component.name=manage-binding-1 -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/component-binding2.jar upgrade-component 2>&1

echo start the component
asadmin start-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT manage-binding-1

####
#now try some more additional negative test cases
####

#upgrade a running component
echo try to upgrade a running component
$JBI_ANT -Djbi.component.name=manage-binding-1 -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/component-binding1.jar upgrade-component 2>&1
echo shutdown the component
asadmin shut-down-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT manage-binding-1

#provide a dir for component archive
echo try to upgrade with a dir
$JBI_ANT -Djbi.component.name=manage-bindng-1 -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist upgrade-component 2>&1

#provide a SA file for component archive
echo try to upgrade with a SA
$JBI_ANT -Djbi.component.name=manage-binding-1 -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/deploy-sa.jar upgrade-component 2>&1

echo uninstall the component
asadmin uninstall-jbi-component --terse=false -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT manage-binding-1

