#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01003.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#esbadmin00203 - Test of exception throwing ServiceUnitManager

echo testname is esbadmin00203
. ./regress_defs.ksh

export BINDING_1
BINDING_1="manage-binding-1"
export BINDING_2
BINDING_2="manage-binding-2"
export SA_1
SA_1="esbadmin00089-sa"
export DIST_DIR
DIST_DIR="$JV_SVC_TEST_CLASSES/dist"
export BINDING_1_ARCHIVE
BINDING_1_ARCHIVE="$DIST_DIR/component-binding1.jar"
export BINDING_2_ARCHIVE
BINDING_2_ARCHIVE="$DIST_DIR/su-init-binding.jar"
export SA_ARCHIVE
SA_ARCHIVE="$DIST_DIR/deploy-sa.jar"

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01003.xml

echo ""
echo "*** Install and start both components"
echo ""
$JBI_ANT -Djbi.install.file=$BINDING_1_ARCHIVE install-component
$JBI_ANT -Djbi.install.file=$BINDING_2_ARCHIVE install-component
$JBI_ANT -Djbi.component.name=$BINDING_1 start-component
$JBI_ANT -Djbi.component.name=$BINDING_2 start-component

echo ""
echo "*** Deploy and start the service assembly ***"
echo ""
$JBI_ANT -Djbi.deploy.file=$SA_ARCHIVE deploy-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_1 start-service-assembly
$JBI_ANT list-service-assemblies

echo ""
echo "*** Stop and shutdown the service assembly ***"
echo ""
$JBI_ANT -Djbi.service.assembly.name=$SA_1 stop-service-assembly
$JBI_ANT -Djbi.service.assembly.name=$SA_1 shut-down-service-assembly
$JBI_ANT list-service-assemblies


echo ""
echo "*** Clean-up ***"
echo ""
$JBI_ANT -Djbi.service.assembly.name=$SA_1 undeploy-service-assembly
$JBI_ANT -Djbi.component.name=$BINDING_1 shut-down-component
$JBI_ANT -Djbi.component.name=$BINDING_1 uninstall-component
$JBI_ANT -Djbi.component.name=$BINDING_2 shut-down-component
$JBI_ANT -Djbi.component.name=$BINDING_2 uninstall-component
