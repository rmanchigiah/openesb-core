#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01600.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                     Tests the facade Component Configuration MBean                        #
#                                                                                           #             
#                                                                                           #
# The test component used in this test registers a custom MBean                             #
#                                                                                           #
# The test :                                                                                #
# (a) Package, install and start the test component                                         #
# (b) Get/Set an attribute                                                                  #
#                                                                                           #
#############################################################################################

echo "jbiadmin01601 : Tests the facade Component Configuration MBean for target=cluster"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.test.component
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.component.configmbeaninstartstop
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.component.configmbeannoattrs

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-with-custom-mbean.jar
COMPONENT_NAME=manage-binding-1
COMPONENT_ARCHIVE1=$JV_SVC_TEST_CLASSES/dist/component-with-config-mbean-rgstrd-in-start.jar
COMPONENT_NAME1=test-component-in-start-stop
COMPONENT_ARCHIVE2=$JV_SVC_TEST_CLASSES/dist/component-with-new-config-mbean-expose-no-attrs.jar
COMPONENT_NAME2=test-component-config-mbean-no-attrs


# Cluster and Member Instance Setup
asadmin create-cluster  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster
createClusterDelay

asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster ccfg-cluster --nodeagent agent1 ccfg-cluster-instance1
createInstanceDelay

asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster ccfg-cluster --nodeagent agent1 ccfg-cluster-instance2
createInstanceDelay

asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance1
startInstanceDelay

asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance2
startInstanceDelay

asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --nodeagent agent1 ccfg-instance
createInstanceDelay

asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-instance
startInstanceDelay

# Setup
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="ccfg-cluster" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" start-component
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE1  -Djbi.target="ccfg-cluster" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1  -Djbi.target="ccfg-cluster" start-component
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE2  -Djbi.target="ccfg-cluster" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2  -Djbi.target="ccfg-cluster" start-component

# Test (b) :
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -DTARGET="ccfg-cluster" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml get.component.configuration

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1 -Djbi.target="ccfg-cluster" -Djbi.config.param.name=HostName -Djbi.config.param.value=dummyhost set-component-configuration
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1 -Djbi.target="ccfg-cluster" stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1 -Djbi.target="ccfg-cluster" -Djbi.config.param.name=HostName -Djbi.config.param.value=dummyhost set-component-configuration

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2 -Djbi.target="ccfg-cluster" -Djbi.config.param.name=HostName -Djbi.config.param.value=dummyhost set-component-configuration
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2 -Djbi.target="ccfg-cluster" stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2 -Djbi.target="ccfg-cluster" -Djbi.config.param.name=HostName -Djbi.config.param.value=dummyhost set-component-configuration

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" stop-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" uninstall-component

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1  -Djbi.target="ccfg-cluster" shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME1  -Djbi.target="ccfg-cluster" uninstall-component

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2  -Djbi.target="ccfg-cluster" shut-down-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME2  -Djbi.target="ccfg-cluster" uninstall-component

###
# Test the fix for issue 156 : Set one component configuration attribute and list component configuration all should be listed
###

## Test for target=cluster
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="ccfg-cluster" install-component
installComponentDelay
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME   -Djbi.target="ccfg-cluster" start-component
startInstanceDelay

asadmin set-jbi-component-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target="ccfg-cluster" --component $COMPONENT_NAME HostName=storm
asadmin show-jbi-binding-component --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target="ccfg-cluster" --configuration  $COMPONENT_NAME 

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" shut-down-component
stopComponentDelay
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-cluster" uninstall-component
uninstallComponentDelay

## Test for target=standalone-instance
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="ccfg-instance" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME   -Djbi.target="ccfg-instance" start-component

asadmin set-jbi-component-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target ccfg-instance --component $COMPONENT_NAME HostName=instance-storm
asadmin show-jbi-binding-component       --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target ccfg-instance --configuration $COMPONENT_NAME 

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-instance" shut-down-component
stopComponentDelay
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME  -Djbi.target="ccfg-instance" uninstall-component
uninstallComponentDelay

# Cluster and Member Instance Cleanup
asadmin stop-instance   --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance1
stopInstanceDelay

asadmin stop-instance   --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance2
stopInstanceDelay

asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance1
deleteInstanceDelay

asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster-instance2
deleteInstanceDelay

asadmin delete-cluster  --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-cluster
deleteClusterDelay

asadmin stop-instance   --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-instance
stopInstanceDelay

asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS ccfg-instance
deleteInstanceDelay
