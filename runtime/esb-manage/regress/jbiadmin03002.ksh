#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin03002.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin03002 : Test fix for CR 6532519 - not able to start schemaorg_apache_xmlbeans.system components in standalone instance"

####
#test to verify that event listener for delete instance unregisters ComponentFacadeMBeans
#start domain  
#delete an instance before any JBI specific activity is done
#execute a lifecycle operation on a schemaorg_apache_xmlbeans.system component and confirm that it fails
#this verifies that the ComponentFacadeMBeans are unregistered for this instance
####

my_test_domain=ESBTest
. ./regress_defs.ksh

echo Starting domain ESBTest
start_domain

echo Deleting instance esbinst1
asadmin delete-instance --port $ESBTest_admin_port $ASADMIN_PW_OPTS esbinst1
deleteInstanceDelay

echo Starting sun-http-binding on target esbinst1
asadmin start-jbi-component --port $ESBTest_admin_port $ASADMIN_PW_OPTS --target=esbinst1 sun-http-binding 2>&1
startComponentDelay

echo Deleting node agent config for esbagent1
asadmin delete-node-agent-config --port $ESBTest_admin_port $ASADMIN_PW_OPTS esbagent1
deleteAgentDelay

echo Deleting node agent esbagent1
asadmin delete-node-agent esbagent1
deleteAgentDelay

echo Stopping domain ESBTest
shutdown_domain
