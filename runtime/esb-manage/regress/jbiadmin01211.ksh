#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01210.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01211 - Java EE application processing rollback test

echo jbiadmin01211 - Java EE application processing rollback test
. ./regress_defs.ksh

export CMD_OPTS
CMD_OPTS="-u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT"

#Test Java EE Service Assembly
export JAVAEE_SA_PATH
export JAVAEE_SA_NAME

JAVAEE_SA_PATH=$JV_SVC_TEST_CLASSES/testdata/javaee-test-sa.zip
JAVAEE_SA_NAME=TestJavaEEServiceAssembly

# Try to deploy an invalid Java EE archive in a JBI service assembly
asadmin deploy-jbi-service-assembly $CMD_OPTS $JAVAEE_SA_PATH | grep ERROR: | cut -d";" -f1

# Verify that the good service unit was not deployed
asadmin list-application $CMD_OPTS | grep JBI-JavaEE 
