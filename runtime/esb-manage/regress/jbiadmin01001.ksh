#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01001 - Shared library upgrade test cases

echo testname is jbiadmin01001
. ./regress_defs.ksh $1

# Install the shared library and a component which depends on it
$JBI_ANT_TARGET -Djbi.install.file=$MANAGE_TEST_SL install-shared-library
$JBI_ANT_TARGET -Djbi.install.file=$MANAGE_TEST_BIND_COMP3 install-component

# Start the component and try to upgrade the shared library (should fail)
$JBI_ANT_TARGET -Djbi.component.name=manage-binding-3 start-component
$JBI_ANT_NEG_TARGET -Djbi.shared.library.name=TestSharedLibrary uninstall-shared-library

# Shutdown the component and try to upgrade the shared library (should succeed)
$JBI_ANT_TARGET -Djbi.component.name=manage-binding-3 shut-down-component
$JBI_ANT_TARGET -Djbi.shared.library.name=TestSharedLibrary uninstall-shared-library
$JBI_ANT        -Djbi.target=domain list-shared-libraries
$JBI_ANT_TARGET -Djbi.install.file=$MANAGE_TEST_SL install-shared-library

# Make sure the component can still start (nothing corrupted)
$JBI_ANT_TARGET -Djbi.component.name=manage-binding-3 start-component

# Clean up
$JBI_ANT_TARGET -Djbi.component.name=manage-binding-3 shut-down-component
$JBI_ANT_TARGET -Djbi.component.name=manage-binding-3 uninstall-component
$JBI_ANT_TARGET -Djbi.shared.library.name=TestSharedLibrary uninstall-shared-library
