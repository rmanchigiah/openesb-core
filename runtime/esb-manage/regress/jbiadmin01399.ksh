#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin01399.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin01399 - Cleanup the cluster setup created by jbiadmin01300

echo testname is jbiadmin01399
. ./regress_defs.ksh

echo Stopping instance : cluster1-instance1
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT cluster1-instance1

echo Deleting instance : cluster1-instance1
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS cluster1-instance1

echo Stopping instance : cluster1-instance2
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT cluster1-instance2

echo Deleting instance : cluster1-instance2
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS cluster1-instance2

. ./cluster-teardown.ksh
