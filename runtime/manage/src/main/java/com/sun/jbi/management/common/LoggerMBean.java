/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LoggerMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.common;

/**
 * LoggerMBean defines standard controls for setting the properties of a
 * single JBI Framework service or JBI Installable Component logger.
 *
 * @author Sun Microsystems, Inc.
 */
public interface LoggerMBean
{
    /**
     * Get the localized display name of this logger.
     * @return String representing the localized display name.
     */
    String getDisplayName();

    /**
     * Get the log level of this logger.
     * @return String representing log level.
     */
    String getLogLevel();

    /**
     * Get the name of this logger.
     * @return String representing the logger name.
     */
    String getLoggerName();

    /**
     * Set the log level of this logger to ALL.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setAll();

    /**
     * Set the log level of this logger to CONFIG.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setConfig();

    /**
     * Set the log level of this logger to null to inherit from its parent.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setDefault();

    /**
     * Set the log level of this logger to FINE.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setFine();

    /**
     * Set the log level of this logger to FINER.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setFiner();

    /**
     * Set the log level of this logger to FINEST.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setFinest();

    /**
     * Set the log level of this logger to INFO.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setInfo();

    /**
     * Set the log level of this logger to OFF.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setOff();

    /**
     * Set the log level of this logger to SEVERE.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setSevere();

    /**
     * Set the log level of this logger to WARNING.
     * @return 0 if operation is successful, otherwise non-zero.
     */
    int setWarning();
}
