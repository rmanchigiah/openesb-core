/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)GenericQueryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  GenericQueryImpl.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.registry.xml;


import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.repository.Archive;
import com.sun.jbi.management.repository.ArchiveType;
import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.management.util.LockManager;

import java.util.*;
import java.util.logging.Logger;

/**
 * This class encapsulates queries which are common to 
 * all targets ( domain / server / cluster ).
 *
 * @author Sun Microsystems, Inc.
 */
public class GenericQueryImpl
    implements com.sun.jbi.management.registry.GenericQuery
{
 
    private Jbi mJbiRegistry;
    private RegistryImpl     mRegistry;
    private boolean          mValidate;
    private boolean          mReadonly;
    private ManagementContext mMgtCtx;
    private StringTranslator mTranslator;
    private Logger           mLog;
    private LockManager      mRegObjLM;
    private RegistryUtil     mRegUtil;
    
    /**
     * Cache for the deployment/installation descriptors
     */
    private Map<String, String> mComponentCache;
    private Map<String, String> mSharedLibraryCache;
    private Map<String, String> mServiceAssemblyCache;
    
    
    private Map<String, com.sun.jbi.management.descriptor.Jbi> mJbiComponentCache;
    private Map<String, com.sun.jbi.management.descriptor.Jbi> mJbiSharedLibraryCache;
    private Map<String, com.sun.jbi.management.descriptor.Jbi> mJbiServiceAssemblyCache;
    
    public GenericQueryImpl(Jbi jbi, ManagementContext mgtCtx, boolean validate,
        RegistryImpl registry)
    {
        mJbiRegistry = jbi;
        mRegistry = registry;
        mValidate = validate;
        mReadonly = registry.getProperty(Registry.REGISTRY_READONLY_PROPERTY) != null;
        mMgtCtx   = mgtCtx;
        mLog      = mgtCtx.getLogger();
        mRegUtil = new RegistryUtil(mgtCtx, registry);
        mTranslator = 
            mgtCtx.getEnvironmentContext().getStringTranslator("com.sun.jbi.management");
        
        mComponentCache       = Collections.synchronizedMap(new HashMap());
        mSharedLibraryCache   = Collections.synchronizedMap(new HashMap());
        mServiceAssemblyCache = Collections.synchronizedMap(new HashMap());
        

        mJbiComponentCache       = Collections.synchronizedMap(new HashMap());
        mJbiSharedLibraryCache   = Collections.synchronizedMap(new HashMap());        
        mJbiServiceAssemblyCache = Collections.synchronizedMap(new HashMap());     
            
        mRegObjLM = mRegistry.getRegistryObjectLockManager();
    }
    
    /** 
     * @param componentName - Component name
     * @return a List<String> of all servers installing a component.
     *         The list excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    public List<String> getServersInstallingComponent(String componentName) throws RegistryException
    {
        ArrayList<String> resServers = new ArrayList();
        
        if ( isComponentRegistered(componentName))
        {
            mRegObjLM.acquireReadLock();
            if ( mJbiRegistry.getServers() != null )
            {
                List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();
                for ( InstalledComponentsListType server : servers )
                {
                    List<ComponentRefType> refs = server.getComponentRef();
                    for ( ComponentRefType compRef : refs )
                    {
                        if ( compRef.getNameRef().equals(componentName) )
                        {
                            resServers.add(server.getNameRef());
                        }
                    }
                }
            }
            mRegObjLM.releaseReadLock();
        }
        
        return resServers;
    }
    
    /** 
     * @param  sharedLibraryName - Shared Library name
     * @return a List<String> of all servers installing a shared library.
     *         The list excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    public List<String> getServersInstallingSharedLibrary(String sharedLibraryName) 
        throws RegistryException
    {
        
        ArrayList<String> resServers = new ArrayList();
        
        if ( isSharedLibraryRegistered(sharedLibraryName))
        {
            mRegObjLM.acquireReadLock();
            try
            {
                if ( mJbiRegistry.getServers() != null )
                {
                    List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

                    for ( InstalledComponentsListType server : servers )
                    {
                        List<SharedLibraryRefType> refs = server.getSharedLibraryRef();

                        for ( SharedLibraryRefType slRef : refs )
                        {
                            if ( slRef.getNameRef().equals(sharedLibraryName) )
                            {
                                resServers.add(server.getNameRef());
                            }
                        }
                    }
                }
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
        }
        return resServers;
        
    }
    
    /** 
     * @param  serviceAssemblyName - Service Assembly name
     * @return a List<String> of all servers deploying a service assembly.
     *         The list excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    public List<String> getServersDeployingServiceAssembly(String serviceAssemblyName) 
        throws RegistryException
    {
        
        ArrayList<String> resServers = new ArrayList();
        
        if ( isServiceAssemblyRegistered(serviceAssemblyName))
        {
            mRegObjLM.acquireReadLock();
            try
            {
                if (mJbiRegistry.getServers() != null)
                {
                    List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();
                    for ( InstalledComponentsListType server : servers )
                    {
                        List<ServiceAssemblyRefType> refs = server.getServiceAssemblyRef();

                        for ( ServiceAssemblyRefType saRef : refs )
                        {
                            if ( saRef.getNameRef().equals(serviceAssemblyName) )
                            {
                                resServers.add(server.getNameRef());
                            }
                        }
                    }
                }
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
        }
        return resServers;
    }  
    
    
    /** 
     * @param componentName - Component name
     * @return a List<String> of all clusters installing a component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */    
      public List<String> getClustersInstallingComponent(String componentName) throws RegistryException
    {
        ArrayList<String> resClusters = new ArrayList();
        if ( isComponentRegistered(componentName))
        {
            mRegObjLM.acquireReadLock();
            try
            {
                if (mJbiRegistry.getClusters() != null)
                {

                    List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();
                    for ( InstalledComponentsListType cluster : clusters )
                    {
                        List<ComponentRefType> refs = cluster.getComponentRef();
                        for ( ComponentRefType compRef : refs )
                        {
                            if ( compRef.getNameRef().equals(componentName) )
                            {
                                resClusters.add(cluster.getNameRef());
                            }
                        }
                    }
                }
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
        }
        
        return resClusters;
    }
    
    /** 
     * @param  sharedLibraryName - Shared Library name
     * @return a List<String> of all clusters installing a shared library.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
      public List<String> getClustersInstallingSharedLibrary(String sharedLibraryName) 
        throws RegistryException
    {
        ArrayList<String> resClusters = new ArrayList();
        
        if ( isSharedLibraryRegistered(sharedLibraryName))
        {
            mRegObjLM.acquireReadLock();
            try
            {
                if (mJbiRegistry.getClusters() != null)
                {
                    List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();
                    for ( InstalledComponentsListType cluster : clusters )
                    {
                        List<SharedLibraryRefType> refs = cluster.getSharedLibraryRef();
                        for ( SharedLibraryRefType slRef : refs )
                        {
                            if ( slRef.getNameRef().equals(sharedLibraryName) )
                            {
                                resClusters.add(cluster.getNameRef());
                            }
                        }
                    }
                }
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
        }
        return resClusters;
    }
    
    /** 
     * @param  serviceAssemblyName - Service Assembly name
     * @return a List<String> of all clusters deploying a service assembly.
     *         The list excludes all non-clustered servers.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     *
     */
    public List<String> getClustersDeployingServiceAssembly(String serviceAssemblyName) 
        throws RegistryException
    {
     
        ArrayList<String> resClusters = new ArrayList();
        
        if ( isServiceAssemblyRegistered(serviceAssemblyName))
        {
            mRegObjLM.acquireReadLock();
            try
            {
                if (mJbiRegistry.getClusters() != null )
                {
                    List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();
                    for ( InstalledComponentsListType cluster : clusters )
                    {
                        List<ServiceAssemblyRefType> refs = cluster.getServiceAssemblyRef();

                        for ( ServiceAssemblyRefType saRef : refs )
                        {
                            if ( saRef.getNameRef().equals(serviceAssemblyName) )
                            {
                                resClusters.add(cluster.getNameRef());
                            }
                        }
                    }
                }
            }
            finally
            {
                mRegObjLM.releaseReadLock();
            }
        }
        return resClusters;
    }   
    
    
    /**
     * @param componentName - component name
     * @return the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public String getComponentInstallationDescriptor(String componentName) throws RegistryException
    {
        String jbiXml = null;
        if ( isComponentRegistered(componentName))
        {
            if ( mComponentCache.containsKey(componentName))
            {
                jbiXml =  mComponentCache.get(componentName);
            }
            else
            {
                Repository repos = mMgtCtx.getRepository();
                Archive componentArchive = null;

                componentArchive =  repos.getArchive(
                    ArchiveType.COMPONENT, componentName);
                    
                if (componentArchive != null)
                {
                    jbiXml = componentArchive.getJbiXmlString(true);
                    mComponentCache.put(componentName, jbiXml);
                }
            }
        }
        return jbiXml;
    }

    
    /**
     * Get the wrapper for the JAXB model for the installation descriptor for
     * a component.
     * @param componentName - component name
     * @return the wrapper for the installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the
     *         in-memory Registry.
     */
    public com.sun.jbi.management.descriptor.ComponentDescriptor 
        getComponentDescriptor(String componentName) throws RegistryException
    {
        com.sun.jbi.management.descriptor.ComponentDescriptor desc = null;
        com.sun.jbi.management.descriptor.Jbi jbiXml =
            getComponentJbi(componentName);

        if ( null != jbiXml )
        {
            desc = new com.sun.jbi.management.descriptor.ComponentDescriptor(jbiXml);
        }
        return desc;
    }
    
    /**
     * @param componentName - component name
     * @return the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public com.sun.jbi.management.descriptor.Jbi 
        getComponentJbi(String componentName) throws RegistryException
    {
        com.sun.jbi.management.descriptor.Jbi jbiXml = null;
        if ( isComponentRegistered(componentName))
        {
            if ( mJbiComponentCache.containsKey(componentName))
            {
                jbiXml =  mJbiComponentCache.get(componentName);
            }
            else
            {
               
                Repository repos = mMgtCtx.getRepository();
                Archive componentArchive = null;                         
                
                componentArchive = repos.getArchive(
                        ArchiveType.COMPONENT, 
                        componentName);

                if ( componentArchive != null )
                {
                    jbiXml = componentArchive.getJbiXml(true);
                    mJbiComponentCache.put(componentName, jbiXml);
                }
            }
        }
        return jbiXml;
    }
    
    /**
     * @param sharedLibraryName - shared library name
     * @return the Installation descriptor for the shared library.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public String getSharedLibraryInstallationDescriptor(String sharedLibraryName) 
        throws RegistryException
    {
        String jbiXml = null;
        if ( isSharedLibraryRegistered(sharedLibraryName))
        {
            if ( mSharedLibraryCache.containsKey(sharedLibraryName))
            {
                jbiXml =  mSharedLibraryCache.get(sharedLibraryName);
            }
            else
            {
                Repository repos = mMgtCtx.getRepository();
                Archive sharedLibraryArchive =  null;

                sharedLibraryArchive =  repos.getArchive(
                    ArchiveType.SHARED_LIBRARY, sharedLibraryName);

                if (sharedLibraryArchive != null)
                {
                    jbiXml = sharedLibraryArchive.getJbiXmlString(true);
                    mSharedLibraryCache.put(sharedLibraryName, jbiXml);
                }
            }
        }
        return jbiXml;
    }

    /**
     * @param sharedLibraryName - shared library name
     * @return the Installation descriptor for the shared library.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public com.sun.jbi.management.descriptor.Jbi getSharedLibraryJbi(String sharedLibraryName) 
        throws RegistryException
    {
        com.sun.jbi.management.descriptor.Jbi jbiXml = null;
        if ( isSharedLibraryRegistered(sharedLibraryName))
        {
            if ( mJbiSharedLibraryCache.containsKey(sharedLibraryName))
            {
                jbiXml =  mJbiSharedLibraryCache.get(sharedLibraryName);
            }
            else
            {
                Repository repos = mMgtCtx.getRepository();
                Archive sharedLibraryArchive =  null;

                sharedLibraryArchive =  repos.getArchive(
                        ArchiveType.SHARED_LIBRARY, 
                        sharedLibraryName);

                if (sharedLibraryArchive != null)
                {
                    jbiXml = sharedLibraryArchive.getJbiXml(true);
                    mJbiSharedLibraryCache.put(sharedLibraryName, jbiXml);
                }
            }
        }
        return jbiXml;
    }
    
    /**
     * @param serviceAssemblyName - service assembly name
     * @return the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public String getServiceAssemblyDeploymentDescriptor(String serviceAssemblyName) throws RegistryException
    {
        String jbiXml = null;
        if ( isServiceAssemblyRegistered(serviceAssemblyName))
        {
            if ( mServiceAssemblyCache.containsKey(serviceAssemblyName))
            {
                jbiXml =  mServiceAssemblyCache.get(serviceAssemblyName);
            }
            else
            {
                Repository repos = mMgtCtx.getRepository();
                Archive serviceAssemblyArchive = repos.getArchive(
                    ArchiveType.SERVICE_ASSEMBLY, serviceAssemblyName);
                if (serviceAssemblyArchive != null)
                {
                    jbiXml = serviceAssemblyArchive.getJbiXmlString(true);
                    mServiceAssemblyCache.put(serviceAssemblyName, jbiXml);
                }
            }
        }
        return jbiXml;
    } 
    
    
    /**
     * @param serviceAssemblyName - service assembly name
     * @param serviceUnitName - service unit name
     * @return the deployment descriptor for the service unit.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public String getServiceUnitDeploymentDescriptor(String serviceAssemblyName,
			String serviceUnitName) throws RegistryException
    {

        String jbiXml = null;
	    
        if ( isServiceAssemblyRegistered(serviceAssemblyName))
        {
            Repository repos = mMgtCtx.getRepository();
            String queryStr =
                serviceAssemblyName + java.io.File.separator + serviceUnitName;
            Archive serviceUnitArchive = repos.getArchive(
                ArchiveType.SERVICE_UNIT, queryStr);
            if (serviceUnitArchive != null)
	    {
                jbiXml = serviceUnitArchive.getJbiXmlString(true);
	    }
        }
        return jbiXml;
    }

    /**
     * @param serviceAssemblyName - service assembly name
     * @return the Installation descriptor for the component.
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public com.sun.jbi.management.descriptor.Jbi getServiceAssemblyJbi(String serviceAssemblyName) throws RegistryException
    {
        com.sun.jbi.management.descriptor.Jbi jbiXml = null;
        if ( isServiceAssemblyRegistered(serviceAssemblyName))
        {
            if ( mJbiServiceAssemblyCache.containsKey(serviceAssemblyName))
            {
                jbiXml =  mJbiServiceAssemblyCache.get(serviceAssemblyName);
            }
            else
            {
                Repository repos = mMgtCtx.getRepository();
                Archive serviceAssemblyArchive = repos.getArchive(
                    ArchiveType.SERVICE_ASSEMBLY, serviceAssemblyName);
                if (serviceAssemblyArchive != null)
                {
                    jbiXml = serviceAssemblyArchive.getJbiXml(true);
                    mJbiServiceAssemblyCache.put(serviceAssemblyName, jbiXml);
                }
            }
        }
        return jbiXml;
    } 
     /**
     * @param saName - service assembly name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    public String getServiceAssemblyArchive(String saName)
        throws RegistryException
    {
        return mRegistry.getRepository().getArchive(ArchiveType.SERVICE_ASSEMBLY, saName).getPath();        
    }
    
    /**
    /**
     * @param componentName - component name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    public String getComponentArchive(String componentName)
        throws RegistryException
    {
        return mRegistry.getRepository().getArchive(ArchiveType.COMPONENT, componentName).getPath();
    }
    
     /**
     * @param sharedLibraryName - sharedLibrary name
     * @return the filename for the associated archive
     * @throws RegistryException if a problem exists.
     */
    public String getSharedLibraryArchive(String sharedLibraryName)
        throws RegistryException
    {
        return mRegistry.getRepository().getArchive(ArchiveType.SHARED_LIBRARY, sharedLibraryName).getPath();
    }    
   
    /**
     * @return true if a component is installed on one or more servers / clusters
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public boolean isComponentInstalled(String componentName) throws RegistryException
    {
        boolean installed = false;
        if ( isComponentRegistered(componentName) )
        {
            List<String> servers  = getServersInstallingComponent(componentName);
            List<String> clusters = getClustersInstallingComponent(componentName);
            if (( servers.size() > 0 ) || ( clusters.size() > 0 ))
            {
                installed = true;
            }
        }
        return installed;
    }
    
    /**
     * @return true if a shared library is installed on one or more servers / clusters
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public boolean isSharedLibraryInstalled(String sharedLibraryName) throws RegistryException
    {
        boolean installed = false;
        
        if ( isSharedLibraryRegistered(sharedLibraryName) )
        {
            List<String> servers  = getServersInstallingSharedLibrary(sharedLibraryName);
            List<String> clusters = getClustersInstallingSharedLibrary(sharedLibraryName);

            if (( servers.size() > 0 ) || ( clusters.size() > 0 ))
            {
                installed = true;
            }
        }
        return installed;
    }
    
    /**
     * @return true if a service assembly is deployed on one or more servers / clusters
     * @throws RegistryException if a read lock cannot be acquired to access the 
     *         in-memory Registry.
     */
    public boolean isServiceAssemblyDeployed(String serviceAssemblyName) throws RegistryException
    {
        boolean deployed = false;
        
        if ( isServiceAssemblyRegistered(serviceAssemblyName) )
        {
        
            List<String> servers  = getServersDeployingServiceAssembly(serviceAssemblyName);
            List<String> clusters = getClustersDeployingServiceAssembly(serviceAssemblyName);

            if (( servers.size() > 0 ) || ( clusters.size() > 0 ))
            {
                deployed = true;
            }
        }
        
        return deployed;
    }
    
    /**
     * @return a List of Servers in the registry
     */
    public List<String> getServers()
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        List servers = new ArrayList();
        try
        {
            ServerListType serverLT = mJbiRegistry.getServers();

            if ( serverLT != null )
            {
                List<InstalledComponentsListType> serverList = serverLT.getServer();
                for ( InstalledComponentsListType server : serverList )
                {
                    servers.add(server.getNameRef());
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return servers;   
    }
    
    /**
     * @return a List of Clusters in the registry
     */
    public List<String> getClusters()
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        List clusters = new ArrayList();        
        try
        {
            ClusterListType clusterLT = mJbiRegistry.getClusters();

            if ( clusterLT != null )
            {
                List<InstalledComponentsListType> clusterList = clusterLT.getCluster();
                for ( InstalledComponentsListType cluster : clusterList )
                {
                    clusters.add(cluster.getNameRef());
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return clusters;
    }
    
    boolean isTargetServer(String targetName)
        throws RegistryException
    {
        return getServers().contains(targetName);
    }
    
    boolean isTargetCluster(String targetName)
        throws RegistryException
    {
        return getClusters().contains(targetName);
    }
    
    boolean isTargetDomain(String targetName)
    {
        return "domain".equals(targetName);
    }
    
    public void removeComponentFromCache(String componentName)
    {
        if ( mComponentCache.containsKey(componentName) )
        {
            mComponentCache.remove(componentName);
        }
        
        if ( mJbiComponentCache.containsKey(componentName) )
        {
            mJbiComponentCache.remove(componentName);
        }
    }
    
    
    public void removeServiceAssemblyFromCache(String serviceAssemblyName)
    {
        if ( mServiceAssemblyCache.containsKey(serviceAssemblyName) )
        {
            mServiceAssemblyCache.remove(serviceAssemblyName);
        }
        
        if ( mJbiServiceAssemblyCache.containsKey(serviceAssemblyName) )
        {
            mJbiServiceAssemblyCache.remove(serviceAssemblyName);
        }
    }
    
    
    public void removeSharedLibraryFromCache(String sharedLibraryName)
    {
        if ( mSharedLibraryCache.containsKey(sharedLibraryName) )
        {
            mSharedLibraryCache.remove(sharedLibraryName);
        }
        
        if ( mJbiSharedLibraryCache.containsKey(sharedLibraryName) )
        {
            mJbiSharedLibraryCache.remove(sharedLibraryName);
        }
    }
    
    public ComponentType getComponentType(String componentName)
        throws RegistryException
    {
        ComponentType compType = null;
        
        if ( isComponentRegistered(componentName) )
        {
            com.sun.jbi.management.descriptor.Jbi jbiXML = getComponentJbi(componentName);
            
            com.sun.jbi.management.descriptor.Component comp = jbiXML.getComponent();
            
            if ( comp != null )
            {
                if ( comp.getType().equalsIgnoreCase("binding-component"))
                {
                    compType = ComponentType.BINDING;
                } else
                    if ( comp.getType().equalsIgnoreCase("service-engine"))
                    {
                        compType = ComponentType.ENGINE;
                    }
            }
        }
        return compType;
    }
    
    /**
     * Return the state of the component on the specified target.
     */
    public ComponentState getComponentState(String componentName, String targetName)
        throws RegistryException
    {
        ComponentState compState = ComponentState.UNKNOWN;
        
        if ( isComponentRegistered(componentName) )
        {
            ComponentRefType comp = getComponent(componentName, targetName);
            
            if ( comp != null )
            {
                compState = ComponentState.valueOfString(comp.getState().value());
            }
        }
        return compState;
    }
    
    public boolean isComponentRegistered(String componentName)
        throws RegistryException
    {
        
        boolean isReadLocked = false;
        boolean isInRegistry = false;
        boolean isInRepository = false;
        boolean isRegistered   = false;
        
        // -- Check if in Registry
        mRegObjLM.acquireReadLock();
        try
        {
            isReadLocked = true;
            Components compTypes = mJbiRegistry.getComponents();

            if ( compTypes != null )
            {
                List<DomainComponentType> comps = compTypes.getComponent();

                for ( DomainComponentType comp: comps)
                {
                    if (comp.getName().equals(componentName))
                    {
                        if (mReadonly)
                        {
                            mRegObjLM.releaseReadLock();
                            isReadLocked = false;
                            return (true);
                        }
                        isInRegistry = true;
                        break;
                    }
                }
            }

            try
            {
                // -- Check if in Repository
                Repository repos = mMgtCtx.getRepository();
                Archive componentArchive = null;

                componentArchive = repos.getArchive(ArchiveType.COMPONENT, componentName);
                if ( componentArchive != null )
                {
                   isInRepository = true;
                }

                if (isInRegistry != isInRepository)
                {
                    mRegObjLM.releaseReadLock();
                    isReadLocked = false;
                    mRegUtil.cleanEntity(ArchiveType.COMPONENT, componentName, isInRegistry, isInRepository);
                }
            }
            catch ( Exception ex )
            {
                mLog.warning(ex.getMessage());
            }
            isRegistered = ( isInRegistry && isInRepository );
        }
        finally
        {
            if (isReadLocked)
            {
                mRegObjLM.releaseReadLock();
            }
        }
        return isRegistered;
    }
    
    public boolean isSharedLibraryRegistered(String sharedLibraryName)
        throws RegistryException
    {
        boolean isReadLocked = false; 
        boolean isInRegistry = false;
        boolean isInRepository = false;
        boolean isRegistered   = false;
        
        // -- Check if in Registry
        mRegObjLM.acquireReadLock();
        try
        {
            isReadLocked = true;
            SharedLibraries slsType = mJbiRegistry.getSharedLibraries();


            if ( slsType != null )
            {
                List<DomainSharedLibraryType> sls = slsType.getSharedLibrary();

                for ( DomainSharedLibraryType sl: sls )
                {
                    if (sl.getName().equals(sharedLibraryName))
                    {
                        if (mReadonly)
                        {
                            mRegObjLM.releaseReadLock();
                            isReadLocked = false;
                            return (true);
                        }
                        isInRegistry = true;
                        break;
                    }
                }
            }

            try
            {
                Repository repos = mMgtCtx.getRepository();
                Archive slArchive = null;

                // -- Check if in Repository
                slArchive = repos.getArchive(ArchiveType.SHARED_LIBRARY, sharedLibraryName);

                if ( slArchive != null )
                {
                    isInRepository = true;
                }

                if (isInRegistry != isInRepository )
                {
                    mRegObjLM.releaseReadLock();
                    isReadLocked = false;
                    mRegUtil.cleanEntity(ArchiveType.SHARED_LIBRARY, sharedLibraryName, isInRegistry, isInRepository);
                }
            }
            catch ( Exception ex )
            {
                mLog.warning(ex.getMessage());            
            }

            isRegistered = ( isInRegistry && isInRepository );
        }
        finally
        {
            if ( isReadLocked )
            {
                mRegObjLM.releaseReadLock();
            }
        }
        return isRegistered;
    }
    

    /**
     * If a service assembly is registered in the registry and not in the repository
     * it is removed from the repository.
     *
     * If a service assembly is registered in the repository and not in the registry
     * then the service assembly is removed from the domain entry as well as any other
     * installed entry.
     *
     * @return true if the service assembly is there in registry and repository
     */
    public boolean isServiceAssemblyRegistered(String serviceAssemblyName)
        throws RegistryException
    {
        
        boolean isReadLocked = false;
        boolean isInRegistry = false;
        boolean isInRepository = false;
        boolean isRegistered   = false;
        
        try
        {
            // -- Check if in Registry

            mRegObjLM.acquireReadLock();
            try
            {
                isReadLocked = true;
                ServiceAssemblies sasType = mJbiRegistry.getServiceAssemblies();

                if ( sasType != null )
                {
                    List<DomainEntityType> sas = sasType.getServiceAssembly();

                    for ( DomainEntityType sa: sas )
                    {
                        if (sa.getName().equals(serviceAssemblyName))
                        {
                            if (mReadonly)
                            {
                                mRegObjLM.releaseReadLock();
                                isReadLocked = false;
                                return (true);
                            }
                             isInRegistry = true;
                            break;
                        }
                    }
                }


                // -- Check if in Repository
                Repository repos = mMgtCtx.getRepository();
                Archive saArchive = repos.getArchive(ArchiveType.SERVICE_ASSEMBLY, serviceAssemblyName);
                isInRepository = saArchive != null;

                if (isInRegistry != isInRepository)
                {
                    mRegObjLM.releaseReadLock();
                    isReadLocked = false;
                    mRegUtil.cleanEntity(ArchiveType.SERVICE_ASSEMBLY, serviceAssemblyName, isInRegistry, isInRepository);
                }
            }
            catch ( Exception ex )
            {
                mLog.warning(ex.getMessage());   
            }
            isRegistered = ( isInRegistry && isInRepository );
        }
        finally
        {
            if (isReadLocked)
            {
                mRegObjLM.releaseReadLock();
            }
        }
        
        return isRegistered;
    }
    
   
    /**
     * @return a List of Service Assemblies registered in the domain.
     */
    public List<String> getRegisteredServiceAssemblies()
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        List<String> saList = new ArrayList();
        try
        {
            ServiceAssemblies sasType = mJbiRegistry.getServiceAssemblies();

            if ( sasType != null )
            {
                List<DomainEntityType> sas = sasType.getServiceAssembly();

                for ( DomainEntityType sa: sas )
                {
                    saList.add(sa.getName());
                }
            }   
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return saList;
    }
    
    /**
     * @return a List of Shared Libraries registered in the domain.
     */
    public List<String> getRegisteredSharedLibraries()
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        List<String> slList = new ArrayList();    
        try
        {
            SharedLibraries slsType = mJbiRegistry.getSharedLibraries();
        
            if ( slsType != null )
            {
                List<DomainSharedLibraryType> sls = slsType.getSharedLibrary();

                for ( DomainSharedLibraryType sl: sls )
                {
                    slList.add(sl.getName());
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return slList;
    }
    
    
    /**
     * @return a List of Components registered in the domain.
     */
    public List<String> getRegisteredComponents()
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        List<String> compList = new ArrayList();        
        try
        {
            Components compTypes = mJbiRegistry.getComponents();
            if ( compTypes != null )
            {
                List<DomainComponentType> comps = compTypes.getComponent();

                for ( DomainComponentType comp: comps)
                {
                    compList.add(comp.getName());
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return compList;
    }
    
   
    /**
     * This method is used to find out if this is a schemaorg_apache_xmlbeans.system
     * component.
     * This method returns the value of schemaorg_apache_xmlbeans.system-install attribute
     * for this component from the registry
     * A component that has schemaorg_apache_xmlbeans.system-install set to true will have
     * its install root under AS_INSTALL/jbi and it should not be
     * deleted on unload of the component's uninstaller.
     * @param componentName the name of the component
     * @return boolean true if schemaorg_apache_xmlbeans.system-install is true, false otherwise
     *
     */
    public boolean isSystemComponent(String componentName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        boolean isSystemComponent = false;
        try
        {
            Components compTypes = mJbiRegistry.getComponents();
            if ( compTypes != null )
            {
                List<DomainComponentType> comps = compTypes.getComponent();   
                for ( DomainComponentType comp: comps)
                {
                    if (comp.getName().equals(componentName)) {

                        if  (comp.isDefaultInstall() == true) {
                            isSystemComponent = true;
                        }
                        break;
                    }
                }            
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return isSystemComponent;
    }
    
    /**
     * This method is used to find out if this is a schemaorg_apache_xmlbeans.system
     * shared library 
     * This method returns the value of schemaorg_apache_xmlbeans.system-install attribute
     * for this shared library from the registry
     * A shared library that has schemaorg_apache_xmlbeans.system-install set to true will have
     * its install root under AS_INSTALL/jbi and it should not be
     * deleted on uninstall
     * @param sharedLibraryName the name of the shared library
     * @return boolean true if schemaorg_apache_xmlbeans.system-install is true, false otherwise
     */
    public boolean isSystemSharedLibrary(String sharedLibraryName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        boolean isSystemSharedLibrary = false;
        try
        {
            SharedLibraries sharedLibTypes = mJbiRegistry.getSharedLibraries();
            if ( sharedLibTypes != null )
            {
                List<DomainSharedLibraryType> sharedLibs = sharedLibTypes.getSharedLibrary();   
                for ( DomainSharedLibraryType sharedLib: sharedLibs)
                {
                    if (sharedLib.getName().equals(sharedLibraryName)) {

                        if (sharedLib.isDefaultInstall() == true) {
                            isSystemSharedLibrary = true;
                        }
                        break;
                    }

                }            
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return isSystemSharedLibrary;
    }    
    
    public boolean doesComponentDeployServiceAssembly(ComponentRefType componentRef, 
        String serviceAssemblyName) throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        boolean deploys = false;
        
        try
        {    
            ServiceUnitListType suList = componentRef.getServiceUnits();

            if ( suList != null )
            {
                List<ServiceUnitType> sus = suList.getServiceUnit();

                for ( ServiceUnitType su : sus )
                {
                    if ( su.getServiceAssemblyRef().equals(serviceAssemblyName))
                    {
                        deploys = true;
                    }
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return deploys;
    }
    
    /**
     * Search for a domain Component with the specified componentName
     *
     * @return the Component with the specified name null if not found
     */
    public DomainComponentType getComponent(String componentName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        DomainComponentType component = null;
        try
        {
            if (mJbiRegistry.getComponents() != null )
            {
                List<DomainComponentType> comps = mJbiRegistry.getComponents().getComponent();

                for ( DomainComponentType comp : comps )
                {
                    if ( comp.getName().equals(componentName))
                    {
                        component = comp;
                    }
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return component;
    }
    
    
    
    public ComponentRefType getComponent(String componentName, String targetName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        ComponentRefType compRefType = null;        
        try
        {
            InstalledComponentsListType target = getInstalledEntities(targetName);    

            if ( target != null )
            {
                List<ComponentRefType> compRefs =  target.getComponentRef();
                for( ComponentRefType compRef : compRefs )
                {
                    if ( compRef.getNameRef().equals(componentName))
                    {
                        compRefType = compRef;
                    }
                }

            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return compRefType;
    }
    
    
    /**
     * Search for a domain Shared Library with the specified slName
     *
     * @return the SharedLibrary with the specified name null if not found
     */
    public DomainSharedLibraryType getSharedLibrary(String slName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        DomainSharedLibraryType sharedLibrary = null;        
        try
        {
            if ( mJbiRegistry.getSharedLibraries() != null )
            {
                List<DomainSharedLibraryType> 
                    sls = mJbiRegistry.getSharedLibraries().getSharedLibrary();

                for ( DomainSharedLibraryType sl : sls )
                {
                    if ( sl.getName().equals(slName))
                    {
                        sharedLibrary = sl;
                    }
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return sharedLibrary;
    }
    
    public SharedLibraryRefType getSharedLibrary(String slName, String targetName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        SharedLibraryRefType slRefType = null;
        try
        {
            InstalledComponentsListType target = getInstalledEntities(targetName);

            if ( target != null )
            {
                List<SharedLibraryRefType> slRefs =  target.getSharedLibraryRef();
                for( SharedLibraryRefType slRef : slRefs )
                {
                    if ( slRef.getNameRef().equals(slName))
                    {
                        slRefType = slRef;
                    }
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return slRefType;
    }
    
    /**
     * Search for a domain Service Assembly with the specified saName
     *
     * @return the SharedLibrary with the specified name null if not found
     */
    public DomainEntityType getServiceAssembly(String saName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        DomainEntityType serviceAssembly = null;
        try
        {
            if ( mJbiRegistry.getServiceAssemblies() != null )
            {
                List<DomainEntityType> 
                    sas = mJbiRegistry.getServiceAssemblies().getServiceAssembly();

                for ( DomainEntityType sa : sas )
                {
                    if ( sa.getName().equals(saName))
                    {
                        serviceAssembly = sa;
                    }
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return serviceAssembly;
    }

    /**
     * Get the ServiceAssemblyRef for a service assembly.
     *
     * @param saName - service assembly name
     * @param targetName - server/cluster name
     * @throws RegistryException on errors
     */
    public ServiceAssemblyRefType getServiceAssembly(String saName, String targetName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        ServiceAssemblyRefType saRefType = null;
        try
        {
            InstalledComponentsListType target = getInstalledEntities(targetName);

            if ( target != null )
            {
                List<ServiceAssemblyRefType> saRefs =  target.getServiceAssemblyRef();
                for( ServiceAssemblyRefType saRef : saRefs )
                {
                    if ( saRef.getNameRef().equals(saName))
                    {
                        saRefType = saRef;
                    }
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return saRefType;
    } 
    /**
     * Get the file name for the domain component.
     *
     * @param componentName - component name
     */
    public String getComponentFileName(String componentName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        String fname = null;        
        try
        {
            DomainEntityType comp = getComponent(componentName);

            if ( comp != null )
            {
                fname = comp.getFileName();
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return fname;
    }
    
    /**
     * Get the file name for the domain shared library.
     *
     * @param slName - shared library  name
     */
    public String getSharedLibraryFileName(String slName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        String fname = null;
        try
        {
            DomainEntityType sl = getSharedLibrary(slName);

            if ( sl != null )
            {
                fname = sl.getFileName();
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return fname;
    }
    
    /**
     * Get the file name for the domain service assembly.
     *
     * @param saName - service assembly name
     */
    public String getServiceAssemblyFileName(String saName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        String fname = null;
        try
        {
            DomainEntityType sa = getServiceAssembly(saName);

            if ( sa != null )
            {
                fname = sa.getFileName();
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return fname;
    }
    
    /**
     * Get the timestamp for the domain component.
     *
     * @param componentName - component name
     */
    public long getComponentTimestamp(String componentName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        long timestamp = 0;
        try
        {
            DomainEntityType comp = getComponent(componentName);

            if ( comp != null )
            {
                timestamp = comp.getTimestamp().longValue();
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return timestamp;
    }
    
    /**
     * Get the file name for the domain shared library.
     *
     * @param slName - shared library  name
     */
    public long getSharedLibraryTimestamp(String slName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        long timestamp = 0;
        try
        {
            DomainEntityType sl = getSharedLibrary(slName);

            if ( sl != null )
            {
                timestamp = sl.getTimestamp().longValue();
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return timestamp;
    }
    
    /**
     * Get the file name for the domain service assembly.
     *
     * @param saName - service assembly name
     */
    public long getServiceAssemblyTimestamp(String saName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        long timestamp = 0;
        try
        {
            DomainEntityType sa = getServiceAssembly(saName);

            if ( sa != null )
            {
                timestamp = sa.getTimestamp().longValue();
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return timestamp;
    }

    /**
     *
     */
    public InstalledComponentsListType getInstalledEntities(String targetName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        InstalledComponentsListType target = null;
        try
        {
            if ( isTargetServer(targetName) )
            {
                List<InstalledComponentsListType> servers = mJbiRegistry.getServers().getServer();

                for ( InstalledComponentsListType server : servers )
                {
                    if ( server.getNameRef().equals(targetName))
                    {
                        target = server;
                    }
                }
            }
            else if ( isTargetCluster(targetName) )
            {  
                List<InstalledComponentsListType> clusters = mJbiRegistry.getClusters().getCluster();

                for ( InstalledComponentsListType cluster : clusters )
                {
                    if ( cluster.getNameRef().equals(targetName))
                    {
                        target = cluster;
                    }
                }
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return target;
    }
   
    /**
     * This method is used to get the value of the attribute upgrade-number from the 
     * domain level entry for the component in the registry
     * @param componentName the componentName
     * @return BigInteger the upgrade number
     * @throws RegistryException if the upgrade number could not be retrieved
     */
    public java.math.BigInteger getComponentUpgradeNumber(String componentName)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        java.math.BigInteger upgradeNumber = null;
        
        try
        {
            Components compTypes = mJbiRegistry.getComponents();
            if ( compTypes != null )
            {
                List<DomainComponentType> comps = compTypes.getComponent();   
                for ( DomainComponentType comp: comps)
                {
                    if (comp.getName().equals(componentName)) 
                    {
                        upgradeNumber =  comp.getUpgradeNumber();
                        break;
                    }
                }            
            }
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return upgradeNumber;
    }        

    /*--------------------------------------------------------------------------------*\
     *                       Get configuration attribute values                       *
    \*--------------------------------------------------------------------------------*/
     
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the runtime target. 
     *
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     * @return the String representation of the attributes value
     */
    public String getAttribute(ConfigurationCategory type, String name)
        throws RegistryException
    {
        String target = mMgtCtx.getEnvironmentContext().getPlatformContext().getTargetName();
        return getAttribute(target, type, name);
    }
    
    /**
     * Get the value of a configuration attribute belonging to the
     * specified category, for the specified target. 
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @exception RegistryException on errors in getting the attribute value
     * @return the String representation of the attributes value
     */
    public String getAttribute(String targetName, ConfigurationCategory type, String name)
        throws RegistryException
    {
        mRegObjLM.acquireReadLock();
        String value = null;
        try
        {
            Map<String, String> attribs = null;           
            if ( isAttributeOverriden(targetName, type, name) )
            {
                attribs = getConfigurationAttributes(targetName, type);
            }
            else
            {
                attribs = getConfigurationAttributes("domain", type);
            }

            value = attribs.get(name);
        }
        finally
        {
            mRegObjLM.releaseReadLock();
        }
        return value;
    }
    
    /**
     * Determine if the domain configuration is present in the registry
     *
     * @return true if the domain-config is defined in the registry
     */
    public boolean isGlobalConfigurationDefined()
    {
        return (getConfig("domain") != null);
    }
    
    /**
     * @return the configuration name based on the target name
     */
    public String getConfigName(String targetName)
    {
        StringBuffer configName = new StringBuffer(targetName);
        configName.append(Registry.CONFIG_SUFFIX);
        return configName.toString();
    }
    
    /** 
     * Determine if a configuration attribute is overriden by a target.
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @param name - identification for the attribute
     * @return true if the attribute is overriden by a target
     */
    public boolean isAttributeOverriden(String targetName, ConfigurationCategory type, 
        String name)
    {
        boolean isOverrriden = false;
        
        ConfigCategoryType configType = getConfigCategory(targetName, type);
        
        if ( configType != null)
        {
           Map propertyMap = getPropertyMap(configType.getProperty());
           isOverrriden = ( propertyMap.get(name.trim()) != null );
        }
        return isOverrriden;
    }
    
    /**
     * @param propertyList a list of PropertyType elements
     * @return the properties in a category in  map keyed by the property name.
     */
    public Map getPropertyMap(List<PropertyType> propertyList)
    {
        HashMap<String, String> propertyMap = new HashMap();
        for ( PropertyType property : propertyList)
        {
            propertyMap.put(property.getName().trim(), property.getValue().trim());
        }
        return propertyMap;
    }
    
    /** 
     * Get a particular configuration category in a target configuration.
     *
     * @param targetName - target instance/cluster name.
     * @param type - configuration category 
     * @return the Configuration Category handle if found or null otherwise
     */
    public ConfigCategoryType getConfigCategory(String targetName, ConfigurationCategory type)
    {
        ConfigCategoryType cfgType = null;
        
        ConfigType config = getConfig(targetName);
        if ( config != null )
        {
            List<ConfigCategoryType> configTypes = config.getConfigType();

            for ( ConfigCategoryType configType : configTypes )
            {
                ConfigurationCategory category = 
                    ConfigurationCategory.valueOf(configType.getCategory().value()); 
                if ( category == type  )
                {
                     cfgType = configType;
                }
            }
        }
        return cfgType;
    }
    
    
    /** 
     * Get a particular configuration for a target.
     *
     * @param targetName - target instance/cluster name.
     * @return the Configuration for the target if found or null otherwise
     */
    public ConfigType getConfig(String targetName)
    {
        ConfigType cfg = null;
        String configName = getConfigName(targetName);
        
        Configs cfgs = mJbiRegistry.getConfigs();
        if ( cfgs != null )
        {
            List<ConfigType> configs = cfgs.getConfig();
            for ( ConfigType config : configs )
            {
                if ( config.getName().equals(configName) )
                {
                    cfg = config;
                }
            }
        }
        return cfg;
    }
    
    /**
     * @return a Map containing all the configuration attributes for a given category
     *         in a given target
     */
    private Map<String, String> getConfigurationAttributes(String targetName, ConfigurationCategory type)
    {
        Map<String, String> attributes;
        ConfigCategoryType category = getConfigCategory(targetName, type);
        
        if ( category != null )
        {
            attributes = getPropertyMap(category.getProperty());
        }
        else
        {
            attributes = new HashMap();
        }
        return attributes;
    }
    
}
