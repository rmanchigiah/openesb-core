/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PropertyFilter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

/**
 * This is a helper class to get Application Server configuration information.
 *
 * @author Sun Microsystems, Inc.
 */
public class PropertyFilter
{
    public static final String SYSTEM_PROPERTY_PREFIX = "${";
    public static final String SYSTEM_PROPERTY_SUFFIX = "}";
    
    
    /**
     * This operation parses the path string and replaces any occurences of
     * a schemaorg_apache_xmlbeans.system property with the value of the schemaorg_apache_xmlbeans.system property.
     *
     * A schemaorg_apache_xmlbeans.system property is one which is enclosed in curly braces with a '$' prefix.
     * Example: ${i.am.a.property}
     */
    
    public static String filterProperties(String string)
    {
        String resStr = string;
        
        if ( string != null )
        {
            int x = string.indexOf(SYSTEM_PROPERTY_PREFIX);
            int y = string.indexOf(SYSTEM_PROPERTY_SUFFIX);

            // -- there aren't any embedded properties
            if ( x == -1 || y == -1)
            {
                return resStr;
            }

            String a = string.substring(0, x);
            String property = string.substring(x+2, y);

            StringBuffer strBuf = new StringBuffer(a);
            strBuf.append(System.getProperty(property));
            if ( y < string.length() )
            {
                String remainder = string.substring(y+1);
                strBuf.append(filterProperties(remainder));
            }
            resStr = strBuf.toString();
        }
        return resStr;
        
    }
    
    /**
     * Replace any occurence of a schemaorg_apache_xmlbeans.system property value with the schemaorg_apache_xmlbeans.system property key
     *
     * @param value - the value to be replaced with the key
     * @param key - the property key whose value is to be searched and replaced.
     */
    public static String replacePropertyValues(String string, String key)
    {
       String value = System.getProperty(key);
       
       if ( value == null )
       {
           return string;
       }
       
       String resStr = string;
       if ( string != null )
       {
           string = string.trim();
           String normalizedString = normalizeString(string);
           
           
           if ( key != null )
           {
               String normalizedValue = normalizeString(value);
               
               int x =  normalizedString.indexOf(normalizedValue);
               int y = x + normalizedValue.length() - 1;
               
               if ( x == -1 || y == -1 )
               {
                   return resStr;
               }
               
               String a = string.substring(0, x);
               
               StringBuffer strBuf = new StringBuffer(a);
               strBuf.append(SYSTEM_PROPERTY_PREFIX);
               strBuf.append(key);
               strBuf.append(SYSTEM_PROPERTY_SUFFIX);
               
               if ( y < normalizedString.length() )
               {
                   String remainder = string.substring(y + 1);
                   strBuf.append(replacePropertyValues(remainder, key));
               }
               resStr = strBuf.toString();
           }
       }
       return resStr;
       
    }

    
    /**
     * Normalize the string for comparison. Replace all '\' with '/'
     *
     * @param srcStr - source string to nromalize
     */
    private static String normalizeString(String srcStr)
    {
        return srcStr.replace('\\', '/');
    }
}
