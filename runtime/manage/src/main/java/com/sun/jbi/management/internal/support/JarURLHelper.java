/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JarURLHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  JarURLHelper.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on May 18, 2005, 12:54 PM
 */

package com.sun.jbi.management.internal.support;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;

import java.net.URL;
import java.net.JarURLConnection;

import java.util.zip.ZipFile;

/**
 * Helper class for Jar URLs.
 *
 * @author Sun Microsystems, Inc.
 */
public class JarURLHelper
{
    /**
     * URL Protocol : File.
     */
    public static final String FILE = "file";
    
    /**
     * URL Protocol : HTTP.
     */
    public static final String HTTP = "http";
    
    /**
     * URL Protocol : jar.
     */
    public static final String JAR = "jar";
    
    /**
     * Jar Separator.
     */
    public static final String JAR_SEPARATOR = "!/";
    
    /**
     * Get the ZipFile from the fileURL.
     *
     * @param fileURL is the File URL.
     * @param translator is the StringTranslator.
     * @throws java.io.IOException on IO related errors.
     * @return the ZipFile for the URL.
     */
    public static ZipFile getZipFileFromURL (URL fileURL, StringTranslator translator) 
        throws java.io.IOException
    {
        if ( FILE.equals (fileURL.getProtocol ())  )
        {
            return new ZipFile (fileURL.getPath ());
        }
        else if ( JAR.equals (fileURL.getProtocol ()))
        {
            JarURLConnection conxn = (JarURLConnection) fileURL.openConnection ();
            conxn.setUseCaches (false);
            return conxn.getJarFile ();
        }
        else
        {
            String errMsg = translator.getString (
                LocalStringKeys.UNSUPPORTED_URL_PROTOCOL, fileURL.getProtocol ());
            
            throw new java.io.IOException (errMsg);
        }
    }
    
    /**
     * Convert the URL to a JAR URL, for example if the File URL is
     * file://deploy/SA.zip it will be converted to jar:file://deploy/SA.zip!/ .
     *
     * @param fileURL is the original JarFile URL.
     * @param translator is the StringTranslator
     * @return a jar URL created from the fileURL.
     * @throws java.io.IOException if the URL cannot be converted to a JAR URL
     */
    public static URL convertToJarURL (URL fileURL,  StringTranslator translator) 
        throws java.io.IOException
    {
        try
        {
            if ( JAR.equalsIgnoreCase (fileURL.getProtocol ()))
            {
                return fileURL;
            }
            if (FILE.equalsIgnoreCase (fileURL.getProtocol ()) ||
                HTTP.equalsIgnoreCase (fileURL.getProtocol ()) )
            {
                return new URL ( JAR + ":" + fileURL.toString () + JAR_SEPARATOR);
            }
            else
            {
                String errMsg = translator.getString (
                    LocalStringKeys.UNSUPPORTED_URL_PROTOCOL, fileURL.getProtocol ());
                
                throw new java.io.IOException (errMsg);
            }
        }
        catch (Exception ex)
        {
            throw new java.io.IOException (ex.getMessage ());
        }
    }
    
}
