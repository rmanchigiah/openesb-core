/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NotifyStandardMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.support;

import javax.management.StandardMBean;
import javax.management.MBeanNotificationInfo;
import javax.management.NotificationEmitter;
import javax.management.NotificationFilter;
import javax.management.NotificationListener;

/**
 * Notification-emitting wrapper for StandardMBean.
 *
 * @author Sun Microsystems, Inc.
 */
public class NotifyStandardMBean extends StandardMBean  implements NotificationEmitter
{
    private NotificationEmitter mNotify;
    
    public NotifyStandardMBean(Object impl, Class intf)
        throws javax.management.NotCompliantMBeanException
    {
        super(impl, intf);
        
        if (!(impl instanceof NotificationEmitter))
        {
            throw new IllegalArgumentException(
                "MBean impl does not implement NotificationEmitter");
        }
        
        mNotify = (NotificationEmitter)impl;
    }
    
    public void addNotificationListener(NotificationListener listener, 
                                        NotificationFilter filter, 
                                        Object obj) 
        throws IllegalArgumentException
    {
        mNotify.addNotificationListener(listener, filter, obj);
        //System.out.println("notify mbean: added listener");
    }
    
    public MBeanNotificationInfo[] getNotificationInfo()
    {
        return mNotify.getNotificationInfo();
    }
    
    public void removeNotificationListener(NotificationListener listener) 
        throws javax.management.ListenerNotFoundException 
    {
        mNotify.removeNotificationListener(listener);
    }
    
    public void removeNotificationListener(NotificationListener listener, 
                                           NotificationFilter filter, 
                                           Object obj)
        throws javax.management.ListenerNotFoundException
    {
        mNotify.removeNotificationListener(listener, filter, obj);
    }
}
