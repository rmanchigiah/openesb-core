/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ManagementContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import java.util.logging.Logger;
import java.util.logging.Level;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectorServer;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.VersionInfo;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.MBeanHelper;
import com.sun.jbi.management.repository.Repository;
import com.sun.jbi.management.registry.Registry;

import com.sun.jbi.JBIProvider;

/**
 * ManagementContext selects attributes from the Framework EnvironmentContext
 * that are used by the management services, and adds attributes that are exclusive
 * to the management services.
 *
 * @author Sun Microsystems, Inc.
 */
public class ManagementContext
{
   
    /** private copy of the framework's EnvironmentContext.  */
    private static EnvironmentContext sEnv = null;

    /** Logger for the management service.  */
    private static Logger sLog = null;

    /** MBeanHelper for the management service.  */
    private static MBeanHelper sMBeanHelper = null;

    /* Management Message Object */
    private static BuildManagementMessageImpl mMMImpl = null;

    /** Repository containing JBI archives.  There is only one instance
     *  of the repository in a JBI environment.
     */
    private static Repository sRepository;
    
    /**
     * Constructs a <CODE>ManagementContext</CODE>.
     * @param anEnv the base framework environment that we re-exporting.
     */
    public ManagementContext(EnvironmentContext anEnv)
    {
        sEnv = anEnv;
        // This logger inherits the level from "com.sun.jbi"
        sLog = Logger.getLogger("com.sun.jbi.management");

        //allocate a MBeanHelper (note the typecast):
        sMBeanHelper = (MBeanHelper) anEnv.getMBeanHelper();
    }

////////////
//Attributes following are copied from the framework's EnvironmentContext.
////////////

    /**
     * Get the installation root directory of the JBI schemaorg_apache_xmlbeans.system.
     * @return String containing the installation root directory path
     */
    public static String getJbiInstallRoot()
    {
        return sEnv.getJbiInstanceRoot();
    }

    /**
     * Get the installation root directory of the JBI schemaorg_apache_xmlbeans.system.
     * @return String containing the installation root directory path
     */
    public static VersionInfo getVersionInfo()
    {
        return sEnv.getVersionInfo();
    }

    /**
     * Get the logger for the ManagementService.
     * @return java.util.Logger is the logger
     */
    public static Logger getLogger()
    {
        return sLog;
    }

    /**
     * Get the MBeanNames service handle.
     * @return The handle to the MBeanNames.
     */
    public static MBeanNames getMBeanNames()
    {
        return (com.sun.jbi.management.MBeanNames) sEnv.getMBeanNames();
    }
    
    /**
     * @return the MBeanNamesImpl for a specific instance
     */
    public MBeanNames getMBeanNames(String instanceName)
    {
        ObjectName 
            someName = getMBeanNames().getBindingMBeanName("xyz", 
                MBeanNames.CONTROL_TYPE_LIFECYCLE);
        
        String domainName = someName.getDomain();
        
        com.sun.jbi.management.support.MBeanNamesImpl mbnNames =
            new com.sun.jbi.management.support.MBeanNamesImpl(domainName, instanceName);
        
        return mbnNames;
    }
    
    /**
     * Get the handle for the MBeanHelper class.
     * @return The handle to the MBeanHelper.
     */
    public static MBeanHelper getMBeanHelper()
    {
        //TODO:  replace this with the framework version once it is available:
        return sMBeanHelper;
    }
    
    /**
     * Get the MBean server with which all MBeans are registered.
     * @return javax.management.MBeanServer the main MBean server
     */
    public static MBeanServer getMBeanServer()
    {
        return sEnv.getMBeanServer();
    }

    /**
     * Get the Central MBean server for the ${ESB}.
     * @return the Master Agent MBean Server Connection
     */
    public static MBeanServerConnection getCentralMBeanServer()
    {   
            return sEnv.getMBeanServer();
    }

    /**
     * Get the Component Manager service handle.
     * @return com.sun.jbi.ComponentManager the Component Manager service.
     */
    public com.sun.jbi.ComponentManager getComponentManager()
    {
        return sEnv.getComponentManager();
    }

    /**
     * Get the Component Query service handle.
     * @return com.sun.jbi.ComponentQuery the Component Query service.
     */
    public com.sun.jbi.ComponentQuery getComponentQuery()
    {
        return sEnv.getComponentQuery();
    }

    /**
     * Get a handle to the class implementing management for the named
     * JBI schemaorg_apache_xmlbeans.system service.
     * @param aServiceName is the name of the management service.
     * @return Object is the class implementing manangment.
     */
    public Object getManagementClass(String aServiceName)
    {
        return sEnv.getManagementClass(aServiceName);
    }

////////////
//Attributes following are only used by the management services:
////////////


    /**
     * Get the JBI_DOMAIN_ROOT.
     * @return string representing JBI domain root directory
     */
    public static String getJbiDomainRoot()
    {
        return System.getProperty("com.sun.jbi.domain.root");
    }

    /**
     * Get the framework environment context.
     * @return reference to the framework EnvironmentContext
     */
    public static EnvironmentContext getEnvironmentContext()
    {
        return sEnv;
    }

    /**
     * Getter for JbiPropertiesFileName.
     * @return the current value of JbiPropertiesFileName.
     * @deprecated - used by jbisan only
     */
    public static String getJbiPropertiesFullPath()
    {        
        return getJbiDomainRoot() + "/jbi/config/jbienv.properties";
    }

    /**
     * Get the management message object.
     * @return BuildManagementMessageImpl is the message object
     */
    public synchronized static BuildManagementMessageImpl getManagementMessageObject()
    {
        if (mMMImpl == null)
        {            
            mMMImpl = new BuildManagementMessageImpl();
        }
        return mMMImpl;
    }

    /**
     * Get the ServiceUnitRegistration handle.
     * @return com.sun.jbi.ServiceUnitRegistation service unit registration handle.
     */
    public com.sun.jbi.ServiceUnitRegistration getServiceUnitRegistration()
    {
        return sEnv.getServiceUnitRegistration();
    }

    /** Private storage for MBeanServerPort: */
    private static int sMBeanServerPort;

    /**
     * Setter for MBeanServerPort.
     * @param aMBeanServerPort - the initialization value.
     */
    public static
    void setMBeanServerPort(int aMBeanServerPort)
    {
        sMBeanServerPort = aMBeanServerPort;
    }

    /**
     * Getter for MBeanServerPort.
     * @return the current value of MBeanServerPort.
     */
    public static int
    getMBeanServerPort()
    {
        return sMBeanServerPort;
    }

    /** Private storage for JmxRemoteServer: */
    private static JMXConnectorServer sJmxRemoteServer;

    /**
     * Setter for JmxRemoteServer.
     * @param aJmxRemoteServer - the initialization value.
     */
    public static
    void setJmxRemoteServer(JMXConnectorServer aJmxRemoteServer)
    {
        sJmxRemoteServer = aJmxRemoteServer;
    }

    /**
     * Getter for JmxRemoteServer.
     * @return the current value of JmxRemoteServer.
     */
    public static JMXConnectorServer
    getJmxRemoteServer()
    {
        return sJmxRemoteServer;
    }


    /** Private storage for JbiRoot: */
    private static String sJbiRoot;

    /**
     * Setter for JbiRoot.
     * @param aJbiRoot - the initialization value.
     */
    public static
    void setJbiRoot(String aJbiRoot)
    {
        sJbiRoot = aJbiRoot;
    }

    /**
     * Getter for JbiRoot.
     * @return the current value of JbiRoot.
     */
    public static String
    getJbiRoot()
    {
        return sJbiRoot;
    }


    /** Private storage for AdminServiceHandle: */
    private static AdminService sAdminServiceHandle;

    /**
     * Setter for AdminServiceHandle.
     * @param aAdminServiceHandle - the initialization value.
     */
    public static
    void setAdminServiceHandle(AdminService aAdminServiceHandle)
    {
        sAdminServiceHandle = aAdminServiceHandle;
    }

    /**
     * Getter for AdminServiceHandle.
     * @return the current value of AdminServiceHandle.
     */
    public static AdminService
    getAdminServiceHandle()
    {
        return sAdminServiceHandle;
    }


    /** Private storage for ConfigurationServiceHandle: */
    private static ConfigurationService sConfigurationServiceHandle;

    /**
     * Setter for ConfigurationServiceHandle.
     * @param aConfigurationServiceHandle - the initialization value.
     *
    public static
    void setConfigurationServiceHandle(ConfigurationService aConfigurationServiceHandle)
    {
        sConfigurationServiceHandle = aConfigurationServiceHandle;
    }*/

    /**
     * Getter for ConfigurationServiceHandle.
     * @return the current value of ConfigurationServiceHandle.
     *
    public static ConfigurationService
    getConfigurationServiceHandle()
    {
        return sConfigurationServiceHandle;
    }*/


    /** Private storage for DeploymentServiceHandle: */
    private static DeploymentService sDeploymentServiceHandle;

    /**
     * Setter for DeploymentServiceHandle.
     * @param aDeploymentServiceHandle - the initialization value.
     */
    public static
    void setDeploymentServiceHandle(DeploymentService aDeploymentServiceHandle)
    {
        sDeploymentServiceHandle = aDeploymentServiceHandle;
    }

    /**
     * Getter for DeploymentServiceHandle.
     * @return the current value of DeploymentServiceHandle.
     */
    public static DeploymentService
    getDeploymentServiceHandle()
    {
        return sDeploymentServiceHandle;
    }


    /** Private storage for InstallationServiceHandle: */
    private static InstallationService sInstallationServiceHandle;

    /**
     * Setter for InstallationServiceHandle.
     * @param aInstallationServiceHandle - the initialization value.
     */
    public static
    void setInstallationServiceHandle(InstallationService aInstallationServiceHandle)
    {
        sInstallationServiceHandle = aInstallationServiceHandle;
    }

    /**
     * Getter for InstallationServiceHandle.
     * @return the current value of InstallationServiceHandle.
     */
    public static InstallationService
    getInstallationServiceHandle()
    {
        return sInstallationServiceHandle;
    }


    /** Private storage for LoggingServiceHandle: */
    private static LoggingService sLoggingServiceHandle;

    /**
     * Setter for LoggingServiceHandle.
     * @param aLoggingServiceHandle - the initialization value.
     */
    public static
    void setLoggingServiceHandle(LoggingService aLoggingServiceHandle)
    {
        sLoggingServiceHandle = aLoggingServiceHandle;
    }

    /**
     * Getter for LoggingServiceHandle.
     * @return the current value of LoggingServiceHandle.
     */
    public static LoggingService
    getLoggingServiceHandle()
    {
        return sLoggingServiceHandle;
    }

    /**
     * Get the ConnectionManager handle.
     * @return The ConnectionManager instance.
     */
    public static com.sun.jbi.messaging.ConnectionManager getConnectionManager()
    {
        return sEnv.getConnectionManager();
    }
    /**
     * Get the interface manager.
     * @return The Interface2Manager instance.
     */
    public com.sun.jbi.messaging.Interface2Manager getInterfaceManager(){
        return sEnv.getInterfaceManager();
    }

    /**
     * Get the FrameworkConfiguration MBean Object Name.
     *
     * @return the FrameworkConfiguration MBean ObjectName
     */
    public static ObjectName getFrameworkConfigurationMBeanName()
    {
        return getMBeanNames().getSystemServiceMBeanName(
                getMBeanNames().SERVICE_NAME_FRAMEWORK,
                getMBeanNames().CONTROL_TYPE_CONFIGURATION
            );
    }
    
    public Repository getRepository()
    {
        return sRepository;
    }
    
    
    public void setRepository(Repository repos)
    {
        sRepository = repos;
    }
    
}
