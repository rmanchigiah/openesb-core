/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DynamicRuntimeConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.LocalStringKeys;

import java.util.Properties;

import javax.management.Descriptor;
import javax.management.modelmbean.ModelMBeanAttributeInfo;


/**
 * InstallationConfigurationFactory defines the configuration parameters required for 
 * installation. Any new parameters would require updates to this class.
 * 
 * @author Sun Microsystems, Inc.
 */
public class InstallationConfigurationFactory 
    extends ConfigurationFactory
{
    public static final String COMPONENT_TIMEOUT          = "componentTimeout";
    public static final String INSTALLATION_TIMEOUT       = "installationTimeout";
    public static final String ENABLE_AUTO_INSTALL        = "autoInstallEnabled";
    public static final String ENABLE_AUTO_REINSTALL      = "autoReinstallEnabled";
    public static final String ENABLE_AUTO_UNINSTALL      = "autoUninstallEnabled";    
    public static final String AUTO_INSTALL_DIR           = "autoInstallDir";
    
    private static final int sNumAttributes = 6;
    
    /**
     * Constructor 
     *
     * @param defProps - default properties
     */
    public InstallationConfigurationFactory(Properties defProps)
    {
        super(defProps, ConfigurationCategory.Installation);
    }
    
    /**
     *
     */
    public ModelMBeanAttributeInfo[] createMBeanAttributeInfo()
    {
         ModelMBeanAttributeInfo[] attributeInfos = new ModelMBeanAttributeInfo[sNumAttributes];
         DescriptorSupport descr;
         String attrName;
         String attrDescr;
         
         // -- Component Timeout
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.COMPONENT_TIMEOUT_DESCR);
         descr.setAttributeName(COMPONENT_TIMEOUT);
         descr.setDisplayName(getString(LocalStringKeys.COMPONENT_TIMEOUT_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.COMPONENT_TIMEOUT_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.COMPONENT_TIMEOUT_DESCR));
         descr.setToolTip(getString(LocalStringKeys.COMPONENT_TIMEOUT_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.COMPONENT_TIMEOUT_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defCompTimeout = mDefaults.getProperty(getQualifiedKey(COMPONENT_TIMEOUT), "0");
         descr.setDefault(Integer.parseInt(defCompTimeout));
         descr.setMinValue("0");
		 descr.setMaxValue(Integer.MAX_VALUE);
         descr.setUnit("milliseconds");
         
         attributeInfos[0] = new ModelMBeanAttributeInfo(
            // name            type   descr      R      W    isIs  Descriptor      
            COMPONENT_TIMEOUT, "int", attrDescr, true, true, false, descr);
         
         // -- Installation Timeout
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.INSTALLATION_TIMEOUT_DESCR);
         descr.setAttributeName(INSTALLATION_TIMEOUT);
         descr.setDisplayName(getString(LocalStringKeys.INSTALLATION_TIMEOUT_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.INSTALLATION_TIMEOUT_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.INSTALLATION_TIMEOUT_DESCR));
         descr.setToolTip(getString(LocalStringKeys.INSTALLATION_TIMEOUT_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.INSTALLATION_TIMEOUT_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defInstTimeout = mDefaults.getProperty(getQualifiedKey(INSTALLATION_TIMEOUT), "0");
         descr.setDefault(Integer.parseInt(defInstTimeout));
		 descr.setMinValue(0);
		 descr.setMaxValue(Integer.MAX_VALUE);
         descr.setUnit("milliseconds");

         attributeInfos[1] = new ModelMBeanAttributeInfo(
            // name               type   descr      R      W    isIs  Descriptor      
            INSTALLATION_TIMEOUT, "int", attrDescr, true, true, false, descr);
         
         // Enable auto-install
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.ENABLE_AUTO_INSTALL_DESCR);
         descr.setAttributeName(ENABLE_AUTO_INSTALL);
         descr.setDisplayName(getString(LocalStringKeys.ENABLE_AUTO_INSTALL_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.ENABLE_AUTO_INSTALL_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.ENABLE_AUTO_INSTALL_DESCR));
         descr.setToolTip(getString(LocalStringKeys.ENABLE_AUTO_INSTALL_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.ENABLE_AUTO_INSTALL_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defEnableAutoInstall = mDefaults.getProperty(getQualifiedKey(ENABLE_AUTO_INSTALL), "true");
         boolean autoInstallEnabled = Boolean.parseBoolean(defEnableAutoInstall);
         descr.setDefault(autoInstallEnabled);
         
         attributeInfos[2] = new ModelMBeanAttributeInfo(
            // name              type       descr      R      W    isIs  Descriptor      
            ENABLE_AUTO_INSTALL, "boolean", attrDescr, true, true, true, descr);
         
         // -  auto-uninstall enabled
         // This property is dependent on the auto-deploy enabled property. i.e. 
         // autoInstallEnabled    autoUninstallEnabled       effective autoUninstallEnabled
         //        0                     0                             0
         //        0                     1                             0         
         //        1                     0                             0
         //        1                     1                             1
         
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.ENABLE_AUTO_UNINSTALL_DESCR);
         descr.setAttributeName(ENABLE_AUTO_UNINSTALL);
         descr.setDisplayName(getString(LocalStringKeys.ENABLE_AUTO_UNINSTALL_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.ENABLE_AUTO_UNINSTALL_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.ENABLE_AUTO_UNINSTALL_DESCR));
         descr.setToolTip(getString(LocalStringKeys.ENABLE_AUTO_UNINSTALL_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.ENABLE_AUTO_UNINSTALL_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defEnableAutoUninstall = mDefaults.getProperty(getQualifiedKey(ENABLE_AUTO_UNINSTALL), "true");
         boolean autoUninstallEnabled = Boolean.parseBoolean(defEnableAutoUninstall) && autoInstallEnabled;
         descr.setDefault(autoUninstallEnabled);
         
         attributeInfos[3] = new ModelMBeanAttributeInfo(
            // name              type       descr      R      W    isIs  Descriptor      
            ENABLE_AUTO_UNINSTALL, "boolean", attrDescr, true, true, true, descr);
         
         // auto-reinstall enabled
         // This property is dependent on the auto-install and auto-uninstall enabled property. i.e. 
         // autoInstallEnabled    autoUninstallEnabled     autoReinstallEnabled  effective 
         //                                                                   autoReinstallEnabled
         //        0                     0                    0                   0
         //        0                     0                    1                   0
         //        0                     1                    0                   0
         //        0                     1                    1                   0
         //        1                     0                    0                   0 
         //        1                     0                    1                   0 
         //        1                     1                    0                   0
         //        1                     1                    1                   1    
         
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.ENABLE_AUTO_REINSTALL_DESCR);
         descr.setAttributeName(ENABLE_AUTO_REINSTALL);
         descr.setDisplayName(getString(LocalStringKeys.ENABLE_AUTO_REINSTALL_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.ENABLE_AUTO_REINSTALL_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.ENABLE_AUTO_REINSTALL_DESCR));
         descr.setToolTip(getString(LocalStringKeys.ENABLE_AUTO_REINSTALL_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.ENABLE_AUTO_REINSTALL_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(false);
         descr.setIsPassword(false);
         String defEnableAutoReinstall = mDefaults.getProperty(getQualifiedKey(ENABLE_AUTO_REINSTALL), "true");
         boolean autoReinstallEnabled = Boolean.parseBoolean(defEnableAutoReinstall) && autoInstallEnabled && autoUninstallEnabled;
         descr.setDefault(autoReinstallEnabled);
         
         attributeInfos[4] = new ModelMBeanAttributeInfo(
            // name              type       descr      R      W    isIs  Descriptor      
            ENABLE_AUTO_REINSTALL, "boolean", attrDescr, true, true, true, descr);         
           
         // Auto Install directory
         descr = new DescriptorSupport();
         attrDescr = getString(LocalStringKeys.AUTO_INSTALL_DIR_DESCR);
         descr.setAttributeName(AUTO_INSTALL_DIR);
         descr.setDisplayName(getString(LocalStringKeys.AUTO_INSTALL_DIR_DISPLAY_NAME));
         descr.setDisplayNameId(getToken(LocalStringKeys.AUTO_INSTALL_DIR_DISPLAY_NAME));
         descr.setDescriptionId(getToken(LocalStringKeys.AUTO_INSTALL_DIR_DESCR));
         descr.setToolTip(getString(LocalStringKeys.AUTO_INSTALL_DIR_TOOLTIP));
         descr.setToolTipId(getToken(LocalStringKeys.AUTO_INSTALL_DIR_TOOLTIP));
         descr.setResourceBundleName("com.sun.jbi.management");
         descr.setIsStatic(true);
         descr.setIsPassword(false);
         descr.setDefault(getAutoInstallDir());
         
         attributeInfos[5] = new ModelMBeanAttributeInfo(
            // name           type                descr      R      W    isIs  Descriptor      
            AUTO_INSTALL_DIR, "java.lang.String", attrDescr, true, false, false, descr);

         return attributeInfos;
    }
    
    /*--------------------------------------------------------------------------------*\
     *                         private helpers                                        *
    \*--------------------------------------------------------------------------------*/
    
    /**
     * @return the abosolute path to the auto-install directory
     */
    private String getAutoInstallDir()
    {
        // JBI autoinstall directory
        com.sun.jbi.EnvironmentContext envCtx = com.sun.jbi.util.EnvironmentAccess.getContext();
        StringBuffer installDir = new StringBuffer(envCtx.getJbiInstanceRoot());
        installDir.append(java.io.File.separator);
        installDir.append(com.sun.jbi.management.system.AutoAdminTask.AUTOINSTALL_DIR);
        
        String defInstallDir = mDefaults.getProperty(getQualifiedKey(AUTO_INSTALL_DIR), 
            installDir.toString());
        
        String absInstalldir = 
            com.sun.jbi.management.util.PropertyFilter.filterProperties(defInstallDir);
        
        if ( absInstalldir != null )
        {
            absInstalldir = absInstalldir.replace('\\', '/');
        }
        
        return absInstalldir;
    }
}
