/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ModelSystemService.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import com.sun.jbi.management.JbiNameInfo;
import com.sun.jbi.management.support.MBeanSet;
import com.sun.jbi.management.common.LoggerMBean;
import com.sun.jbi.management.registry.Registry;
import com.sun.jbi.StringTranslator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;
import javax.management.NotificationFilter;
import javax.management.MBeanNotificationInfo;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.jbi.management.LifeCycleMBean;

/**
 * The ModelSystemService implements the management controls for a standard
 * JBI Framework schemaorg_apache_xmlbeans.system service.
 *
 * @author Sun Microsystems, Inc.
 */
public abstract class ModelSystemService 
    implements LifeCycleMBean, NotificationEmitter
{
    /** JMX object names for all JBI schemaorg_apache_xmlbeans.system "service" mbeans: */
    protected ObjectName mAdminServiceMBeanName;
    protected ObjectName mConfigServiceMBeanName;
    protected ObjectName mInstallServiceMBeanName;
    protected ObjectName mLoggingServiceMBeanName;
    protected ObjectName mDeployServiceMBeanName;
    protected ObjectName mMessageServiceMBeanName;

    protected com.sun.jbi.EnvironmentContext mEnv;

    //allocate mbean sets for INITIAL and START/STOP mbeans:
    protected MBeanSet mInitialMBeans, mStartMBeans;

    /** Logger to be used by ModelSystemService methods: */
    protected Logger mLogger;

    /** handle to our MBean Server: */
    protected MBeanServer mMBeanServer;

    /** handle to our MBean Namer: */
    protected com.sun.jbi.management.MBeanNames mMBeanNames;

    /** handle to jbi schemaorg_apache_xmlbeans.system or installed component identification: */
    protected JbiNameInfo mJbiNameInfo;

    /** User-friendly display name to be used by ModelSystemService methods: */
    protected String mDisplayName;
    
    /**
     * The default logger implementation, all logger tasks are delegated to 
     * this MBean
     */
    protected LoggerMBean mLoggerImpl;    

    /** JMX object names for standard mbeans: */
    protected ObjectName mLifeCycleMBeanName;
    protected ObjectName mLoggerMBeanName;
    protected ObjectName mStatisticsMBeanName;

    protected final String mModelName = "ModelSystemService";

    /** Handle to StringTranslator for message translation: */  
    protected StringTranslator mTranslator;

    /** allocate a logger, and then initialize the ModelSystemService. */
    protected void initModelSystemService(com.sun.jbi.EnvironmentContext anEnv, JbiNameInfo aJbiName)
    {
        //allocate a logger, since none was provided::
        mLogger = Logger.getLogger(this.getClass().getName());

        //call the secondary form of init:
        initModelSystemService(anEnv, mLogger, aJbiName);
    }

    /** Initialize the ModelSystemService.  This does not register any mbeans. */
    protected void initModelSystemService(com.sun.jbi.EnvironmentContext anEnv, Logger aLogger, JbiNameInfo aJbiName)
    {
        //get access to environment implementation, if different:
        mEnv = (com.sun.jbi.EnvironmentContext) anEnv;

        //save handle to jbi schemaorg_apache_xmlbeans.system or installed component identification:
        mJbiNameInfo = aJbiName;
        mDisplayName = aJbiName.name();

        //save logger handle. Note that the logger's level is inherited from
        // the main JBI schemaorg_apache_xmlbeans.system logger (com.sun.jbi) if no level has been
        // explicitly set for this logger.
        mLogger = aLogger;
        
        try 
        {
            mTranslator = (StringTranslator) mEnv.getStringTranslator("com.sun.jbi.management");
        } catch (Exception e) {
            mLogger.warning(e.getMessage());
        }
        
        // Instantiate the Default Logger Implementation
        try
        {
            mLoggerImpl = 
                new com.sun.jbi.management.support.SystemServiceLoggerMBeanImpl(mLogger, mDisplayName);
        }
        catch ( Exception ex )
        {
            String[] params = new String[]{mDisplayName, 
                                           mLogger.getName(), 
                                           ex.getMessage()}; 
            String locMsg = mTranslator.getString(
                    com.sun.jbi.management.LocalStringKeys.
                        JBI_ADMIN_LOGGER_MBN_CREATE_FAILED, params);
            
            mLogger.warning(locMsg);
        }
        
        mMBeanServer = mEnv.getMBeanServer();
        
        try {
            //why doesn't this work for model components????
            //(throws a class-cast exception).
            //mMBeanNames = (com.sun.jbi.management.MBeanNames) mEnv.getMBeanNames();

            //work-around for above:
            com.sun.jbi.management.support.MBeanNamesImpl mbnamesImpl  = 
                new com.sun.jbi.management.support.MBeanNamesImpl(
                    mEnv.getMBeanNames().getJmxDomainName(),
                    mEnv.getPlatformContext().getInstanceName());
            mMBeanNames = (com.sun.jbi.management.MBeanNames) mbnamesImpl;

            //create MBEAN sets for INITIAL and START states:
            mInitialMBeans = new MBeanSet(mMBeanServer, mLogger);
            mStartMBeans = new MBeanSet(mMBeanServer, mLogger);

            //create Object names for standard mbeans, and populate mbean sets:
            initModelSystemMBeanNames();
        } catch (Exception e) {
            mLogger.warning(e.getMessage());
        }

        mLogger.log(Level.FINE, "initModelSystemService: Initialization complete for {0}", mJbiNameInfo.name());
    }

    /** perform one-time startup tasks for this model */
    protected void bootstrap()
    {
        mLogger.log(Level.FINE, "ModelSystemService: START bootstrap for ''{0}''", mJbiNameInfo.name());

        //register INITIAL mbean set:
        mInitialMBeans.register();
        mLogger.log(Level.FINE, "ModelSystemService: FINISH bootstrap for ''{0}''", mJbiNameInfo.name());
    }

    ////////////////
    //implementation of LifeCycleMBean
    ////////////////

    /**
     * Start the item.
     * 
     * @exception javax.jbi.JBIException if the item fails to start.
     */
    public void start() throws javax.jbi.JBIException
    {
        //register START/STOP mbean set:
        mStartMBeans.register();
    }

    /**
     * Stop the item. This suspends current messaging activities.
     * 
     * @exception javax.jbi.JBIException if the item fails to stop.
     */
    public void stop() throws javax.jbi.JBIException
    {
        //unregister START/STOP mbean set:
        mStartMBeans.unregister();
    }

    /**
     * Shut down the item. This releases resources and returns the item
     * to an uninitialized state.
     *
     * @exception javax.jbi.JBIException if the item fails to shut down.
     */
    public void shutDown() throws javax.jbi.JBIException
    {
        //stop management mbean lifecycle:
        try { 
            //make sure lifecycle is stopped:
            mMBeanServer.invoke(mLifeCycleMBeanName, "stop", new Object[0], new String[0]);
        } catch (Exception e) {
            mLogger.warning(e.toString());
            e.printStackTrace();
        }

        try { 
            //unregister INITIAL mbean set:
            mInitialMBeans.unregister();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the current state of this managed compononent.
     * 
     * @return the current state of this managed component (must be one of the 
     *         string constants defined by this interface)
     */
    public String getCurrentState()
    {
        // For now, just return "Unknown" until states are stored.
        return UNKNOWN;
    }
    
    /** ## These methods provide access to registry-related interfaces. ## */
    
    /** 
     * Get a reference to the JBI registry.
     */
    protected Registry getRegistry()
    {
        return (com.sun.jbi.management.registry.Registry)mEnv.getRegistry();
    }    
    
    /** 
     * Get a reference to ComponentQuery.
     */
    protected com.sun.jbi.ComponentQuery getComponentQuery()
    {
        try
        {
            return getRegistry().getComponentQuery();
        }
        catch (com.sun.jbi.management.registry.RegistryException regEx)
        {
            throw new RuntimeException(regEx.getMessage());
        }
    }    
    
    /**
     * create Object names for required ModelSystemService mbeans,
     * and populate the INITIAL state mbean set.
     */
    private void initModelSystemMBeanNames()
    {
        ////////////
        //lifecycle:
        ////////////
        mLifeCycleMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mJbiNameInfo.name(),
                mMBeanNames.CONTROL_TYPE_LIFECYCLE
            );
        mInitialMBeans.add(
                mLifeCycleMBeanName,
                javax.jbi.management.LifeCycleMBean.class,
                this
            );

        /////////
        //logger:
        /////////
        if ( mLoggerImpl != null )
        {
            mLoggerMBeanName = mMBeanNames.getSystemServiceLoggerMBeanName(
                    mJbiNameInfo.name(),
                    mLogger
                );
            mInitialMBeans.add(
                    mLoggerMBeanName,
                    com.sun.jbi.management.common.LoggerMBean.class,
                    mLoggerImpl
                );
        }

        /////////
        //statistics:
        /////////
        mStatisticsMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mJbiNameInfo.name(),
                mMBeanNames.CONTROL_TYPE_STATISTICS
            );

        initSystemServiceMBeanNames();
    }

    /**
     * create Object names for all System Service MBeans.
     */
    private void initSystemServiceMBeanNames()
    {
        mAdminServiceMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_ADMIN_SERVICE,
                mMBeanNames.CONTROL_TYPE_ADMIN_SERVICE
            );
        mConfigServiceMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_CONFIG_SERVICE,
                mMBeanNames.CONTROL_TYPE_CONFIG_SERVICE
            );
        mDeployServiceMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_DEPLOY_SERVICE,
                mMBeanNames.CONTROL_TYPE_DEPLOY_SERVICE
            );
        mInstallServiceMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_INSTALL_SERVICE,
                mMBeanNames.CONTROL_TYPE_INSTALL_SERVICE
            );
        mLoggingServiceMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_LOGGING_SERVICE,
                mMBeanNames.CONTROL_TYPE_LOGGING_SERVICE
            );
        mMessageServiceMBeanName = mMBeanNames.getSystemServiceMBeanName(
                mMBeanNames.SERVICE_NAME_MESSAGE_SERVICE,
                mMBeanNames.CONTROL_TYPE_MESSAGE_SERVICE
            );
    }

    //allocate our broadcast support class:
    private NotificationBroadcasterSupport mEmitter = new NotificationBroadcasterSupport();

//Implementation for NotificationBroadcaster, extended by NotificationEmitter
    /**
     * Adds a listener to this MBean.
     *
     * @param listener The listener object which will handle the
     * notifications emitted by the broadcaster.
     * @param filter The filter object. If filter is null, no
     * filtering will be performed before handling notifications.
     * @param handback An opaque object to be sent back to the
     * listener when a notification is emitted. This object cannot be
     * used by the Notification broadcaster object. It should be
     * resent unchanged with the notification to the listener.
     *
     * @exception IllegalArgumentException Listener parameter is null.
     *
     * @see #removeNotificationListener
     */
    public void addNotificationListener(NotificationListener listener,
            NotificationFilter filter, Object handback)
        throws java.lang.IllegalArgumentException
    {
        mEmitter.addNotificationListener(listener, filter, handback);
    }

    /**
     * Removes a listener from this MBean.  If the listener
     * has been registered with different handback objects or
     * notification filters, all entries corresponding to the listener
     * will be removed.
     *
     * @param listener A listener that was previously added to this
     * MBean.
     *
     * @exception ListenerNotFoundException The listener is not
     * registered with the MBean.
     *
     * @see #addNotificationListener
     * @see NotificationEmitter#removeNotificationListener
     */
    public void removeNotificationListener(NotificationListener listener)
        throws ListenerNotFoundException
    {
        mEmitter.removeNotificationListener(listener);
    }

    /**
     * <p>Returns an array indicating, for each notification this
     * MBean may send, the name of the Java class of the notification
     * and the notification type.</p>
     *
     * <p>It is not illegal for the MBean to send notifications not
     * described in this array.  However, some clients of the MBean
     * server may depend on the array being complete for their correct
     * functioning.</p>
     *
     * @return the array of possible notifications.
     */
    public MBeanNotificationInfo[] getNotificationInfo()
    {
        return (new MBeanNotificationInfo[0]);
    }

//Implementaion for NotificationEmitter, which extends NotificationBroadcaster

    /**
     * <p>Removes a listener from this MBean.  The MBean must have a
     * listener that exactly matches the given <code>listener</code>,
     * <code>filter</code>, and <code>handback</code> parameters.  If
     * there is more than one such listener, only one is removed.</p>
     *
     * <p>The <code>filter</code> and <code>handback</code> parameters
     * may be null if and only if they are null in a listener to be
     * removed.</p>
     *
     * @param listener A listener that was previously added to this
     * MBean.
     * @param filter The filter that was specified when the listener
     * was added.
     * @param handback The handback that was specified when the listener was
     * added.
     *
     * @exception ListenerNotFoundException The listener is not
     * registered with the MBean, or it is not registered with the
     * given filter and handback.
     */
    public void removeNotificationListener(NotificationListener listener,
           NotificationFilter filter,
           Object handback)
        throws ListenerNotFoundException
    {
        mEmitter.removeNotificationListener(listener, filter, handback);
    }

    /**
     * Getter for local NotificationBroadcaster.
     *
     * @return NotificationBroadcaster
     */
    public NotificationBroadcasterSupport getNotificationBroadcaster()
    {
        return mEmitter;
    }
}
