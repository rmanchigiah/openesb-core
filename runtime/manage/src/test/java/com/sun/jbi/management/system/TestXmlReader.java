/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestXmlReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import org.xml.sax.EntityResolver;

import com.sun.jbi.management.internal.support.XmlReader;
import java.io.File;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests Xml Parser/validation.
 * @author Sun Microsystems, Inc.
 */

public class TestXmlReader
{

    /**
     * one sample jbi.xml filename
     */
    private String mConfig1 = null;

    /**
     * another sample jbi.xml filename
     */
    private String mConfig2 = null;
    
    @Before
    public void setUp() {
    //    String srcroot = Util.getSourceRoot();
        String testPath = "target/test-classes";
        mConfig1 = testPath + "/support/binding1.xml";
        mConfig2 = testPath + "/support/flawed1.xml";
        
        // Instantiate an Environment Context instance and  set the context in EnvironmentAccess
        Util.createEnvironmentContext();
    }
    
    /**
     * Test entity resolver.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void entityLoader()
        throws Exception
    {
        String testname = "entityLoader";
        try 
        {
            // -- Test the resolveEntity method
            EntityResolver er = new XmlReader(); 
            System.out.println("er.resolver.resolveEntity: " +
                er.resolveEntity(null, "./regress/jbi.xsd"));
            System.out.println("er.resolver.resolveEntity: " +
                er.resolveEntity(null, "./jbi.xsd"));
        }
        catch (Exception anEx)
        {
            anEx.printStackTrace();
            fail(testname + ": failed due to -" + anEx.getMessage());
            throw anEx;
        }
    }
    
    /**
     * Test the load without any validation.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadAndParse()
        throws Exception
    {
        String testname = "loadAndParse";
        try 
        {
            // -- Test the loadAndParse method
            XmlReader xmlReader = new XmlReader(); 
            String doc = xmlReader.loadAndParse(mConfig1, true);
            assertNotNull("The Document was not loaded correctly.", doc); 
        }
        catch (Exception anEx)
        {
            anEx.printStackTrace();
            fail(testname + ": failed due to -" + anEx.getMessage());
            throw anEx;
        }
    }
    
    /**
     * Test the load with validation.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void loadWithValidate()
        throws Exception
    {
        String testname = "loadWithValidate";
        try 
        {
            // -- Test the validate method
            XmlReader xmlReader = new XmlReader(); 
            boolean valid = xmlReader.validate(mConfig1);
            assertTrue("The Document did not get validated.", valid);
            String doc = xmlReader.loadAndParse(mConfig1, true);
            assertNotNull("The Document was not loaded correctly.", doc); 
        }
        catch (Exception anEx)
        {
            anEx.printStackTrace();
            fail(testname + ": failed due to -" + anEx.getMessage());
            throw anEx;
        }
    }
    
    /**
     * Test the load (for a flawed jbi.xml) without any validation.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void badLoadAndParse()
        throws Exception
    {
        String testname = "badLoadAndParse";
        try 
        {
            // -- Test the loadAndParse method
            XmlReader xmlReader = new XmlReader(); 
            String doc = xmlReader.loadAndParse(mConfig2, true);
            assertNull("Document was not flagged as invalid.", doc); 
        }
        catch (Exception anEx)
        {
            anEx.printStackTrace();
            fail(testname + ": failed due to -" + anEx.getMessage());
            throw anEx;
        }
    }
    
    /**
     * Test the load (for a flawed jbi.xml) with validation.
     * @throws Exception if an unexpected error occurs
     */
    @Test
    public void badValidate()
        throws Exception
    {
        String testname = "badValidate";
        try 
        {
            // -- Test the validate method
            XmlReader xmlReader = new XmlReader(); 
            boolean valid = xmlReader.validate(mConfig2);
            fail("The Document got incorrectly validated.");
        }
        catch (Exception anEx)
        {
            // expect this exception -- ignore it.
            assertTrue("caught expected exception", true);
        }
    }
}
