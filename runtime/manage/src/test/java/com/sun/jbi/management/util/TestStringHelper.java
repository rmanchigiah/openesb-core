/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestStringHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

/**
 *
 * @author Sun Microsystems, Inc.
 */

public class TestStringHelper
    extends junit.framework.TestCase
{
    
    public TestStringHelper(String testName)
    {
        super(testName);
    }
    
    /**
     * Test converting an ineteger String to int
     */
    public void testConvertStringToInteger()
        throws Exception
    {
        Integer integer = (Integer) StringHelper.convertStringToType("int", "123");
        int value = 123;
        assertEquals(value, integer.intValue());
    }
    
    /**
     * Test converting an ineteger String to boolean
     */
    public void testConvertStringToBoolean()
        throws Exception
    {
        Boolean flag = (Boolean) StringHelper.convertStringToType("boolean", "false");
        boolean value = false;
        assertEquals(value, flag.booleanValue());
    }
    
    /**
     * Test converting an ineteger String to String .. well
     */
    public void testConvertStringToString()
        throws Exception
    {
        String str = (String) StringHelper.convertStringToType("java.lang.String", "testString");
        String value = "testString";
        assertEquals(value, str);
    }
    
 
}
