#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00502.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#manage00502 - test the autodeploy functionality.
#the stand-alone JBI container version of this test is manage00512.ksh.
#note that the "functionality" of (this) autodeploy under GlassFish is a no-op!

testname=`echo "$0" | sed 's/^.*\///' | sed 's/\..*//'`
echo testname is $testname
. ./regress_defs.ksh

autoDeployDir="$JBI_DOMAIN_ROOT/jbi/autodeploy"
statusDir="$autoDeployDir/.autodeploystatus"

rm -rf $autoDeployDir
mkdir -p $autoDeployDir

echo "Copying appropriate service assembly into autodeploy directory."
ant -q -emacs -propertyfile "$JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml
autoDeployDelay

echo "After autodeploy ..."
echo "File count: $autoDeployDir"
ls -l $autoDeployDir | wc -l
echo "File count: $statusDir"
ls -l $statusDir | wc -l
echo "autoDeployed area success files"
ls -l $autoDeployDir | grep _deployed | wc -l
echo "autoDeployed area failed files"
ls -l $autoDeployDir | grep _notDeployed | wc -l
echo "recursive listing"
cd $autoDeployDir
find . -type f -print | sort

# clean up
# $JBI_ANT -Djbi.shared.library.name=b3c97e99-bcb2-4ee0-95e4-60e1cf1408aa uninstall-shared-library
# $JBI_ANT -Djbi.component.name=JbiCalculator shut-down-component
# $JBI_ANT -Djbi.component.name=JbiCalculator uninstall-component
