/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UIMBeanFactoryReferenceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.runtime.mbeans;

import javax.jbi.JBIException;
import javax.management.ObjectName;

import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.MBeanNames;

/**
 * This class is a factory implemenation that creates the UIMBean impl depending
 * on the RI configuration.
 * 
 * @author graj
 * 
 */
public class UIMBeanFactoryReferenceImpl extends UIMBeanFactory {

    private static ObjectName sFrameworkConfigMBeanObjectName = null;

    /**
     * 
     */
    public UIMBeanFactoryReferenceImpl() {
    }

    /**
     * creates the UIMBean implemenation.
     * 
     * @param aContext
     *            jbi context
     * @throws javax.jbi.JBIException
     *             on error
     * @return UIMBean implemenation
     */
    public JBIAdminCommandsUIMBean createJBIAdminCommandsUIMBean(
            EnvironmentContext aContext) throws JBIException {
        // return RI Impl
        AbstractUIMBeanImpl.logDebug("Creating RI Impl of UIMBean");
        return new JBIAdminCommandsUIMBeanImpl(aContext);
    }

    /**
     * creates the JBIStatisticsMBean.
     * @param aContext EnvironmentContext
     * @throws javax.jbi.JBIException on error
     * @return UIMBean implemenation
     */
    public JBIStatisticsMBean createJBIStatisticsMBean(
            EnvironmentContext aContext) 
    throws JBIException {
        return new JBIStatisticsMBeanImpl(aContext);
    }        

    /**
     * gets the framework configuration mbean object name
     * 
     * @param aContext
     *            framework context
     * @return mbean object name
     */
    protected static ObjectName getFrameworkConfigMBeanObjectName(
            EnvironmentContext aContext) {
        if (UIMBeanFactoryReferenceImpl.sFrameworkConfigMBeanObjectName == null) {
            MBeanNames mbeanNames = (com.sun.jbi.management.MBeanNames) aContext
                    .getMBeanNames();
            UIMBeanFactoryReferenceImpl.sFrameworkConfigMBeanObjectName = mbeanNames
                    .getSystemServiceMBeanName(
                            MBeanNames.SERVICE_NAME_FRAMEWORK,
                            MBeanNames.CONTROL_TYPE_CONFIGURATION);
        }
        return UIMBeanFactoryReferenceImpl.sFrameworkConfigMBeanObjectName;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

    }

}
