#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)runtime-ui-00153.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

. ./regress_defs.ksh

echo "runtime-ui-00153 : Test getting component logger level with component name = null."

COMPONENT_NAME=sun-http-binding

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00153.xml set.component.logger

# Test fix for Issue 279 : Set a component logger which does not exist
asadmin start-jbi-component  -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT  $COMPONENT_NAME
asadmin set-jbi-component-logger --component=$COMPONENT_NAME -u $AS_ADMIN_USER $ASADMIN_PW_OPTS --port $ASADMIN_PORT  fred=INFO


