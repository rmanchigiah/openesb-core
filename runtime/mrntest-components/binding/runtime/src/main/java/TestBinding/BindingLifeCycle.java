/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingLifeCycle.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package TestBinding;

import java.io.File;
import java.io.FileWriter;

import java.util.Random;
import java.util.logging.Logger;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.management.ObjectName;

import javax.xml.namespace.QName;
        
import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
        

/**
 * This class implements ComponentLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Sun Microsystems, Inc.
 */
public class BindingLifeCycle
    implements ComponentLifeCycle, Component, Runnable
{
    /**
     * Engine context passed down from framework to this transformation engine.
     */
    private ComponentContext mContext = null;

    /**
     * Engine context passed down from framework to this transformation engine.
     */
    private DeliveryChannel mChannel = null;

    /**
     * Refernce to logger.
     */
    private Logger mLogger = null;

    private Thread                      mRunThread = null;
    private boolean                     mStopping = false;
    private Random                      mRandom = new Random(System.nanoTime());
    private MessageExchangeFactory     mFactory;
    private QName                       mOperation = new QName("operation1");
    private QName                       mService = new QName("NMR-Test-Service");
    private QName                       mService2 = new QName("NMR-Test-Service2");
    private QName                       mLinkedService = new QName("NMR-Test-Linked-Service");
    private QName                       mLinkedService2 = new QName("NMR-Test-Linked-Service2");
    private ServiceEndpoint             mEndpoint;
    private ServiceEndpoint             mEndpoint2;
    private ServiceEndpoint             mLinkedEndpoint;
    private ServiceEndpoint             mLinkedEndpoint2;
    private long			mCount;
   
    /**
     *    
     */
    private boolean mInitSuccess = false;

    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public ObjectName getExtensionMBeanName()
    {
        return null;
    }

    public ServiceUnitManager getSUManager()
    {
        return null;
    }

    //javax.jbi.component.ComponentLifeCycle methods

    /**
     * Initialize the transformation engine. This performs initialization
     * required by  the transformation engine but does not make it ready to
     * process messages.  This method is called immediately after installation
     * of the Transformation engine. It is also called when the JBI framework
     * is starting up, and any time the transformation engine is being
     * restarted after previously being shut down through a call to
     * shutDown().
     *
     * @param context the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext context)
        throws javax.jbi.JBIException
    {
        mLogger = Logger.getLogger(this.getClass().getPackage().getName()); 
        
        mContext = context;
        
        mInitSuccess = true;
    }

    /**
     * Shutdown the transformation engine. This performs cleanup before the BPE
     * is terminated. Once this method has been called, init() must be called
     * before the transformation engine can be started again with a call to
     * start().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         shut down.
     */
    public void shutDown()
        throws javax.jbi.JBIException
    {
        mContext = null;
    }

    /**
     * Start the transformation engine. This makes the BPE ready to  process
     * messages. This method is called after init() completes when the JBI
     * framework is starting up, and when the transformation engine is being
     * restarted after a previous call to shutDown().  If stop() was called
     * previously but shutDown() was not, start() can be called without a call
     * to init().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         start.
     */
    public void start()
        throws javax.jbi.JBIException
    {

        if (!mInitSuccess)
        {

            return;
        }

        mChannel = mContext.getDeliveryChannel();
        mFactory = mChannel.createExchangeFactory();

        if (mRunThread == null)
        {
            mStopping = false;
            mRunThread = new Thread(this, "NMR-Test-Binding");
            mRunThread.setDaemon(true);
            mRunThread.start();
        }
        
    }

    /**
     * Stop the transformation engine. This makes the BPE stop accepting
     * messages for processing. After a call to this method, start() can be
     * called again without first calling init().
     *
     * @throws javax.jbi.JBIException if the transformation engine is unable to
     *         stop.
     */
    public void stop()
        throws javax.jbi.JBIException
    {
        // To add code to stop all services
        try
        {
            mLogger.info("NMR TestBinding-run: stopping binding");
            if (mRunThread != null)
            {
		synchronized (this)
                {
		    mStopping = true;
		}
		Thread.sleep(20);
                try
                {
                    mRunThread.join(500);
		    mRunThread.interrupt();
		    mRunThread.join();
                }
                catch (InterruptedException ie)
                {

                }
            }

	    try
	    {
	        FileWriter      fw = new FileWriter(mContext.getWorkspaceRoot()
	    	    + File.separator + "binding.stats");
	        fw.write("Send " + Long.valueOf(mCount).toString() + " Messages\n");
	        fw.write(mCount == 0 ? "FAILED\n" : "SUCCESS\n");
	        fw.close();
	        fw = new FileWriter(mContext.getWorkspaceRoot()
	    	    + File.separator + "binding.info");
	        fw.write(mChannel.toString());
	        fw.close();
	    }
	    catch (java.io.IOException e)
	    {
	    }

            if (mChannel != null)
            {
                mLogger.info("Binding: " + mChannel.toString());
                mChannel.close();
            }
        }
        catch (Throwable ex)
        {
            ex.printStackTrace();
        }
        mLogger.info("NMR TestBinding-run: stopped");

    }
    
     //-------------------------------Component---------------------------------
   
        /**
     * Returns an object of TransformationEngineLifeCycle.
     *
     * @return ComponentLifeCycle Object implementing
     *         javax.jbi.component.ComponentLifeCycle interface.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return this;
    }

    /**
     * Returns an object implementing javax.jbi.component.ServiceUnitManager 
     * interface.
     *
     * @return ServiceUnitManager Object implementing
     *         javax.jbi.component.ServiceUnitManager interface.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        return null;
    }
    
    /**
     *
     * @param ref ServiceEndpoint object
     *
     * @return Descriptor Object implementing javax.jbi.servicedesc.Descriptor
     *         interface.
     */
    public Document getServiceDescription(ServiceEndpoint ref)
    {
	org.w3c.dom.Document desc = null;

	try
	{
	}
	catch (Exception e)
	{
    		e.printStackTrace();
	}
	return desc;

    }

    
    /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. The consumer is described by the given 
     *  capabilities, and JBI has already ensured that a fit exists between the 
     *  set of required capabilities of the provider and the available 
     *  capabilities of the consumer, and vice versa. This matching consists of
     *  simple set matching based on capability names only. <br><br>
     *  Note that JBI assures matches on capability names only; it is the 
     *  responsibility of this method to examine capability values to ensure a 
     *  match with the consumer.
     *  @param endpoint the endpoint to be used by the consumer
     *  @param exchange the proposed message exchange to be performed
     *  @param consumerCapabilities the consumers capabilities and requirements
     *  @return true if this provider component can perform the the given 
     *   exchange with the described consumer
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. Ths provider is described 
     *  by the given capabilities, and JBI has already ensure that a fit exists 
     *  between the set of required capabilities of the consumer and the 
     *  available capabilities of the provider, and vice versa. This matching 
     *  consists of simple set matching based on capability names only. <br><br>
     *  Note that JBI assures matches on capability names only; it is the 
     *  responsibility of this method to examine capability values to ensure a 
     *  match with the provider.
     *  @param exchange the proposed message exchange to be performed
     *  @param providerCapabilities the providers capabilities and requirements
     *  @return true if this consurer component can interact with the described
     *   provider to perform the given exchange
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
    
    public void run()
    {
        mEndpoint = mContext.getEndpointsForService(mService)[0];
        mEndpoint2 = mContext.getEndpointsForService(mService2)[0];
        mLinkedEndpoint = mContext.getEndpointsForService(mLinkedService)[0];
        mLinkedEndpoint2 = mContext.getEndpointsForService(mLinkedService2)[0];
	if (mEndpoint == null)
        {
            mLogger.info("Endpoint not found: " + mService);
        }
	if (mEndpoint2 == null)
        {
            mLogger.info("Endpoint not found: " + mService2);
        }
	if (mLinkedEndpoint == null)
        {
            mLogger.info("Service not found: " + mLinkedService);
        }
	if (mLinkedEndpoint2 == null)
        {
            mLogger.info("Service not found: " + mLinkedService2);
        }

        try
        {
            for (;;)
            {
		synchronized (this)
                {
		    if (mStopping)
		    {
			break;
		    }
		}

                try
                {
                    generateRequest();
                }
                catch (javax.jbi.messaging.MessagingException mex)
                {
		    logExceptionInfo("NMR TestBinding-run: MessagingException : ", mex);
                    break;
                }
                mCount++;
                if ((mCount % 100) == 0)
                {
                    Thread.yield();
                }
                if ((mCount % 1000) == 0)
                {
                    Thread.sleep(20);
                }
		if (mCount > 500000)
		{
		    break;
		}
           }
        }
        catch (Exception e)
        {
            logExceptionInfo("NMR TestBinding-run: Exception : ", e);
        }
	sendCommand("STOP");
        mStopping = true;
    }

    void logExceptionInfo(String reason, Throwable t)
    {
        StringBuffer                    sb = new StringBuffer();
        java.io.ByteArrayOutputStream   b;
        Throwable                       nextT = t;
        
        sb.append(reason);
        while (t != null)
        {
            if (nextT != null)
            {
                java.io.PrintStream ps = new java.io.PrintStream(b = new java.io.ByteArrayOutputStream());
                t.printStackTrace(ps);
                sb.append(" Exception (");
                sb.append(b.toString());
                sb.append(") ");    
            }
            nextT = t.getCause();
            if (nextT != null)
            {
                sb.append("Caused by: ");
            }
            t = nextT;
        }
        
        mLogger.warning(sb.toString());        
    }

    
    void generateRequest()
        throws javax.jbi.messaging.MessagingException
    {
        MessageExchange     me;
        int                 rand = mRandom.nextInt(100);
       
        if (rand >= 95)
        {
            me = mFactory.createInOptionalOutExchange();
            doInOptionalOut((InOptionalOut)me);
        }
        else if (rand >= 90)
        {
            me = mFactory.createRobustInOnlyExchange();
            doRobustInOnly((RobustInOnly)me);
        }
        else if (rand >= 75)
        {
            me = mFactory.createInOnlyExchange();
            doInOnly((InOnly)me);
        }
        else 
        {
            me = mFactory.createInOutExchange();
            doInOut((InOut)me);
        }
   }
    
    void doInOnly(InOnly me)
        throws javax.jbi.messaging.MessagingException
    {
        me.setInMessage(me.createMessage());
        me.setOperation(mOperation);
	setEndpoint(me);
	if (mRandom.nextInt(100) < 30)
	{	
            mChannel.sendSync(me);
	}
	else
	{
	    mChannel.send(me);
	    me = (InOnly)mChannel.accept();
	}
    }

    void doInOut(InOut me)
        throws javax.jbi.messaging.MessagingException
    {
        me.setInMessage(me.createMessage());
        me.setOperation(mOperation);
	setEndpoint(me);
	if (mRandom.nextInt(100) < 30)
	{	
            mChannel.sendSync(me);
	}
	else
	{
	    mChannel.send(me);
	    me = (InOut)mChannel.accept();
	}
        if (me.getStatus().equals(ExchangeStatus.ACTIVE))
        {
            if (mRandom.nextInt(100) > 90)
            {
                me.setStatus(ExchangeStatus.ERROR);
            }
            else
            {
                me.setStatus(ExchangeStatus.DONE);
            }
            mChannel.send(me);
        }
    }
    
    void doRobustInOnly(RobustInOnly me)
        throws javax.jbi.messaging.MessagingException
    {
        me.setInMessage(me.createMessage());
        me.setOperation(mOperation);
	setEndpoint(me);
	if (mRandom.nextInt(100) < 30)
	{	
            mChannel.sendSync(me);
	}
	else
	{
	    mChannel.send(me);
	    me = (RobustInOnly)mChannel.accept();
	}
        if (me.getStatus().equals(ExchangeStatus.ACTIVE))
        {
            if (me.getFault() != null)
            {
                if (mRandom.nextInt(100) > 90)
                {
                    me.setStatus(ExchangeStatus.ERROR);
                }
                else
                {
                    me.setStatus(ExchangeStatus.DONE);
                }
                mChannel.send(me);
            }
         }
    }
    
    void doInOptionalOut(InOptionalOut me)
        throws javax.jbi.messaging.MessagingException
    {
        me.setInMessage(me.createMessage());
        me.setOperation(mOperation);
	setEndpoint(me);
	if (mRandom.nextInt(100) < 30)
	{	
            mChannel.sendSync(me);
	}
	else
	{
	    mChannel.send(me);
	    me = (InOptionalOut)mChannel.accept();
	}
        if (me.getStatus().equals(ExchangeStatus.ACTIVE))
        {
            if (me.getOutMessage() != null)
            {
                if (mRandom.nextInt(100) > 90)
                {
                    me.setFault(me.createFault());
		    if (mRandom.nextInt(100) < 30)
		    {	
		        mChannel.sendSync(me);
		    }
		    else
		    {
		        mChannel.send(me);
			me = (InOptionalOut)mChannel.accept();
		    }
                }
                else
                {
                    if (mRandom.nextInt(100) >= 98)
                    {
                        me.setStatus(ExchangeStatus.ERROR);
                    }
                    else
                    {
                        me.setStatus(ExchangeStatus.DONE);
                    }
                    mChannel.send(me);
                }
            }
            else
            {
                if (mRandom.nextInt(100) >= 90)
                {
                    me.setStatus(ExchangeStatus.ERROR);
                }
                else
                {
                    me.setStatus(ExchangeStatus.DONE);
                }
                mChannel.send(me);              
            }              
         }
    }

    void setEndpoint(MessageExchange me)
    {
	int rand = mRandom.nextInt(100);
	if (rand < 10)
	{
	    me.setEndpoint(mEndpoint);
	}
	else if (rand < 30)
	{
	    me.setEndpoint(mLinkedEndpoint);
	}
	else if (rand < 60)
	{
	    me.setEndpoint(mLinkedEndpoint2);
	}
	else
	{
	    me.setEndpoint(mEndpoint2);
	}
    }

    void sendCommand(String command)
    {
	try
	{
	    mLogger.info("NMR TestBinding-run: Send STOP");
	    InOnly	me = mFactory.createInOnlyExchange();
	    me.setInMessage(me.createMessage());
	    me.setOperation(new QName(command));
	    me.setEndpoint(mEndpoint);
	    mChannel.send(me);
	    mLogger.info("NMR TestBinding-run: Sent STOP");
	}
	catch (Exception e)
	{
		mLogger.info("NMR TestBinding failed to sendCommand");
	}
    }
}
