#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)framework00008.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

# Tests the component extension mechanism (lib/ext) for the Java SE platform. 

#regress setup
. ./regress_defs.ksh

# package components
ant -emacs -q -f framework00008.xml package

# copy our base class to the lib/ext directory
rm -rf $JBISE_HOME/lib/ext
mkdir -p $JBISE_HOME/lib/ext/com/sun/jbi/framework
cp $JV_FRAMEWORK_BLD_DIR/com/sun/jbi/framework/AbstractComponent.class $JBISE_HOME/lib/ext/com/sun/jbi/framework

# start the framework
start_jbise &
startInstanceDelay

# install components
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/ext-engine.jar install-component
installComponentDelay

# start components
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-extension-component" start-component
startComponentDelay

# Query the state of our components and service assemblies
$JBISE_ANT -Djbi.service.engine.name="test-extension-component" list-service-engines

# shutdown components
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-extension-component" shut-down-component
stopComponentDelay

# uninstall components
$JBISE_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-extension-component" uninstall-component
uninstallComponentDelay

shutdown_jbise
stopInstanceDelay

# ### END
