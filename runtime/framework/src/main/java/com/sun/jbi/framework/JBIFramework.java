/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JBIFramework.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.JBIProvider;
import com.sun.jbi.management.system.ManagementContext;
import com.sun.jbi.platform.PlatformContext;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.naming.Context;
import javax.naming.InitialContext;


/**
 * This is the top-level class that provides the lifecycle for the JBI
 * framework. The methods defined here are called by a wrapper that is specific
 * to a particular application server implementation.
 *
 * @author Sun Microsystems, Inc.
 */
public class JBIFramework 
{
    /**
     * The global context for the entire framework environment.
     */
    protected EnvironmentContext mEnvironment;

    /**
     * The Logger instance for the framework.
     */
    private Logger mLog;

    /**
     * Total elapsed time used by framework startup, in milliseconds.
     */
    private long mStartupElapsedTime;

    /**
     * Total elapsed time used by framework shutdown, in milliseconds.
     */
    private long mShutdownElapsedTime;

    /**
     * The StringTranslator instance for the framework.
     */
    private StringTranslator mTranslator;

    /**
     * Framework start failure flag
     */
    private boolean mStartFailed;

    /**
     * Component Framework started flag
     */
    private boolean mCfStarted;

    /**
     * Component Registry started flag
     */
    private boolean mCrStarted;
    
    /**
     * Configuration Service started flag
     */
    private boolean mCsStarted;
    
    /**
     * Management Service started flag
     */
    private boolean mMsStarted;

    /**
     * Normalized Message Service started flag
     */
    private boolean mNmsStarted;

    /**
     * Management Runtime Service started flag
     */
    private boolean mMrsStarted;
    
    /**
     * Constant for the dummy file name of "timestamp.ref"
     */
    private final static String TIMESTAMP_REF = "timestamp.ref";
    
    /**
     * Flag used to indicate that the framework has started completely.
     * In the case of a lazy initialization, mFrameworkReady will remain
     * false until the first request for a system resource (e.g. registry),
     * at which point the framework will be started completely and this flag
     * will be set to true.
     */
    private boolean mFrameworkReady;

    /**
     * Flag used to indicate that this is a "lazy" initializtion. This happens
     * when there are no components that need to be any state other than
     * <code>SHUTDOWN</code>. In this case, , mFrameworkReady will remain
     * false until the first request for a system resource (e.g. registry),
     * at which point the framework will be started completely.
     */
    private boolean mLazyInit;
    
    /**
     * This method initializes the JBI framework. 
     * 
     * @param initialProperties the Properties provider.
     * @param platformContext the PlatformContext instance.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void init(
            PlatformContext platformContext, 
            Properties initialProperties)
        throws javax.jbi.JBIException
    {
        // Get time this method was entered
        long startTime = System.currentTimeMillis();

        // Create and initialize the Environment Context. This is a singleton.
        mEnvironment = new EnvironmentContext(
                platformContext, this, initialProperties);
        mEnvironment.setJbiInitTime(startTime);

        // Initialize common utility class.
        com.sun.jbi.util.jmx.MBeanUtils.init(platformContext.getMBeanServer());

        // Obtain the logger and message translator for the framework.
        mLog = mEnvironment.getLogger();
        mLog.finest("JBIFramework.init entered");
        mTranslator = (StringTranslator)
            mEnvironment.getStringTranslator("com.sun.jbi.framework");

        mLog.fine(mTranslator.getString(LocalStringKeys.JBI_INITIALIZING));
        
        // Initialize the JBI repository
        initRepository();

        // Initialize core services. If one fails, exit immediately.
        boolean initFailed = false;
        String serviceName = null;
        try
        {
            /**
             * Initialize the Configuration Service. 
             * NOTE : This should always be the first service being initialized
             * since other services depend on the Configuration MBeans registered
             * by this service.
             */
            serviceName = mTranslator.getString(LocalStringKeys.CS_NAME);
            mEnvironment.getConfigurationService().initService(mEnvironment);
            
            // Initialize the Normalized Message Service
            serviceName = mTranslator.getString(LocalStringKeys.NMS_NAME);
            mEnvironment.getNormalizedMessageService().initService(mEnvironment);

            // Initialize the Component Registry
            serviceName = mTranslator.getString(LocalStringKeys.CR_NAME);
            mEnvironment.getComponentRegistry().initService(mEnvironment);

            // Initialize the Component Framework
            serviceName = mTranslator.getString(LocalStringKeys.CF_NAME);
            mEnvironment.getComponentFramework().initService(mEnvironment);

            // Initialize the Management Service LAST
            serviceName = mTranslator.getString(LocalStringKeys.MS_NAME);
            mEnvironment.getManagementService().initService(mEnvironment);
        }
        catch ( Throwable initEx )
        {
            initFailed = true;
            logFailure(serviceName, initEx, LocalStringKeys.INIT_SERVICE_FAILED);
        }

        // If any service failed to initialize, prevent the framework from
        // starting.
        if ( initFailed )
        {
            String msg =
                mTranslator.getString(LocalStringKeys.INIT_FRAMEWORK_FAILED);
            mLog.severe(msg);
            throw new javax.jbi.JBIException(msg);
        }

        mLog.fine(mTranslator.getString(LocalStringKeys.JBI_INITIALIZED));
        mLog.finest("JBIFramework.init exited");

        // Add total time used by this method to startup elapsed time
        mStartupElapsedTime = System.currentTimeMillis() - startTime;
    }

    /**
     * Here all we do is initialize the JNDI naming context and the Management
     * Runtime Service. This is the minimal initialization required.
     * @param context contains the JBI InitialContext
     * @param namingprefix used from JNDI resolution.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void startup(InitialContext context, String namingprefix)
        throws javax.jbi.JBIException
    {
        // Get time this method was entered
        long startTime = System.currentTimeMillis();
        mLog.finest("JBIFramework.startup entered");

        mLog.fine(mTranslator.getString(LocalStringKeys.JBI_STARTING));

        mEnvironment.setNamingContext(context);
        mEnvironment.setNamingPrefix(namingprefix);

        // Initialize the Management Runtime Service. This creates and registers
        // the DAS EventNotifierMBean if this is the central admin server. After
        // that is done, we can call the instance EventNotifierMBean to contact
        // the DAS EventNotiferMBean.
        try
        {
            removeStoppedStateFile();
            mEnvironment.getMgmtRuntimeService().initService(mEnvironment);
            createStartedStateFile();
            ((EventNotifier) mEnvironment.getNotifier()).instanceStarting();
        }
        catch ( Throwable mrsEx )
        {
            mStartFailed = true;
            logFailure(mTranslator.getString(LocalStringKeys.MRS_NAME),
                mrsEx, LocalStringKeys.INIT_SERVICE_FAILED);
        }

        // If any service failed to start, prevent the framework from
        // starting.

        if ( mStartFailed )
        {
            String msg =
                mTranslator.getString(LocalStringKeys.START_FRAMEWORK_FAILED);
            mLog.severe(msg);
            throw new javax.jbi.JBIException(msg);
        }

        // Register the Logger MBean for the Framework
        mEnvironment.createLoggerMBeans();
        
        // Set up instance/cluster name properties as convenience for components

        PlatformContext platform = mEnvironment.getPlatformContext();
        String instanceName = platform.getInstanceName();
        System.setProperty("com.sun.jbi.instanceName", instanceName);
        mLog.log(Level.FINE, "set system property com.sun.jbi.instanceName to {0}", instanceName);
        Boolean clustered = platform.isInstanceClustered(instanceName);
        System.setProperty("com.sun.jbi.isClustered", clustered.toString());
        mLog.log(Level.FINE, "set system property com.sun.jbi.isClustered to {0}", clustered.toString());
        if ( clustered )
        {
            String clusterName = platform.getTargetName();
            System.setProperty("com.sun.jbi.clusterName", clusterName);
            mLog.log(Level.FINE, "set system property com.sun.jbi.clusterName to {0}", clusterName);
        }

        String readyMsg = mTranslator.getString(LocalStringKeys.JBI_READY);
        mLog.info(readyMsg);
        mEnvironment.getNotifier().emitRuntimeNotification(
                EventNotifier.EventType.Ready, readyMsg);
        mLog.finest("JBIFramework.startup exited");
        // Add total time used by this method to startup elapsed time
        mStartupElapsedTime += System.currentTimeMillis() - startTime;
        setStartupTime();
    }

    /**
     * This method prepares the JBI and component resources for recovery.
     * 
     * @throws javax.jbi.JBIException if any error occurs.
     */
     public void prepare()
        throws javax.jbi.JBIException
    {
        // Get time this method was entered
        long startTime = System.currentTimeMillis();
        mLog.finest("JBIFramework.prepare entered");

        // Start the Component Registry
        try
        {
            //
            //  Defer the cleaning until we know we are going to start.
            //
            com.sun.jbi.management.system.ManagementContext mgmtCtx = 
                mEnvironment.getManagementService().getManagementContext();

            mgmtCtx.getRepository().cleanRepository();
            
            mEnvironment.getComponentRegistry().startService();
            mCrStarted = true;
        }
        catch ( Throwable crEx )
        {
            mStartFailed = true;
            logFailure(mTranslator.getString(LocalStringKeys.CR_NAME),
                crEx, LocalStringKeys.START_SERVICE_FAILED);
            String msg =
                mTranslator.getString(LocalStringKeys.INIT_FRAMEWORK_FAILED);
            mLog.severe(msg);
            throw new javax.jbi.JBIException(msg);
        }

        // Inform the ComponentFramework that it can prepare components now.
        mEnvironment.getComponentFramework().prepare();

        mLog.finest("JBIFramework.prepare exited");

        // Add total time used by this method to startup elapsed time
        mStartupElapsedTime += System.currentTimeMillis() - startTime;     
    }
    

    /**
     * This method makes the JBI framework ready to process requests. 
     * This is where the remaining framework services are
     * started, as all AppServer services are now available.
     * @param wait when true, waits for startup to complete before returning.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void ready(boolean wait)
        throws javax.jbi.JBIException
    {
        // Get time this method was entered
        long startTime = System.currentTimeMillis();

        mLog.finest("JBIFramework.ready entered");

        if ( mStartFailed )
        {
            return;
        }

        // Start core services. If one fails, exit immediately.
        String serviceName = null;
        try
        {
            // Start the Configuration Service
            serviceName = mTranslator.getString(LocalStringKeys.CS_NAME);
            mEnvironment.getConfigurationService().startService();
            mCsStarted = true;
            
            // Start the Normalized Message Service
            serviceName = mTranslator.getString(LocalStringKeys.NMS_NAME);
            mEnvironment.getNormalizedMessageService().startService();
            mNmsStarted = true;

            // Start the Component Framework
            serviceName = mTranslator.getString(LocalStringKeys.CF_NAME);
            mEnvironment.getComponentFramework().startService();
            mCfStarted = true;

            // Start the Management Service
            serviceName = mTranslator.getString(LocalStringKeys.MS_NAME);
            mEnvironment.getManagementService().startService();
            mMsStarted = true;

            // Start the Management Runtime Service
            // DO NOT MOVE THIS - this must remain in the ready() processing
            // to ensure that the appserver domain config has completed first.
            serviceName = mTranslator.getString(LocalStringKeys.MRS_NAME);
            mEnvironment.getMgmtRuntimeService().startService();
            mMrsStarted = true;

        }
        catch ( Throwable startEx )
        {
            mStartFailed = true;
            logFailure(serviceName, startEx, LocalStringKeys.START_SERVICE_FAILED);
        }

        // If any service failed to start, prevent the framework from
        // starting.
        if ( mStartFailed )
        {
            String msg =
                mTranslator.getString(LocalStringKeys.START_FRAMEWORK_FAILED);
            mLog.severe(msg);
            throw new javax.jbi.JBIException(msg);
        }

        // Inform the ComponentFramework that it can startup components now.
        mEnvironment.getComponentFramework().ready(wait);

        //
        // Mark the framework as ready, and signal anyone waiting in
        // frameworkReady().
        //
        synchronized (this)
        {
            mFrameworkReady = true;
            this.notifyAll();
        }

        // Add total time used by this method to startup elapsed time and
        // log the total startup time. Also add the time used by the prepare()
        // method in case it ran. Update statistics for total startup time and
        // last restart time.

        mStartupElapsedTime += System.currentTimeMillis() - startTime;
        mLog.fine(mTranslator.getString(LocalStringKeys.JBI_STARTUP_TIME, getStartupTime()));
        setStartupTime();
        mEnvironment.getFrameworkStatistics().setLastRestartTime(new Date());

        String startedMsg = mTranslator.getString(LocalStringKeys.JBI_STARTED);
        mLog.info(startedMsg);
        mEnvironment.getNotifier().emitRuntimeNotification(
                EventNotifier.EventType.Started, startedMsg);
        mLog.finest("JBIFramework.ready exited");
    }

    /**
     * This method returns the startup time for the JBI Framework. It may be overridden
     * for specific AS types. Sun AS provider includes the cost of synchronization.
     * @return long - time for startup
     */
    public long getStartupTime()
    {
        return (mStartupElapsedTime);
    }
    
    public void setStartupTime()
    {
        mEnvironment.getFrameworkStatistics().setStartupTime(getStartupTime());        
    }
    
    /**
     * This method stops the JBI framework and all its services and
     * components.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void shutdown()
        throws javax.jbi.JBIException
    {
        // Get time this method was entered
        long startTime = System.currentTimeMillis();

        mLog.finest("JBIFramework.shutdown entered");
        mLog.fine(mTranslator.getString(LocalStringKeys.JBI_SHUTTING_DOWN));

        boolean errors = false;

        String shutMsg = mTranslator.getString(LocalStringKeys.JBI_SHUT_DOWN); 
        EventNotifier notifier = (EventNotifier) mEnvironment.getNotifier();
        notifier.emitRuntimeNotification(
            EventNotifier.EventType.Stopped, shutMsg);

        if ( mMrsStarted )
        {
            try
            {
                removeStartedStateFile();
                mEnvironment.getMgmtRuntimeService().stopService();
                mMrsStarted = false;
            }
            catch ( Throwable mrsEx )
            {
                errors = true;
                logFailure(mTranslator.getString(LocalStringKeys.MRS_NAME),
                    mrsEx, LocalStringKeys.STOP_SERVICE_FAILED);
            }
        }
        
        if ( mCfStarted )
        {
            try
            {
                mEnvironment.getComponentFramework().stopService();
                mCfStarted = false;
            }
            catch ( Throwable cfEx )
            {
                errors = true;
                logFailure(mTranslator.getString(LocalStringKeys.CF_NAME),
                    cfEx, LocalStringKeys.STOP_SERVICE_FAILED);
            }
        }
        if ( mCrStarted )
        {
            try
            {
                mEnvironment.getComponentRegistry().stopService();
                mCrStarted = false;
            }
            catch ( Throwable crEx )
            {
                errors = true;
                logFailure(mTranslator.getString(LocalStringKeys.CR_NAME),
                    crEx, LocalStringKeys.STOP_SERVICE_FAILED);
            }
        }
        if ( mNmsStarted )
        {
            try
            {
                mEnvironment.getNormalizedMessageService().stopService();
                mNmsStarted = false;
            }
            catch ( Throwable nmsEx )
            {
                errors = true;
                logFailure(mTranslator.getString(LocalStringKeys.NMS_NAME),
                    nmsEx, LocalStringKeys.STOP_SERVICE_FAILED);
            }
        }
        if ( mMsStarted )
        {
            try
            {
                mEnvironment.getManagementService().stopService();
                mMsStarted = false;
            }
            catch ( Throwable msEx )
            {
                errors = true;
                logFailure(mTranslator.getString(LocalStringKeys.MS_NAME),
                    msEx, LocalStringKeys.STOP_SERVICE_FAILED);
            }
        }
        if ( mCsStarted )
        {
            try
            {
                mEnvironment.getConfigurationService().stopService();
                mCsStarted = false;
            }
            catch ( Throwable msEx )
            {
                errors = true;
                logFailure(mTranslator.getString(LocalStringKeys.CS_NAME),
                    msEx, LocalStringKeys.STOP_SERVICE_FAILED);
            }
        }

        if ( !errors )
        {
            mLog.fine(shutMsg);
        }
        else
        {
            mLog.warning(mTranslator.getString(
                LocalStringKeys.STOP_FRAMEWORK_FAILED));
        }

        notifier.instanceStopping();
        createStoppedStateFile();
        mLog.finest("JBIFramework.shutdown exited");

        // Add total time used by this method to shutdown elapsed time
        mShutdownElapsedTime += System.currentTimeMillis() - startTime;
    }

    /**
     * This method terminates the JBI framework and releases all resources.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void terminate()
        throws javax.jbi.JBIException
    {
        // Get time this method was entered
        long startTime = System.currentTimeMillis();

        mLog.finest("JBIFramework.terminate entered");
        mLog.fine(mTranslator.getString(LocalStringKeys.JBI_TERMINATING));
 
        mEnvironment.destroyInstance();

        mLog.info(mTranslator.getString(LocalStringKeys.JBI_TERMINATED));
        mLog.finest("JBIFramework.terminate exited");

        // Add total time used by this method to shutdown elapsed time
        mShutdownElapsedTime += System.currentTimeMillis() - startTime;
        mLog.fine(mTranslator.getString(LocalStringKeys.JBI_SHUTDOWN_TIME, mShutdownElapsedTime));
    }

    /**
     * Get the global framework environment context.
     * @return The global framework environment context.
     */
    public com.sun.jbi.framework.EnvironmentContext getEnvironment() 
    {
        return mEnvironment;
    }    
    
    synchronized public void enterLazyMode()
    {   
        //
        //  Say that we are in lazy mode and signal any waiters.
        //
        mLog.fine("JBIFramework: Enter lazy mode...");
        mLazyInit = true;
        this.notifyAll();
    }
    
    /**
     * Initialize the Framework (if not already initialized.)
     *
     * This happens in a few situations. These situations arise because the Framework
     * is lazy initialized if it can be determined that JBI is effectively unused.
     * This typically happens in the DAS if no components will be started. It can also
     * happen in a server instance in the same situation. 
     *
     * In the DAS the framework will complete initialization in three different ways:
     *  1) An instance requests a registry download for synchronization purposes.
     *  2) A JBI client command is received and processed.
     *  3) A cluster change occurred and we need to perform some framework actions.
     *
     * In the server instance case the Framework will be started when a JBI client request
     * is forward by the DAS to this instance (similar to 2) above.)
     *
     */
    synchronized public void frameworkReady()
        throws RuntimeException
    {
        //
        //  See if the Framework is ready yet.
        //
        while (!mFrameworkReady)
        {
            //
            //  If we decided on lazy initialization, then complete initialization.
            //
            if (mLazyInit)
            {
                //
                // Framework has not entered the ready state yet, so let's try
                // to get there now.
                try
                {
                    mLog.fine("JBIFramework: Transition to ready...");
                    //
                    //  Make sure that we say the framework is ready, so that we don't attempt
                    //  to start the framework again when the registry is accessed.
                    //
                    prepare();
                    ready(true);
                }
                catch ( Throwable ex )
                {
                    mFrameworkReady = false;
                    mLog.log(Level.FINE, "Exception occurred making the " +
                        "runtime ready", ex);
                    throw new RuntimeException(ex.toString());
                }
                finally
                {                    
                    //
                    //  Signal any other waiters.
                    //
                    this.notifyAll();   
                }
            }
            else
            {
                try
                {
                    //
                    //  Must be waiting for normal mode startup to complete.
                    //
                    this.wait();
                }
                catch (InterruptedException ie)
                {
                    
                }
            }
        }
    }
       
    /** Indicates if the framework has entered the ready state.  This flag
     *  is used by on-demand initialization logic to determine if the 
     *  framework was initialized but not started.
     *  @return true if the framework is in the ready state, false otherwise.
     */
    synchronized boolean isFrameworkReady()
    {
        return mFrameworkReady;
    }

    /**
     * ONLY USED FOR FRAMEWORK JUNIT TESTS
     */
    synchronized void setFrameworkReady()
    {
        mFrameworkReady = true;
    }
   

    /**
     * Log a service warning during initialization, startup, or shutdown.
     * @param ex The exception causing the failure.
     * @param msgKey The message key for the failure message to be logged.
     */
    private void logWarning(Throwable ex, String msgKey)
    {
        String msg = ex.getMessage();
        msg = (null != msg) ? msg :
            mTranslator.getString(LocalStringKeys.NO_MESSAGE);
        mLog.log(Level.WARNING,
                 mTranslator.getString(msgKey, ex.getClass().getName(), msg),
                 ex);
    }
 
    /**
     * Log a service failure during initialization, startup, or shutdown.
     * @param service The name of the service which failed.
     * @param ex The exception causing the failure.
     * @param msgKey The message key for the failure message to be logged.
     */
    private void logFailure(String service, Throwable ex, String msgKey) 
    {
        String msg = ex.getMessage();
        msg = (null != msg) ? msg :
            mTranslator.getString(LocalStringKeys.NO_MESSAGE);
        mLog.log(Level.SEVERE, mTranslator.getString(msgKey, service,
            ex.getClass().getName(), msg), ex);
    }
   
    /**
     * Initialize the persistent Repository Init time, this time could
     * be NFS time which is different from the local machine time.
     */
     private void setRepositoryInitTime(
        com.sun.jbi.management.repository.Repository repository)
     {
        String tmpString = mEnvironment.getJbiInstanceRoot() +
                           File.separator +
                           com.sun.jbi.util.Constants.TMP_FOLDER;
        File tmpDir = new File(tmpString);
        try
        {
            if (!tmpDir.exists())
            {
                tmpDir.mkdirs();
            }

            /* A server instance root could be on a NFS directory, and the
             * timestamp of a NFS file/directory goes by clock of the machine
             * where the NFS is mounted, not the local machine's clock. In order
             * to take NFS factor into account, here we use the timestamp.ref
             * (a dummy file which is under a server instance root dir) creation
             * time as repository init time, instead of using server process
             * local time. This repository init time will be used to clean up
             * the repository later on.
             */
            File tsRefFile = new File(tmpString + File.separator + TIMESTAMP_REF);
            if (tsRefFile.exists())
            {
                tsRefFile.delete();
            }
            tsRefFile.createNewFile();
            repository.setRepositoryInitTime(tsRefFile.lastModified());
        }
        catch ( IOException ioEx )
        {
            /* If the creation of the temporary file on the NFS fails, just
             * revert to using the local system time as the repository init
             * time as a last resort. Log a warning that this has been done
             * as there could possibly be some repository issues.
             */
            repository.setRepositoryInitTime(System.currentTimeMillis());
            logWarning(ioEx, LocalStringKeys.JBI_REPOSITORY_TIMESTAMP_WARNING);
        }
     }

    /**
     * Initialize the persistent Registry that is used to populate the local
     * Component Registry.
     * @return the Registry instance that will provide the persistent registry.
     * @throws javax.jbi.JBIException if the registry fails to initialize.
     */
    com.sun.jbi.management.registry.Registry initRegistry()
        throws javax.jbi.JBIException
    {
        com.sun.jbi.management.registry.Registry registry = null;

        String regFolder = mEnvironment.getJbiInstanceRoot() +
            java.io.File.separator + "config";

        java.util.Properties props = new java.util.Properties();
        props.setProperty(
            com.sun.jbi.management.registry.Registry.REGISTRY_FOLDER_PROPERTY,
            regFolder );
 
        // Create an instance of management context
        com.sun.jbi.management.system.ManagementContext mgmtCtx = 
            new com.sun.jbi.management.system.ManagementContext(mEnvironment);
       
        registry =  com.sun.jbi.management.registry.RegistryBuilder.buildRegistry( 
            new com.sun.jbi.management.registry.RegistrySpecImpl(
                com.sun.jbi.management.registry.RegistryType.XML, props, mgmtCtx));
        return registry;
    }
    
    /** 
     * Initialize the file system repository containing all JBI installation
     * and deployment archives.
     * @throws javax.jbi.JBIException failed to initialize repository
     */
    private void initRepository()
        throws javax.jbi.JBIException
    {
        // Create an instance of management context
        com.sun.jbi.management.system.ManagementContext mgmtCtx = 
            new com.sun.jbi.management.system.ManagementContext(mEnvironment);
       
        com.sun.jbi.management.repository.Repository repository = null;
        try
        {
            // Initialize the repository and store a reference to it in the 
            // management context.
            repository = new com.sun.jbi.management.repository.Repository(mgmtCtx);
            setRepositoryInitTime(repository);
            mgmtCtx.setRepository(repository);
        }
        catch (com.sun.jbi.management.repository.RepositoryException repEx)
        {
            throw new javax.jbi.JBIException(repEx);
        }
    }    
    
    /**
     * Cleanup the .jbi_admin_running file and create the .jbi_admin_stopped 
     * file.
     */
    private void createStoppedStateFile()
    {
        
        // remove the .jbi_admin_running file
        String parentDir = mEnvironment.getJbiInstanceRoot() 
            + java.io.File.separator + com.sun.jbi.util.Constants.TMP_FOLDER;
                
        removeStartedStateFile();
        
        // create the .jbi_admin_stopped state file
        java.io.File adminStoppedFile = new java.io.File (parentDir, 
            com.sun.jbi.util.Constants.ADMIN_STOPPED_FILE);
        try
        {
            adminStoppedFile.createNewFile();
        }
        catch ( java.io.IOException ioex)
        {
            String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_STATE_FILE_CREATION_FAILED,
                    com.sun.jbi.util.Constants.ADMIN_STOPPED_FILE, 
                    ioex.getClass().getName(), ioex.getMessage());
            mLog.log(Level.FINE, errMsg, ioex);
        }
    }  
    
    /**
     * Remove the started state file if it exists
     */     
    private void removeStartedStateFile()
    {
                // remove the .jbi_admin_running file
        String parentDir = mEnvironment.getJbiInstanceRoot() 
            + java.io.File.separator + com.sun.jbi.util.Constants.TMP_FOLDER;
                
        java.io.File adminStartedFile = new java.io.File (parentDir, 
            com.sun.jbi.util.Constants.ADMIN_RUNNING_FILE);
        if ( adminStartedFile.exists() )
        {
            adminStartedFile.delete();
        }
    }
    /**
     * Cleanup the .jbi_admin_stopped file and create the .jbi_admin_running 
     * file.
     */
    private void createStartedStateFile()
    {
        
        // remove the .jbi_admin_stopped file
        String parentDir = mEnvironment.getJbiInstanceRoot() 
            + java.io.File.separator + com.sun.jbi.util.Constants.TMP_FOLDER;
        
        
        removeStoppedStateFile();
        
        // create the .jbi_admin_running state file
        java.io.File adminStartedFile = 
            new java.io.File (parentDir, 
                com.sun.jbi.util.Constants.ADMIN_RUNNING_FILE);
        try
        {
            adminStartedFile.createNewFile();
        }
        catch ( java.io.IOException ioex)
        {
            String errMsg = mTranslator.getString(
                    LocalStringKeys.JBI_STATE_FILE_CREATION_FAILED,
                    com.sun.jbi.util.Constants.ADMIN_RUNNING_FILE, 
                    ioex.getClass().getName(), ioex.getMessage());
            mLog.log(Level.FINE, errMsg, ioex);
        }
    }
    
    /**
     * Remove the stopped state file if it exists
     */
    private void removeStoppedStateFile()
    {
        // remove the .jbi_admin_stopped file
        String parentDir = mEnvironment.getJbiInstanceRoot() 
            + java.io.File.separator + com.sun.jbi.util.Constants.TMP_FOLDER;
        
        java.io.File adminStoppedFile = new java.io.File ( parentDir,
            com.sun.jbi.util.Constants.ADMIN_STOPPED_FILE);
        if ( adminStoppedFile.exists() )
        {
            adminStoppedFile.delete();
        }
    }
}
