/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Operation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

/**
 * This is an abstract class that provides a generalized way for the framework
 * to perform a set of operations on BCs and SEs in parallel on separate
 * threads. In conjunction with the OperationCounter class, it can wait for
 * all of the operations to complete, with an optional timeout.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class Operation implements Runnable
{
    /**
     * Arguments to be passed to the operation.
     */
    private Object mArguments[];

    /**
     * Completion flag set to true if the operation completed.
     */
    private boolean mCompleted;

    /**
     * The OperationCounter instance to use.
     */
    private OperationCounter mCounter;

    /**
     * An exception thrown by the operation, if any.
     */
    private Throwable mException;

    /**
     * A return value from the operation, if any.
     */
    private Object mReturnValue;

    /**
     * The thread on which this operation is running.
     */
    private Thread mThread;

    /**
     * Constructor.
     *
     * @param counter - the optional OperationCounter instance that should be
     * associated with this Operation. If this is not null, its increment()
     * method is called here.
     * @param arguments - the arguments to be passed to the process() method,
     * which is called to perform the operation.
     */
    Operation(OperationCounter counter, Object arguments[])
    {
        mArguments = arguments;
        mCounter = counter;
        mCompleted = false;
        if ( null != mCounter )
        {
            mCounter.increment();
        }
    }
 
    /**
     * Run the operation. Save the thread on which this operation is running in
     * case of a timeout, so that the parent thread can interrupt this one.
     * When the operation completes, save its return value and set the completed
     * flag. If an OperationCounter is present, decrement its counter. If the
     * operation throws an exception, save it. Upon return from this method,
     * this thread terminates.
     */
    public final void run()
    {
        mThread = Thread.currentThread();
        try
        {
            mReturnValue = process(mArguments);
        }
        catch ( Throwable ex )
        {
            mException = ex;
        }
        mCompleted = true;
        if ( null != mCounter )
        {
            mCounter.decrement();
        }
        return;
    }

    /**
     * Returns true if this operation completed.
     *
     * @return true if the operation has completed, false if it has not.
     */
    final boolean completed()
    {
        return mCompleted;
    }

    /**
     * Returns one argument that was provided to the constructor and passed
     * to the operation.
     *
     * @param index the index of the argument to be returned.
     * @return The argument that was provided to the operation.
     */
    final Object getArgument(int index)
    {
        return mArguments[index];
    }

    /**
     * Returns the arguments that were provided to the constructor and passed
     * to the operation.
     *
     * @return The arguments that were provided to the operation.
     */
    final Object[] getArguments()
    {
        return mArguments;
    }

    /**
     * Returns any exception thrown by the operation, or null if no exception
     * was thrown.
     *
     * @return The exception from the operation or null.
     */
    final Throwable getException()
    {
        return mException;
    }

    /**
     * Returns any return value from the operation, or null if there was none.
     *
     * @return The return value from the operation or null.
     */
    final Object getReturnValue()
    {
        return mReturnValue;
    }

    /**
     * Returns the thread on which this operation is running.
     *
     * @return The thread on which the operation is running.
     */
    final Thread getThread()
    {
        return mThread;
    }

    /**
     * Process the operation. This method must be overridden to perform the
     * desired processing.
     *
     * @param arguments the arguments to be provided to the operation.
     * @return the returned value from the operation as an object or null if
     * no value was returned.
     * @throws Throwable if any error occurs.
     */
    abstract Object process(Object arguments[]) throws Throwable;

    /**
     * Reset this instance for a new operation. This clears all results of
     * a previous operation.
     */
    final void reset()
    {
        mCompleted = false;
        mException = null;
        mReturnValue = null;
        mThread = null;
    }
}
