/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SharedLibrary.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;

import java.net.URLEncoder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * SharedLibrary holds the name and class path for a Shared Library.
 *
 * @author Sun Microsystems, Inc.
 */
public class SharedLibrary
    implements com.sun.jbi.ComponentInfo, java.io.Serializable
{
   /**
    * Indicator as to whether the shared class loader is to be "inverted".
    * Its value is true if the search order for the shared class loader is
    * to be inverted relative to the normal search order - self-first rather
    * than parent-first. This is persisted across restarts of the JBI framework.
    */
    private boolean mClassLoaderSelfFirst;

   /**
    * An array that contains all of the elements of the classpath for
    * this Shared Library.
    */
    private ArrayList mClassPathElements;

   /**
    * The description of this Shared Library.
    */
    private String mDescription;

   /**
    * The root directory into which this Shared Library is installed.
    */
    private String mInstallRoot;

   /**
    * The unique name of this Shared Library.
    */
    private String mName;
    
    /**
     * The jbi.xml String
     */
    private String mJbiXmlString;

   /**
    * The StringTranslator instance to use for messages.
    */
    private transient StringTranslator mTranslator;

   /**
    * Constructs a SharedLibrary object with the specified name, description,
    * installation root directory, and list of class path elements.
    * @param name The unique name of this Shared Library.
    * @param desc The description of this Shared Library.
    * @param root The installation root directory for this Shared Library.
    * @param elementList A list of class path elements comprising this
    * Shared Library. Each element is a string containing either a directory
    * path or a jar file path. The elements must be in the order in which
    * they should appear in the class path.
    * @throws java.lang.IllegalArgumentException if any arguments are invalid.
    */
    SharedLibrary(String name, String desc, String root, List elementList)
    {
        this(name, desc, root, false, elementList);
    }

   /**
    * Constructs a SharedLibrary object with the specified component name,
    * description, component root directory, and list of class path elements.
    * @param name The unique name of this Shared Library.
    * @param desc The description of this Shared Library.
    * @param root The component root directory for this Shared Library.
    * @param isSelfFirst A flag set to true to force the class loader for
    * this Shared Library to use a self-first hierarchy, or false to use the
    * normal parent-first hierarchy.
    * @param elementList A list of class path elements comprising this
    * Shared Library. Each element is a string containing either a directory
    * path or a jar file path. The elements must be in the order in which
    * they should appear in the class path.
    * @throws java.lang.IllegalArgumentException if any arguments are invalid.
    */
    SharedLibrary(String name, String desc, String root, boolean isSelfFirst,
        List elementList)
    {
        mTranslator = (StringTranslator)
            EnvironmentContext.getInstance().getStringTranslatorFor(this);

        if ( null == name )
        {
            throw new java.lang.IllegalArgumentException(
                mTranslator.getString(LocalStringKeys.NULL_ARGUMENT,
                "name"));
        }
        if ( null == elementList )
        {
            throw new java.lang.IllegalArgumentException(
                mTranslator.getString(LocalStringKeys.NULL_ARGUMENT,
                "elementList"));
        }
        if ( 0 == elementList.size() )
        {
            throw new java.lang.IllegalArgumentException(
                mTranslator.getString(LocalStringKeys.EMPTY_LIST_ARGUMENT,
                "elementList"));
        }
        mName = name;
        mDescription = desc;
        mInstallRoot = root;
        mClassLoaderSelfFirst = isSelfFirst;
        mClassPathElements = new ArrayList(elementList);
    }
    
    /**
     * Constructor for building an instance from a ComponentInfo
     * @param slInfo - SharedLibrary Info
     */
    SharedLibrary(com.sun.jbi.ComponentInfo slInfo)
    {
        mName                 = slInfo.getName();
        mDescription          = slInfo.getDescription();
        mInstallRoot          = slInfo.getInstallRoot();
        mClassLoaderSelfFirst = slInfo.isClassLoaderSelfFirst();
        mClassPathElements    = new ArrayList(slInfo.getClassPathElements());
    }

   /**
    * Compare another object with this one for equality.
    * @param object The object to be compared with this one.
    * @return True if the object is equal to this one, false
    * if they are not equal.
    */
    public boolean equals(Object object)
    {
        if ( null == object )
        {
            return false;
        }
        if ( !(object instanceof SharedLibrary) )
        {
            return false;
        }
        SharedLibrary component = (SharedLibrary) object;
        if ( !mName.equals(component.getName()) )
        {
            return false;
        }
        if ( ComponentType.SHARED_LIBRARY != component.getComponentType() )
        {
            return false;
        }
        if ( null != component.getComponentClassName() )
        {
            return false;
        }
        if ( null != component.getSharedLibraryNames() )
        {
            return false;
        }
        if ( !mClassPathElements.equals(
                 component.getClassPathElements()) )
        {
            return false;
        }
        return true;
    }

   /**
    * Get the complete classpath for this Shared Library.
    * @return The list of elements of the classpath for this Shared Library,
    * in the correct order, as a valid class path string.
    */
    public String getClassPathAsString()
    {
        String sep = System.getProperty("path.separator");
        Iterator i = mClassPathElements.iterator();
        StringBuffer sb = new StringBuffer("");
        boolean first = true;
        while ( i.hasNext() )
        {
            if ( !first )
            {
                sb.append(sep);
            }
            else
            {
                first = false;
            }
            sb.append((String) i.next());
        }
        return new String(sb);
    }

   /**
    * Get the list of elements that comprise the classpath for this
    * Shared Library.
    * @return The list of elements of the classpath for this Shared Library,
    * in the correct search order.
    */
    public List getClassPathElements()
    {
        return mClassPathElements;
    }

   /**
    * Get the component class name. For a Shared Library, this method always
    * returns null.
    * @return Always null.
    */
    public String getComponentClassName()
    {
        return null;
    }

   /**
    * Get the installation root directory for this Shared Library.
    * @return The installation root directory path.
    */
    public String getInstallRoot()
    {
        return mInstallRoot;
    }

   /**
    * Get the component type. This always returns SHARED_LIBRARY.
    * @return The component type, which is always SHARED_LIBRARY.
    */
    public ComponentType getComponentType()
    {
        return ComponentType.SHARED_LIBRARY;
    }

   /**
    * Get the component type as a string.
    * @return The component type as a string ("Shared Library").
    */
    public String getComponentTypeAsString()
    {
        return mTranslator.getString(LocalStringKeys.SHARED_LIBRARY);
    }

   /**
    * Get the description of this Shared Library.
    * @return The description of the Shared Library.
    */
    public String getDescription()
    {
        return mDescription;
    }

   /**
    * Get the unique name of this Shared Library.
    * @return The unique name of the Shared Library.
    */
    public String getName()
    {
        return mName;
    }

   /**
    * Get the list of deployed SUs. For a Shared Library, this method always
    * returns null.
    * @return Always null.
    */
    public List getServiceUnitList()
    {
        return null;
    }
 	 
   /**
    * Get the list of required Shared Library names. For a Shared Library,
    * this method call always returns null.
    * @return Always null.
    */
    public List getSharedLibraryNames()
    {
        return null;
    }

   /**
    * Get the component status. This always returns SHUTDOWN.
    * @return The component status, which is always SHUTDOWN.
    */
    public ComponentState getStatus()
    {
        return ComponentState.SHUTDOWN;
    }

   /**
    * Get the component status as a string. This always returns "Installed".
    * @return The component status string, which is always "Installed".
    */
    public String getStatusAsString()
    {
        return ComponentState.SHUTDOWN.toString();
    }
 	 
    /**
     * @return the Installation Descriptor [jbi.xml] for the
     * Shared Library.
     */
    public String getInstallationDescriptor()
    {
        return mJbiXmlString;
    }
    
   /**
    * Get the hash code for this Shared Library.
    * @return The hash code.
    */
    public int hashCode()
    {
        int hashCode = 0;

        hashCode += mName.hashCode();
        hashCode += ComponentType.SHARED_LIBRARY.hashCode();
        hashCode += mClassPathElements.hashCode();

        return hashCode;
    }

   /**
    * Check to see if the shared class loader should use the self-first
    * hierarchy.
    * @return true if the class loader should use the self-first hierarchy,
    * false if it should use parent-first.
    */
    public boolean isClassLoaderSelfFirst()
    {
        return mClassLoaderSelfFirst;
    }

   /**
    * Set the flag that determines the hierarchy for the shared class loader,
    * which is true for self-first or false for parent-first.
    * @param isSelfFirst is true for a self-first class loading hierarchy, or
    * false for a parent-first hierarchy.
    */
    public void setClassLoaderSelfFirst(boolean isSelfFirst)
    {
        mClassLoaderSelfFirst = isSelfFirst;
    }

    /**
     * @param jbiXml - the string representation of the installation descriptor
     * for this Shared Library
     */
    public void setInstallationDescriptor(String jbiXml)
    {
        mJbiXmlString = jbiXml;
    } 
    
   /**
    * Convert to a String.
    * @return The String representation of this SharedLibrary.
    */
    public String toString()
    {
        String sep = ",\n";
        StringBuffer b = new StringBuffer();
        b.append("Name = " + mName);
        b.append(sep);
        b.append("Description = " + mDescription);
        b.append(sep);
        b.append("InstallRoot = " + mInstallRoot);
        b.append(sep);
        b.append("ClassLoaderSelfFirst = " + mClassLoaderSelfFirst);
        b.append(sep);
        b.append("ClassPathElements = " + mClassPathElements);
        return b.toString();
    }

   // The following methods are part of the ComponentInfo interface but are
   // not applicable to a SharedLibrary. These methods all throw an
   // UnsupportedOperationException if they are called.

   /**
    * Get the class name of the bootstrap implementation.
    * @return nothing, always throws an exception.
    * @throws UnsupportedOperationException if called.
    */
    public String getBootstrapClassName()
    {
        throw new UnsupportedOperationException();
    }

   /**
    * Get the class path elements needed in the bootstrap runtime environment.
    * @return nothing, always throws an exception.
    * @throws UnsupportedOperationException if called.
    */
    public java.util.List getBootstrapClassPathElements()
    {
        throw new UnsupportedOperationException();
    }

   /**
    * Get the installation properties.
    * @return nothing, always throws exception.
    * @throws UnsupportedOperationException if called.
     */
    public java.util.Map getProperties()
    {
        throw new UnsupportedOperationException();
    }
    
   /**
    * Get the workspace root directory.
    * @return nothing, always throws exception.
    * @throws UnsupportedOperationException if called.
    */
    public String getWorkspaceRoot()
    {
        throw new UnsupportedOperationException();
    }

   /**
    * Check to see if the bootstrap class loader should use a self-first
    * search hierarchy.
    * @return nothing, always throws an exception.
    * @throws UnsupportedOperationException if called.
    */
    public boolean isBootstrapClassLoaderSelfFirst()
    {
        throw new UnsupportedOperationException();
    }
    
}
