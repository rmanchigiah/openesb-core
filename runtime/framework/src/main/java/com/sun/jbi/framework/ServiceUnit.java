/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceUnit.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.Iterator;
import com.sun.jbi.ServiceUnitState;

/**
 * This class holds information about Service Unit deployed to a Binding
 * Component (BC) or Service Engine (SE). This information is used by the JBI
 * framework to manage Service Unit life cycles. The information is persisted
 * by the Component Registry across restarts of the JBI framework.
 *
 * @author Sun Microsystems, Inc.
 */
public class ServiceUnit implements com.sun.jbi.ServiceUnitInfo
{
    /**
     * The unique name for this Service Unit.
     */
    private String mName;
 
    /**
     * The full directory path to the deployment information for this Service
     * Unit.
     */
    private String mFilePath;
 
    /**
     * The unique name of the containing Service Assembly.
     */
    private String mSaName;
 
    /**
     * The current state of this Service Unit. Valid states are:
     * <UL>
     * <LI><CODE>SHUTDOWN</CODE> - not initialized; initialization must be
     *                             done before starting</LI>
     * <LI><CODE>STOPPED</CODE> - initialized, but not yet started</LI>
     * <LI><CODE>STARTED</CODE> - available to accept requests</LI>
     * </UL>
     * State changes allowed are:
     * <UL>
     * <LI><CODE>SHUTDOWN</CODE> --&GT <CODE>STOPPED</CODE></LI>
     * <LI><CODE>STOPPED</CODE> --&GT <CODE>STARTED</CODE></LI>
     * <LI><CODE>STARTED</CODE> --&GT <CODE>STOPPED</CODE></LI>
     * <LI><CODE>STOPPED</CODE> --&GT <CODE>SHUTDOWN</CODE></LI>
     * </UL>
     */
    private ServiceUnitState mState;
 
    /**
     * The desired state of this Service Unit. This is persisted across restarts
     * of the JBI framework. This state is set whenever a state change is
     * requested, and is used to determine whether the last known state at
     * shutdown is actually the desired state.
     */
    private ServiceUnitState mDesiredState;

    /**
     * The StringTranslator to be used for constructing messages.
     */
    private transient StringTranslator mTranslator;
    
    /**
     * The target component, i.e. the component to which this Service Unit is
     * deployed.
     */
    private String mTargetComponent;

    /**
     * Create a new ServiceUnit instance.
     * @param saName is the unique name for the Service Assembly that contains
     * this Service Unit.
     * @param name is the unique name for this Service Unit.
     * @param filePath is the full path to the file defining this Service Unit.
     */
    public ServiceUnit(String saName, String name, String filePath)
    {
        mTranslator = (StringTranslator)
            EnvironmentContext.getInstance().getStringTranslatorFor(this);
        mSaName = saName;
        mName = name;
        mFilePath = filePath;
        mState = ServiceUnitState.SHUTDOWN;
        mDesiredState = ServiceUnitState.SHUTDOWN;
    }
    
    /**
     * Create a new ServiceUnit instance.
     * @param saName is the unique name for the Service Assembly that contains
     * this Service Unit.
     * @param name is the unique name for this Service Unit.
     * @param filePath is the full path to the file defining this Service Unit.
     * @param targetComp is the name of the component to which this Service Unit
     * is deployed.
     */
    public ServiceUnit(String saName, String name, String filePath, String targetComp)
    {
        mTranslator = (StringTranslator)
            EnvironmentContext.getInstance().getStringTranslatorFor(this);
        mSaName = saName;
        mName = name;
        mFilePath = filePath;
        mState = ServiceUnitState.SHUTDOWN;
        mDesiredState = ServiceUnitState.SHUTDOWN;
        mTargetComponent = targetComp;
    }
    
    /**
     * Create a new ServiceUnit instance from a ServiceUnitInfo instance.
     * @param suInfo is the instance to use to create the new instance.
     */
    public ServiceUnit(com.sun.jbi.ServiceUnitInfo suInfo)
    {
        mTranslator      = (StringTranslator)
            EnvironmentContext.getInstance().getStringTranslatorFor(this);
        mSaName          = suInfo.getServiceAssemblyName();
        mName            = suInfo.getName();
        mFilePath        = suInfo.getFilePath();
        mState           = ServiceUnitState.SHUTDOWN;
        mDesiredState    = suInfo.getState();
        mTargetComponent = suInfo.getTargetComponent();
    }

    /**
     * Compare another Object with this one for equality. Note that the
     * state fields are not checked, only the fields that are not mutable.
     * @param object The object to be compared with this one.
     * @return True if the object is equal to this Component, false
     * if they are not equal.
     */
    public boolean equals(Object object)
    {
        if ( null == object )
        {
            return false;
        }
        if ( !(object instanceof ServiceUnit) )
        {
            return false;
        }
        ServiceUnit su = (ServiceUnit) object;
        if ( !mSaName.equals(su.getServiceAssemblyName()) )
        {
            return false;
        }
        if ( !mName.equals(su.getName()) )
        {
            return false;
        }
        if ( !mFilePath.equals(su.getFilePath()) )
        {
            return false;
        }
        return true;
    }

    /**
     * Get the desired state for this Service Unit.
     * @return the desired state.
     */
    public ServiceUnitState getDesiredState()
    {
        return mDesiredState;
    }

    /**
     * Get the file path for this Service Unit.
     * @return the file path.
     */
    public String getFilePath()
    {
        return mFilePath;
    }

    /**
     * Get the unique name for this Service Unit.
     * @return the Service Unit name.
     */
    public String getName()
    {
        return mName;
    }

    /**
     * Get the unique name for the Service Assembly that contains this Service
     * Unit.
     * @return the Service Assembly name.
     */
    public String getServiceAssemblyName()
    {
        return mSaName;
    }

    /**
     * Get the state of this Service Unit.
     * @return the state, either SHUTDOWN, STARTED, or STOPPED.
     */
    public ServiceUnitState getState()
    {
        return mState;
    }

    /**
     * Get the state of this Service Unit as a string value.
     * @return the state as a string, either "shutdown", "started", or "stopped".
     */
    public String getStateAsString()
    {
        return mState.toString();
    }

    /**
     * Get a Service Unit state as a localized string value.
     * @param state a state, either SHUTDOWN, STARTED, or STOPPED.
     * @return the state as a string, either "Shutdown", "Started", or "Stopped"
     * in the current locale.
     */
    public String getStateAsString(ServiceUnitState state)
    {
        return state.toString();
    }

    /**
     * Get the target component for this Service Unit. This is the name of the
     * component to which the Service Unit is deployed.
     *
     * @return the target component name.
     */
    public String getTargetComponent()
    {
        return mTargetComponent;
    }
    
    /**
     * Get the hash code for this Service Unit instance.
     * @return the hash code.
     */
    public int hashCode()
    {
        int hashCode = 0;

        hashCode += mName.hashCode();
        hashCode += mSaName.hashCode();
        hashCode += mFilePath.hashCode();

        return hashCode;
    }

    /**
     * Check to see if this Service Unit is shut down.
     * @return true if the Service Unit is shut down, false if not.
     */
    public boolean isShutdown()
    {
        return (ServiceUnitState.SHUTDOWN == mState);
    }

    /**
     * Check to see if this Service Unit is started.
     * @return true if the Service Unit is started, false if not.
     */
    public boolean isStarted()
    {
        return (ServiceUnitState.STARTED == mState);
    }

    /**
     * Check to see if this Service Unit is stopped.
     * @return true if the Service Unit is stopped, false if not.
     */
    public boolean isStopped()
    {
        return (ServiceUnitState.STOPPED == mState);
    }

    /**
     * Set the desired Service Unit state.
     * @param state the state to be set.
     * Valid states are SHUTDOWN, STOPPED, and STARTED.
     * @throws java.lang.IllegalArgumentException if the state is not valid.
     */
    void setDesiredState(ServiceUnitState state)
    {
        if ( ServiceUnitState.SHUTDOWN != state && 
             ServiceUnitState.STOPPED  != state && 
             ServiceUnitState.STARTED  != state )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.INVALID_ARGUMENT, "state", state));
        }
        mDesiredState = state;
    }

    /**
     * Set the Service Unit state. This method is used ONLY by startup and unit
     * test code. It does no validation of the state.
     * @param state the state to be set.
     * Valid states are SHUTDOWN, STOPPED, and STARTED.
     */
    void setState(ServiceUnitState state)
    {
        mState = state;
    }

    /**
     * Set the Service Unit state to shutdown.
     */
    public void setShutdown()
    {
        // If the Service Unit is started it cannot be set to shutdown, only
        // to stopped.

        if ( isStarted() )
        {
            throw new java.lang.IllegalStateException(mTranslator.getString(
                LocalStringKeys.SU_INVALID_STATE_CHANGE,
                mTranslator.getString(LocalStringKeys.SU_STATE_STARTED),
                mTranslator.getString(LocalStringKeys.SU_STATE_SHUTDOWN)));
        }
        mState = ServiceUnitState.SHUTDOWN;
    }

    /**
     * Set the Service Unit state to started.
     */
    public void setStarted()
    {
        if ( isShutdown() )
        {
            throw new java.lang.IllegalStateException(mTranslator.getString(
                LocalStringKeys.SU_INVALID_STATE_CHANGE,
                mTranslator.getString(LocalStringKeys.SU_STATE_SHUTDOWN),
                mTranslator.getString(LocalStringKeys.SU_STATE_STARTED)));
        }
        mState = ServiceUnitState.STARTED;
    }

    /**
     * Set the Service Unit state to stopped.
     */
    public void setStopped()
    {
        mState = ServiceUnitState.STOPPED;
    }

    /**
     * Set the target component for this Service Unit. This is the name of the
     * component to which the Service Unit is deployed.
     *
     * @param componentName the target component name.
     */
    public void setTargetComponent(String componentName)
    {
        mTargetComponent = componentName;
    }

    /**
     * Convert to a String.
     * @return the String representation of this ServiceUnit.
     */
    public String toString()
    {
        String sep = new String(",\n");
        StringBuffer b = new StringBuffer();
        b.append("Name = " + mName);
        b.append(sep);
        b.append("FilePath = " + mFilePath);
        b.append(sep);
        b.append("State = " + mState.toString());
        b.append(sep);
        b.append("Desired state = " + mDesiredState.toString());
        b.append(sep);
        b.append("Service Assembly name = " + mSaName);
        b.append("\n");

        return b.toString();
    }
}
