/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestOperation.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import java.util.Date;

/**
 * Tests for the Operation class.
 *
 * @author Sun Microsystems, Inc.
 */
public class TestOperation
    extends junit.framework.TestCase
{
    /**
     * OperationCounter.
     */
    private OperationCounter mCounter;

    /**
     * MyOperation (extends Operation).
     */
    private MyOperation mOperation;

    /**
     * Arguments to operation.
     */
    private Object mArguments[];

    /**
     * Constant for 1 second interval in milliseconds.
     */
    private static final long ONE_SECOND = 1000;

    /**
     * Constant for 5 second interval in milliseconds.
     */
    private static final long FIVE_SECONDS = 5000;

    /**
     * Constant for integer 3.
     */
    private static final int THREE = 3;

    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestOperation(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Setup for the test. This creates the OperationCounter instance.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
        mArguments = new Object[THREE];
        mArguments[0] = "MyOperation";
        mArguments[1] = "FirstParameter";
        mArguments[2] = "LastParameter";
        mCounter = new OperationCounter();
        mOperation = new MyOperation(mCounter, mArguments);
    }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * Test the run() method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testRun()
        throws Exception
    {
        Date start;
        Date stop;
        new Thread(mOperation, "testRun").start();
        start = new Date();
        synchronized (mCounter)
        {
            if ( 0 < mCounter.getValue() )
            {
                mCounter.wait(FIVE_SECONDS);
            }
        }
        stop = new Date();
        long interval = stop.getTime() - start.getTime();
        assertTrue("Run() failed, wait() timed out",
                   FIVE_SECONDS > interval);
    }

    /**
     * Test the completed() method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testCompleted()
        throws Exception
    {
        assertFalse("completed() returned wrong value",
                    mOperation.completed());
        synchronized (mCounter)
        {
            new Thread(mOperation, "testCompleted").start();
            if ( 0 < mCounter.getValue() )
            {
                mCounter.wait(ONE_SECOND);
            }
        }
        assertTrue("completed() returned wrong value",
                   mOperation.completed());
    }

    /**
     * Test the getArgument() method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetArgument()
        throws Exception
    {
        assertSame("getArgument(0) returned wrong object",
                   mArguments[0], mOperation.getArgument(0));
        assertSame("getArgument(1) returned wrong object",
                   mArguments[1], mOperation.getArgument(1));
        assertSame("getArgument(2) returned wrong object",
                   mArguments[2], mOperation.getArgument(2));
    }

    /**
     * Test the getArguments() method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetArguments()
        throws Exception
    {
        assertSame("getArguments() returned wrong array",
                   mArguments, mOperation.getArguments());
    }

    /**
     * Test the getException() method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetException()
        throws Exception
    {
        mArguments[2] = new Exception("TestException");
        assertNull("getException() returned wrong value",
                   mOperation.getException());
        synchronized (mCounter)
        {
            new Thread(mOperation, "testGetException").start();
            if ( 0 < mCounter.getValue() )
            {
                mCounter.wait(ONE_SECOND);
            }
        }
        assertEquals("getException() returned wrong value",
                    mArguments[2], mOperation.getException());
    }

    /**
     * Test the getReturnValue() method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetReturnValue()
        throws Exception
    {
        assertNull("getReturnValue() returned wrong value",
                    mOperation.getReturnValue());
        synchronized (mCounter)
        {
            new Thread(mOperation, "testGetReturnValue").start();
            if ( 0 < mCounter.getValue() )
            {
                mCounter.wait(ONE_SECOND);
            }
        }
        assertSame("getReturnValue() returned wrong value",
                   mArguments[0], mOperation.getReturnValue());
    }

    /**
     * Test the getThread() method.
     * @throws Exception if an unexpected error occurs.
     */
    public void testGetThread()
        throws Exception
    {
        assertNull("getThread() returned non-null value when null expected",
                   mOperation.getThread());
        Thread t = new Thread(mOperation, "testGetThread");
        synchronized (mCounter)
        {
            t.start();
            if ( 0 < mCounter.getValue() )
            {
                mCounter.wait(ONE_SECOND);
            }
        }
        assertSame("getThread() returned wrong value: ",
                   t, mOperation.getThread());   
    }

    /**
     * Class to represent an Operation.
     */
    class MyOperation extends Operation
    {
        /**
         * Constructor.
         *
         * @param counter the OperationCounter instance associated with this
         * operation.
         * @param args an array of arguments containing one String argument.
         */
        MyOperation(OperationCounter counter, Object args[])
        {
            super(counter, args);
        }

        /**
         * Separate thread to do some work.
         * @param argumentList an array of arguments for the process.
         * @return the first argument from the argumentList.
         * @throws Throwable if any error occurs.
         */
        public Object process(Object argumentList[]) throws Throwable
        {
            if ( argumentList[2] instanceof Exception )
            {
                System.out.println(argumentList[0] + " on thread " +
                                   Thread.currentThread().getName() +
                                   " threw Exception at " + new Date());
                throw (Exception) argumentList[2];
            }
            else
            {
                System.out.println(argumentList[0] + " on thread " +
                                   Thread.currentThread().getName() +
                                   " completed at " + new Date());
            }
            return argumentList[0];
        }
    }

}
