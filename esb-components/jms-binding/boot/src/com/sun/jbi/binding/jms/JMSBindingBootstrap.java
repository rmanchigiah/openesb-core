/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSBindingBootstrap.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import com.sun.jbi.common.ConfigFileValidator;

import java.io.File;

import java.util.logging.Logger;


import javax.jbi.component.Bootstrap;
import javax.jbi.component.InstallationContext;


import org.w3c.dom.Document;

/**
 * The bootstrap class is invoked by the JBI run time during installation of
 * the binding. The JMS binding reads the config file information and checks
 * for the validity of the information in the config file.  It then loads this
 * config information and allows the installation to  complete. During
 * uninstallation any resource held up by the binding is freed.
 *
 * @author Sun Microsystems Inc.
 */
public class JMSBindingBootstrap
    implements Bootstrap
{
    /**
     * Is the context containing information from the install command and from
     * the BC jar file.
     */
    private InstallationContext mContext;

    /**
     * Internal handle to the logger instance.
     */
    private Logger mLogger;

    /**
     * Creates a new instance of JMSBindingBootstrap.
     */
    public JMSBindingBootstrap()
    {
        mLogger = Logger.getLogger(this.getClass().getPackage().getName());
    }

    /**
     * Obtains the optional installer configuration MBean ObjectName. If none
     * is provided by this BC, returns null.
     *
     * @return ObjectName which represents the MBean registered by the init()
     *         method. If none was registered, returns null.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Cleans up any resources allocated by the bootstrap implementation,
     * including deregistration of the extension MBean, if applicable. This
     * method will be called after the onInstall() or onUninstall() method is
     * called, whether it succeeds or fails.
     *
     * @throws javax.jbi.JBIException when cleanup processing fails to complete
     *         successfully.
     */
    public void cleanUp()
        throws javax.jbi.JBIException
    {
    }

    /**
     * Initializes the installation environment for a BC. This method is
     * expected to save any information from the installation context that may
     * be needed by other methods.
     *
     * @param installContext is the context containing information from the
     *        install command and from the BC jar file.
     *
     * @throws javax.jbi.JBIException when there is an error requiring that the
     *         installation be terminated.
     */
    public void init(InstallationContext installContext)
        throws javax.jbi.JBIException
    {
        mContext = installContext;

        String configFile = null;
        String schemaFile = null;

        // Parse config file to ensure conformness to schema
        configFile =
            mContext.getInstallRoot() + File.separatorChar + "jms_config.xml";
        schemaFile =
            mContext.getInstallRoot() + File.separatorChar + "schema"
            + File.separatorChar + "jms_config.xsd";

        ConfigFileValidator validator =
            new ConfigFileValidator(schemaFile, configFile);
        validator.setValidating();
        validator.validate();

        Document d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLogger.severe("Invalid config file " + configFile);
            mLogger.severe("Error " + validator.getError());
            throw new javax.jbi.JBIException(validator.getError(), null);
        }
    }

    /**
     * Called at the beginning of installation of JMS Binding . For this jms
     * binding, all the required installation tasks have been taken care by
     * the InstallationService.
     *
     * @throws javax.jbi.JBIException when there is an error requiring that the
     *         installation be terminated.
     */
    public void onInstall()
        throws javax.jbi.JBIException
    {
        mLogger.info("**JMS Binding installation begins**");
    }

    /**
     * Called at the beginning of uninstallation of JMSBinding . For this jms
     * binding, all the required uninstallation tasks have been taken care of
     * by the InstallationService.
     *
     * @throws javax.jbi.JBIException when there is an error requiring that the
     *         uninstallation be terminated.
     */
    public void onUninstall()
        throws javax.jbi.JBIException
    {
        mLogger.info("**JMS Binding installation ends**");
    }
}
