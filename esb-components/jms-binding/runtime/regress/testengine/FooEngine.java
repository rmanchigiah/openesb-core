/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FooEngine.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package testengine;

import org.w3c.dom.Document;

import java.io.File;

import java.util.logging.Logger;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;

import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.messaging.DeliveryChannel;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

import org.w3c.dom.DocumentFragment;

/**
 * This class implements EngineLifeCycle. The JBI framework will start this
 * engine class automatically when JBI framework starts up.
 *
 * @author Sun Microsystems, Inc.
 */
public class FooEngine implements ComponentLifeCycle, Component
{
    /**
     * In Only MEP.
     */
    public static final String IN_ONLY =
        "http://www.w3.org/ns/wsdl/in-only";

    /**
     * In Out MEP.
     */
    public static final String IN_OUT = "http://www.w3.org/ns/wsdl/in-out";

    /**
     * In Optional Out MEP.
     */
    public static final String IN_OPTIONAL_OUT =
        "http://www.w3.org/ns/wsdl/in-opt-out";

    /**
     * Robust In Only MEP.
     */
    public static final String ROBUST_IN_ONLY =
        "http://www.w3.org/ns/wsdl/robust-in-only";

    /**
     *    
     */
    private static final String SERVICE_NAMESPACE =
        "http://sun.com/testengine.wsdl";
    
    private static final String INTERFACE_NAME = "transformIF";

    /**
     *    
     */
    private static final String INBOUND_SERVICE_NAME = "testengineInboundService";

    /**
     *    
     */
    private static final String OUTBOUND_SERVICE_NAME = "testEngineOutboundService";

    /**
     *    
     */
    private static final String ENDPOINT = "testendpoint";

    /**
     *    
     */
    public Thread mRecv;

    /**
     *    
     */
    private ServiceEndpoint mEndpoint;

    /**
     *
     */

    /**
     *    
     */
    private DeliveryChannel mChannel;
    
    /**
     *    
     */
    private MessageExchangeFactory mFactory;

    /**
     * Environment conext passed down from framework to this stockquote engine.
     */
    private ComponentContext mContext = null;

    /**
     * Refernce to logger.
     */
    private Logger mLog = null;

    /**
     *    
     */
    private MessageExchange mReq;

    /**
     *    
     */
    private ServiceEndpoint mInboundReference;

    /**
     *    
     */
    private ServiceEndpoint mOutboundReference;

    /**
     *    
     */
    private boolean mRunFlag = true;


    /**
     * Get the JMX ObjectName for any additional MBean for this BC. If there is
     * none, return null.
     *
     * @return ObjectName the JMX object name of the additional MBean or null
     *         if there is no additional MBean.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Initialize the test engine. This performs initialization required
     *
     * @param jbiContext the JBI environment mContext
     *
     * @throws javax.jbi.JBIException if the stockquote engine is unable to
     *         initialize.
     */
    public void init(javax.jbi.component.ComponentContext jbiContext)
        throws javax.jbi.JBIException
    {
        mLog = Logger.getLogger(this.getClass().getPackage().getName());
        mLog.info(" JMS Test Engine : test engine INIT invoked");

        if (jbiContext == null)
        {
            throw (new javax.jbi.JBIException("Context is null",
                new NullPointerException()));
        }

        /* Assign values */
        mContext = jbiContext;
        Payload.mDefaultPath =
            mContext.getInstallRoot() + File.separatorChar + "testengine"
            + File.separatorChar + "payload.xml";
        mLog.info(" JMS Test Engine :  INIT METHOD FINISHED ");
    }

    /**
     * Shutdown the SE. This performs cleanup before the SE is terminated.
     * Once this has been called, init() must be called before the SE can be
     * started again with a call to start().
     *
     * @throws javax.jbi.JBIException if the SE is unable to shut down.
     */
    public void shutDown() throws javax.jbi.JBIException
    {
        mLog.info(" JMS Test Engine : test Engine for JMS bidning Shutdown-Start");
        mLog.info(" JMS Test Engine : test Engine for JMS binding Shutdown-End");
    }

    /**
     */
    public void start() throws javax.jbi.JBIException
    {
        mLog.info(" JMS Test Engine :  Test  ENGINE for JMS binding Started ");

        try
        {
            mChannel = mContext.getDeliveryChannel();
            mFactory = mChannel.createExchangeFactory();

            QName outbound =
                new QName(SERVICE_NAMESPACE, OUTBOUND_SERVICE_NAME);
            //mOutboundReference = mChannel.activateEndpoint(outbound, ENDPOINT);

            QName inbound = new QName(SERVICE_NAMESPACE, INBOUND_SERVICE_NAME);
            mInboundReference = mContext.activateEndpoint(inbound, ENDPOINT);
            mLog.info(" JMS Test Engine : Activated the inbound service " + inbound.toString());            
        }
        catch (MessagingException me)
        {
            me.printStackTrace();
        }

        if (mChannel == null)
        {
            mLog.severe(" JMS Test Engine : Cannot get Engine Channel from context ");

            return;
        }

        doTestGoodOutOnly();

        doTestGoodOutIn();
        
        doTestBadOutIn();
        
       // doTestGoodRobustOutOnly();

       // doTestBadRobustOutOnly();

        mRecv = new Thread(new Receiver());
        mRecv.start();

        mLog.info(" JMS Test Engine :  START FINISHED  ");
    }

    /**
     */
    public void stop() throws javax.jbi.JBIException
    {
        mLog.info(" JMS Test Engine : STOP INVOKED ");
        mRunFlag = false;

        try
        {
            mContext.deactivateEndpoint(mInboundReference);
            mRecv.interrupt();
            mRecv.join();
        }
        catch (Exception e)
        {
            ;
        }

        mLog.info(" JMS Test Engine :  STOP FINISHED ");
    }
   
    
    private void doTestGoodOutIn()
    {
        NormalizedMessage outMsg;
        Exception error = null;
        InOut inout;
        Fault flt = null;

        try
        {
            // create the exchange
            inout = mFactory.createInOutExchange();
            outMsg = inout.createMessage();

            // set the stuff we know
            inout.setService(new QName(SERVICE_NAMESPACE, OUTBOUND_SERVICE_NAME));
            inout.setOperation(new QName(SERVICE_NAMESPACE, "getMessageOutIn"));

            inout.setInterfaceName(new QName(SERVICE_NAMESPACE , INTERFACE_NAME));
            // lookup the endpoint reference and set on exchange
            ServiceEndpoint [] eprefs = null;
            eprefs = mContext.getEndpointsForService(new QName(SERVICE_NAMESPACE, 
                OUTBOUND_SERVICE_NAME));
            if (eprefs == null)
            {
                mLog.severe("No end points for service");             
            mLog.severe(" JMS Test Engine : JMSTest-GOODOUTIN-Failed");
                return;
            }
            inout.setEndpoint(eprefs[0]);

            // set the payload
            Payload.setPayload(outMsg);
            inout.setInMessage(outMsg);

            // Now we can proceed normally            
            mChannel.send(inout);

            // receive the response
            inout = (InOut) mChannel.accept(2000);
            if (inout == null)
            {             
                mLog.info(" JMS Test Engine : JMSTest-GOODOUTIN-Failed");   
                return;
            }
            // check for a fault or error
            error = inout.getError();
            flt = inout.getFault();
            if ((error != null) || (flt != null))
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODOUTIN-Failed");
            }
            if (inout.getStatus() == ExchangeStatus.ACTIVE)
            {
                inout.setStatus(ExchangeStatus.DONE);
                mChannel.send(inout);            
                mLog.severe(" JMS Test Engine : JMSTest-GOODOUTIN-Passed");
            }
        }
        catch (Exception e)
        {            
            mLog.severe(" JMS Test Engine : JMSTest-GOODOUTIN-Failed");
            e.printStackTrace();
        }
    }
    
    
    private void doTestBadOutIn()
    {
        NormalizedMessage outMsg;
        Exception error = null;
        InOut inout;
        Fault flt = null;

        try
        {
            // create the exchange
            inout = mFactory.createInOutExchange();
            outMsg = inout.createMessage();

            // set the stuff we know
            inout.setService(new QName(SERVICE_NAMESPACE , OUTBOUND_SERVICE_NAME));
            inout.setOperation(new QName(SERVICE_NAMESPACE, "getMessageOutIn"));
            
            inout.setInterfaceName(new QName(SERVICE_NAMESPACE , INTERFACE_NAME));
            // lookup the endpoint reference and set on exchange
            ServiceEndpoint [] eprefs = null;
            eprefs = mContext.getEndpointsForService(new QName(SERVICE_NAMESPACE, OUTBOUND_SERVICE_NAME));
            if (eprefs == null)
            {
                mLog.severe("No end points for service");                      
                mLog.info(" JMS Test Engine : JMSTest-BADOUTIN-Failed");
                return;
            }
            inout.setEndpoint(eprefs[0]);

            // set the payload
            Payload.setBadPayload(outMsg);
            inout.setInMessage(outMsg);

            // Now we can proceed normally            
            mChannel.send(inout);

            // receive the response
            inout = (InOut) mChannel.accept(3000);
            
            if (inout == null)
            {             
                mLog.info(" JMS Test Engine : JMSTest-BADOUTIN-Failed because timed out");   
                return;
            }

            // check for a fault or error
            error = inout.getError();
            flt = inout.getFault();

            if ((error != null) || (flt != null))
            {
                mLog.info(" JMS Test Engine : JMSTest-BADOUTIN-Passed");                
            }
            else
            {
                mLog.info(" JMS Test Engine : JMSTest-BADOUTIN-Failed because " +
                "there is no error or fault");
                mLog.info("Messag params " + 
                        inout.getEndpoint().getServiceName());
            }
            
            if (inout.getStatus() == ExchangeStatus.ACTIVE)
            {
                inout.setStatus(ExchangeStatus.DONE);
                mChannel.send(inout);
            }
            
        }
        catch (Exception e)
        {
            mLog.info(" JMS Test Engine : JMSTest-BADOUTIN-Failed because of exception");
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestInOnly()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            InOnly inonly = (InOnly) mReq;

            if (inonly.getInMessage() == null)
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODINONLY-Failed");
                return;
            }

            if (inonly.getInMessage().getContent() == null)
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODINONLY-Failed");
                return;
            }

            inonly.setStatus(ExchangeStatus.DONE);
            mChannel.send(inonly);
            mLog.info(" JMS Test Engine : JMSTest-GOODINONLY-Passed");
        }
        catch (Exception e)
        {
            mLog.severe(" JMS Test Engine : JMSTest-GOODINONLY-threw-Exception" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestInOut()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            InOut inout = (InOut) mReq;
            NormalizedMessage outmsg = inout.createMessage();

            if (inout.getStatus() == ExchangeStatus.DONE)
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODINOUT-Passed");

                return;
            }

            if (inout.getStatus() == ExchangeStatus.ERROR)
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODINOUT-Failed");

                return;
            }
            
            if (inout.getFault() != null)
            {             
                mLog.info(" JMS Test Engine : JMSTest-GOODINOUT-Failed");

                return;   
            }

            if (inout.getInMessage() == null)
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODINOUT-Failed");

                return;
            }
            Payload.setResponse(outmsg);
            inout.setOutMessage(outmsg);
            mChannel.send(inout);
        }
        catch (Exception e)
        {
            mLog.severe(" JMS Test Engine : JMSTest-GOODINOUT-threw-Exception" + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestGoodOutOnly()
    {
        mLog.info(" JMS Test Engine : JMS Test engine sending good out-only message to binding");

        NormalizedMessage outMsg;
        InOnly inOnly;

        try
        {
            // create the exchange
            inOnly = mFactory.createInOnlyExchange();
            outMsg = inOnly.createMessage();

            // set the stuff we know
            inOnly.setService(new QName(SERVICE_NAMESPACE, OUTBOUND_SERVICE_NAME));
            inOnly.setOperation(new QName(SERVICE_NAMESPACE, "getMessageOutOnly"));
            
            inOnly.setInterfaceName(new QName(SERVICE_NAMESPACE , INTERFACE_NAME));
            ServiceEndpoint mEndpoint;

            // lookup the endpoint reference and set on exchange
            ServiceEndpoint [] eprefs = null;
            eprefs = mContext.getEndpointsForService(new QName(SERVICE_NAMESPACE,
                        OUTBOUND_SERVICE_NAME));
            if (eprefs == null)
            {
                mLog.severe("No end points for service");             
                mLog.info(" JMS Test Engine : JMSTest-GOODOUTONLY-Failed");
                return;
            }
            inOnly.setEndpoint(eprefs[0]);

            // set the payload
            Payload.setPayload(outMsg);

            // set the message on the exchange
            inOnly.setInMessage(outMsg);

            // send the exchange
            mChannel.send(inOnly);

            // receive the response
            inOnly = (InOnly) mChannel.accept(2000);
            if (inOnly == null)
            {             
                mLog.info(" JMS Test Engine : JMSTest-GOODOUTONLY-Failed");   
            }            
            else if (inOnly.getStatus() == ExchangeStatus.DONE)
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODOUTONLY-Passed");
            }
            else
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODOUTONLY-Failed");
            }
        }
        catch (Exception e)
        {
            mLog.severe(" JMS Test Engine : JMSTest-GOODOUTONLY-threw-Exception");
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void doTestRobustInOnly()
    {
        /* start accpeting the in-only, in-out and robust in-only
         */
        try
        {
            RobustInOnly robinonly = (RobustInOnly) mReq;

            if (robinonly.getInMessage() == null)
            {
                mLog.info(" JMS Test Engine : JMSTest-GOODROBUSTINONLY-Failed");
                robinonly.setError(new Exception("Message is null"));
                mChannel.send(robinonly);

                return;
            }

            if (robinonly.getInMessage().getContent() == null)
            {
                mLog.info(" JMS Test Engine : JMSTest-ROBUSTINONLY-Failed");

                return;
            }

            robinonly.setStatus(ExchangeStatus.DONE);
            mChannel.send(robinonly);
            mLog.info(" JMS Test Engine : JMSTest-ROBUSTINONLY-Passed");
        }
        catch (Exception e)
        {
            mLog.severe(" JMS Test Engine : JMSTest-ROBUSTINONLY-threw-Exception"
                + e.getMessage());
            e.printStackTrace();
        }
    }

    public Document getServiceDescription(javax.jbi.servicedesc.ServiceEndpoint ServiceEndpoint)
    {
        return null;
    }
    
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager()
    {
        return null;
    }
    
    public javax.jbi.component.ComponentLifeCycle getLifeCycle() 
    {
        return this;
    }
    
    
     /** This method is called by JBI to check if this component, in the role of
     *  provider of the service indicated by the given exchange, can actually 
     *  perform the operation desired. 
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /** This method is called by JBI to check if this component, in the role of
     *  consumer of the service indicated by the given exchange, can actually 
     *  interact with the the provider completely. 
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }
    
    /**
     * Resolve the given endpoint reference, given the capabilities of the
     * given consumer. This is called by JBI when it is attempting to resolve
     * the given endpoint reference on behalf of a component.
     * @param epr the endpoint reference, in some XML dialect understood by the
     * appropriate component (usually a Binding Component).
     * @return the service endpoint for the endpoint reference;
     * <code>null</code> if the endpoint reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }

    /**
     *   
     *
     * @author Sun Microsystems, Inc.
     * 
     */
    public class Receiver implements Runnable
    {
        /**
         *
         */
        public void run()
        {
            mLog.info(" JMS Test Engine : Test engine receiver started");

            try
            {
                while (mRunFlag)
                {
                    mLog.info("Receiving requests for " + mInboundReference.getServiceName()
                        + mInboundReference.getEndpointName());
                    mReq = mChannel.accept(10000);
                    mLog.info(" JMS Test Engine : Got message in JMS test ebgine");
                   /* 
                    // start test for resolver               
                    Descriptor d =
                        mChannel.getEndpointDescriptor(mReq.getEndpoint());

                    mLog.info(" JMS Test Engine : Got a descriptor with "
                        + d.getDescription().getDocumentElement().getLocalName());

                    */
                    if (mReq.getPattern().toString().trim().equals(IN_OUT))
                    {
                        doTestInOut();
                    }
                    else if (mReq.getPattern().toString().trim().equals(IN_ONLY))
                    {
                        doTestInOnly();
                    }
                    else if (mReq.getPattern().toString().trim().equals(ROBUST_IN_ONLY))
                    {
                        doTestRobustInOnly();
                    }
                }
            }
            catch (Exception e)
            {
                mLog.severe(" JMS Test Engine : Exception at receiver");
                mLog.info(" JMS Test Engine : Test engine receiver ended");
                e.printStackTrace();
            }
        }
    }
}
