/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DummyClient.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package testclient;

import java.io.*;

import java.util.*;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;

/*
 * @(#)DummyClient.java        1.5 04/09/15
 *
 * Copyright (c) 2001-2003 Sun Microsystems, Inc. All Rights Reserved.
 *
 * Sun grants you ("Licensee") a non-exclusive, royalty free, license to use,
 * modify and redistribute this software in source and binary code form,
 * provided that i) this copyright notice and license appear on all copies of
 * the software; and ii) Licensee does not utilize the software in a manner
 * which is disparaging to Sun.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY
 * IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NON-INFRINGEMENT, ARE HEREBY EXCLUDED. SUN AND ITS LICENSORS SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THE SOFTWARE OR ITS DERIVATIVES. IN NO EVENT WILL SUN OR ITS
 * LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT,
 * INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF
 * OR INABILITY TO USE SOFTWARE, EVEN IF SUN HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGES.
 *
 * This software is not designed or intended for use in on-line control of
 * aircraft, air traffic, aircraft navigation or aircraft communications; or in
 * the design, construction, operation or maintenance of any nuclear
 * facility. Licensee represents and warrants that it will not use or
 * redistribute the Software for such purposes.
 */
import javax.naming.*;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class DummyClient
{
    static String def_windows_url = "file:///";
    static String def_unix_url = "file:///tmp/imqobjects";
    static String OPERATION_NAMESPACE = "http://sun.com/testengine.wsdl";

    /**
     *    
     */
    Queue receivequeue;

    /**
     *
     */

    /**
     *    
     */
    Queue sendqueue;

    /**
     *    
     */
    Queue temporaryqueue;

    /**
     *
     */

    /**
     *    
     */
    QueueConnection queueConnection;

    /**
     *
     */

    /**
     *    
     */
    QueueConnectionFactory mQcf;

    /**
     *
     */

    /**
     *    
     */
    QueueReceiver queueReceiver;
    
    QueueReceiver tmpReceiver;

    /**
     *
     */

    /**
     *    
     */
    QueueSender queueSender;
    
    
    QueueSender tmpqueueSender;

    /**
     *
     */

    /**
     *    
     */
    QueueSession queueSession;

    /**
     *
     */

    /**
     *    
     */
    String MYQCF_LOOKUP_NAME = "MyQueueConnectionFactory";

    /**
     *
     */

    /**
     *    
     */
    String RECEIVE_QUEUE_LOOKUP_NAME = "receive_queue";

    /**
     *
     */

    /**
     *    
     */
    String SEND_QUEUE_LOOKUP_NAME = "send_queue";

    /**
     *
     */

    /**
     *    
     */
    String TEMP_QUEUE_LOOKUP_NAME = "external_queue";

    /**
     *
     */

    /**
     *    
     */
    TextMessage msg;

    /**
     *
     */

    /**
     *    
     */
    TextMessage rcvMsg;
    
    /**
     *
     */

    /**
     *    
     */
    private InitialContext mInitialContext;

    /**
     * Creates a new DummyClient object.
     *
     * @param url
     */
    public DummyClient(String url)
    {
        Hashtable env;
        Context mInitialContext = null;

        env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY,
            "com.sun.jndi.fscontext.RefFSContextFactory");
        env.put(Context.PROVIDER_URL, url);

        try
        {
            mInitialContext = new InitialContext(env);
        }
        catch (NamingException ne)
        {
            System.err.println(" JMS Client : Failed to create InitialContext.");
            System.err.println(" JMS Client : The Context.PROVIDER_URL used/specified was: "
                + url);
            System.err.println(
                "Please make sure that the path to the above URL exists");
            System.err.println(
                "and matches with the objstore.attrs.java.naming.provider.url");
            System.err.println(
                "property value specified in the imqobjmgr command files:");
            ne.printStackTrace();
            System.exit(-1);
        }

        try
        {
            /*
            System.out.println(
                "Looking up Queue Connection Factory object with lookup name: "
                + MYQCF_LOOKUP_NAME);
             */
            mQcf =
                (javax.jms.QueueConnectionFactory) mInitialContext.lookup(MYQCF_LOOKUP_NAME);
        //    System.out.println(" JMS Client : Queue Connection Factory object found.");
        }
        catch (NamingException ne)
        {
            System.err.println(
                "Failed to lookup Queue Connection Factory object.");
            ne.printStackTrace();
            System.exit(-1);
        }

        javax.jms.Queue response = null;

        try
        {
           /* System.out.println(" JMS Client : Looking up Queue object with lookup name: "
                + MYQCF_LOOKUP_NAME);
            */
            sendqueue =
                (javax.jms.Queue) mInitialContext.lookup(SEND_QUEUE_LOOKUP_NAME);
           // System.out.println(" JMS Client : Send Queue object found.");
            receivequeue =
                (javax.jms.Queue) mInitialContext.lookup(RECEIVE_QUEUE_LOOKUP_NAME);
            //System.out.println(" JMS Client : Receive Queue object found.");
            temporaryqueue =
                (javax.jms.Queue) mInitialContext.lookup(TEMP_QUEUE_LOOKUP_NAME);
            //System.out.println(" JMS Client : Temporary Queue object found.");
        }
        catch (NamingException ne)
        {
            System.err.println(" JMS Client : Failed to lookup Queue object.");
            System.err.println(
                "Please make sure you have created the Queue object using the command:");
            System.err.println(" JMS Client : \timqobjmgr -i add_q.props");

            System.err.println(" JMS Client : \nThe exception details:");
            ne.printStackTrace();
            System.exit(-1);
        }

        //System.out.println(" JMS Client : ");


    }

    /**
     * Creates a new doDummyOutOnly object.
     */
    private   void doTestOutOnly()
    {
        try
        {

            // Tell the provider to start sending messages.
            queueConnection.start();

            javax.jms.Message msg = tmpReceiver.receive(15000);
            
            if (msg == null)
            {
               // System.out.println(" JMS Client : JMSTest Out only Failed");

                return;
            }
            else
            {
              //  System.out.println(" JMS Client : JMSTest Out only Passed");
                msg.acknowledge();
                return;
            }
            
        }
        catch (Exception e)
        {
            System.out.println(" JMS Client : JMSTest Out only Failed");

            return;
        }
    }

    /**
     * Creates a new doTestOutIn object.
     *
     * @param time
     */
    private  void doTestOutIn(long time)
    {
        try
        {

            // Tell the provider to start sending messages.
            queueConnection.start();
            
            javax.jms.Message msg = tmpReceiver.receive(time);
            
            if (msg == null)
            {
              //  System.out.println(" JMS Client : JMSTest Out in Failed");

                return;
            }
            else
            {
                msg.acknowledge();

                if (msg instanceof javax.jms.TextMessage)
                {
                    TextMessage txt = (TextMessage) msg;
                    javax.jms.Destination dest = msg.getJMSReplyTo();
                    TextMessage resp = queueSession.createTextMessage();
                    String s = txt.getText();
                    String reader =   new String(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?><jbi:message xmlns:msgns=\"http://sun.com/testengine.wsdl\" type=\"msgns:transformOutput\" version=\"1.0\" xmlns:jbi=\"http://java.sun.com/xml/ns/jbi/wsdl-11-wrapper\"><jbi:part><response>What is OpenESB</response></jbi:part></jbi:message>");
   
                    if (s == null)
                    {
                        resp.setText("<message>Cannot process Message</message>");
                        resp.setStringProperty("JBI_STATUS", "ERROR");
                    }
                    else if (s.trim().indexOf("<message>Bad_JMS_Message</message>") > 0)
                    {
                        resp.setText("<message>Cannot process Message</message>");
                        resp.setStringProperty("JBI_STATUS", "FAULT");
                    }
                    else
                    {
                        resp.setText(reader);
                        resp.setStringProperty("JBI_STATUS", "SUCCESS");
                    }
                    
                    /*System.out.println("JMS Client : Sending out in response to" 
                        + dest.toString());
                    */
                    /** set the correlation id
                     */
                    resp.setJMSCorrelationID(txt.getJMSCorrelationID());
                    
                    tmpqueueSender = (QueueSender) queueSession.createProducer(dest);
                    tmpqueueSender.send(resp);
                    tmpqueueSender.close();
                    // System.out.println(" JMS Client : JMSTest Out in Success");
                }
                else
                {
                  //  System.out.println(" JMS Client : JMSTest Out in Failed");
                }

                return;
            }
            
        }
        catch (Exception e)
        {
            System.out.println(" JMS Client : JMSTest Out in Failed");
            e.printStackTrace();

            return;
        }
    }

    /**
     * Creates a new doTestRobustOutOnly object.
     *
     * @param time
     */
    private  void doTestRobustOutOnly(long time)
    {
        try
        {

            // Tell the provider to start sending messages.
            queueConnection.start();

            javax.jms.Message msg = tmpReceiver.receive(time);
            
            if (msg == null)
            {
              //  System.out.println(" JMS Client : JMSTest Robust Out only Failed");

                return;
            }
            else
            {
                msg.acknowledge();
                
                if (msg instanceof javax.jms.TextMessage)
                {
                    TextMessage txt = (TextMessage) msg;
                    javax.jms.Destination dest = msg.getJMSReplyTo();
                    TextMessage resp = queueSession.createTextMessage();
                    String s = txt.getText();

                    if (s == null)
                    {
                        resp.setText("<message>Cannot process Message</message>");
                        resp.setStringProperty("JBI_STATUS", "ERROR");
                    }
                    else if (s.trim().indexOf("<message>Bad_JMS_Message</message>") > 0)
                    {
                        resp.setText("<message>Cannot process Message</message>");
                        resp.setStringProperty("JBI_STATUS", "FAULT");
                    }
                    else
                    {
                      //  System.out.println(" JMS Client : JMSTest Robust Out Only Success");

                        return;
                    }

                    resp.setJMSCorrelationID(txt.getJMSCorrelationID());
                    tmpqueueSender = (QueueSender)queueSession.createProducer(dest);
                    tmpqueueSender.send(resp);
                    tmpqueueSender.close();
                  //  System.out.println(" JMS Client : JMSTest Robust Out Only Success");
                }
                else
                {
                   // System.out.println(" JMS Client : JMSTest Robust Out Only Failed");
                }

                return;
            }
            
        }
        catch (Exception e)
        {
            System.out.println(" JMS Client : JMSTest Robust Out only Failed");
            e.printStackTrace();

            return;
        }
    }

    private void doTests()
    {
        if (!init())
        {         
            System.out.println(" JMS Client : JMS Client : Cannot initialize");   
            System.exit(-1);
        }        
        
        //System.out.println(" JMS Client : Test 1: Test Out only");
        doTestOutOnly();        
        //System.out.println(" JMS Client : Test 2: Test Good Out in");
        doTestOutIn(3000);     
        //System.out.println(" JMS Client : Test 3: Test Bad Out in");
        doTestOutIn(3000);        
        
        try
        {
            // Wait for sometime before starting other tests
            Thread.sleep(3000);
        }
        catch (Exception e)
        {
            ;
        }
        //System.out.println(" JMS Client : Test 6: Test In only");
        doTestInOnly(3000);
        //System.out.println(" JMS Client : Test 7: Test Good in out");
        doTestGoodInOut(3000);
        
                
        if (!cleanup())
        {         
            System.out.println(" JMS Client : JMS Client : Cannot clean up");   
        }
        
    }
    /**
     * DOCUMENT ME!
     *
     * @param args NOT YET DOCUMENTED
     */
    public static void main(String [] args)
    {
        String url = def_unix_url;

        if (args.length > 0)
        {
            url = args[0];
        }
        

        DummyClient simple_client = new DummyClient(url);
        
        //WSDL 11 tests
        
        simple_client.doTests();
        
       //WSDL 20 tests
//        simple_client.doTests();
    }

    /**
     *
     *
     * @return  NOT YET DOCUMENTED
     */
    private  String getBadString()
    {
        String s = new String("<message>Bad_JMS_Message</message>");

        return s;
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    private  String getString()
    {
        /*
           File f = new File("sample.xml");
           FileReader reader = null;
           char [] buf = new char[2048];
           try
           {
           reader = new FileReader(f);
           reader.read(buf, 0, 2000);
           }
           catch (Exception e)
           {
                   e.printStackTrace();
           }
        
           String s = new String(buf);
           System.out.println(" JMS Client : Sending " + s);
         */
        String s = new String(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?><jbi:message xmlns:msgns=\"http://sun.com/testengine.wsdl\" type=\"msgns:transformInput\" version=\"1.0\" xmlns:jbi=\"http://java.sun.com/xml/ns/jbi/wsdl-11-wrapper\"><jbi:part><request>What is OpenESB</request></jbi:part></jbi:message>");
 

        return s;
    }

    /**
     *
     *
     * @param time  NOT YET DOCUMENTED
     */
    private  void doTestBadInOut(long time)
    {
        try
        {

            TextMessage msg = queueSession.createTextMessage(getBadString());
            msg.setStringProperty("JBI_OPERATION", "getMessageInOut");            
            msg.setStringProperty("JBI_OPERATION_NAMESPACE", OPERATION_NAMESPACE);
            msg.setJMSReplyTo(receivequeue);

         /*   System.out.println(" JMS Client : Publishing a message to Queue: "
                + sendqueue.getQueueName());
          */
            queueSender.send(msg, DeliveryMode.NON_PERSISTENT, 4, 0);
            // Wait for it to be sent back.
            
               
            TextMessage rcvMsg = (TextMessage) queueReceiver.receive(time);
            
            if (rcvMsg == null)
            {
                // System.out.println(" JMS Client : JMSTest Bad inout success");

                return;
            }

            String status = null;

            try
            {
                rcvMsg.acknowledge();
                status = rcvMsg.getStringProperty("JBI_STATUS");
            }
            catch (JMSException e)
            {
                e.printStackTrace();
            }

            if (status == null)
            {
              //  System.out.println(" JMS Client : JMSTest Bad inout failed");
            }
            else if (status.trim().equals("ERROR"))
            {
              //  System.out.println(" JMS Client : JMSTest Bad inout success");
            }
            else if (status.trim().equals("FAULT"))
            {
                // System.out.println(" JMS Client : JMSTest Bad inout success");
            }
            else
            {
            //    System.out.println(" JMS Client : JMSTest Bad inout failed");
            }
         
        }
        catch (JMSException e)
        {
            System.err.println(" JMS Client : JMS Exception: " + e);
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     *
     *
     * @param time  NOT YET DOCUMENTED
     */
    private  void doTestBadRobustInOnly(long time)
    {
        try
        {

            TextMessage msg = queueSession.createTextMessage(getBadString());
            msg.setStringProperty("JBI_OPERATION", "getMessageRobustInOnly");            
            msg.setStringProperty("JBI_OPERATION_NAMESPACE", OPERATION_NAMESPACE);
            msg.setJMSReplyTo(receivequeue);

           /* System.out.println(" JMS Client : Publishing a message to Queue: "
                + sendqueue.getQueueName());
            */
            queueSender.send(msg, DeliveryMode.NON_PERSISTENT, 4, 0);
            // Wait for it to be sent back.
            

            TextMessage rcvMsg = null;
            rcvMsg = (TextMessage) queueReceiver.receive(time);
            
            if (rcvMsg == null)
            {
              //  System.out.println(" JMS Client : JMSTest Bad robust in only failed");

                return;
            }
            else
            {
                String status = null;

                try
                {
                    rcvMsg.acknowledge();
                    status = rcvMsg.getStringProperty("JBI_STATUS");
                }
                catch (JMSException e)
                {
                    e.printStackTrace();
                }

                if (status == null)
                {
                  //  System.out.println(" JMS Client : JMSTest Bad robust inonly failed");
                }
                else if (status.trim().equals("ERROR"))
                {
                //    System.out.println(" JMS Client : JMSTest Bad robust inonly success");
                }
                else if (status.trim().equals("FAULT"))
                {
                  //  System.out.println(" JMS Client : JMSTest Bad robust inonly success");
                }
                else
                {
                   // System.out.println(" JMS Client : JMSTest Bad robust inonly failed");
                }
            }
            
        }
        catch (JMSException e)
        {
            System.out.println(" JMS Client : JMSTest Bad robust inonly failed");
            System.err.println(" JMS Client : JMS Exception: " + e);
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     *
     *
     * @param time  NOT YET DOCUMENTED
     */
    private  void doTestGoodRobustInOnly(long time)
    {
        try
        {

            TextMessage msg = queueSession.createTextMessage(getString());
            msg.setStringProperty("JBI_OPERATION", "getMessageRobustInOnly");            
            msg.setStringProperty("JBI_OPERATION_NAMESPACE", OPERATION_NAMESPACE);
            msg.setJMSReplyTo(receivequeue);

           /* System.out.println(" JMS Client : Publishing a message to Queue: "
                + sendqueue.getQueueName());
            */
            queueSender.send(msg, DeliveryMode.NON_PERSISTENT, 4, 0);
            // Wait for it to be sent back.
            

            TextMessage rcvMsg = null;
            rcvMsg = (TextMessage) queueReceiver.receive(time);
            
            if (rcvMsg == null)
            {
              //  System.out.println(" JMS Client : JMSTest Good robust in only success");

                return;
            }
            else
            {
              //  System.out.println(" JMS Client : JMSTest Good robust in only failed");
                rcvMsg.acknowledge();

                return;
            }
           
        }
        catch (JMSException e)
        {
            System.err.println(" JMS Client : JMS Exception: " + e);
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     *
     *
     * @param time  NOT YET DOCUMENTED
     */
    private  void doTestInOnly(long time)
    {
        try
        {

            TextMessage msg = queueSession.createTextMessage(getString());
            msg.setStringProperty("JBI_OPERATION", "getMessageInOnly");            
            msg.setStringProperty("JBI_OPERATION_NAMESPACE", OPERATION_NAMESPACE);
          /*  System.out.println(" JMS Client : Publishing a message to Queue: "
                + sendqueue.getQueueName());
           */
            queueSender.send(msg, DeliveryMode.NON_PERSISTENT, 4, 0);
           // System.out.println(" JMS Client : JMSTest In only success");
        }
        catch (JMSException e)
        {
            System.out.println(" JMS Client : JMSTest Inponly failed");
            System.err.println(" JMS Client : JMS Exception: " + e);
            e.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param time NOT YET DOCUMENTED
     */
    private  void doTestGoodInOut(long time)
    {
        try
        {

            TextMessage msg = queueSession.createTextMessage(getString());
            msg.setStringProperty("JBI_OPERATION", "getMessageInOut");            
            msg.setStringProperty("JBI_OPERATION_NAMESPACE", OPERATION_NAMESPACE);
            msg.setJMSReplyTo(receivequeue);

           /* System.out.println(" JMS Client : Publishing a message to Queue: "
                + sendqueue.getQueueName());
            */
            queueSender.send(msg, DeliveryMode.NON_PERSISTENT, 4, 0);
            // Wait for it to be sent back.
            

            TextMessage rcvMsg = (TextMessage) queueReceiver.receive(time);
            
            if (rcvMsg == null)
            {
              //  System.out.println(" JMS Client : JMSTest Good inout success");

                return;
            }

            String status = null;

            try
            {
                rcvMsg.acknowledge();
                status = rcvMsg.getStringProperty("JBI_STATUS");
            }
            catch (JMSException e)
            {
                e.printStackTrace();
            }

            if (status == null)
            {
               // System.out.println(" JMS Client : JMSTest Good inout success");
            }
            else if (status.trim().equals("ERROR"))
            {
               // System.out.println(" JMS Client : Error status ");
            }
            else if (status.trim().equals("FAULT"))
            {
               // System.out.println(" JMS Client : Fault staus");
            }
            else
            {
               // System.out.println(" JMS Client : JMSTest Good inout success");
            }
            
        }
        catch (JMSException e)
        {
            System.err.println(" JMS Client : JMS Exception: " + e);
            e.printStackTrace();
            System.exit(-1);
        }
    }
    
    private  boolean cleanup()
    {
        try
        {
            queueReceiver.close();
            queueSender.close();
            tmpReceiver.close();
            queueSession.close();
            queueConnection.close();
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    private  boolean init()
    {
        try
        {
            queueConnection = mQcf.createQueueConnection();
            queueSession = 
                queueConnection.createQueueSession(false,
                    Session.AUTO_ACKNOWLEDGE);
            queueSender = queueSession.createSender(sendqueue);
            queueReceiver = queueSession.createReceiver(receivequeue);            
            tmpReceiver = queueSession.createReceiver(temporaryqueue);            
        }
        catch (JMSException e)
        {
            System.err.println(" JMS Client : Failed to create connection.");
            System.err.println(" JMS Client : Please make sure that the broker was started.");

            System.err.println(" JMS Client : \nThe exception details:");
            e.printStackTrace();
            return false;      
            
        }
        return true;
    }
    
        
    private  boolean closeSender()
    {
        try
        {
            queueSender.close();
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }
    
    
    /**
     *
     */
    private  void usage()
    {
        System.err.println(" JMS Client : Usage: "
            + "\tjava TestClient[Context.PROVIDER_URL]\n"
            + "\nOn Unix:\n\tjava TestClient" + def_unix_url
            + "\nOn Windows:\n\tjava TestClient" + def_windows_url);
    }
}
