/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import java.util.logging.Level;

/**
 * This is a constants repository and is used by all JMS binding components
 * running in the JBI container.
 *
 * @author Sun Microsystems Inc.
  */
public interface JMSConstants
{
    /**
     * Default Log Level.
     */
    Level DEFAULT_LOG_LEVEL = Level.INFO;

    /**
     * Default Operation name.
     */
    String DEFAULT_OPERATION_NAME = "default";

    /**
     * In_Only Pattern name.
     */
    String IN_ONLY_PATTERN = "http://www.w3.org/2004/03/wsdl/in-only";

    /**
     * In_Optional_Out Pattern name.
     */
    String IN_OPTIONAL_OUT_PATTERN =
        "http://www.w3.org/2004/03/wsdl/in-opt-out";

    /**
     * In_Out Pattern name.
     */
    String IN_OUT_PATTERN = "http://www.w3.org/2004/03/wsdl/in-out";

    /**
     * Identifies the header name for propagating Fault / Error to JMS.
     */
    String JMS_FAULT_HEADER = "MESSAGE_STATUS";

    /**
     * Identifies the Header key for MEP.
     */
    String JMS_HEADER_MEP = "MEP";

    /**
     * Message context property which hold http response code.
     */
    String JMS_HEADER_OPERATION = "OPERATION";

    // List of Package Names.

    /**
     * JMS Package Name.
     */
    String JMS_PACKAGE_NAME = "binding.jms";

    /**
     * Package name for configuration classes.
     */
    String CONFIG_PACKAGE_NAME = JMS_PACKAGE_NAME + ".config";

    /**
     * 
     */
    String DEPLOY_PACKAGE_NAME = JMS_PACKAGE_NAME + ".deploy";

    /**
     *    
     */
    String HANDLER_PACKAGE_NAME = JMS_PACKAGE_NAME + ".handler";

    /**
     *    
     */
    String MONITORING_PACKAGE_NAME = JMS_PACKAGE_NAME + ".monitoring";

    /**
     *    
     */
    String MQ_PACKAGE_NAME = JMS_PACKAGE_NAME + ".mq";

    /**
     * NEW_LINE_CHARACTER.
     */
    String NEW_LINE = System.getProperty("line.separator");

    /**
     * Identifies header in the normalized message for correlation ID.
     */
    String NMS_CORRELATION_ID = "CORRELATION_ID";

    /**
     * Message Context property which holds SOAP header information.
     */
    String NMS_HEADER_DELIVERY_MODE = "DELIVERY_MODE";

    /**
     * Package name for classes handling outbound requests.
     */
    String OUTBOUND_PACKAGE_NAME = JMS_PACKAGE_NAME + ".outbound";

    /**
     * Out_In Pattern name.
     */
    String OUT_IN_PATTERN = "http://www.w3.org/2004/03/wsdl/out-in";

    /**
     * Out_Only Pattern name.
     */
    String OUT_ONLY_PATTERN = "http://www.w3.org/2004/03/wsdl/out-only";

    /**
     * Out_In Pattern name.
     */
    String OUT_OPTIONAL_IN_PATTERN =
        "http://www.w3.org/2004/03/wsdl/out-opt-in";

    /**
     * Property which holds the registry file location.
     */
    String REGISTRY_FILE_LOCATION = "REGISTRY_FILE_LOCATION";

    /**
     * Property which holds the registry File Name, should be same as in config
     * file.
     */
    String REGISTRY_FILE_NAME = "REGISTRY_FILE_NAME";

    /**
     * Robust In_Only Pattern name.
     */
    String ROBUST_IN_ONLY_PATTERN =
        "http://www.w3.org/2004/03/wsdl/robust-in-only";

    /**
     * Out_In Pattern name.
     */
    String ROBUST_OUT_ONLY_PATTERN =
        "http://www.w3.org/2004/03/wsdl/robust-out-only";

    /**
     * Schema directory name.
     */
    String SCHEMA_DIR_NAME = "schema";

    /**
     * Package name for thread framework.
     */
    String THREAD_PACKAGE_NAME = JMS_PACKAGE_NAME + ".threads";

    /**
     * Package name for utility classes.
     */
    String UTIL_PACKAGE_NAME = JMS_PACKAGE_NAME + ".util";

    /**
     * Default value for max threads.
     */
    int DEFAULT_MAX_THREADS = 10;

    /**
     * Default value for min threads.
     */
    int DEFAULT_MIN_THREADS = 2;

    /**
     * Default Thread sleep time in milliseconds.
     */
    int DEFAULT_THREAD_SLEEP_TIME = 100;

    /**
     * Indicates that an endpoint has been deployed for the service URL and
     * that engine has not activated the inbound service channel.
     */
    int ENGINE_NOT_RUNNING = 406;

    /**
     * Indicates that the request message conforms to XML standards but not to
     * SOAP standards.
     */
    int INVALID_REQUEST_MESSAGE = 412;

    /**
     * Indicates that the message processing has been completed.
     */
    int JBI_DONE = 100;

    /**
     * Indicates that an error has been set in the Message Exchange.
     */
    int JBI_ERROR = 103;

    /**
     * Indicates that a fault has been set in the Message Exchange.
     */
    int JBI_FAULT = 102;

    /**
     * Indicates that the message has been successfully processed.
     */
    int JBI_SUCCESS = 101;

    /**
     * Default value of stop timeout.
     */
    long DEFAULT_STOP_TIMEOUT = 5000;
}
