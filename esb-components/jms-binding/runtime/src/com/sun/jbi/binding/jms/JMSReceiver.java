/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSReceiver.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.framework.WorkManager;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import com.sun.jbi.binding.jms.handler.MessageHandler;

import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.MessageExchange;

/**
 * The JMS Reciever class receives the Normalized Message from the  NMS and
 * writes the contents to the directory specified in endpoints.xml.
 *
 * @author Sun Microsystems Inc.
 */
class JMSReceiver
    implements Runnable, JMSBindingResources
{
    /**
     *  Receiver work manager.
     */
    private static final String RECEIVER = "RECEIVER";
    /**
     * Channel.
     */
    private DeliveryChannel mChannel;

    /**
     * Logger Object.
     */
    private Logger mLogger;

    /**
     * Normalized Message.
     */
    private MessageExchange mExchange;

    /**
     * Message processor.
     */
    private MessageProcessor mProcessor;

    /**
     * Helper for i18n.
     */
    private StringTranslator mStringTranslator;

    /**
     * Work Manager.
     */
    private WorkManager mWorkManager;

    /**
     * Creates the JMSReceiver Thread.
     *
     * @param bc Thread group for this receiver
     */
    public JMSReceiver(DeliveryChannel bc)
    {
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
        mChannel = bc;
        mWorkManager =
            WorkManager.getWorkManager(ConfigConstants.NMS_WORK_MANAGER);
        mWorkManager.setMinThreads(JMSBindingContext.getInstance().getConfig()
                                                    .getMinThreadCount());
        mWorkManager.setMaxThreads(JMSBindingContext.getInstance().getConfig()
                                                    .getMaxThreadCount());
        mProcessor = new MessageProcessor(bc);
    }

    /**
     * Blocking call on the service channel to receive the message. Right now,
     * we wait for 10 seconds. Can be made to configure.
     */
    public void run()
    {
        mLogger.info(mStringTranslator.getString(JMS_RECEIVER_START));
        mWorkManager.start();
        mLogger.info("Started the work manager in JMSReceiver");
        while (true)
        {
            try
            {
                mExchange = mChannel.accept();
                if (mExchange != null)
                {
                    MessageHandler handler = null;
                    mProcessor.setExchange(mExchange);
                    mLogger.info("Received the message " + 
                                    mExchange.getExchangeId());
                    handler = mProcessor.process();

                    if (handler == null)
                    {
                        mLogger.severe(mStringTranslator.getString(
                                JMS_INVALID_MESSAGE, mExchange.getExchangeId()));

                        continue;
                    }

                    if (!mWorkManager.processCommand(handler))
                    {
                        mLogger.info(mStringTranslator.getString(
                                JMS_NO_FREE_THREAD));
                    }
                }
            }
            catch (Exception e)
            {
                mLogger.info(e.getMessage());
                mWorkManager.cease();

                return;
            }
        }
    }

    /**
     * Stops the receiving thread.
     */
    public void stopReceiving()
    {
        mLogger.info(mStringTranslator.getString(JMS_RECEIVER_STOP));

    }
}
