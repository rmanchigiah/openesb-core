/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSBindingContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.binding.jms.config.Config;
import com.sun.jbi.binding.jms.deploy.EndpointRegistry;
import com.sun.jbi.binding.jms.framework.WorkManager;
import com.sun.jbi.binding.jms.mq.MQManager;

import java.util.logging.Level;

import java.util.logging.Logger;

import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;


/**
 * Context object to store sequencing engine specific informaiton.
 *
 * @author Sun Microsystems Inc.
 */
public final class JMSBindingContext
{
    /**
     * This object.
     */
    private static JMSBindingContext sJMSContext;

    /**
     * Engine context.
     */
    private ComponentContext mContext;

    /**
     *  Configuration object.
     */
    private Config mConfig;

    /**
     * Endpoint manager.
     */
    private EndpointManager mEndpointManager;

    /**
     * Endpoint resistry.
     */
    private EndpointRegistry mRegistry;

    /**
     * Logger object.
     */
    private Logger mLogger;
    /**
     * MQ manager.
     */
    private MQManager mMQManager;
    /**
     * Message registry.
     */
    private MessageRegistry mMessageRegistry;
    /**
     * String translator.
     */
    private StringTranslator mStringTranslator;
    /**
     * JMS work manager for amnaging threads.
     */
    private WorkManager mJMSWorkManager;

    /**
     * Creates a new JMS Binding context object.
     */
    private JMSBindingContext()
    {
    }

    /**
     * Gets the binding channel.
     *
     * @return bidning channel
     */
    public DeliveryChannel getChannel()
    {
        DeliveryChannel chnl = null;

        if (mContext != null)
        {
            try
            {
                chnl = mContext.getDeliveryChannel();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return chnl;
    }

    /**
     * Setter for property mConfig.
     *
     * @param mConfig New value of property mConfig.
     */
    public void setConfig(Config mConfig)
    {
        this.mConfig = mConfig;
    }

    /**
     * Getter for property mConfig.
     *
     * @return Value of property mConfig.
     */
    public Config getConfig()
    {
        return mConfig;
    }

    /**
     * Sets the binding context.
     *
     * @param ctx binding context.
     */
    public void setContext(ComponentContext ctx)
    {
        mContext = ctx;
    }

    /**
     * Returns the context.
     *
     * @return binding context implementation.
     */
    public ComponentContext getContext()
    {
        return mContext;
    }

    /**
     * Used to grab a reference of this object.
     *
     * @return an initialized SequencingEngineContext reference
     */
    public static synchronized JMSBindingContext getInstance()
    {
        if (sJMSContext == null)
        {
            synchronized (JMSBindingContext.class)
            {
                if (sJMSContext == null)
                {
                    sJMSContext = new JMSBindingContext();
                }
            }
        }

        return sJMSContext;
    }

    /**
     * Setter for property mEndpointManager.
     *
     * @param mEndpointManager New value of property mEndpointManager.
     */
    public void setEndpointManager(EndpointManager mEndpointManager)
    {
        this.mEndpointManager = mEndpointManager;
    }

    /**
     * Getter for property mEndpointManager.
     *
     * @return Value of property mEndpointManager.
     */
    public EndpointManager getEndpointManager()
    {
        return mEndpointManager;
    }

    /**
     * Setter for property mJMSWorkManager.
     *
     * @param mJMSWorkManager New value of property mJMSWorkManager.
     */
    public void setJMSWorkManager(WorkManager mJMSWorkManager)
    {
        this.mJMSWorkManager = mJMSWorkManager;
    }

    /**
     * Getter for property mJMSWorkManager.
     *
     * @return Value of property mJMSWorkManager.
     */
    public WorkManager getJMSWorkManager()
    {
        return mJMSWorkManager;
    }

    /**
     * Sets the logger.
     *
     * @param logger Logger.
     */
    public void setLogger(Logger logger)
    {
        mLogger = logger;
    }

    /**
     * Returns the logger.
     *
     * @return logger object.
     */
    public Logger getLogger()
    {
        if (mLogger == null)
        {
            mLogger = Logger.getLogger("com.sun.jbi.binding.jms");
        }

        return mLogger;
    }

    /**
     * Returns the logger.
     *
     * @param pkg package name.
     *
     * @return logger object.
     */
    public Logger getLogger(String pkg)
    {
        Logger logger = Logger.getLogger(JMSConstants.JMS_PACKAGE_NAME);

        try
        {
            if (mConfig != null)
            {
                Level loglevel = mConfig.getLogLevel();
                logger.setLevel(loglevel);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return logger;
    }

    /**
     * Setter for property mMQManager.
     *
     * @param mMQManager New value of property mMQManager.
     */
    public void setMQManager(MQManager mMQManager)
    {
        this.mMQManager = mMQManager;
    }

    /**
     * Getter for property mMQManager.
     *
     * @return Value of property mMQManager.
     */
    public MQManager getMQManager()
    {
        return mMQManager;
    }

    /**
     * Setter for property mMessageRegistry.
     *
     * @param mMessageRegistry New value of property mMessageRegistry.
     */
    public void setMessageRegistry(MessageRegistry mMessageRegistry)
    {
        this.mMessageRegistry = mMessageRegistry;
    }

    /**
     * Getter for property mMessageRegistry.
     *
     * @return Value of property mMessageRegistry.
     */
    public MessageRegistry getMessageRegistry()
    {
        return mMessageRegistry;
    }

    /**
     * Setter for property mRegistry.
     *
     * @param mRegistry New value of property mRegistry.
     */
    public void setRegistry(EndpointRegistry mRegistry)
    {
        this.mRegistry = mRegistry;
    }

    /**
     * Getter for property mRegistry.
     *
     * @return Value of property mRegistry.
     */
    public EndpointRegistry getRegistry()
    {
        return mRegistry;
    }

    /**
     * String translator implementation.
     *
     *
     * @return string translator implementation.
     */
    public StringTranslator getStringTranslator()
    {

        if (mStringTranslator != null)
        {
            return mStringTranslator;
        }

        try
        {
            com.sun.jbi.component.ComponentContext jbiContext =
                (com.sun.jbi.component.ComponentContext) (mContext);
            mStringTranslator =
                jbiContext.getStringTranslatorFor(this);
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }

        return mStringTranslator;
    }
}
