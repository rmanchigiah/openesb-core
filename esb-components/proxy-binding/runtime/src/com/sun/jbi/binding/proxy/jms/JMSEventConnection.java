/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSEventConnection.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.jms;

import com.sun.jbi.binding.proxy.LocalStringKeys;
import com.sun.jbi.binding.proxy.RegistrationInfo;

import com.sun.jbi.binding.proxy.util.Translator;

import com.sun.jbi.binding.proxy.connection.EventInfo;
import com.sun.jbi.binding.proxy.connection.EventInfoFactory;

import com.sun.jbi.binding.proxy.connection.EventConnection;
import com.sun.jbi.binding.proxy.connection.Event;

import javax.jms.Connection;
import javax.jms.MessageConsumer;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Topic;
import javax.jms.Session;
import javax.jms.StreamMessage;

import java.util.logging.Logger;

import javax.xml.namespace.QName;

/**
 * Encapsulates the logic of a Event connection. The event connection is assumed to be
 * called from a single thread using pull-style processing. The producer and consumer are 
 * attached to the same session which allows messages sent by this producer to not be seen
 * by this consumer.
 *
 * @author Sun Microsystems, Inc
 */
public class JMSEventConnection 
        implements EventConnection
{
    private Logger                  mLog;
    private Session                 mSession;
    private Topic                   mTopic;
    private MessageProducer         mProducer;
    private MessageConsumer         mConsumer;
    
    JMSEventConnection(Connection connection, Topic topic)
        throws com.sun.jbi.binding.proxy.connection.ConnectionException
    {
        mLog = Logger.getLogger("com.sun.jbi.binding.proxy.jms");
        mTopic = topic;
        
        try
        {
            mSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            /* Publisher used to send registration info. */
            mProducer  = mSession.createProducer(mTopic);

            /* Subscriber used to receive registration info (true = ignore local messages) */
            mConsumer = mSession.createConsumer(mTopic, null, true);       
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.ConnectionException(jEx);
        }
    }        

    public void sendEvent(EventInfo info)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        JMSEvent       e;
        
        try
        {
            e = new JMSEvent(mSession.createStreamMessage(), info.getEventName());
            info.encodeEvent(e);
            mProducer.send(e.getMessage());
        }
        catch (javax.jms.JMSException jmsEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jmsEx);
        }
    }

    public void sendEventTo(EventInfo info, String id)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        JMSEvent       e;
        
        try
        {
            e = new JMSEvent(mSession.createStreamMessage(), info.getEventName());
            info.encodeEvent(e);
            mProducer.send(e.getMessage());
        }
        catch (javax.jms.JMSException jmsEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jmsEx);
        }
    }
    
    public EventInfo receiveEvent(long timeout)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        EventInfo       ei = null;
        
        try
        {
            JMSEvent           e;
            StreamMessage      m;
            String             t;
            
            m = (StreamMessage)mConsumer.receive(timeout);  
            if (m != null)
            {                
                e = new JMSEvent(m);
                ei = EventInfoFactory.getInstance().newInstance(e);
                if (ei == null)
                {
                    mLog.info("Received unknown event type (" + e.getEventName() + ")");
                }
            }
            return (ei);
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new com.sun.jbi.binding.proxy.connection.EventException(jEx);
        }
    }
   

}
