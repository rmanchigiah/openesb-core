/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MEPOutputStream.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy.util;

import com.sun.jbi.binding.proxy.ExchangeEntry;
import com.sun.jbi.binding.proxy.ProxyBinding;
import com.sun.jbi.binding.proxy.LocalStringKeys;


import java.io.ObjectOutputStream;
import java.io.ByteArrayOutputStream;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import java.util.logging.Logger;

import javax.activation.DataHandler;

import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;


import javax.xml.namespace.QName;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/** 
 * Implementation of an output stream that can serialize a MEP.
 * @author Sun Microsystems, Inc
 */
public class MEPOutputStream 
        extends java.io.OutputStream
{
        private ByteArrayOutputStream       mOS;
        private ObjectOutputStream          mOOS;
        private int                         mCount;
        private int                         mStreams;
        private byte[]                      mBytes;
        private Transformer                 mTransform;
        static private Logger              mLog;
        
        public MEPOutputStream(ProxyBinding proxyBinding)
            throws javax.jbi.messaging.MessagingException
        {
            mOS = new ByteArrayOutputStream();

            mCount = 0;
            mBytes = new byte[MEPInputStream.BUFFER_SIZE];
            if (mLog == null)
            {
                mLog = proxyBinding.getLogger("output");
            }
            
            try
            {
                // initialize transformer details
                mTransform = TransformerFactory.newInstance().newTransformer();            
            }
            catch (javax.xml.transform.TransformerFactoryConfigurationError tfcEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPOUTPUT_TRANSFORMER_ERROR), tfcEx);
            }
            catch (javax.xml.transform.TransformerConfigurationException cfgEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPOUTPUT_TRANSFORMER_ERROR), cfgEx);
            }
        }
        
        public byte[] writeException(String id, Exception e)
        {
            byte[]          bytes;
            
            try
            {
                mOOS = new ObjectOutputStream(this);
                
                //
                //  Type Exception
                //
                mOOS.writeByte(MEPInputStream.TYPE_EXCEPTION);
                
                //
                //  Version 1
                //
                mOOS.writeByte(MEPInputStream.VERSION_1);
                
                //
                //  We always write the exchangeId.
                //
                mOOS.writeUTF(id);
                
                //
                //  The exception info.
                //
                mOOS.writeObject(e);
                
                //
                //  Get the resulting bytes.
                //
                mOOS.close();
                mOOS = null;
                bytes = mOS.toByteArray();
                mOS.reset();
                return (bytes);
            }
            catch (java.io.IOException ioEx)
            {
               return (null); 
            }
        }
        
        public byte[] writeIsMEPOk(ExchangeEntry ee)
            throws javax.jbi.messaging.MessagingException
        {
            byte[]              bytes;
            boolean             send;
            NormalizedMessage   msg;
            Set                 props;
            
            try
            {
                mOOS = new ObjectOutputStream(this);

                //
                //  Type Exchange
                //
                mOOS.writeByte(MEPInputStream.TYPE_ISMEPOK);

                //
                //  Version 1.
                //
                mOOS.writeByte(MEPInputStream.VERSION_1);

                //
                //  Send the related exchangeId.
                //
                mOOS.writeUTF(ee.getRelatedExchange().getExchangeId());

                //
                //  Send the contents of the Exchange.
                //
                writeExchange(ee.getMessageExchange(), ee);
                
                //
                //  Get the resulting bytes.
                //
                mOOS.close();
                mOOS = null;
                bytes = mOS.toByteArray();
                mOS.reset();
                ee.sendBytes(bytes.length);
                return (bytes);
            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(LocalStringKeys.MEPOUTPUT_IO_ERROR), ioEx);
            }
        }

        public byte[] writeMEPOk(String id, boolean okay)
        {
            byte[]      bytes;
            
            try
            {
                mOOS = new ObjectOutputStream(this);
                //
                //  Type Exception
                //
                mOOS.writeByte(MEPInputStream.TYPE_MEPOK);
                
                //
                //  Version 1
                //
                mOOS.writeByte(MEPInputStream.VERSION_1);
                
                //
                //  We always write the exchangeId.
                //
                mOOS.writeUTF(id);
                
                //
                //  The exception info.
                //
                mOOS.writeBoolean(okay);
                
                //
                //  Get the resulting bytes.
                //
                mOOS.close();
                mOOS = null;
                bytes = mOS.toByteArray();
                mOS.reset();
                return (bytes);
            }
            catch (java.io.IOException ioEx)
            {
               return (null); 
            }
                
        }
        
        public byte[] writeMEP(MessageExchange me, ExchangeEntry ee)
            throws javax.jbi.messaging.MessagingException
        {
            byte[]              bytes;
            boolean             send;
            NormalizedMessage   msg;
            Set                 props;
            
            try
            {
                mOOS = new ObjectOutputStream(this);

                //
                //  Type Exchange
                //
                mOOS.writeByte(MEPInputStream.TYPE_MEP);

                //
                //  Version 1.
                //
                mOOS.writeByte(MEPInputStream.VERSION_1);

                //
                //  Send the contents of the Exchange.
                //
                writeExchange(me, ee);
                
                //
                //  Get the resulting bytes.
                //
                mOOS.close();
                mOOS = null;
                bytes = mOS.toByteArray();
                mOS.reset();
                ee.sendBytes(bytes.length);
                return (bytes);
            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(LocalStringKeys.MEPOUTPUT_IO_ERROR), ioEx);
            }
        }

        void writeExchange(MessageExchange me, ExchangeEntry ee)
            throws javax.jbi.messaging.MessagingException
        {
            boolean            send;
            NormalizedMessage   msg;
            Set                 props;

            try
            {
                //
                //  We always write the exchangeId.
                //
                mOOS.writeUTF(me.getExchangeId());   

                //
                //  The following are only sent the first time.
                //
                mOOS.writeBoolean(send = !ee.checkStatus(ExchangeEntry.STATUS_FIRSTSENT));
                if (send)
                {
                    mOOS.writeUTF(me.getPattern().toString());
                    writeQName(me.getEndpoint().getServiceName());
                    mOOS.writeUTF(me.getEndpoint().getEndpointName());
                    writeQName(me.getOperation());
                    ee.setStatus(ExchangeEntry.STATUS_FIRSTSENT);
                }

                //
                //  Conditional send of the status.
                //
                send = (me.getStatus() != ExchangeStatus.ACTIVE) &&
                    !ee.checkStatus(ExchangeEntry.STATUS_STATUSSENT);
                mOOS.writeBoolean(send);
                if (send)
                {
                    mOOS.writeUTF(me.getStatus().toString());
                    ee.setStatus(ExchangeEntry.STATUS_STATUSSENT);
                }

                //
                //  Conditional send of the exception.
                //
                send = (me.getError() != null) &&
                    !ee.checkStatus(ExchangeEntry.STATUS_ERRORSENT);
                mOOS.writeBoolean(send);
                if (send)
                {
                    mOOS.writeObject(me.getError());
                    ee.setStatus(ExchangeEntry.STATUS_ERRORSENT);
                }

                //
                //  Conditional send of the fault message
                //
                send = ((msg = me.getFault()) != null) &&
                    !ee.checkStatus(ExchangeEntry.STATUS_FAULTSENT);
                mOOS.writeBoolean(send);
                if (send)
                {
                    writeMessage(msg);
                    ee.setStatus(ExchangeEntry.STATUS_FAULTSENT);
                }

                //
                //  Conditional send of the in message
                //
                send = ((msg = me.getMessage("in")) != null) &&
                    !ee.checkStatus(ExchangeEntry.STATUS_INSENT);
                mOOS.writeBoolean(send);
                if (send)
                {
                    writeMessage(msg);
                    ee.setStatus(ExchangeEntry.STATUS_INSENT);
                }

                //
                //  Conditional send of the out message
                //
                send = ((msg = me.getMessage("out")) != null) &&
                    !ee.checkStatus(ExchangeEntry.STATUS_OUTSENT);
                mOOS.writeBoolean(send);
                if (send)
                {
                    writeMessage(msg);
                    ee.setStatus(ExchangeEntry.STATUS_OUTSENT);
                }                

                //
                //  See if any properties have changed during this step of the exchange.
                //
                send = (props = ((com.sun.jbi.messaging.MessageExchange)me).getDeltaProperties()).size() > 0;
                mOOS.writeBoolean(send);
                if (send)
                {
                    writeMEProperties(props);
                }
            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                        Translator.translate(LocalStringKeys.MEPOUTPUT_IO_ERROR), ioEx);
            }
            
       }
        
        void writeMEProperties(Set props)
            throws javax.jbi.messaging.MessagingException
        {
            Map.Entry   entry = null;
            Object      value;
            boolean    writeProps;
            
            try
            {
                mOOS.writeInt(props.size());            
                for (Iterator i = props.iterator(); i.hasNext();)
                {
                    entry = (Map.Entry)i.next();
                    mOOS.writeUTF((String)entry.getKey());
                    mOOS.writeObject(entry.getValue());
                }
            }
            catch (java.io.NotSerializableException nsEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPOUTPUT_ME_PROP_ERROR, entry.getKey()), nsEx);

            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPOUTPUT_ME_PROP_ERROR, entry.getKey()), ioEx);
            }
        }
        
        void writeMessage( NormalizedMessage nm)
            throws java.io.IOException,
                   javax.jbi.messaging.MessagingException
        {
            mOOS.writeBoolean(nm != null);
            if (nm != null)
            {
                mOOS.writeBoolean(!(nm instanceof Fault));
                writeNMProperties(nm);
                try
                {
                    Source     c = nm.getContent();
                    
                    mOOS.writeBoolean(c != null);
                    if (c != null)
                    {
                        String      systemId = c.getSystemId();
                        
                        mOOS.writeBoolean(systemId != null);
                        if (systemId != null)
                        {
                            mOOS.writeUTF(systemId);
                        }
//                        {
//                            ByteArrayOutputStream   baos;
//                            mTransform.transform(c, new StreamResult(baos = new ByteArrayOutputStream()));
//                            MEPInputStream.dumpBuffer("Normalized Message: ", baos.toByteArray(), baos.size());
//                        }
                        startStream();
                        mTransform.transform(c, new StreamResult(this));
                        endStream();
                    }
                }
                catch (javax.xml.transform.TransformerException tEx)
                {
                    throw new javax.jbi.messaging.MessagingException(tEx);
                }
                writeAttachments(nm);
            }

        }

        void writeNMProperties(NormalizedMessage nm)
            throws javax.jbi.messaging.MessagingException
        {
            Set    props = nm.getPropertyNames();
            String  name = null;
            Object  value;
            
            try
            {
                mOOS.writeInt(props.size());            
                for (Iterator i = props.iterator(); i.hasNext();)
                {
                    name = (String)i.next();
                    value = nm.getProperty(name);
                    mOOS.writeUTF(name);
                    mOOS.writeObject(value);
                }
            }
            catch (java.io.NotSerializableException nsEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPOUTPUT_NM_PROP_ERROR, name), nsEx);

            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPOUTPUT_NM_PROP_ERROR, name), ioEx);
            }
        }

        void writeAttachments(NormalizedMessage nm)
            throws javax.jbi.messaging.MessagingException
        {
            Set             props = nm.getAttachmentNames();
            String          name = null;
            DataHandler     value;
            
            try
            {
                mOOS.writeInt(props.size());            
                for (Iterator i = props.iterator(); i.hasNext();)
                {
                    name = (String)i.next();
                    value = nm.getAttachment(name);
                    mOOS.writeUTF(name);
                    mOOS.writeUTF(value.getContentType());
                    mOOS.writeUTF(value.getName());
                    startStream();
                    value.writeTo(this);
                    endStream();
                }
            }
            catch (java.io.NotSerializableException nsEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPOUTPUT_ME_ATTACH_ERROR, name), nsEx);
            }
            catch (java.io.IOException ioEx)
            {
                throw new javax.jbi.messaging.MessagingException(
                    Translator.translate(LocalStringKeys.MEPOUTPUT_ME_ATTACH_ERROR, name), ioEx);
            }
        }

        void writeQName(QName qname)
            throws java.io.IOException
        {
            mOOS.writeUTF(qname.getNamespaceURI());
            mOOS.writeUTF(qname.getLocalPart());
            mOOS.writeUTF(qname.getPrefix());
            
        }
        
        public void close()
            throws java.io.IOException
        {
            while (mStreams > 0)
            {
                endStream();
            }
            flush();
            mOS.write(0);
            mOS.write(0);
            mOS.write(0);
            mOS.flush();
            mOS.close();
        }
        
        public void flush()
            throws java.io.IOException
        {
            if (mStreams == 0)
            {
                if (mCount != 0)
                {
                    blockFlush();
                }
                mCount = 0;
            }
        }
        
        public void write(byte[] b)
            throws java.io.IOException
        {
            write(b, 0, b.length);
        }
        
        public void write(byte[] b, int off, int len)
            throws java.io.IOException
        {
            while (mCount + len >= MEPInputStream.BUFFER_SIZE)
            {
                System.arraycopy(b, off, mBytes, mCount, MEPInputStream.BUFFER_SIZE - mCount);
                off += MEPInputStream.BUFFER_SIZE - mCount;
                len -= MEPInputStream.BUFFER_SIZE - mCount;
                mCount = MEPInputStream.BUFFER_SIZE;
                blockFlush();
            }
            if (len != 0)
            {
                System.arraycopy(b, off, mBytes, mCount, len);
                mCount += len;
            }
       }
       
        public void write(int b)
            throws java.io.IOException
        {
            mBytes[mCount++] = (byte)b;
            if (mCount >= MEPInputStream.BUFFER_SIZE)
            {
                blockFlush();
            }
        }
        
        private void startStream()
            throws java.io.IOException
        {
            mOOS.flush();
            blockFlush();
            mStreams++;
            write(mStreams);
        }
 
        private void endStream()
            throws java.io.IOException
        {
            mOOS.flush();
            blockFlush();
            mStreams--;
        }
        
        private void blockFlush()
            throws java.io.IOException
        {
            if (mCount != 0)
            {
                if (mLog.isLoggable(java.util.logging.Level.FINE))
                {
                    MEPInputStream.dumpBuffer("MEPOutputStream ("+ mStreams + ") Len (" + mCount + ")", mBytes, mCount);
                }
                mOS.write(mStreams);
                mOS.write(mCount >> 8);
                mOS.write(mCount & 255);
                mOS.write(mBytes, 0, mCount);
                mCount = 0;
            }
        }
}
