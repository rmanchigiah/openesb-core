/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestRegistrationInfo.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;

/**
 * Tests for the InstanceEntry class
 *
 * @author Sun Microsystems, Inc.
 */
public class TestRegistrationInfo extends junit.framework.TestCase
{    
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestRegistrationInfo(String aTestName)
    {
        super(aTestName);
    }
    

    /**
     * Setup for the test. This creates the ComponentRegistry instance
     * and other objects needed for the tests.
     * @throws Exception when set up fails for any reason.
     */
    public void setUp()
        throws Exception
    {
        super.setUp();
   }

    /**
     * Cleanup for the test.
     * @throws Exception when tearDown fails for any reason.
     */
    public void tearDown()
        throws Exception
    {
        super.tearDown();
    }

// =============================  test methods ================================

    /**
     * testGetters
     * @throws Exception if an unexpected error occurs
     */
    public void testGetters()
           throws Exception
    {
        QName               sn;
        ServiceEndpoint     se = new ServiceEndpointImpl(sn = new QName("service"), "endpoint");
        RegistrationInfo    ri;
        
        ri = new RegistrationInfo(se, "i", null, "add");
        assertEquals(ri.getInstanceId(), "i");
        assertEquals(ri.getEndpointName(), "endpoint");
        assertEquals(ri.getServiceName(), sn);
        assertEquals(ri.getClassLoader(), null);
        assertEquals(ri.getAction(), "add");       
    }
    
    /**
     * testHashCode
     * @throws Exception if an unexpected error occurs
     */
    public void testHashCode()
           throws Exception
    {
        QName               sn = new QName("service");
        QName               sn2 = new QName("service", "uri");
        String              e = "endpoint";
        String              e2 = "endpoint2";
        
        ServiceEndpoint     se = new ServiceEndpointImpl(sn, e);
        ServiceEndpoint     se2 = new ServiceEndpointImpl(sn2, e);
        ServiceEndpoint     se3 = new ServiceEndpointImpl(sn, e2);
        ServiceEndpoint     se4 = new ServiceEndpointImpl(sn2, e2);
        RegistrationInfo    ria = new RegistrationInfo(se, "i", null, "add");
        RegistrationInfo    rib = new RegistrationInfo(se, "i", null, "add");
        RegistrationInfo    ria2 = new RegistrationInfo(se2, "i", null, "add");
        RegistrationInfo    rib2 = new RegistrationInfo(se2, "i", null, "add");
        RegistrationInfo    ria3 = new RegistrationInfo(se3, "i", null, "add");
        RegistrationInfo    rib3 = new RegistrationInfo(se3, "i", null, "add");
        RegistrationInfo    ria4 = new RegistrationInfo(se4, "i", null, "add");
        RegistrationInfo    rib4 = new RegistrationInfo(se4, "i", null, "add");
        
        assertEquals(ria.hashCode(), rib.hashCode());
        assertEquals(ria2.hashCode(), rib2.hashCode());
        assertEquals(ria3.hashCode(), rib3.hashCode());
        assertEquals(ria4.hashCode(), rib4.hashCode());
    }

    /**
     * testEquals
     * @throws Exception if an unexpected error occurs
     */
    public void testEquals()
           throws Exception
    {
        QName               sn = new QName("service");
        QName               sn2 = new QName("service", "uri");
        String              e = "endpoint";
        String              e2 = "endpoint2";
        
        ServiceEndpoint     se = new ServiceEndpointImpl(sn, e);
        ServiceEndpoint     se2 = new ServiceEndpointImpl(sn2, e);
        ServiceEndpoint     se3 = new ServiceEndpointImpl(sn, e2);
        ServiceEndpoint     se4 = new ServiceEndpointImpl(sn2, e2);
        RegistrationInfo    ria = new RegistrationInfo(se, "i", null, "add");
        RegistrationInfo    rib = new RegistrationInfo(se, "i", null, "add");
        RegistrationInfo    ria2 = new RegistrationInfo(se2, "i", null, "add");
        RegistrationInfo    rib2 = new RegistrationInfo(se2, "i", null, "add");
        RegistrationInfo    ria3 = new RegistrationInfo(se3, "i", null, "add");
        RegistrationInfo    rib3 = new RegistrationInfo(se3, "i", null, "add");
        RegistrationInfo    ria4 = new RegistrationInfo(se4, "i", null, "add");
        RegistrationInfo    rib4 = new RegistrationInfo(se4, "i", null, "add");
        
        assertTrue(ria.equals(rib));
        assertTrue(rib.equals(ria));
        assertTrue(ria2.equals(rib2));
        assertTrue(rib2.equals(ria2));
        assertTrue(ria3.equals(rib3));
        assertTrue(rib3.equals(ria3));
        assertTrue(ria4.equals(rib4));
        assertTrue(rib4.equals(ria4));
    }
}
