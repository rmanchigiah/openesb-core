/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import java.util.ArrayList;

/**
 * Implementation of ServiceEndpoint for junit test cases.
 *
 * @author Sun Microsystems, Inc.
 */
public class EventImpl implements com.sun.jbi.binding.proxy.connection.Event
{    
   ArrayList    mList;
    int         mReadIndex;
    
    EventImpl()
    {
        mList = new ArrayList();
        mReadIndex = 0;
   }
    
    public String getEventName()
    {
        return (null);
    }
    
	public String getSender()
	{
		return (null);
	}

    public String getString()
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        if (mReadIndex < mList.size())
        {
            Object      o = mList.get(mReadIndex++);
            if (o instanceof String)
            {
                return ((String)o);
            }
        }
        throw new com.sun.jbi.binding.proxy.connection.EventException("Size or type mismatch");
    }
    
    public long getLong()
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        if (mReadIndex < mList.size())
        {
            Object      o = mList.get(mReadIndex++);
            if (o instanceof Long)
            {
                return (((Long)o).longValue());
            }
        }
        throw new com.sun.jbi.binding.proxy.connection.EventException("Size or type mismatch");
        
    }
    
    public void putString(String value)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        mList.add(value);
    }
    
    public void putLong(long value)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
       mList.add(new Long(value)); 
    }
    
    public Object getObject()
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        if (mReadIndex < mList.size())
        {
            Object      o = mList.get(mReadIndex++);
            return (o);
        }
        throw new com.sun.jbi.binding.proxy.connection.EventException("Size or type mismatch");        
        
    }
    
    public void putObject(Object value)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        mList.add(value);
    }
    
}
