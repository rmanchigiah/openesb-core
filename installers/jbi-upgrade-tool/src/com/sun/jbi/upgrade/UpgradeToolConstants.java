/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)UpgradeToolConstants.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.upgrade;

import java.io.File;

/**
 *
 * This class stores all the constants required
 */
public class UpgradeToolConstants {

    public static final String USAGE = "jbi.upgrade.tool.usage";
    public static final String EXCEPTION_SUMMARY =
                                   "jbi.upgrade.tool.exception.summary";
    public static final String JBI_CORE_UPGRADE_TOOL_JAR =
                                              "jbi-upgrade-tool.jar";
    public static final String RESOURCE_BUNDLE = "com.sun.jbi.upgrade.Bundle";
    public static final String DEFAULT_LOGGER_NAME =
                                   "com.sun.jbi.upgrade.JBIUpgradeTool";

    public static final String APPSERVER_DOMAINS_DIR = "domains";
    public static final String JBI_INSTALL_DIR = "jbi";
    public static final String JBI_COMPONENT = "component";
    public static final String JBI_COMPONENTS_DIR = "components";
    public static final String JBI_SHARED_LIBRARY = "shared-library";
    public static final String JBI_SHARED_LIBRARIES_DIR = "shared-libraries";
    public static final String JBI_SERVICE_ASSEMBLY = "service-assembly";
    public static final String JBI_SERVICE_ASSEMBLIES_DIR = "service-assemblies";
    public static final String DEFAULT_INSTALL = "default-install";
    public static final String FILE_NAME = "file-name";
    public static final String UPGRADE_NUMBER = "upgrade-number";
    public static final String TIME_STAMP = "timestamp";
    public static final String SERVERS = "servers";
    public static final String CONFIGS = "configs";
    public static final String SERVER = "server";
    public static final String CLUSTERS = "clusters";
    public static final String CLUSTER = "cluster";
    public static final String COMPONENT_REF = "component-ref";
    public static final String SHARED_LIBRARY_REF = "shared-library-ref";
    public static final String SERVICE_ASSEMBLY_REF = "service-assembly-ref";
    public static final String JBI_INSTALL_ROOT_DIR = "install_root";
    public static final String NAME_REF = "name-ref";
    public static final String JBI_META_INF_DIR = "META-INF";
    public static final String INSTALL_ROOT = "install_root";
    public static final String COMPONENT_WORKSPACE = "workspace";
    public static final String ORIGINAL_VERSION_NUMBER = "original";

    public static final String JBI_REGISTRY_NOT_FOUND =
              "jbi.upgrade.tool.jbi.registry.not.found";

    public static final String JBI_REGISTRY_PARSING_ERROR =
              "jbi.upgrade.tool.jbi.registry.parsing.error";

    public static final String INSTALL_ROOT_NOT_FOUND =
              "jbi.upgrade.tool.jbi.appserverInstallRoot.not.found";

    public static final String DOMAIN_ROOT_NOT_FOUND =
              "jbi.upgrade.tool.jbi.domain.root.not.found";

    public static final String JBI_XML_NOT_FOUND =
              "jbi.upgrade.tool.jbi.xml.not.found";

    public static final String COMPONENT_INSTALL_JAR_NOT_FOUND =
              "jbi.upgrade.tool.component.install.jar.not.found";

    public static final String COMPONENT_INSTALL_ROOT_NOT_FOUND =
              "jbi.upgrade.tool.component.install.root.not.found";

    public static final String SHARED_LIB_INSTALL_ROOT_NOT_FOUND =
              "jbi.upgrade.tool.shared.lib.install.root.not.found";

    public static final String SERVICE_ASSEMBLY_INSTALL_ROOT_NOT_FOUND =
              "jbi.upgrade.tool.service.assembly.install.root.not.found";

    public static final String COMPONENT_DIR_NOT_FOUND =
              "jbi.upgrade.tool.component.dir.not.found";

    public static final String MORE_THAN_TWO_COMPONENT_INSTALLJARS_FOUND =
              "jbi.upgrade.tool.more.than.two.component.dirs.found";

    public static final String UNKNOWN_IO_EXCEPTION =
              "jbi.upgrade.tool.unknown.io.exception";

    public static final String JBI_XML_PARSING_EXCEPTION =
              "jbi.upgrade.tool.jbi.xml.parsing.error";

    public static final String COMPONENT_FOUND_IN_DEST_DOMAIN =
              "jbi.upgrade.tool.component.found.in.dest.domain";

    public static final String DESTINATION_DOMAIN_ROOT_NOT_FOUND =
              "jbi.upgrade.tool.dest.domain.root.not.found";

    public static final String SOURCE_DOMAIN_ROOT_NOT_FOUND =
              "jbi.upgrade.tool.src.domain.root.not.found";

    public static final String SHARED_LIB_FOUND_IN_DEST_DOMAIN =
              "jbi.upgrade.tool.shared.lib.found.in.dest.domain";

    public static final String SERVICE_ASSEMBLY_FOUND_IN_DEST_DOMAIN =
              "jbi.upgrade.tool.service.assembly.found.in.dest.domain";

    public static final String UNJAR_ERROR =
              "jbi.upgrade.tool.io.unjar.error";

    public static final String DIRECTORY_NOT_FOUND =
              "jbi.upgrade.tool.directory.not.found";

    public static final String CAN_NOT_CREATE_FILE =
              "jbi.upgrade.tool.can.not.create.file";

    public static final String IO_EXCEPTION_DURING_FILE_COPY =
              "jbi.upgrade.tool.io.exception.during.file.copy";

    public static final String UPGRADING_SYSTEM_COMPONENTS =
              "jbi.upgrade.tool.info.upgradeing.system.components";

    public static final String PARSING_JBI_REGISTRY =
              "jbi.upgrade.tool.info.parsing.jbi.registry";

    public static final String STARTING_COMPONENT_UPGRADE =
              "jbi.upgrade.tool.info.starting.component.upgrade";

    public static final String UPGRADE_COMPONENT_SUCCESSFUL =
              "jbi.upgrade.tool.info.upgrade.component.successful";

    public static final String NEWER_VERSION_COMPONENT_FOUND =
              "jbi.upgrade.tool.info.newer.version.component.found";

    public static final String UPGRADING_SYSTEM_SHARED_LIBRARIES =
              "jbi.upgrade.tool.info.upgradeing.system.shared.libraries";

    public static final String STARTING_SHARED_LIBRARY_UPGRADE =
              "jbi.upgrade.tool.info.starting.shared.library.upgrade";

    public static final String UPGRADE_SHARED_LIBRARY_SUCCESSFUL =
              "jbi.upgrade.tool.info.upgrade.shared.library.successful";

    public static final String NEWER_VERSION_SHARED_LIBRARY_FOUND =
              "jbi.upgrade.tool.info.newer.version.shared.library.found";

    public static final String MIGRATING_NON_SYSTEM_COMPONENTS =
              "jbi.upgrade.tool.info.migrating.non.system.components";

    public static final String MIGRATING_NON_SYSTEM_SHARED_LIBRARIES =
              "jbi.upgrade.tool.info.migrating.non.system.shared.libraries";

    public static final String MIGRATING_SERVICE_ASSEMBLIES =
              "jbi.upgrade.tool.info.migrating.service.assemblies";

    public static final String STARTING_SERVICE_ASSEMBLIY_UPGRADE =
              "jbi.upgrade.tool.info.starting.service.assembly.upgrade";

    public static final String UPGRADE_SERVICE_ASSEMBLY_SUCCESSFUL =
              "jbi.upgrade.tool.info.upgrade.service.assembly.successful";

    public static final String ERROR_WHEN_UPDATING_JBI_REGISTRY =
              "jbi.upgrade.tool.error.when.updating.jbi.registry";

}
