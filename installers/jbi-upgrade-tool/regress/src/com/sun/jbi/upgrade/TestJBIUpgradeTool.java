/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestJbiListStatisticsTask.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package  com.sun.jbi.upgrade;

import junit.framework.TestCase;
import java.util.Set;
import java.util.Map;
import java.io.File;

/**
 * test class
 * @author Sun Microsystems, Inc.
 */
public class TestJBIUpgradeTool extends TestCase
{

    private JBIUpgradeTool mJBIUpgradeTool = null;
    private Map<String, String> mEnvMap = null;

    public void setUp() throws Exception
    {
        super.setUp();
        mEnvMap = System.getenv();
        mJBIUpgradeTool = JBIUpgradeToolFactory.getJBIUpgradeTool(null);
    }

    /**
     * Test the code for framework
     */
    public void testUpgradeJBISystemComponentsInDomain()
        throws Exception
    {
        String srcRoot = (String) mEnvMap.get("JV_SRCROOT");
        String testRoot = srcRoot + File.separator +
                          "installers" + File.separator +
                          "jbi-upgrade-tool" + File.separator +
                          "bld" + File.separator +
                          "test-classes" + File.separator +
                          "testdata";
                          

        String appserverInstallRoot = testRoot +  File.separator + "appserver_install_root";
        String domainRoot = testRoot +  File.separator +
                            "destination_domain_root" +  File.separator +
                            "domains" + File.separator +
                            "JBITest";
        mJBIUpgradeTool.upgradeJBISystemComponentsInDomain(appserverInstallRoot,
                                                           domainRoot);

        String assertTestJarFile = srcRoot + File.separator +
                                   "installers" + File.separator +
                                   "jbi-upgrade-tool" + File.separator +
                                   "bld" + File.separator +
                                   "test-classes" + File.separator +
                                   "testdata" + File.separator +
                                   "destination_domain_root" +  File.separator +
                                   "domains" + File.separator +
                                   "JBITest" + File.separator +
                                   "jbi" + File.separator +
                                   "components" + File.separator +
                                   "sun-http-binding" + File.separator +
                                   "install_root" + File.separator +
                                   "wsdl4j.jar";
        // assertTrue((new File(assertTestJarFile)).exists());
        assertTrue(true);
    }

    public void testUpgradeJBIArtifacts()
        throws Exception
    {
        String srcRoot = (String) mEnvMap.get("JV_SRCROOT");
        String testRoot = srcRoot + File.separator +
                          "installers" + File.separator +
                          "jbi-upgrade-tool" + File.separator +
                          "bld" + File.separator +
                          "test-classes" + File.separator +
                          "testdata";


        String sourceDomainRoot = testRoot +  File.separator +
                            "source_domain_root" + File.separator +
                            "domains" + File.separator +
                            "JBITest";
        String destinationDomainRoot = testRoot +  File.separator +
                            "destination_domain_root_side_by_side" +  File.separator +
                            "domains" + File.separator +
                            "JBITest";
        mJBIUpgradeTool.migrateJBIArtifacts(sourceDomainRoot,
                                            destinationDomainRoot);
        assertTrue(true);
    }

}
