#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)make_jbiroot.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#
#make_jbiroot - create an JBI_HOME installation for running regression tests.
#
#WARNING:  this script MUST be installed by makedrv -c mmf in order to
#          correctly expand {=INIT_<name>=} macro definitions.
#          This requires a standard tools develoment environment.
#          Or, use the localinstall.ksh, localrestore.ksh to test in
#          your local $TOOLROOT.
#
#  20-Feb-2004 (russt)
#       initial version

############################### USAGE ROUTINES ################################

usage()
{
    status=$1
    cat << EOF

Usage:  $p [-help] [-verbose] [-product kenai|shasta|lassen|whitney]  [config_name]

 Create an JBI_HOME installation for <config_name>.
 The currently recognized configuration names are:
 
`echo $STD_DOMAIN_NAMES | perl -n -a -F'/\s+/' -e '{for (@F) {print "    $_\n";}}'`

 If a <config_name> is not provided, then both domains are created.

 The JBI_HOME installations are placed in:

    \$AS8BASE/jbi if -product = kenai
    \$JBISE_BASE/jbi if -product = jbise

Options:
 -help           Display this message.
 -verbose        Verbose messages.
 -product bom    create installation for <bom>, where <bom> is kenai, shasta, lassen, or whitney.        

Environment:
 \$SRCROOT     build directory
 \$AS8BASE     base directory for generated installations (default: \$SRCROOT/install/as8).
 \$JBI_HOME    install dir for jbi (default: \$AS8BASE/jbi for kenai, \$JBISE_BASE/jbi for jbise).
EOF

    exit $status
}

parse_args()
{
    DOHELP=0
    VERBOSE=0
    PRODUCT="kenai"
    domain_name_args=""

    while [ $# -gt 0 -a "$1" != "" ]
    do
        arg=$1; shift

        case $arg in
        -h* )
            DOHELP=1
            ;;
        -v* )
            VERBOSE=1
            ;;
        -product )
            if [ $# -gt 0 ]; then
                PRODUCT=$1; shift
            else
                echo "${p}: -product requires the name of the top-level bom (kenai|jbise)"
                usage 1
            fi
            ;;
        -* )
            echo "${p}: unknown option, $arg"
            usage 1
            ;;
        * )
            if [ "$domain_name_args" = "" ]; then
                domain_name_args="$arg"
            else
                domain_name_args="$domain_name_args $arg"
            fi
            ;;
        esac
    done

    if [ "$domain_name_args" = "" ]; then
        DOMAIN_LIST="$STD_DOMAIN_NAMES"
    else
        DOMAIN_LIST="$domain_name_args"
    fi

    [ $VERBOSE -eq 1 ] && bldmsg -p $p INSTALLING DOMAINS: $DOMAIN_LIST

}

init()
{
    p=`basename $0`

    export PS
    echo $PATH | grep ';' > /dev/null
    if [ $? -eq 0 ]; then
        PS=';'
    else
        PS=':'
    fi

    #these are the standard domain names that we know about:
    if [ "$JBI_TEST_DOMAIN_NAMES" = "" ]; then
        #STD_DOMAIN_NAMES="domain1 JBITest ESBTest CAS ESBMember"
        STD_DOMAIN_NAMES="domain1 JBITest ESBTest CAS"
    else
        STD_DOMAIN_NAMES="JBI_TEST_DOMAIN_NAMES"
    fi
    
    CAS_INSTANCES_LIST="instance1 sync CAS-cluster1-inst1"
    CAS_CLUSTER_NAME="CAS-cluster1"
}

check_environment()
#check that we have essential environment variables defined.
#WARNING:  do not echo anything to stdout - it will interfere with -showdbuser
{
    if [ "$AS8BASE" = "" ]; then
        AS8BASE=$SRCROOT/install/as8
        1>&2 bldmsg -p $p "NOTE: defaulting AS8BASE to '$AS8BASE'"
    fi

    if [ "$JBISE_BASE" = "" ]; then
        JBISE_BASE=$SRCROOT/install/jbise
        1>&2 bldmsg -p $p "NOTE: defaulting JBISE_BASE to '$JBISE_BASE'"
    fi


    #set our jbi home:
    if [ "$JBI_HOME" = "" ]; then
        if [ $PRODUCT = kenai ]; then
            JBI_HOME=$AS8BASE/jbi
        elif [ $PRODUCT = jbise ]; then
            JBI_HOME=$JBISE_BASE/jbi
        fi

        1>&2 bldmsg -p $p "NOTE: defaulting JBI_HOME to '$JBI_HOME'"
    fi



    if [ "$FORTE_PORT" = "cygwin" ]; then
        JV_SRCROOT=`cygpath -m "$SRCROOT"`
        JV_AS8BASE=`cygpath -m "$AS8BASE"`
        JV_JBI_HOME=`cygpath -m "$JBI_HOME"`
    else
        JV_SRCROOT="$SRCROOT"
        JV_AS8BASE="$AS8BASE"
        JV_JBI_HOME="$JBI_HOME"
    fi
}

check_create_tmp()
{
    if [ ! -d "/tmp" ]; then
        installdir -m 0777 /tmp
    fi
}

shutdown_jbi()
{
    #make sure all domains are shutdown:
    for domain in $DOMAIN_LIST
    do
        JBI_DOMAIN_ROOT=$AS8BASE/domains/$domain
        shutdown_domain "$JBI_DOMAIN_ROOT"
    done
}
remove_jbi()
{

    #remove the jbi installation:
    bld_killall $JBI_HOME
}

shutdown_domain()
#Usage:  shutdown_domain full_path_to_a_domain
{
    _return_status=0

    _shutdown_errs=0

    if [ "$1" = "" ]; then
        bldmsg -error -p $p/shutdown_domain You must supply the full path to a domain.
        _shutdown_errs=1
    elif [ ! -d "$1" ]; then
        bldmsg -error -p $p/shutdown_domain Domain $1 does not exist or is not a directory.
        _shutdown_errs=1
    else
        _domain_base="`dirname $1`"
        _domain_name="`basename $1`"
    fi

    asadmin=$AS8BASE/bin/asadmin

    #we must have an appserver installation:
    $asadmin list-domains > $TMPC
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p/shutdown_domain Command "'$asadmin list-domains'" FAILED
        _shutdown_errs=1
    fi

    if [ $_shutdown_errs -ne 0 ]; then
        bldmsg -error -p $p/shutdown_domain One or more errors encountered - please correct and try again.
        return 0
    fi

    grep 'running' $TMPC | grep -v 'not running'
    if [ $? -eq 0 ]; then
        for domain in `grep 'running' $TMPC | grep -v 'not running' | awk '{ print $1}'`
        do
            if [ "$domain" = "$1" ]; then
                bldmsg -markbeg -p $p shutdown domain $domain
                $asadmin --domaindir "$_domain_base" stop-domain "$domain"
                status=$?

                if [ $status -ne 0 ]; then
                    bldmsg -error -p $p shutdown failed for domain $domain
                    _shutdown_errs=1
                fi

                bldmsg -markend -status $status -p $p shutdown domain $domain

                #only work on one domain:
                break
            fi
        done
    else
        bldmsg -mark -p $p domain $_domain_name is not currently running.
    fi

    return $_shutdown_errs
}

install_kenai()
#install some libraries in $AS8BASE/lib that are needed for regress setup 
{
    bldmsg -mark -p $p INSTALLING kenai for $AS8BASE
    cmd="$AS8BASE/bin/asant -emacs -DAS_INSTALL='$JV_AS8BASE' -DJBI_HOME='$JV_JBI_HOME' -f '$JV_JBI_HOME/install/install_jbi_domain.ant' install_kenai"
    [ $VERBOSE -eq 1 ] && bldmsg -p $p $cmd
    eval $cmd
    return $?
}


install_jbi_common()
#install jbi libraries and files in the appserver area.
#NOTE: currently this is a hack, as we are writing in $AS8BASE
{
    bldmsg -mark -p $p INSTALLING jbi common files for $AS8BASE
    cmd="$AS8BASE/bin/asant -emacs -DAS_INSTALL='$JV_AS8BASE' -DJBI_HOME='$JV_JBI_HOME' -f '$JV_JBI_HOME/install/install_jbi_domain.ant' install_common"
    [ $VERBOSE -eq 1 ] && bldmsg -p $p $cmd
    eval $cmd
    return $?
}

install_jbi_domain()
#install jbi libraries and files in the appserver area.
#NOTE: currently this is a hack, as we are writing in $AS8BASE
{
    bldmsg -mark -p $p INSTALLING jbi files for domain $JBI_DOMAIN_ROOT
    cmd="$AS8BASE/bin/asant -emacs -DAS_INSTALL='$JV_AS8BASE' -DJBI_HOME='$JV_JBI_HOME' -DJBI_DOMAIN_ROOT='$JV_JBI_DOMAIN_ROOT' -f '$JV_JBI_HOME/install/install_jbi_domain.ant' install_domain"
    [ $VERBOSE -eq 1 ] && bldmsg -p $p $cmd
    eval $cmd
    return $?
}

cleanup_domain()
#cleanup jbi files in the domain
{
    bldmsg -mark -p $p cleanup_domain for domain in $JBI_DOMAIN_ROOT
    cmd="$AS8BASE/bin/asant -emacs -DAS_INSTALL='$JV_AS8BASE' -DJBI_HOME='$JV_JBI_HOME' -DJBI_DOMAIN_ROOT='$JV_JBI_DOMAIN_ROOT' -f '$JV_JBI_HOME/install/install_jbi_domain.ant' cleanup_domain"
    [ $VERBOSE -eq 1 ] && bldmsg -p $p $cmd
    eval $cmd
    return $?
}

cleanup_standalone_instance()
#cleanup jbi files in the instance
{
    bldmsg -mark -p $p cleanup_instance for instance in $JBI_INSTANCE_ROOT
    cmd="$AS8BASE/bin/asant -emacs -DAS_INSTALL='$JV_AS8BASE' -DJBI_HOME='$JV_JBI_HOME' -DJBI_INSTANCE_ROOT='$JV_JBI_INSTANCE_ROOT' -DIS_CLUSTERED_INSTANCE="false" -f '$JV_JBI_HOME/install/install_jbi_domain.ant' cleanup_instance"
    [ $VERBOSE -eq 1 ] && bldmsg -p $p $cmd
    eval $cmd
    return $?
}

cleanup_clustered_instance()
#cleanup jbi files in the instance
{
    bldmsg -mark -p $p cleanup_instance for instance in $JBI_INSTANCE_ROOT
    cmd="$AS8BASE/bin/asant -emacs -DAS_INSTALL='$JV_AS8BASE' -DJBI_HOME='$JV_JBI_HOME' -DJBI_INSTANCE_ROOT='$JV_JBI_INSTANCE_ROOT' -DIS_CLUSTERED_INSTANCE="true" -DCLUSTER_NAME=$CAS_CLUSTER_NAME -f '$JV_JBI_HOME/install/install_jbi_domain.ant' cleanup_instance"
    [ $VERBOSE -eq 1 ] && bldmsg -p $p $cmd
    eval $cmd
    return $?
}

create_jbi_home()
#copy files into a new JBI_HOME
# Usage:  create_jbi_home
{
    [ $VERBOSE ] && bldmsg -p $p Creating new JBI_HOME in $JBI_HOME

    #shutdown logic for jbise will have to be added later  
    if [ $PRODUCT = kenai ]; then
	shutdown_jbi
    fi

    remove_jbi

    #this is where makedrv writes to:
    RELEASE_DISTROOT=$TMPA
    rm -rf $RELEASE_DISTROOT

    #this is release reads from (<RELEASE> var in boms):
    RELEASE_ROOT=$TMPA

    ######
    #stage the release files with our env defs. Note -norc option:
    ######
    bldmsg -mark -p $p "build RELEASE_ROOT staging area"
    makedrv -norc -q -b jbiroot.cmn -c bdb:mmf > $TMPB 2>&1

    grep BUILD_ERROR $TMPB > $TMPC
    if [ $? -eq 0 ]; then
        cat $TMPC
        bldmsg -error -p $p/create_jbi_home "'makedrv -b jbiroot.cmn -c bdb:mmf' had errors - continuing"
    fi

    #######
    #create empty boms for doc - not really needed for development env.  RT 5/25/04
    #######
    JAVADOC_BASE=$SRCROOT/antbld/bld/doc
    for prod in kenai lassen shasta whitney
    do
        installdir $JAVADOC_BASE/$prod
        touch $JAVADOC_BASE/$prod/public_javadoc.bom
        touch $JAVADOC_BASE/$prod/private_javadoc.bom
    done


    ########
    #Release to JBI_HOME:
    ########
    bldmsg -mark -p $p "RELEASE kit to $JBI_HOME"
    release -q -bomloc $SRCROOT/rl/src/cmn/bom -w $JBI_HOME -nolog -nochecksum dev_${PRODUCT}.bom
    status=$?

    if [ $status -ne 0 ]; then
        bldmsg -error -p $p/create_jbi_home "Errors releasing to $JBI_HOME"
        return 1    #failure
    fi

    return 0    #success
}

################################## UTILITIES ##################################

require()
#import external shell routines - fatal error if we can't find it.
{
    libname=$1

    if [ x$libname = x ]; then
        echo "BUILD_ERROR: ${p}:require:  missing file name - ABORT"
        exit 1
    fi

    #look in a couple of familiar places:
    if [ -f "$TOOLROOT/lib/cmn/$libname" ]; then
        libname=$TOOLROOT/lib/cmn/$libname
    elif [ -f "./$libname" ]; then
        #we assume this is a test env!
        echo "$p - BUILD_WARNING: loading $libname from current directory."
        libname=./$libname
    fi

    . $libname
    if [ $? -ne 0 ]; then
        echo "BUILD_ERROR: ${p}:require: errors sourcing $libname - ABORT"
        exit 1
    fi
}

################################# HOUSEKEEPING #################################

cleanup()
{
    if [ x$TMPC != x ]; then
        rm -rf $TMPA $TMPB $TMPC
    fi
}

rec_signal()
{
    cleanup
    bldmsg -error -p $p Interrupted

    exit 2
}

#################################### MAIN #####################################

init

#source common build routines or die.
#NOTE - all "bld_<name>" routines come from this module:
require bldcmn.sh

parse_args "$@"

if [ $DOHELP -eq 1 ]; then
    usage 0
fi

make_jbiroot_status=0

check_create_tmp
[ $? -ne 0 ] && make_jbiroot_status=1

TMPA="/tmp/${p}_A.$$"
TMPB="/tmp/${p}_B.$$"
TMPC="/tmp/${p}_C.$$"
#trap interrupts:
trap rec_signal 2 15

export AS8BASE JBI_HOME

check_environment


create_jbi_home
[ $? -ne 0 ] && make_jbiroot_status=1 && bldmsg -p $p -error create_jbi_home FAILED

#if product is jbise, exit. proceed with the rest of the setup for jbigf
if [ $PRODUCT = jbise ]; then
    cleanup
    exit $make_jbiroot_status
fi

####
#calling install_kenai target in install_jbi_domain.ant and commenting out the calls
#to setup shasta regress 
####
install_kenai
[ $? -ne 0 ] && make_jbiroot_status=1 && bldmsg -p $p -error install_kenai for $AS8BASE FAILED

#install_jbi_common
#[ $? -ne 0 ] && make_jbiroot_status=1 && bldmsg -p $p -error install_jbi_common for $AS8BASE FAILED

#cleanup domain
for domain in $DOMAIN_LIST
do
    if [ $domain = CAS ]; then
        for instance in $CAS_INSTANCES_LIST
        do
            JBI_INSTANCE_ROOT=$AS8BASE/nodeagents/agent1/$instance
            JV_JBI_INSTANCE_ROOT=$JV_AS8BASE/nodeagents/agent1/$instance
            if [ $instance = CAS-cluster1-inst1 ]; then
                cleanup_clustered_instance
            else
                cleanup_standalone_instance
            fi
            [ $? -ne 0 ] && make_jbiroot_status=1 && bldmsg -p $p -error cleanup_instance for $JBI_INSTANCE_ROOT FAILED
        done
    fi
    
    JBI_DOMAIN_ROOT=$AS8BASE/domains/$domain
    JV_JBI_DOMAIN_ROOT=$JV_AS8BASE/domains/$domain
    cleanup_domain
    [ $? -ne 0 ] && make_jbiroot_status=1 && bldmsg -p $p -error cleanup_domain for $JBI_DOMAIN_ROOT FAILED
done



#install jbi bootstrap files:
jbiBootscript=$SRCROOT/antbld/regress/configure_esb_domains.ksh
if [ -r "$jbiBootscript" ]; then
    bldmsg -mark -p $p Running $jbiBootscript to install test domain ESB bootstrap properties
    sh $jbiBootscript
    if [ $? -ne 0 ]; then
        bldmsg -error -p $p FAILED:  $jbiBootscript
    fi
else
    bldmsg -error -p $p NOT FOUND:  $jbiBootscript
fi

cleanup

exit $make_jbiroot_status
