#!/usr/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)update_external_repos.sh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


usage()
{
    status=$1

    cat << EOF

    Usage:  update_external_repos (options...) 
            update_external_repos -build 060920_1
            update_external_repos -snapshot -build 060920_1


    Options:
    -help                 Display this message.
    -build <BUILD>        Use maven components from a given build.  DEFAULT is to use latest build 
    -snapshot             Use the SNAPSHOT components from a given build.  This is the DEFAULT  
    -release              Use the RELASE components from a given build.  Do NOT use this unless you are sure about it

EOF
    exit $status
}


parse_args()
{
  export MAVENDIR
  export DOHELP

  DOHELP=0
  MAVENDIR="snapshot"

  while [ $# -gt 0 -a "$1" != "" ]
  do
    arg=$1; shift
    case $arg in
        -h* )
         DOHELP=1
         ;;
        -snapshot )
           MAVENDIR="snapshot"
         ;;
        -release )
           MAVENDIR="release"
         ;;
        -build )
          if [ $# -gt 0 ]; then
            BLDDATE=$1; shift
          else
            echo "USAGE ERROR : -build requires the number of the build you want to use (060920)"
            usage 1
          fi
          ;;
        -* )
          echo "USAGE ERROR : unknown option, $arg"
          usage 1
          ;;
        * )
          echo "USAGE ERROR : unknown option, $arg"
          usage 1
          ;;
     esac 
   done
}



################# MAIN ######################
parse_args "$@"

if [ $DOHELP -eq 1 ]; then
   usage 0
else
   if [ ! -d "${KIT_DISTROOT}/${CODELINE}/Build${BLDDATE}/maven/publish/${MAVENDIR}-repository/open-esb" ]; then
     echo can NOT locate this directory: 
     echo "${KIT_DISTROOT}/${CODELINE}/Build${BLDDATE}/maven/publish/${MAVENDIR}-repository/open-esb" 
     exit
   fi

   if [ x$EXTERNAL_CVSROOT = "x" ]; then
     echo Need to set EXTERNAL_CVSROOT before running... exiting.
     exit
   fi

   if [ x$INTERNAL_CVSROOT = "x" ]; then
     echo Need to set INTERNAL_CVSROOT before running... exiting.
     exit
   fi


   echo "Starting to set the environment"
   echo "We will upload the maven components from this directory:"
   echo "${KIT_DISTROOT}/${CODELINE}/Build${BLDDATE}/maven/publish/${MAVENDIR}-repository/open-esb" 
   echo ""
   echo "EXTERNAL_CVSROOT is $EXTERNAL_CVSROOT"
   echo "INTERNAL_CVSROOT is $INTERNAL_CVSROOT"

   cd $SRCROOT 

   echo "Removing old repos_update dir" 
   rm -rf $SRCROOT/cvs_repos_logs
   echo "Removing old local external repos" 
   rm -rf $SRCROOT/cvs_repos_external
   echo "Removing old local internal repos" 
   rm -rf $SRCROOT/cvs_repos_internal

   mkdir $SRCROOT/cvs_repos_logs

   echo ""
   echo "Writing environment to $SRCROOT/cvs_repos_logs/env.log"
   echo "MAVEN components are : ${KIT_DISTROOT}/${CODELINE}/Build${BLDDATE}/maven/publish/${MAVENDIR}-repository/open-esb"  > $SRCROOT/cvs_repos_logs/env.log
   echo "EXTERNAL_CVSROOT is $EXTERNAL_CVSROOT"  >> $SRCROOT/cvs_repos_logs/env.log
   echo "INTERNAL_CVSROOT is $INTERNAL_CVSROOT"  >> $SRCROOT/cvs_repos_logs/env.log
   echo ""  >> $SRCROOT/cvs_repos_logs/env.log
   env >> $SRCROOT/cvs_repos_logs/env.log

   echo ""
   echo "Finished setting the environment"
   echo ""


   #1.  checkout internal/external

   echo "Checking out new local version of external repos... log is ${SRCROOT}/cvs_repos_logs/external_co.log"
   cvs -d $EXTERNAL_CVSROOT co -d cvs_repos_external open-esb > ${SRCROOT}/cvs_repos_logs/external_co.log 

   echo "Checking out new local version of internal repos... log is ${SRCROOT}/cvs_repos_logs/internal_co.log"
   cvs -d $INTERNAL_CVSROOT co -d cvs_repos_internal open-esb > ${SRCROOT}/cvs_repos_logs/internal_co.log
   echo ""

   #2 Remove all the stuff from SRCROOT/cvs_repos_internal you do not want copied to SRCROOT/cvs_repos_external
   echo "Fixing $SRCROOT/cvs_repos_internal to contain only what we want to move out to the external site"
   rm -rf $SRCROOT/cvs_repos_internal/arch
   rm -rf $SRCROOT/cvs_repos_internal/genesis
   rm -rf $SRCROOT/cvs_repos_internal/project
   #rm -rf $SRCROOT/cvs_repos_internal/t3
   rm -rf $SRCROOT/cvs_repos_internal/t3/src/jakarta
   rm -rf $SRCROOT/cvs_repos_internal/t3/src/netbeans
   rm -rf $SRCROOT/cvs_repos_internal/t3/src/other
   #rm -rf $SRCROOT/cvs_repos_internal/tl
   rm -rf $SRCROOT/cvs_repos_internal/tmp
   #rm -rf $SRCROOT/cvs_repos_internal/installers
   rm -rf $SRCROOT/cvs_repos_internal/installers/command-line
   rm -rf $SRCROOT/cvs_repos_internal/installers/gui
   rm -rf $SRCROOT/cvs_repos_internal/internal-packages
   rm -rf $SRCROOT/cvs_repos_internal/www
   echo "Finished fixing $SRCROOT/cvs_repos_internal"
   echo ""

   #2.B Remove all the stuff from SRCROOT/cvs_repos_external you do not want to have changed 
   echo "Fixing $SRCROOT/cvs_repos_external to contain only what we want to move out to the external site"
   rm -rf $SRCROOT/cvs_repos_external/www
   echo "Finished fixing $SRCROOT/cvs_repos_external"
   echo ""


   #3 Copy from kits server the repo stuff into cvs_repos_internal
   mkdir -p $SRCROOT/cvs_repos_internal/repo/open-esb
   cd $SRCROOT/cvs_repos_internal/repo/open-esb
   echo "cp -r ${KIT_DISTROOT}/${CODELINE}/Build${BLDDATE}/maven/publish/${MAVENDIR}-repository/open-esb/* ." 
   cp -r ${KIT_DISTROOT}/${CODELINE}/Build${BLDDATE}/maven/publish/${MAVENDIR}-repository/open-esb/* . 

   #4.  run ddiff
   echo "Determining changes with ddiff...."
   ddiff -fdiff -nocvs -table $SRCROOT/cvs_repos_internal $SRCROOT/cvs_repos_external > $SRCROOT/cvs_repos_logs/ddiff.log
   echo "Finished determining changes with ddiff"
   echo ""

   #5.  write a perl script that parses the ddiff table and generates the cvs commands.
   echo "Starting to parse ddiff log.... logs are under ${SRCROOT}/cvs_repos_logs/"
   perl $TOOLROOT/lib/cmn/parse_ddiff_logs.pl
   echo "Finished parsing ddiff logs and creating cvs statements.... logs are under ${SRCROOT}/logs/"
   echo ""

   #6.  rsync internal -> external, but don't delete anything in external yet
   echo "Using rsync to update external repos ... log is ${SRCROOT}/logs/rsync_update.log"
   ### ORG ###rsync -rctpogC -v $SRCROOT/cvs_repos_internal/ $SRCROOT/cvs_repos_external > ${SRCROOT}/cvs_repos_logs/rsync_update.log
   rsync -rctpog -v --exclude=CVS $SRCROOT/cvs_repos_internal/ $SRCROOT/cvs_repos_external > ${SRCROOT}/cvs_repos_logs/rsync_update.log
   echo "Finished rsync update"
   echo ""
fi

exit

### DO THESE COMMANDS BY HAND

## A ##
## run the cvs commands generated in the steps above to add and remove dirs and files  
##perl $TOOLROOT/lib/cmn/add_remove_cvs.pl

## B ##
## Manually check the files and check them in by hand from $SRCROOT/cvs_repos_external
