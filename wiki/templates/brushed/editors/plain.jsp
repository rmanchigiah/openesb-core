<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki"%>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.tags.*" %>
<%@ page import="com.ecyrd.jspwiki.ui.*" %>

<%--
        This is a plain editor for JSPWiki.
--%>
<%
  /* see commonheader.jsp */
  String prefEditAreaHeight = (String) session.getAttribute( "prefEditAreaHeight" );
  String defaultMetadata = "DefaultMetadata";

  WikiContext context = WikiContext.findContext( pageContext ); 
  WikiEngine engine = context.getEngine();
  String usertext = EditorManager.getEditedText( pageContext ); 
  /* small refactorings and moved back into brushed.js
  TemplateManager.addResourceRequest( context, "script", "scripts/searchreplace.js" );
   */
%>
  <wiki:CheckRequestContext context="edit">
  <wiki:NoSuchPage> <%-- cloning a page --%>
  <%
    String clone = request.getParameter( "clone" ); 
    if( clone != null )
    {
      WikiPage p = engine.getPage( clone );
      if( p != null )
      {
        usertext = engine.getPureText( p );
      }
    }
  %>
  </wiki:NoSuchPage>
  <%
    if( usertext == null )
    {
        usertext = engine.getPureText( context.getPage() );
    }
  %>
  </wiki:CheckRequestContext>
  <wiki:CheckRequestContext context="comment">
  <%
    WikiPage p = engine.getPage( "CommentTemplate" );
    if( p != null )
    {
      usertext = engine.getPureText( p );
    }
  %>
  </wiki:CheckRequestContext>  
  <% if( usertext == null ) usertext = "";  %>

<div style="width:100%" >
<form action="<wiki:CheckRequestContext 
     context='edit'><wiki:EditLink format='url'/></wiki:CheckRequestContext><wiki:CheckRequestContext 
     context='comment'><wiki:CommentLink format='url'/></wiki:CheckRequestContext>" 
      method="post" accept-charset="<wiki:ContentEncoding/>"
        name="editForm" class="wikiform"
    onsubmit="return WikiForm.submitOnce( this );"
     enctype="application/x-www-form-urlencoded" >
  <p>
  <%-- Edit.jsp relies on these being found.  So be careful, if you make changes. --%>
  <input type="hidden" name="page" value="<wiki:Variable var='pagename'/>" />
  <input type="hidden" name="action" value="save" />
  <input type="hidden" name="edittime"  
        value='<%= pageContext.getAttribute("lastchange",PageContext.REQUEST_SCOPE) %>' />
  </p>
  <wiki:CheckRequestContext context="comment">
  <p>
    <input type="submit" value="Save" name="ok" style="display:none;"/>
    <input type="button" value="Save" name="proxy1" onclick="this.form.ok.click();" 
      accesskey="s"
          title="Append your comment to the current page [ s ]" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="submit" value="Preview" name="preview" style="display:none;"/>
    <input type="button" value="Preview" name="proxy2" onclick="this.form.preview.click();" 
      accesskey="v"
          title="Preview comment [ v ]" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="submit" value="Cancel" name="cancel" style="display:none;"/>
    <input type="button" value="Cancel" name="proxy3" onclick="this.form.cancel.click();" 
      accesskey="q" 
          title="Cancel editing. Your comment will be lost. [ q ]" />
  </p>
  <textarea id="editorarea" 
          name="<%= EditorManager.REQ_EDITEDTEXT %>" 
          rows="10" cols="80" 
         class="editor" ><%= TextUtil.replaceEntities(usertext) %></textarea>
  <p>
    <table border="0" class="small">
      <tr>
        <td><label for="authorname" accesskey="n">Your <u>n</u>ame</label></td>
        <td><input type="text" name="author" id="authorname" value="<wiki:UserName/>" /></td>
        <td><label for="rememberme">Remember me?</label>
            <input type="checkbox" name="remember" id="rememberme" /></td>
      </tr>
       <tr>
         <td><label for="link" accesskey="m">Homepage or e<u>m</u>ail</label></td>
         <td colspan="2">
           <input type="text" name="link" id="link" size="40" 
                  value='<%= pageContext.getAttribute("link",PageContext.REQUEST_SCOPE) %>' />
         </td>
       </tr>
      </table>
   </p>
   </wiki:CheckRequestContext>

   <wiki:CheckRequestContext context="edit"> 
   <%-- remove this if you want plain jspwiki editing instead  
   <textarea id="editorarea" 
           name="<%= EditorManager.REQ_EDITEDTEXT %>" 
           rows="<%= prefEditAreaHeight %>" cols="80" 
          class="editor" ><%= TextUtil.replaceEntities(usertext) %></textarea>
   --%>
   <input type="hidden" 
            id="editorarea" 
          name="<%= EditorManager.REQ_EDITEDTEXT %>" 
          rows="<%= prefEditAreaHeight %>" cols="80" 
         value="<%= TextUtil.replaceEntities(usertext) %>" />
   <p>
     <input type="submit" value="Save" name="ok" style="display:none;"/>
     <input type="button" value="Save" name="proxy1" onclick="this.form.ok.click();" 
       accesskey="s"
           title="Save the current page [ s ]" />
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <input type="submit" value="Preview" name="preview" style="display:none;"/>
     <input type="button" value="Preview" name="proxy2" onclick="this.form.preview.click();" 
       accesskey="v"
           title="Preview [ v ]" />
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <input type="submit" value="Cancel" name="cancel" style="display:none;"/>
     <input type="button" value="Cancel" name="proxy3" onclick="this.form.cancel.click();" 
       accesskey="q" 
           title="Cancel editing. Your changes will be lost. [ q ]" />

     <%--HIDDEN FEATURE 
         OVERWRITE VERSION -- uncomment to activate this , also change edit.js
         see http://www.jspwiki.org/wiki/VersionOverwrite
     <wiki:PageExists>
     <%
       java.util.Date now  = new java.util.Date();       
       long anHourAgo      = now.getTime() - (60*60*1000);   
       String lastchange   = (String) pageContext.getAttribute("lastchange",PageContext.REQUEST_SCOPE);
       long lastChangeTime = Long.parseLong( lastchange );
       String lastAuthor   = context.getPage().getAuthor();
       String thisAuthor   = null; //reported by Nascif Abousalh-Net
       if( context.getWikiSession().getUserPrincipal() != null ) 
       {
         thisAuthor = context.getWikiSession().getUserPrincipal().getName();
       }
     %>
     <wiki:UserCheck status="authenticated">
       <% if( (lastAuthor != null ) &&    /* reported by Terry Steichen--twice */
              (lastAuthor.equals(thisAuthor)) ) /* should be the same author */ 
          {   
            if( lastChangeTime > anHourAgo ) /*changes within one hour */ 
            {  
        %>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Overwrite version: <input type="checkbox" name="overwrite" checked="checked" />
       <%
            } /* changes within one hour */ 
          } /* should be the same author */ 
        %>
     </wiki:UserCheck>
     </wiki:PageExists>
     --%>
     
   </p>
   </wiki:CheckRequestContext>
</form>
</div>

<div style="width:100%">
<form action="#" name="sectionForm" id="sectionForm">
<wiki:CheckRequestContext context="edit"> 
<%-- added stuff for brushed edit sections --%>
  <select id="sectionSelector" name="sectionSelector" size="1">
    <option selected="selected" value="0">( all )</option>
    <option value="-1">( metadata editor )</option>
  </select>
  <textarea id="workarea"
          rows="<%= prefEditAreaHeight %>" 
          wrap="virtual" class="editor" >
  </textarea>
  <div id="meta" style="display:none;">
   <%
      /** Prepare predefined metadata 
       ** Read the predefined metadata from a page called "DefaultMetadata" into a hashmap 
       ** inspired by TasksXXX plugins, by Nascif Abousalh Neto
       **/
      String dmText = "";
      WikiPage dmPage = engine.getPage( defaultMetadata );
      if( dmPage != null )  dmText = engine.getPureText( dmPage );

      Map dmMap = new TreeMap();
      java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("^\\|\\s*([A-Za-z\\d]*)\\s*\\|([^|]*)$");
      java.io.BufferedReader  reader  = new java.io.BufferedReader( new java.io.StringReader( dmText ) );
      String line;
      while( (line = reader.readLine()) != null ) 
      {
        java.util.regex.Matcher m = pattern.matcher(line); // parse |name|value pairs
        if( m.matches() ) dmMap.put( m.group(1).trim(), m.group(2).trim() );
      }
      reader.close();
      Set dmKeys = dmMap.keySet();
   %>
  <fieldset><legend>Page Variables</legend>
    <table id="metadata" >
      <tr>
        <th>Name</th>
        <th>Value</th>
        <td><span class="action" 
                  title="Add new Metadata"
                onclick="EditTools.addRow( 'metadata' );" >Add</span></td>
      </tr>
    </table>
    <p><wiki:Link page="<%= defaultMetadata %>" title="Edit this page to set your wiki default metadata" >Predefined Page Variables</wiki:Link> : 
    <%
       for( Iterator iter = dmKeys.iterator(); iter.hasNext(); ) 
       {
         String key = (String)( iter.next() );
    %>  
           <span class="action" 
                 title="Add new metadata"
               onclick="EditTools.addRow('metadata','default-<%= key %>');"><%= key %></span>         
    <%  
       }  
    %>
    </p>
  </fieldset>
  <fieldset><legend>Page Permissions</legend>
    <table id="permissions">
      <tr>
        <th>Roles, Groups or Users</th>
        <th>Permission</th>
        <td><span class="action" 
                  title="Add new Permission"
                onclick="EditTools.addRow( 'permissions' );" >Add</span></td>
      </tr>
    </table>  
    <p>
    Predefined Roles: 
    <span class="action" 
          title="'Anonymous' means the user is not logged in, and has not supplied a cookie"
        onclick="EditTools.addRow( 'permissions', null,'Anonymous');">Anonymous</span> 
    <span class="action" 
          title="'Asserted' means partially-trusted user, using a persistent cookie" 
        onclick="EditTools.addRow( 'permissions', null, 'Asserted');">Asserted</span> 
    <span class="action" 
          title="'Authenticated' means the user logged in with a login id and password"
        onclick="EditTools.addRow( 'permissions', null, 'Authenticated');">Authenticated</span>
    <span class="action" 
          title="'All' means anybody, regardless of authentication status" 
        onclick="EditTools.addRow( 'permissions', null, 'All');">All</span>
    </p>
  </fieldset>
  </div>
  <div style="display:none;" ><%-- default permission and metadata controls --%> 
    <table>
    <tr id="default-metadata">
    <td><input onchange="EditTools.onPageChange();"
                onfocus="if( this.value == this.defaultValue ) this.value = ''; "
                 onblur="if( this.value == '' ) this.value = this.defaultValue; " 
                   size="32" 
                   type="text" 
                  value="(enter variable name)" ></input></td>
    <td><input onchange="EditTools.onPageChange();"
                   size="32" 
                   type="text" 
                  value=""></input></td>
    <td><span class="action" 
              title="Remove this item"
            onclick="EditTools.removeRow(this);" >Remove</span></td>
    </tr>
    <% /* generate default metadata entries */
       for( Iterator iter = dmKeys.iterator(); iter.hasNext(); ) 
       {
         String key = (String)( iter.next() );
         String val = (String)( dmMap.get(key) );
         String[] vArr = val.split( ";" );
         if( vArr.length > 1 )
         {          
           val = "<select onchange='EditTools.onPageChange();' size='1'>";
           for( int i=0; i < vArr.length; i++ )
           {
             String v = vArr[i];
             val += "<option";
             if( v.startsWith("*") ) { v = v.substring(1); val += " selected=\'selected\' "; }
             val += ">" + v + "</option>";
           }
           val+="</select>";
         }
         else
         {
           val = engine.textToHTML( context, val );
           val = "<input type='text' size='32' onchange='EditTools.onPageChange();' value=\'" + val + "\' />";
         }
    %>     
         <tr id="default-<%=key%>" >
           <td><%= key %></td>
           <td><%= val %></td>
           <td><span class="action" 
                     title="Remove this item"
                   onclick="EditTools.removeRow(this);" >Remove</span></td>
         </tr>
    <%
       } 
    %>  
    <tr id="default-permissions">
    <td><input onchange="EditTools.onPageChange();"
                onfocus="if( this.value == this.defaultValue ) this.value = ''; "
                 onblur="if( this.value == '' ) this.value = this.defaultValue; " 
                   size="32"
                   type="text" 
                  value="(enter a User, Group or Role)" ></input></td>
    <td><select onchange="EditTools.onPageChange();"
                    size="1">
      <option value="(none)" >(select a Permission)</option>
      <option value="view"   >View</option>
      <option value="comment">View, Comment</option>
      <option value="edit"   >View, Comment, Edit</option>
      <option value="rename" >View, Comment, Edit, Rename</option>
      <option value="delete" >View, Comment, Edit, Rename, Delete</option>        
    </select></td>
    <td><span class="action" 
              title="Remove this item"
            onclick="EditTools.removeRow(this);" >Remove</span></td>
    </tr>
    </table>
  </div>
 
<%-- till here brushed edit sections --%>
</wiki:CheckRequestContext>

<%-- EditTools --%>
<div id="tb">
<div>
<button type="button" id="tbLink"   title="Insert [selected text|Link]"     
                                                             onmousedown="EditTools.link( )" >Link</button>
<button type="button" id="tbH1"     title="Heading 1"        onmousedown="EditTools.linePrefix( '!!!' )" >H1</button>
<button type="button" id="tbH2"     title="Heading 2"        onmousedown="EditTools.linePrefix( '!!' )" >H2</button>
<button type="button" id="tbH3"     title="Heading 3"        onmousedown="EditTools.linePrefix( '!')" >H3</button>
<button type="button" id="tbUL"     title="UnorderedList"    onmousedown="EditTools.linePrefix( '*')" >UL</button>
<button type="button" id="tbOL"     title="Ordered List"     onmousedown="EditTools.linePrefix( '#')" >OL</button>
<button type="button" id="tbBR"     title="Force Line Break" onmousedown="EditTools.inlineText( '\\\\\r\n')" >BR</button>             
<button type="button" id="tbHR"     title="Horizontal ruler" onmousedown="EditTools.blockText( '----\r\n$')" >HR</button>             
<button type="button" id="tbCODE"   title="Code block"       onmousedown="EditTools.blockText( '{{{\r\n$\r\n}}}\r\n')" >Code</button>           
<em class="spacer">|</em>
<button type="button" id="tbB"      title="Bold"             onmousedown="EditTools.inlineText( '__$__')" >Bold</button>        
<button type="button" id="tbI"      title="Italic"           onmousedown="EditTools.inlineText( '\'\'$\'\'')" >Italic</button>              
<button type="button" id="tbMono"   title="Monospaced"       onmousedown="EditTools.inlineText( '{{$}}')" >Mono</button>           
<button type="button" id="tbSUP"    title="Superscript"      onmousedown="EditTools.inlineText( '%%sup $%%')" >Sup</button>            
<button type="button" id="tbSUB"    title="Subscript"        onmousedown="EditTools.inlineText( '%%sup $%%')" >Sub</button>            
<button type="button" id="tbSTRIKE" title="Strikethrough"    onmousedown="EditTools.inlineText( '%%strike $%%')" >Strike</button>            
<em class="spacer">|</em>
<span  onmouseover="Element.show('tbColorPopup');"
       onmouseout= "Element.hide('tbColorPopup');" ><span id="tbColorPopup" style="display:none;" class="tbcolorpopup" 
                                                             onmousedown="EditTools.inlineText('%%(color:$$;)$%%')">Font Color</span>
  <button type="button" id="tbColor"  >Color</button>
</span>
<span  onmouseover="Element.show('tbBackgroundPopup');"
       onmouseout= "Element.hide('tbBackgroundPopup');" ><span id="tbBackgroundPopup" style="display:none;" class="tbcolorpopup" 
                                                             onmousedown="EditTools.inlineText('%%(background-color:$$;)$%%')" >Background Color</span>
  <button type="button" id="tbBackground" >Background Color</button>           
</span>                                                    
<em class="spacer">|</em>
<button type="button" id="tbLEFT"   title="Left Align"       onmousedown="EditTools.blockText( '%%(text-align:left;)\r\n$%%\r\n')" >Left</button>
<button type="button" id="tbCENTER" title="Center"           onmousedown="EditTools.blockText( '%%center\r\n$%%\r\n')" >Center</button>
<button type="button" id="tbRIGHT"  title="Right Align"      onmousedown="EditTools.blockText( '%%(text-align:right;)\r\n$%%\r\n')" >Right</button>
<button type="button" id="tbJUST"   title="Justify"          onmousedown="EditTools.blockText( '%%(text-align:justify;)\r\n$%%\r\n')" >Justify</button>
<button type="button" id="tbQUOTE"  title="Quote text"       onmousedown="EditTools.blockText( '%%(border-left:4px solid silver;padding-left: 1.5em;)\r\n$%%\r\n')" >Quote</button>
<em class="spacer">|</em>
<button type="button" id="tbTOC"    title="Table Of Contents"   onmousedown="EditTools.blockText( '[{TableOfContents }]\r\n$' );" >TOC</button>
<button type="button" id="tbTAB"    title="Tabbed Sections"     onmousedown="EditTools.blockText( '%%tabbedSection\r\n%%tab-TabTile1\r\n$\r\n%%\r\n%%tab-TabTitle2\r\n\r\n%%\r\n%%\r\n' );" >Tabs</button>
<button type="button" id="tbIMG"    title="Insert Image"        onmousedown="EditTools.blockText( '[{Image src=\'$\' width=\'\' height=\'\' align=\'left|center|right\' }]\r\n' );" >Image</button>
<button type="button" id="tbTABLE"  title="Insert Table"        onmousedown="EditTools.blockText( '%%sortable\r\n%%table-filter\r\n|| header1 || header2\r\n| cell1    | cell2\r\n%%\r\n%%\r\n$' );" >Table</button>
<button type="button" id="tbTIME"   title="Insert Current Time" onmousedown="EditTools.blockText( '[{CurrentTimePlugin format=\'dd MMM yyyy\' }]\r\n$' );" >Time</button>
<button type="button" id="tbPLUGIN" title="Insert Plugin"       onmousedown="EditTools.blockText( '[{$ }]\r\n' );" >Plugin</button>
<em class="spacer">|</em>
<button type="button" id="tbCOPY"   title="Copy"  onmousedown="EditTools.clipboard( '$' )" >Copy</button>
<button type="button" id="tbCUT"    title="Cut"   onmousedown="EditTools.clipboard( '' )" >Cut</button>
<button type="button" id="tbPASTE"  title="Paste" onmousedown="EditTools.paste( )" >Paste</button>
</div>
<div>
<%-- Search and replace section --%>
  <label for="tbFIND">Find:</label>
  <input type="text" name="tbFIND" id="tbFIND" size="16" />
  &nbsp;
  <label for="tbREPLACE">Replace:</label>
  <input type="text" name="tbREPLACE" id="tbREPLACE" size="16" />
  &nbsp;
  <input type="checkbox" name="tbMatchCASE" id="tbMatchCASE" /> <label for="tbMatchCASE">Match Case</label>
  &nbsp;
  <input type="checkbox" name="tbREGEXP"    id="tbREGEXP" /> <label for="tbREGEXP">RegExp</label>
  &nbsp;
  <input type="checkbox" name="tbGLOBAL"    id="tbGLOBAL" checked /> <label for="tbGLOBAL">Replace all</label>
  &nbsp;
  <input type="button" name="replace" id="replace" value="Replace" onmousedown="EditTools.doReplace()" />
  &nbsp;
  <input type="button" name="tbREDO"  id="tbREDO"  value="Redo"    onmousedown="EditTools.redoTextarea()" title="Redo" disabled />
  &nbsp;
  <input type="button" name="tbUNDO"  id="tbUNDO"  value="Undo"    onmousedown="EditTools.undoTextarea()" title="Undo [ z ]" disabled accesskey="z"/>
</div>
</div>

</form>
</div>