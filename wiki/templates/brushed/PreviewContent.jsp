<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki"%>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.tags.*" %>
<%@ page import="com.ecyrd.jspwiki.ui.*" %>

<% 
  WikiContext context = WikiContext.findContext( pageContext ); 
  String usertext = (String)pageContext.getAttribute( EditorManager.ATTR_EDITEDTEXT,
                                                      PageContext.REQUEST_SCOPE ); 
  if( usertext == null ) usertext = ""; 
  String action = "comment".equals(request.getParameter("action")) ? 
                  context.getURL(WikiContext.COMMENT,context.getPage().getName()) : 
                  context.getURL(WikiContext.EDIT,context.getPage().getName());
%>

<%-- Inserts page content for preview. --%>
<wiki:TabbedSection>
  <wiki:Tab id="previewcontent" title="Preview Page">

  <div class="information">
    This is a PREVIEW!  Hit "Keep Editing [E]" to go back to the editor,
    hit "Save [S]" if you're happy with what you see. 

  <form action="<%= action %>" 
        method="post" accept-charset="<wiki:ContentEncoding/>" 
          name="editForm" class="wikiform"
      onsubmit="return WikiForm.submitOnce( this );"
       enctype="application/x-www-form-urlencoded" >
    <p>
      <input type="hidden" name="author"   value="<%=session.getAttribute("author")%>" />
      <input type="hidden" name="link"     value="<%=session.getAttribute("link")%>" />
      <input type="hidden" name="remember" value="<%=session.getAttribute("remember")%>" />
      <input type="hidden" name="page"     value="<wiki:Variable var="pagename"/>" />
      <input type="hidden" name="action"   value="save" />
      <input type="hidden" name="edittime" value="<%=pageContext.getAttribute(
                                                 "lastchange",
                                                 PageContext.REQUEST_SCOPE )%>" />
    <textarea style="display:none;" 
           readonly="true"
                 id="editorarea" 
               name="<%=EditorManager.REQ_EDITEDTEXT%>" 
               rows="4" 
               cols="80"><%=TextUtil.replaceEntities(usertext)%></textarea>
    </p>
    <input type="submit" value="Keep editing" name="edit" style="display:none;"/>
    <input type="button" value="Keep editing" name="proxy1" 
        onclick="this.form.edit.click();" 
      accesskey="e"
          title="Continue to edit the current page [ e ]" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="submit" value="Save" name="ok" style="display:none;"/>
    <input type="button" value="Save" name="proxy2" 
        onclick="this.form.ok.click();" 
      accesskey="s"
          title="Save the current page [ s ]" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <input type="submit" value="Cancel" name="cancel" style="display:none;"/>
    <input type="button" value="Cancel" name="proxy3" 
        onclick="this.form.cancel.click();" 
      accesskey="q" 
          title="Cancel editing. Your changes will be lost. [ q ]" />
  </form>
  </div>

  <div class="previewcontent">
    <wiki:Translate><%=EditorManager.getEditedText(pageContext)%></wiki:Translate>
  </div>

  <div class="information">
    This is a PREVIEW!  Hit "Keep Editing [E]" to go back to the editor,
    or hit "Save [S]" if you're happy with what you see. 
  </div>
 
  </wiki:Tab>
</wiki:TabbedSection>