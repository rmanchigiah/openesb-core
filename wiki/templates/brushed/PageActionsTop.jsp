<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%!
  /* replace wiki:PageDate with timezone support */
  private String wiki_PageDate( WikiPage page, String format, String timezone )
  {
    if( page == null ) return "" ;
    java.util.Date date = page.getLastModified();  
    if( date == null ) return "&lt;never&gt;" ;
    java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( format );
    java.util.TimeZone tz = java.util.TimeZone.getDefault();
    try 
    {
      tz.setRawOffset( Integer.parseInt( timezone ) );
    }
    catch( Exception e) { /* dont care */ } ;
    fmt.setTimeZone( tz );     
    return fmt.format( date ) ;
  }  
%>
<%
  /* see commonheader.jsp */
  String prefDateFormat = (String) session.getAttribute("prefDateFormat");
  String prefTimeZone   = (String) session.getAttribute("prefTimeZone");
%>
<%
  /* should become a wiki:FrontPage jsp tag */
  String homePage = "Main";
  WikiContext wikiContext = WikiContext.findContext(pageContext);
  try 
  { 
    homePage = wikiContext.getEngine().getFrontPage(); 
  } 
  catch( Exception  e )  { /* dont care */ } ;
%>
<%-- similar to PageActionsBottom, except for
     ** additional accesskey definitions
     ** additional Login/Logout, quick2Bottom 
     ** no quick2Top, actionPageInfo  
  --%>
<div id="actionsTop" class="pageactions"> 
<!--
  <span class="actionHome">
    <a href="<wiki:LinkTo page='<%= homePage %>' format='url' />"
       title="Go to home page <%= homePage %> ">Home</a>
  </span>
-->  
  <wiki:CheckRequestContext context='view|info|diff|upload'>
    <wiki:Permission permission="edit">
      <span class="actionEdit">
        <wiki:PageType type="page">
<%--        <wiki:Link title="Edit current page [ e ]"
                     context="edit"
               accessKey="e">Edit</wiki:Link>
--%>                   
          <a href="<wiki:EditLink format='url' />" 
            title="Edit current page [ e ]" accesskey="e" >Edit</a>
        </wiki:PageType>
        <wiki:PageType type="attachment">
          <a href="<wiki:BaseURL/>Edit.jsp?page=<wiki:ParentPageName />" 
            title="Edit parent page [ e ]" accesskey="e" >Edit parent</a>
        </wiki:PageType>
      </span>
    </wiki:Permission>
    
    
<!--
    <wiki:Permission permission="comment">
      <span class="actionComment">
        <wiki:PageType type="page">
          <a href="<wiki:CommentLink format='url' />" 
            title="Add a new comment to the current page" >Comment?</a>
        </wiki:PageType>
        <wiki:PageType type="attachment">
          <a href="<wiki:BaseURL/>Comment.jsp?page=<wiki:ParentPageName />" 
            title="Add a new comment to the parent page" >Comment?</a>
        </wiki:PageType>
      </span>
    </wiki:Permission>
  -->
  </wiki:CheckRequestContext>
  
  <span class="actionIndex">
     <a href="<wiki:LinkTo page='PageIndex' format='url' />"
       title="Alphabetically sorted list of pages [ x ]" accesskey="x">Index</a>
  </span>
  
  <span class="actionRecentChanges">
     <a href="<wiki:LinkTo page='RecentChanges' format='url' />"
        title="Pages sorted by modification date [ u ]" accesskey="u">Changes</a>
  </span>

  <wiki:CheckRequestContext context='!prefs'>
    <wiki:CheckRequestContext context='!preview'>
      <span class="actionPrefs">
        <a href="<wiki:LinkTo page='UserPreferences' format='url' />"
           title="Set User Preferences" >Prefs</a>
      </span>
    </wiki:CheckRequestContext>
  </wiki:CheckRequestContext>

<!--
  <span class="quicklinks quick2Bottom">
    <a href="#footer" title="Go to Bottom" >&raquo;</a>
  </span>
-->
  <div style="clear:both; height:0;"> </div> 
</div>
