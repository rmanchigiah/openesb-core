<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>

<%-- If the page is an older version, then offer a note and a possibility
     to restore this version as the latest one. --%>

<wiki:CheckVersion mode="notlatest">
  <div class="warning">
  This is version <wiki:PageVersion/>.  
  It is not the current version, and thus it cannot be edited.<br />
  <wiki:Link>[Back to current version]</wiki:Link>&nbsp;&nbsp;
  <wiki:EditLink version="this">[Restore this version]</wiki:EditLink>
  </div>
</wiki:CheckVersion>

<%-- Inserts no text if there is no page. --%>
<wiki:InsertPage />

<wiki:NoSuchPage>
  <%-- FIXME: Should also note when a wrong version has been fetched. --%>
  This page does not exist.  Why don't you go and
  <wiki:EditLink>create it</wiki:EditLink>?
</wiki:NoSuchPage>
