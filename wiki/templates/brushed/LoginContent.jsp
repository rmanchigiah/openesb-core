<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>

<wiki:TabbedSection>

  <wiki:UserCheck status="notauthenticated">
  <wiki:Tab id="logincontent" title="Login">
    <wiki:Include page='LoginTab.jsp'/>  
  </wiki:Tab>
  </wiki:UserCheck>

  <wiki:UserCheck status="authenticated">
  <wiki:Tab id="logoutcontent" title="Logout">
    <wiki:Include page='LogoutTab.jsp'/>  
  </wiki:Tab>
  </wiki:UserCheck>

  <wiki:Permission permission='editProfile'>
  <wiki:Tab id="registercontent" title="Register New User">
    <wiki:Include page='ProfileTab.jsp'/>  
  </wiki:Tab>
  </wiki:Permission>

  <wiki:Permission permission="createGroups">
  <wiki:Tab id="newGroup" title="User Group" >
    <wiki:Include page="GroupTab.jsp" />
  </wiki:Tab>
  </wiki:Permission>

</wiki:TabbedSection>