<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%
  /* see commonheader.jsp */
  String prefEditorType     = (String) session.getAttribute("prefEditorType");
  prefEditorType = "plain";  //"FCK";
%>

<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>

<head>
  <title><wiki:Variable var="ApplicationName" /> Edit: <wiki:PageName /></title>
  <meta name="ROBOTS" content="NOINDEX" />
  <wiki:Include page="commonheader.jsp"/>
  <% if( prefEditorType.equals("FCK") ) { %>
  <script type="text/javascript" src="scripts/fckeditor/fckeditor.js"></script>
  <% } %>
</head>

<wiki:CheckRequestContext context="edit">
  <body class="edit" >
</wiki:CheckRequestContext>

<wiki:CheckRequestContext context="comment">
  <body class="comment" >
</wiki:CheckRequestContext>

<div id="wikibody" >

  <wiki:Include page="Header.jsp" />
  
  <wiki:Include page="PageActionsTop.jsp"/>

  <div id="page"><wiki:Content/></div> 

  <wiki:Include page="Favorites.jsp"/> 

  <wiki:Include page="PageActionsBottom.jsp"/>

  <wiki:Include page="Footer.jsp" />

  <div style="clear:both; height:0px;" > </div>

</div>

</body>
</html>
