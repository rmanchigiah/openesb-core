<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%-- Provides a simple searchbox that can be easily included anywhere
     on the page --%>

<form action="<wiki:Link jsp="Search.jsp" format="url"/>"
    onsubmit="SearchBox.submit( this.query.value )"
 onmouseover="Element.show('searchboxMenu');"
  onmouseout= "Element.hide('searchboxMenu');"
        name="searchForm" id="searchForm" 
        accept-charset="<wiki:ContentEncoding />">
  <div>
  <%-- search images --%>
  <input onblur="if( this.value == '' ) { this.value = 'Search'}; " 
        onfocus="if( this.value == 'Search' ) { this.value = ''}; "
           type="text" value="Wiki search" name="query" id="query" size="24" 
      accesskey="f"></input>
  </div>  
  <div id="searchboxMenu" style='display:none;'>
    <div>
      <a href="javascript://nop/" 
         onclick="SearchBox.navigation( '<wiki:Link format="url" page="__PAGEHERE__"/>','<wiki:Variable var='pagename' />' );return false;"
         title="View the selected page">view</a> 
      | 
      <a href="javascript://nop/" 
         onclick="SearchBox.navigation( '<wiki:Link format="url" page="__PAGEHERE__" context="edit"/>','<wiki:Variable var='pagename' />' );return false;"
         title="Edit the selected page">edit</a> 
      | 
      <a href="javascript://nop/" 
         onclick="SearchBox.clone( '<wiki:Link format="url" page="__PAGEHERE__" context="edit"/>','<wiki:Variable var='pagename' />' );return false;"
         title="Clone this page">clone</a> 
      |
      <a href="javascript://nop/" 
         onclick="SearchBox.navigation( '<wiki:BaseURL />Search.jsp?query=__PAGEHERE__','<wiki:Variable var='pagename' />' );return false;"
         title="Advanced Search">find</a> 
      [ f ]
    </div>
    <div id="searchResult" > </div>
    <div id="recentSearches" > </div>
  </div>

</form>
<script type="text/javascript">
//<![CDATA[    
    function updateSearchResult( )
    {  
      if( $F('query') == 'Search' ) return;
      new Ajax.Updater(
        'searchResult', 
        '<wiki:Link format="url" templatefile="FindAjax.jsp"/>',
        { 
          asynchronous: true,
          parameters: Form.serialize( 'searchForm' ) + "&compact=true"
        } ); 
    } ;
    new Form.Element.Observer( "query", 1, updateSearchResult ); 
//]]>
</script>
    
   