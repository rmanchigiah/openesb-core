###########
# SECTION 1 
# Modify the 6 values in this section so that they are specific to your environment
#
# Set SRCROOT to the path which contains the code that you checked out in step b above 
# For both cygwin and unix it should be something like : "/bld/main/open-esb"
export SRCROOT="/home/nheron/workspace-netbeans-src/open-esb" 
export JV_SRCROOT="/home/nheron/workspace-netbeans-src/open-jbi-components" 
#
# Set AS8BASE to the path which contains your installation of Sun's glassfish App Server
# For cygwin it should be something like : "/cygdrive/c/sun/glassfish" 
# For unix it should be something like : " "/bld/glassfish" 
export AS8BASE="/home/nheron/java/glassfish_openesb" 
export JV_GFBASE=$AS8BASE
#
# Set CVSROOT to your java.net CVSROOT
export CVSROOT=":pserver:nheron@cvs.dev.java.net:/cvs"   
#
# Set JAVA_HOME to the path which contains your installation of Java SDK 1.5.0
# For cygwin it should be something like : "/cygdrive/c/jdk1.5.0_06
# For unix it should be something like : "/usr/jdk/jdk1.5.0_06" 
#export JAVA_HOME="/home/nheron/java/jdk1.5.0_15"   
export JAVA_HOME="/home/nheron/java/jdk1.5.0_22"   
#export JAVA_HOME="/home/nheron/java/jdk1.6.0_21"
#
# Set PERL5_HOME to the path which contains your installation of Perl 5
# For cygwin it should be something like : "/lib/perl5/5.8"
# For unix it should be someting like : "/usr/perl5/5.8.4"
export PERL5_HOME="/usr/lib/perl5/5.12.1" 
#
# Set M2_HOME to the path which contains your maven installation
# For cygwin it should be something like : "/cygdrive/c/maven-2.0.4"
# For unix it should be something like : "/bld/maven-2.0.4"
#export M2_HOME="/home/nheron/java/apache-maven-2.2.1"  
export M2_HOME="/home/nheron/java/apache-maven-2.0.9"  
#
###########
# SECTION 2
# If you already have ANT version 1.6.2 or greater you can comment out this setting of ANT_HOME
# If you do NOT have ANT version 1.6.2 or if you want to use the ANT distribution that ships with glassfish then leave this line uncommented 
#export ANT_HOME="${AS8BASE}/lib/ant"
#
###########
# SECTION 3
# Don't change any of the values in this section
export TOOLROOT="${SRCROOT}/../tools"
export PATH="${M2_HOME}/bin:${ANT_HOME}/bin:${PATH}"
#
source ${TOOLROOT}/boot/buildenv.sh
#
# Setup mavan aliases
M2_COMMON_OPTIONS="-DSRCROOT='${JV_SRCROOT}' -Dmaven.repo.local='${JV_SRCROOT}/m2/repository'"
alias smvn="mvn ${M2_COMMON_OPTIONS} -Dmaven.test.skip=true"
alias smvntest="mvn ${M2_COMMON_OPTIONS} -Dmaven.test.skip=false"
#
###########
# SECTION 4
# ONLY keep and modify these environmental variables if you are on a Solaris platform
# On all other platforms comment out the 2 env variables below
# These variables are needed on Solaris in order for the build to access the gmake binary  
# If you are using Solaris 10 then you would configure them like this:
#export MAKEDRV_MAKE_COMMAND="gmake"
#export PATH="/usr/sfw/bin:${PATH}"
###########
# SECTION 5
# ONLY uncomment and modify these environmental variables if you are using NetBeans to build the Open ESB Project
# For cygwin set NETBEANS home to the location of your NetBeans installation, something like :
# "/cygdrive/c/netbeans", or "/netbeans-5.5.beta2"
# For unix it should be something like : "/usr/local/cmn/netbeans-5.5.beta2"
# DO NOT change the runide alias.  All you need to do is uncomment it
export NETBEANS_HOME="/home/nheron/java/netbeans-6.9.1"
#alias runide='${NETBEANS_HOME}/bin/netbeans -J-Dmaven.repo.local=${JV_SRCROOT}/m2/repository'
alias runide='${NETBEANS_HOME}/bin/netbeans -J-Dmaven.repo.local=/home/nheron/.m2/repository'
