/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentConfigurationHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;

/**
 * This is a utility class to convert a set of properties into 
 * application variables or application configuration and vice versa.
 *
 */
public class ComponentConfigurationHelper
{
    /** The String Translator */
    private StringTranslator mTranslator;
    
    /** Application Variable Composite Type */
    private static CompositeType sAppVarComposite;
    
    /** Application Variable Tabular Type */
    private static TabularType   sAppVarTable;
    
    /** Application Variable Composite Item Names */
    private static String[] sAppVarItemNames;
    
    /** The index of the Application Variables **/
    private static String[] sappVarRowIndex;
    
    /** Logger */
    private static Logger mLog;
    
    /** Application Configuration element name in component config meta-data */
    private static final String APP_CONFIG_ELEMENT_NAME = "ApplicationConfiguration";
    

    /** Application Variables element name in component config meta-data */
    private static final String APP_VAR_ELEMENT_NAME = "ApplicationVariable";
    
    /** isPasswordField attribute in component config meta-data */
    private static final String IS_PASSWD_ATTRIB_NAME = "isPasswordField";
     
    /** encrypted attribute in component config meta-data */
    private static final String ENCRYPTED_ATTRIB_NAME = "encrypted";
    
    private static final String NAME_ATTRIB_NAME = "name";
    
    private static final String PROPERTY_ELEMENT_NAME = "Property";
    
     private static final String PROPERTY_GROUP_ELEMENT_NAME = "PropertyGroup";
    
    private static final String IS_REQUIRED_ATTRIB_NAME = "isRequiredField";
    
    private static final String REQUIRED_ATTRIB_NAME = "required";
    
    public static final String DEFAULT_APP_VAR_TYPE = "STRING";
    
    /** Configuration NS */
    public static final String CONFIGURATION_NS   = "http://www.sun.com/jbi/Configuration/V1.0";
    
    private static final Pattern NUMBER_PATTERN = Pattern.compile("\\d");
    
    private void initOpenTypes()
        throws javax.management.openmbean.OpenDataException
    {
            // Define the Application Variables CompositeType
            sAppVarItemNames = new String[] {"name", 
                                "value", 
                                "type" };

            String[] appVarItemDesc = { "Application variable name", 
                                        "Application variable value", 
                                        "Application variable type" };

            OpenType[] appVarRowAttrTypes = { 
                                        SimpleType.STRING, 
                                        SimpleType.STRING, 
                                        SimpleType.STRING };

            sappVarRowIndex = new String[]{ "name" };


            sAppVarComposite = new CompositeType("ApplicationVariableComposite",
                                                  "Application variable name and value pair",
                                                   sAppVarItemNames,
                                                   appVarItemDesc,
                                                   appVarRowAttrTypes);

            sAppVarTable = new TabularType("ApplicationVariableList",
                                           "List of application variables",
                                                    sAppVarComposite,
                                                    sappVarRowIndex);
    }
    
        
    /** Constructor */
    public ComponentConfigurationHelper()
    {
        try
        {
            mLog = Logger.getLogger("com.sun.jbi.util");
            mTranslator = new StringTranslator("com.sun.jbi.util", null);
            initOpenTypes();
        }
        catch ( javax.management.openmbean.OpenDataException opex )
        {
            throw new RuntimeException(opex);
        }
    }
    /**
     * @return the CompositeType for Application Variables.
     */
    public CompositeType getApplicationVariablesType()
    {
        return sAppVarComposite;
    }
    /**
     * @return the Application Variables Tabular Type
     */
    public TabularType getApplicationVariableTabularType()
    {
        return sAppVarTable;
    }
    
    /**
     *  @return the application variables item names
     */
    public String[] getApplicationVariableItemNames()
    {
        return sAppVarItemNames;
    }
    
    /**
     * Convert a Properties Object to a Application Variable TabularData.
     * The property name is the name of the Property and the value is 
     * of the Pattern "[type]value"
     *
     * @param props - the properties to convert to TabularData.
     * @return the TabularData constructed from the Properties
     */
    public TabularData convertToApplicationVariablesTable(Properties props)
        throws Exception
    {
         TabularData tabularData = new TabularDataSupport(sAppVarTable);
         
         Set names =  props.keySet();
         for ( Object name : names )
         {
             String appVarName = (String) name;
             String value = props.getProperty(appVarName);
             String appVarType  = getAppVarType(value, DEFAULT_APP_VAR_TYPE);
             String appVarValue = getAppVarValue(value);
             
             if ( appVarType != null )
             {
                 appVarType = convertTypeCase(appVarType);
             }
             
             CompositeData cd = new CompositeDataSupport(getApplicationVariablesType(),
                sAppVarItemNames, new String[] {appVarName, appVarValue, appVarType});
             tabularData.put(cd);
         }
         
         return tabularData;
    }
    
    /**
     * Convert Apllication Variables TabularData to a set of Properties.
     */
    public Properties convertToApplicationVariablesProperties(TabularData appVars)
        throws Exception
    {
        Properties props = new Properties();
        
        Set names = appVars.keySet();
        
        for ( Object name : names )
        {
            Object[] key = ( (java.util.List) name).toArray();
            
            CompositeData cd = appVars.get(key);
            
            if ( cd != null )
            {
                String appVarName =  (String) cd.get(sAppVarItemNames[0]);
                String appVarValue = (String) cd.get(sAppVarItemNames[1]);
                String appVarType =  (String) cd.get(sAppVarItemNames[2]);
                
                if ( appVarType == null )
                {
                    appVarType = DEFAULT_APP_VAR_TYPE;
                }
                
                if ( appVarValue == null ) 
                {
                    appVarValue = "null";
                }
                
                String value = "[" + convertTypeCase(appVarType) + "]" + appVarValue;
                props.put(appVarName, value);
            }
            
        }
        return props;
    }

    /**
     * Convert Apllication Variables CompositeData to a set of Properties.
     *
     * @param appVar - application variable composite
     */
    public Properties convertToApplicationVariableProperty(CompositeData appVar)
        throws Exception
    {
        Properties props = new Properties();
        
        if ( appVar != null )
        {
            String appVarName =  (String) appVar.get(sAppVarItemNames[0]);
            String appVarValue = (String) appVar.get(sAppVarItemNames[1]);
            String appVarType =  (String) appVar.get(sAppVarItemNames[2]);

            if ( appVarType == null )
            {
                appVarType = DEFAULT_APP_VAR_TYPE;
            }
            
            if ( appVarValue == null )
            {
                appVarValue = "null";
            }

            String value = "[" + convertTypeCase(appVarType) + "]" + appVarValue;
            props.put(appVarName, value);
        }
        return props;
    }
    
    /**
     * Create a Application Variable Composite from the passed in name, value and type
     *
     * @param name - the name
     * @param type - the type string
     * @param value - the value
     * @return the CompositeData constructed from the parameters
     */
    public CompositeData createApplicationVariableComposite(String name, String value, String type)
        throws Exception
    {
        if ( type == null )
        {
            type = DEFAULT_APP_VAR_TYPE;
        }
        return  new CompositeDataSupport(getApplicationVariablesType(),
            sAppVarItemNames, new String[] {name, value, type});
    }
    
    /**
     * Create a Application Variable Composite from the passed in name, and the property 
     * value string which is of format : [type]value
     *
     * @param name - the name
     * @param typeAndValueStr - string which is of format : [type]value
     * @return the CompositeData constructed from the parameters
     */
    public CompositeData createApplicationVariableComposite(String name, String typeAndValueStr)
        throws Exception
    {
        return  new CompositeDataSupport(getApplicationVariablesType(),
            sAppVarItemNames, new String[] {name, getAppVarValue(typeAndValueStr), 
                getAppVarType(typeAndValueStr, DEFAULT_APP_VAR_TYPE)});
    }
    
    
    
    /**
     * @return the "type" from the "[type]value" string.
     */
    public String getAppVarType(String str, String defStr)
        throws Exception
    {
        str = str.trim();
        int indexOfRP = str.indexOf('[');
        int indexOfLP = str.indexOf(']');
        
        if ( indexOfRP != 0  || indexOfLP == -1 )
        {
            return defStr;
        }
        
        String type = str.substring(indexOfRP+1, indexOfLP);
        
        if ( type == null)
        {
            type = defStr;
        }
        else if ( type.trim().equals(""))
        {
            type = defStr;
        }
        
        String rsltStr = type;
        
        if ( type != null )
        {
            rsltStr = convertTypeCase(type);
        }
        return rsltStr;
    }
    
    /**
     * @return the "value" from the "[type]value" string.
     */
    public String getAppVarValue(String str)
        throws Exception
    {
        int indexOfRP = str.indexOf('[');
        int indexOfLP = str.indexOf(']');
        
        if ( indexOfRP != 0  || indexOfLP == -1 )
        {
            return str;
        }
        
        return str.substring(indexOfLP+1, str.length());
    }
    
    /**
     * Convert a CompositeData to Properties.
     *
     * @param cd
     *      the composite data to convert to Properties
     * @return 
     *      the properties representation of the CompositeData
     *
     */
    public Properties convertCompositeDataToProperties(CompositeData cd)
    {
        Properties appConfigProps = new Properties();
        CompositeType ct = cd.getCompositeType();  
        Set<String> keys = ct.keySet();

        for ( String key : keys )
        {
            Object obj = cd.get(key);

            if ( obj != null )
            {
                Properties props = convertToString(ct.getType(key), obj, key);
                appConfigProps.putAll(props);
            }
        }
        return appConfigProps;
    }
    
   /**
    *
    * @param props 
    *         the properties to convert to a CompositeType
    * @param appConfigType 
    *         application configuration CompositeType
    * @throws Exception if the properties cannot be completely mapped into the 
    *         CompositeType
    */
    public CompositeData convertPropertiesToCompositeData(Properties props,
        CompositeType appConfigType)
        throws Exception
    {
        /**
         * For each Composite item, look for a matching property
         * If one exists then convert the value to the expected OpenType as defined 
         * in the CompositeType.  If the value cannot be converted to the expected
         * type an exception is thrown
         */
        Set<String> keySet = appConfigType.keySet();
        propertyKeysValidCheck(props.keySet(), keySet ); 
        Vector<String> itemNames = new Vector(); 
        
        Vector<Object> itemValues = new Vector();
               
        int i = 0;
        for ( String itemName : keySet )
        {
            OpenType itemType = appConfigType.getType(itemName);
        
            if ( itemType.isArray())
            {
                // get all the properties that match the pattern itemName.n
                // into a String[] array
                int indexSuffix = 0;
                boolean done = false;
                List itemValueArray = new ArrayList();
                do 
                {
                    String suffixedItemName = itemName + "." + indexSuffix;
                    indexSuffix += 1;
                    if ( props.containsKey(suffixedItemName))
                    {
                        itemValueArray.add(props.getProperty(suffixedItemName));
                    }
                    else
                    {
                        done = true;
                    }
                    
                }while ( !done );
                
                if ( !itemValueArray.isEmpty() )
                {
                    itemNames.add(itemName);
                    itemValues.add(convertToOpenType(itemType, itemValueArray));
                }
                else
                {
                    itemNames.add(itemName);
                    itemValues.add(null);
                }
            }
            else
            {   
                if ( props.containsKey(itemName) )
                {
                    String itemStrValue = (String) props.getProperty(itemName);
                    itemNames.add(itemName);
                    itemValues.add(convertToOpenType(itemType, itemStrValue));
                }
                else
                {
                    itemNames.add(itemName);
                    itemValues.add(null);
                }
            }

        }
        
        CompositeData cd = new CompositeDataSupport(
                    appConfigType,
                    itemNames.toArray(new String[itemNames.size()]),
                    itemValues.toArray(new Object[itemValues.size()])); 
        
        return cd;
    }
    
    /*------------------------------------------------------------------------*\
     *                  Utilities to help password obfuscation                *
    \*------------------------------------------------------------------------*/
    
    /**
     * @param attributeName - name of the property
     * @param configDoc  - configuration xml document 
     * @return true if a component configuration attribute is a passwordfiled
     *  
     */
    public boolean isPassword(String attributeName, Document configDoc)
        throws Exception
    {
        boolean isPasswordField = false;
        
        if ( configDoc != null )
        {   
            if ( isOldConfigNS(configDoc) )
            {
                attributeName = getActualAttributeName(attributeName);
                Element attribElement = (Element) configDoc.getElementsByTagNameNS(
                        "*", attributeName).item(0);
                if ( attribElement != null )
                {
                    String isPwdStr = attribElement.getAttribute(IS_PASSWD_ATTRIB_NAME);
                    isPasswordField = Boolean.parseBoolean(isPwdStr);
                }
                else
                {                    
                    String errMsg = mTranslator.getString(
                        LocalStringKeys.CCFG_ATTR_DEF_MISSING_IN_XML,
                        attributeName);
                    mLog.fine(errMsg);
                }
            }
            else
            {
                /**
                 * Get all the Property elements and look for the one with
                 * name=attributeName
                 */
                org.w3c.dom.NodeList properties = 
                        configDoc.getElementsByTagNameNS(
                            "*", PROPERTY_ELEMENT_NAME);
                
                int numNodes = properties.getLength();
                
                for ( int i=0; i < numNodes; i++) 
                {
                    Element propertyElement = (Element) properties.item(i);
                    if ( attributeName.equals(propertyElement.getAttribute(NAME_ATTRIB_NAME)))
                    {
                        isPasswordField = Boolean.parseBoolean(
                                propertyElement.getAttribute(ENCRYPTED_ATTRIB_NAME)); 
                    }
                }
                
            }
        }
        return isPasswordField; 
    }
    
        
    /**
     * @param attributeName - name of the property
     * @param configDoc  - configuration xml document 
     * @return true if a component configuration attribute is a required field
     *  
     */
    public boolean isRequired(String attributeName, Document configDoc)
        throws Exception
    {
        boolean isRequiredField = false;
        
        if ( configDoc != null )
        {   
            if ( isOldConfigNS(configDoc) )
            {
                attributeName = getActualAttributeName(attributeName);
                Element attribElement = (Element) configDoc.getElementsByTagNameNS(
                        "*", attributeName).item(0);
                if ( attribElement != null )
                {
                    String isRqdStr = attribElement.getAttribute(IS_REQUIRED_ATTRIB_NAME);
                    isRequiredField = Boolean.parseBoolean(isRqdStr);
                }
                else
                {                    
                    String errMsg = mTranslator.getString(
                        LocalStringKeys.CCFG_ATTR_DEF_MISSING_IN_XML,
                        attributeName);
                    mLog.fine(errMsg);
                }
            }
            else
            {
                /**
                 * Get all the Property elements and look for the one with
                 * name=attributeName
                 */
                org.w3c.dom.NodeList properties = 
                        configDoc.getElementsByTagNameNS(
                            "*", PROPERTY_ELEMENT_NAME);
                
                int numNodes = properties.getLength();
                
                for ( int i=0; i < numNodes; i++) 
                {
                    Element propertyElement = (Element) properties.item(i);
                    if ( attributeName.equals(propertyElement.getAttribute(NAME_ATTRIB_NAME)))
                    {
                        isRequiredField = Boolean.parseBoolean(
                                propertyElement.getAttribute(REQUIRED_ATTRIB_NAME)); 
                    }
                }
                
            }
        }
        return isRequiredField; 
    }
    
    /**
     *
     * @param configDoc  - configuration xml document 
     * @return true if the components configuration document declares
     *         support for component configuration attributes
     *  
     */
    public boolean isComponentConfigSupported(Document configDoc)
    {
        boolean isConfigSupported = false;
        
        if ( configDoc != null )
        {   
            if ( !isOldConfigNS(configDoc) )
            {

                Element root = configDoc.getDocumentElement();
                org.w3c.dom.NodeList children = root.getChildNodes();
                
                int numNodes = children.getLength();
                
                for ( int i=0; i < numNodes; i++) 
                {
                    org.w3c.dom.Node child = children.item(i);
                    if ( PROPERTY_ELEMENT_NAME.equals(child.getLocalName()) ||
                         PROPERTY_GROUP_ELEMENT_NAME.equals(child.getLocalName()) )
                    {
                       isConfigSupported = true; 
                       break;
                    }
                }
                
            }
        }
        return isConfigSupported; 
    }
    
    /**
     *
     * @param configDoc  - configuration xml document 
     * @return true if the components configuration document declares
     *         support for application variables
     *  
     */
    public boolean isAppVarsSupported(Document configDoc)
    {
        boolean isAppVarsSupported = false;
        
        if ( configDoc != null )
        {   
            if ( !isOldConfigNS(configDoc) )
            {
                Element root = configDoc.getDocumentElement();
                org.w3c.dom.NodeList children = root.getChildNodes();
                
                int numNodes = children.getLength();
                
                for ( int i=0; i < numNodes; i++) 
                {
                    org.w3c.dom.Node child =  children.item(i);
                    if ( APP_VAR_ELEMENT_NAME.equals(child.getLocalName()) )
                    {
                       isAppVarsSupported = true; 
                       break;
                    }
                }
                
            }
        }
        return isAppVarsSupported; 
    }
    
        /**
     *
     * @param configDoc  - configuration xml document 
     * @return true if the components configuration document declares
     *         support for application configuration
     *  
     */
    public boolean isAppConfigSupported(Document configDoc)
    {
        boolean isAppConfigSupported = false;
        
        if ( configDoc != null )
        {   
            if ( !isOldConfigNS(configDoc) )
            {
                Element root = configDoc.getDocumentElement();
                org.w3c.dom.NodeList children = root.getChildNodes();
                
                int numNodes = children.getLength();
                
                for ( int i=0; i < numNodes; i++) 
                {
                    org.w3c.dom.Node child = children.item(i);
                    if ( APP_CONFIG_ELEMENT_NAME.equals(child.getLocalName()) )
                    {
                       isAppConfigSupported = true; 
                       break;
                    }
                }
                
            }
        }
        return isAppConfigSupported; 
    }
    
    /**
     *
     */
    public String getActualAttributeName(String name)
    {
        String attribName = name;
        String attribSuffix = "";
        int index = name.lastIndexOf('.');
        if (index != -1)
        {
            attribSuffix = name.substring( index + 1, name.length() );
            if ( attribSuffix != null &&
                 NUMBER_PATTERN.matcher(attribSuffix).matches() )
            {
                attribName = name.substring(0, index);
            }
        }
        return attribName;
        
    }
    /**
     * Convert a string value to the expected OpenType.
     *
     * @param itemStrValue 
     *            string value of the item
     * @param opnenType
     *            the open type to convert the String to.
     * @return 
     *        an instance of the OpenType
     * @exception
     *       throw an exception if the string cannot be converted to the OpenType
     */
    private Object convertToOpenType(OpenType openType, String itemStrValue)
        throws Exception
    {
        try
        {
            String className = openType.getClassName();

            if ( className.equals(CompositeData.class.getName()))
            {
                CompositeType subCompositeType = (CompositeType) openType;
                
               // convert the properties to composite data
                Properties cdProps = convertToProperties(itemStrValue) ;
                CompositeData cd = convertPropertiesToCompositeData(cdProps, subCompositeType);
                return cd;   
            }
            else
            {
                Class objClass = Class.forName(className);
                Class[] argClass = new Class[] {String.class};
                java.lang.reflect.Constructor objConstructor = objClass.getConstructor(argClass);
                String[] argValue = { itemStrValue };
                return objConstructor.newInstance(argValue);
            }
            
        }
        catch ( Exception ex )
        {
            String errMsg = mTranslator.getString(LocalStringKeys.CCFG_FAILED_CONVERSION_TO_OPEN_TYPE, 
                itemStrValue, openType.getTypeName());
            throw new Exception(errMsg);
        }
    }
    
    
    /**
     * Convert a string array value to the expected OpenType array. 
     *
     * @param itemStrValue 
     *            string value of the item
     * @param opnenType
     *            the open type to convert the String to.
     * @return 
     *        an instance of the OpenType
     * @exception
     *       throw an exception if the string cannot be converted to the OpenType
     */
    private Object convertToOpenType(OpenType openType, List<String> itemValueArray)
        throws Exception
    {
        if ( openType.isArray())
        {
            ArrayType arrayType = (ArrayType) openType;
            
            Object[] objArray = (Object[]) java.lang.reflect.Array.newInstance(
                Class.forName(arrayType.getElementOpenType().getClassName()), 
                itemValueArray.size());
            int i = 0;
            for ( String itemValue : itemValueArray )
            {
                objArray[i++] = convertToOpenType(arrayType.getElementOpenType(), itemValue);
            }
            return objArray;
        }
        
        
        String errMsg = mTranslator.getString(
            LocalStringKeys.CCFG_FAILED_CONVERSION_TO_OPEN_TYPE_AR, 
                itemValueArray.toString(), openType.getTypeName());
        throw new Exception(errMsg);
    }
    
    /**
     * 
     * - If item is an array convert to a list of keys : itemName.0..n
     * - If its a CompositeData convert that to a comma separated n/v pairs
     * - If it is a SimpleType convert to string
     *
     * @return the String representation of the type
     */
    private Properties convertToString(OpenType type, Object value, String itemName)
    {
        Properties props = new Properties();
        if ( type.isArray() )
        {
            Object[] itemArray = (Object[]) value;
            
            for ( int i = 0; i < itemArray.length; i++ )
            {
                props.put(itemName + "." + i, itemArray[i].toString());
            }
        }
        else
        {
            if ( type.getClassName().equals(CompositeData.class.getName()) )
            {
                if ( value != null )
                {
                    Properties cdProps = convertCompositeDataToProperties((CompositeData)value);
                    Set keys = cdProps.keySet();
                    StringBuffer compositeStrBuf = new StringBuffer("\"");
                    for( java.util.Iterator itr=keys.iterator(); itr.hasNext(); )
                    {
                        String key = (String) itr.next();
                        compositeStrBuf.append(key + "=" + cdProps.get(key));
                        if ( itr.hasNext())
                        {
                            compositeStrBuf.append(", ");
                        }
                        else
                        {
                            compositeStrBuf.append("\"");
                        }
                    }
                    props.put(itemName, compositeStrBuf.toString());
                }
            }
            else
            {
                props.put(itemName, value.toString());
            }
        }
        return props;
    }
    
    /**
     * @param propsStr
     *          comma delimites n/v pair string example \"n1=v1, n2=v1\"
     * @return the Properties created from the comma separated n/v pair string.
     */
    private Properties convertToProperties(String propsStr)
    {
        propsStr.trim();
        
        if ( propsStr.startsWith("\"") )
        {
            propsStr = propsStr.substring(1);
        }
        if ( propsStr.endsWith("\"") )
        {
            propsStr = propsStr.substring(0,propsStr.length()-1);
        }
       
        String[] nvPairs = propsStr.split(",", -1);
        Properties props = new Properties();
        for ( String nvPair : nvPairs )
        {
            String[] nv = nvPair.split("=", 2);
            if ( nv.length == 2 )
            {
                props.put(nv[0].trim(), nv[1].trim());
            }
        }
        return props;
    }
    
    /**
     * @returns true if the configuration element passed in adheres to the old
     * schema.
     */
    private boolean isOldConfigNS(Document configDoc)
    {
        Element configElement = configDoc.getDocumentElement();
        
        return !(configElement.getNamespaceURI().equals(CONFIGURATION_NS));
    }
    
    /**
     * Check if there are any extra properties not relevant to the app config
     *  
     * @param propKeys - keys in the properties
     * @param itemNames - app config item names
     * @throws Exception if there are invalid keys.
     */
    private void propertyKeysValidCheck(Set propKeys, Set itemNames)
        throws Exception
    {
        Vector<String> invalidKeys = new Vector();
        
        for ( Object obj : propKeys )
        {
            boolean found = false;
            
            String key = getActualAttributeName((String) obj);
            
            for ( Object itemName : itemNames )
            {
                if ( itemName.equals(key) )
                {
                    found = true;
                }
            }
            
            if ( !found )
            {
                invalidKeys.add( (String) key);
                
            }
        }
        
        if ( !invalidKeys.isEmpty() )
        {
            String errMsg = mTranslator.getString(
                            LocalStringKeys.CCFG_CD_EXTRA_PROPS,
                             convertToString(invalidKeys));
            throw new Exception(errMsg);
        }
    }
    
    /**
     * Convert the objects in the collection to string.
     *
     * @param set a non-empty set
     */
    protected String convertToString(java.util.Collection colxn)
    {
        StringBuffer strBuf = new StringBuffer("[ ");
        if ( !colxn.isEmpty() )
        {
            java.util.Iterator itr = colxn.iterator();
            while( itr.hasNext() )
            {
                strBuf.append(itr.next().toString());
                if ( itr.hasNext() )
                {
                    strBuf.append(", ");
                }
                else
                {
                    strBuf.append(" ");
                }
            }
        }
        strBuf.append(" ]");
        return strBuf.toString();
    } 
    
    /**
     * Convert the type case
     *
     * @param type the type string
     */
    private String convertTypeCase(String type)
    {
        if ( type != null )
        {
            return type.toUpperCase();
        }
        
        return type;
        /**
        String firstChar = type.substring(0,1);
        String remainingChars = type.substring(1, type.length());
        return ( firstChar.toUpperCase() + remainingChars.toLowerCase());
         */
    }
    
}
