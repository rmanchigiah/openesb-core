/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestEnvironmentAccess.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.util;

import com.sun.jbi.EnvironmentContext;

/**
 * Tests for the EnvironmentAccess class.
 *
 * @author Mark S White
 */
public class TestEnvironmentAccess
    extends junit.framework.TestCase
{
    /**
     * The constructor for this testcase, forwards the test name to
     * the jUnit TestCase base class.
     * @param aTestName String with the name of this test.
     */
    public TestEnvironmentAccess(String aTestName)
    {
        super(aTestName);
    }

    /**
     * Test the getContext() and setContext() methods.
     */
    public void testGetSetContext()
    {
        // Call the getter before the setter is called. A null return value
        // is expected.

        assertNull("Non-null value returned, expected a null",
            EnvironmentAccess.getContext());

        // Now create a fake EnvironmentContext and call the setter, then the
        // getter method again. The fake EnvironmentContext should be returned.

        ScaffoldEnvironmentContext fakeContext = new ScaffoldEnvironmentContext();
        EnvironmentAccess.setContext(fakeContext);
        assertSame("Wrong instance returned",
            EnvironmentAccess.getContext(), fakeContext);

        // Now create another fake EnvironmentContext and call the setter, then
        // the getter method again. The original fake EnvironmentContext should
        // be returned.

        ScaffoldEnvironmentContext newContext = new ScaffoldEnvironmentContext();
        EnvironmentAccess.setContext(newContext);
        assertSame("Wrong instance returned",
            EnvironmentAccess.getContext(), fakeContext);
    }
}
