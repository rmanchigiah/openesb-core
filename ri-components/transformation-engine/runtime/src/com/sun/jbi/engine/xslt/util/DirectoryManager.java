/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DirectoryManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.engine.xslt.TEResources;

import java.io.File;


/**
 * 
 *
 * @author Sun Microsystems, Inc.
 * 
 */
public class DirectoryManager
    implements TEResources
{
    /**
     *    
     */
    private static StringTranslator sTranslator =
        new StringTranslator("com.sun.jbi.engine.xslt", null);

    /**
     * DOCUMENT ME!
     *
     * @param dirName DOCUMENT ME!
     *
     * @throws Exception exception
     */
    public static void deleteDir(String dirName)
        throws Exception
    {

        File dir = new File(dirName);

        if (dir.isDirectory())
        {
            String [] children = dir.list();

            for (int i = 0; i < children.length; i++)
            {

                boolean success =
                    (new File(dirName + File.separator + children[i])).delete();

                if (!success)
                {
                    throw new Exception(sTranslator.getString(
                            TEResources.DELETE_FAILED));
                }
            }
        }

        // The directory is now empty so delete it
        boolean success = dir.delete();

        if (!success)
        {
            throw new Exception(sTranslator.getString(TEResources.DELETE_FAILED));
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param directoryOrFile NOT YET DOCUMENTED
     *
     * @throws Exception exception
     */
    public static void deleteRecursively(String directoryOrFile)
        throws Exception
    {
        if (directoryOrFile == null)
        {
            return;
        }

        File directory = new File(directoryOrFile);

        try
        {
            if (directory.isFile())
            {
                directory.delete();

                return;
            }

            File [] fileList = directory.listFiles();

            if (fileList == null)
            {
                deleteRecursively(directory.getAbsolutePath());

                return;
            }

            if (fileList.length == 0)
            {
                directory.delete();

                return;
            }
            else
            {
                for (int i = 0; i < fileList.length; i++)
                {
                    File entry = fileList[i];

                    if (entry.isFile())
                    {
                        entry.delete();
                    }

                    if (entry.isDirectory())
                    {
                        deleteRecursively(entry.getAbsolutePath());
                    }
                }

                boolean success = directory.delete();

                if (!success)
                {
                    throw new Exception(directory.getAbsolutePath()
                        + sTranslator.getString(TEResources.DELETE_FAILED));
                }
            }
        }
        catch (Exception e)
        {
            throw new Exception(directory.getAbsolutePath()
                + sTranslator.getString(TEResources.DELETE_FAILED));
        }

        return;
    }
}
