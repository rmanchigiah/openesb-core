/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SUDescriptorReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt.util;

import com.sun.jbi.engine.xslt.TEResources;
import com.sun.jbi.engine.xslt.TransformationEngineContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Attr;

import org.w3c.dom.NamedNodeMap;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.namespace.QName;

import java.util.StringTokenizer;


/**
 * 
 * @author Sun Microsystems Inc.
 */
public class SUDescriptorReader
    extends UtilBase
    implements TEResources
{
    /**
     * Document object of the XML config file.
     */
    private Document mDoc;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * i18n
     */
    private StringTranslator mTranslator;

    /**
     *    
     */
    private ConfigBean[] mEPList;

    /**
     * The total number of end points in the config file
     */
    private int mTotalEndpoints = 0;

    private int mNoOfConsumers;
    
    private int mNoOfProviders;
    
    
    /**
     * Creates a new ConfigReader object.
     */
    public SUDescriptorReader()
    {
        mLog = TransformationEngineContext.getInstance().getLogger("");
        mTranslator = new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());
        setValid(true);
    }
    
    /**
     *
     *
     * @return  NOT YET DOCUMENTED
     */
    public int getConsumerCount()
    {
        return mNoOfConsumers;
    }

    /**
     *
     *
     * @param node  NOT YET DOCUMENTED
     * @param ep  NOT YET DOCUMENTED
     */
    public void setEndpoint(
        Node node,
        ConfigBean ep)
    {
        NamedNodeMap map = node.getAttributes();

        try
        {
            String epname = map.getNamedItem("endpoint-name").getNodeValue();
            String sername = map.getNamedItem("service-name").getNodeValue();
            mLog.finer("LOOK OUT" + epname + " " + sername);

            ep.setValue(ConfigData.SERVICE_NAMESPACE, getNamespace(sername));
            ep.setValue(ConfigData.SERVICENAME_LOCALPART, getLocalName(sername));
            ep.setValue(ConfigData.ENDPOINT, epname);
        }
        catch (Exception e)
        {
            mLog.severe("Failed loading DD");
            e.printStackTrace();
            setError(getError() + mTranslator.getString(TE_LOAD_DD_FAILED)
                + "\n" + e.getMessage());
            setValid(false);
        }
        
    }
    
    public boolean isPresent(QName sername, String epname)
    {
        mLog.finer("Cheking for " + sername + " " + " " + epname);
        for (int ep = 0; ep < mEPList.length; ep++)
        {
            QName sname = new QName ((String)mEPList[ep].getValue(ConfigData.SERVICE_NAMESPACE).elementAt(0), 
                            (String)mEPList[ep].getValue(ConfigData.SERVICENAME_LOCALPART).elementAt(0) );
            
            mLog.finer("Comparing "+ sname.toString()+ " with "+ sername.toString());
            
            String epn = (String)mEPList[ep].getValue(ConfigData.ENDPOINT).elementAt(0);
            
            mLog.finer("And Comparing "+ epn+ " with "+ epname);
            
            if ((sname.toString().equals(sername.toString()))
                && (epn.equals(epname)))
            {
                mLog.finer("Found Service with name: "+ sname.toString() + " and endpoint name: "+ epname);
                return true;
            }                         
        }
        mLog.finer("isPresent returning false. Nothing useful found.");
        return false;
    }

    /**
     *
     *
     * @return  NOT YET DOCUMENTED
     */
    public int getProviderCount()
    {
        return mNoOfProviders;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Document doc)
    {
        try
        {
            mDoc = doc;
            mDoc.getDocumentElement().normalize();
            //check();
        }
        catch (Exception genException)
        {
            mLog.severe(mTranslator.getString(TE_LOAD_DD_FAILED));
            genException.printStackTrace();
            setException(genException);
            setError(getError() + mTranslator.getString(TE_LOAD_DD_FAILED)
                + "\n" + genException.getMessage());
            setValid(false);
        }
    }
    
    public ConfigBean[] loadServices()
    {
        NodeList providers = mDoc.getElementsByTagName("provides");
        mNoOfProviders = providers.getLength();
            
        mEPList = new ConfigBean[mNoOfProviders];

        for (int i = 0; i < mNoOfProviders; i++)
        {
            Node node = providers.item(i);
            ConfigBean sb = new ConfigBean();
            setEndpoint(node, sb);

            if (!isValid())
            {
                setError(mTranslator.getString(TE_LOAD_DD_FAILED) + "\n"
                    + getError());

                return null;
            }

            mEPList[i] = sb;
        }
        
        return mEPList;
    }
    /**
     *
     */
    public void check()
    {
        try
        {
            NodeList providers = mDoc.getElementsByTagName("provides");
            mNoOfProviders = providers.getLength();

            NodeList consumers = mDoc.getElementsByTagName("consumes");
            mNoOfConsumers = consumers.getLength();
            
            if (getConsumerCount() != 0)
            {
                setError(mTranslator.getString(TE_INCONSISTENT_CONSUMER_DATA));
                setValid(false);
            }       

            if (getProviderCount() != 1)
            {
                setError(mTranslator.getString(TE_INCONSISTENT_PROVIDER_DATA));
                setValid(false);
            }
        }
        catch (Exception e)
        {
            mLog.severe("Failed loading DD");
            e.printStackTrace();
            setError(getError() + mTranslator.getString(TE_LOAD_DD_FAILED)
                + "\n" + e.getMessage());
            setValid(false);
        }
    }

    /**
     * Gets the local name from the quname.
     *
     * @param qname Qualified name of service.
     *
     * @return String local name
     */
    private String getLocalName(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");

        try
        {
            if (tok.countTokens() == 1)
            {
                return qname;
            }

            tok.nextToken();

            return tok.nextToken();
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * Gets the namespace from the qname.
     *
     * @param qname Qname of service
     *
     * @return namespace namespace of service
     */
    private String getNamespace(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");
        String prefix = null;

        try
        {
            if (tok.countTokens() == 1)
            {
                return "";
            }

            prefix = tok.nextToken();

            NamedNodeMap map = mDoc.getDocumentElement().getAttributes();

            for (int j = 0; j < map.getLength(); j++)
            {
                Node n = map.item(j);

                if (n.getLocalName().trim().equals(prefix.trim()))
                {
                    return n.getNodeValue();
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

        return "";
    }
    
    // ********************************* New APIs *********************** //
    
    public String getExtensionAttribute(String attrName)
    {
        NamedNodeMap attrNodeMap = null;
        String attrValue = null;
        try
        {
            // Retrieve the "TEServiceExtns" node from the document
            NodeList nl = mDoc.getElementsByTagNameNS(ConfigData.TE_NAMESPACE, "TEServiceExtns");
            Node teXtnNode = nl.item(0);
            attrNodeMap = teXtnNode.getAttributes();

            for(int i=0; i<attrNodeMap.getLength(); i++)
            {
                if(attrNodeMap.item(i).getLocalName().equals(attrName))
                {
                    attrValue = ((Attr)attrNodeMap.item(i)).getValue();
                    break;
                }
            }

            mLog.fine("Extn Attribute:" + attrValue);
        }
        catch(Exception e)
        {
            mLog.severe("Exception Getting Attribute: "+e.getMessage());
            e.printStackTrace();
        }
        catch(Throwable t)
        {
            t.printStackTrace();
            mLog.severe("Got a throwable thing"+t.getMessage());
        }
        return attrValue;
    }
    
    public String getExtnNodeValue(String nodename)
    {
        String nodeVal = null;
        NamedNodeMap nodemap = null;
        
        NodeList nl = mDoc.getElementsByTagNameNS(ConfigData.TE_NAMESPACE, "TEServiceExtns");
        Node teXtnNode = nl.item(0);
        NodeList chNodes = teXtnNode.getChildNodes();
        
        for(int i=0; i < chNodes.getLength(); i++ )
        {
            if(chNodes.item(i).getNodeType() == Node.ELEMENT_NODE)
            {
                if( chNodes.item(i).getLocalName().equals(nodename) )
                {
                    Element el = (Element) chNodes.item(i);
                    try
                    {
                        nodeVal = ((Node) (el.getChildNodes().item(0))).getNodeValue().trim();
                    }
                    catch (NullPointerException ne)
                    {
                        ne.printStackTrace();
                    }
                    mLog.fine(nodename+" = "+ nodeVal);
                    break;
                }
            }
        }

        return nodeVal;
    }
    
    /**
     * DOCUMENT ME!
     *
     * @param nodeList - Node List
     * @param name - element name (need not be an extension element)
     *
     * @return Node matching node.
     */
    private Node getElementNodeByName(
        NodeList nodeList,
        String name)
    {
        Node retNode = null;

        for (int g = 0; g < nodeList.getLength(); g++)
        {
            Node n = nodeList.item(g);

            if ((n != null) && (n.getNodeType() == Node.ELEMENT_NODE))
            {
                mLog.finer("Node:"+n.getNodeName());
                if ((n.getLocalName().equals(name)))
                {
                    retNode = n;
                    break;
                }
            }
        }
        // outer for loop
        return retNode;
    }
    
    /**
     * Utility method for setting the Bean object from the XML Nodes.
     *
     * @param node The node which needs to be
     * @param sb The end point bean object which has to be updated
     * @param tagName the tag name which needs to be read.
     */
    private void setByTagName(
        Node node,
        ConfigBean sb,
        String tagName)
    {
        Element ele = (Element) node;

        mLog.finer(" Element: "+ele.toString());
        mLog.finer(" tagName: "+tagName);
        NodeList namelist =
            ele.getElementsByTagNameNS(ConfigData.TE_NAMESPACE, tagName);

        if (namelist != null)
        {
            for (int i = 0; i < namelist.getLength(); i++)
            {
                Element name = (Element) namelist.item(i);
                String sValue = null;

                try
                {
                    sValue =
                        ((Node) (name.getChildNodes().item(0))).getNodeValue()
                         .trim();
                }
                catch (NullPointerException ne)
                {
                    ne.printStackTrace();

                    return;
                }

                mLog.finer("** " + tagName + " = " + sValue + " **");
                sb.setValue(tagName, sValue);
            }
        }
        else
        {
            mLog.finer("namelist null for tagname " + tagName);
        }
    }
    
    public void populateServiceInformation(ConfigBean sBean)
    {
        String val = getExtnNodeValue(ConfigData.SERVICEID);

        if (val != null)
        {
            mLog.finer("Service Id = " + val);
            sBean.setValue(ConfigData.SERVICEID, val);
        }
        else
        {
            mLog.severe("Service Id is NULL.");
        }

        val = getExtnNodeValue(ConfigData.CREATE_CACHE);

        if (val != null)
        {
            mLog.finer("Create Cache = " + val);
            sBean.setValue(ConfigData.CREATE_CACHE, val);
        }
        else
        {
            mLog.finer("Create Cache is NULL.");
        }

        val = getExtnNodeValue(ConfigData.COLUMN_SEPARATOR);

        if (val != null)
        {
            mLog.finer("Column Separator = " + val);
            sBean.setValue(ConfigData.COLUMN_SEPARATOR, val);
        }
        else
        {
            mLog.finer("Column Separator is not specified.");
        }

        val = getExtnNodeValue(ConfigData.FIRST_ROW_COL_HEADERS);

        if (val != null)
        {
            mLog.finer("First Column Headers? = " + val);
            sBean.setValue(ConfigData.FIRST_ROW_COL_HEADERS, val);
        }
        else
        {
            mLog.finer("First Column Header is Not specified..");
        }
        
        populateColumnInfo(sBean);
    }
    
    private void populateColumnInfo(ConfigBean bean)
    {
        NodeList nl = mDoc.getElementsByTagNameNS(ConfigData.TE_NAMESPACE, "TEServiceExtns");
        Node teXtnNode = nl.item(0);
        NodeList chNodes = teXtnNode.getChildNodes();
        
        Node colhs = getElementNodeByName(chNodes, ConfigData.COLUMN_HEADERS);
        
        if (colhs != null)
        {
            setByTagName(colhs, bean, ConfigData.COLUMN_HEADER);
        }
        
    }
}
