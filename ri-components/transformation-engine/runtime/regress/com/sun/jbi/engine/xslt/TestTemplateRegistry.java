/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestTemplateRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import junit.framework.*;
import java.io.File;
import java.io.FileInputStream;
import java.util.logging.Logger;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.Templates;


/**
 * DOCUMENT ME!
 *
 * @author root
 */
public class TestTemplateRegistry extends TestCase
{

    /**
     * DOCUMENT ME!
     */
    private StreamSource mSource;

    /**
     * DOCUMENT ME!
     */
    private TemplateCommand mCommand;

    /**
     * DOCUMENT ME!
     */
    private TemplateRegistry mTemplateRegistry;

    /**
     * DOCUMENT ME!
     */
    private TemplateRegistry mTemplateRegistry2;

    /**
     * DOCUMENT ME!
     */
    private String mXslt;

    /**
     * Creates a new TestTemplateRegistry object.
     *
     * @param testName DOCUMENT ME!
     */
    public TestTemplateRegistry(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestTemplateRegistry.class);
        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        System.out.println("testSetup");
        /*String srcroot = System.getProperty("junit.srcroot");

        try
        {
            mXslt = srcroot + "/engine/xslt/regress/data/test.xslt";
            mSource = new StreamSource ( new FileInputStream(mXslt));
            mCommand = new TemplateCommand("key1", mSource);
        }
        catch (Exception jbiException)
        {
            jbiException.printStackTrace();
        }*/

    }

    /**
     * Test of getRegistry method, of class com.sun.jbi.engine.xslt.util.TemplateRegistry.
     */
    public void testGetRegistry()
    {
        /*System.out.println("testGetRegistry");
        mTemplateRegistry = TemplateRegistry.getRegistry();
        assertNotNull("Retrieved Template is Null", mTemplateRegistry);*/
    }

    /**
     * Test if the retrieved registry is the same, of class com.sun.jbi.engine.xslt.util.TemplateRegistry.
     */
    public void testCompareRegistry()
    {
        /*System.out.println("testCompareRegistry");
        mTemplateRegistry = TemplateRegistry.getRegistry();
        mTemplateRegistry2 = TemplateRegistry.getRegistry();
        assertNotNull("Retrieved Template Registry is Null", mTemplateRegistry);
        assertNotNull("Retrieved Template Registry is Null", mTemplateRegistry2);
        assertEquals("putTransformerfactory failed", mTemplateRegistry, mTemplateRegistry2);*/
    }

    /**
     * Test of get method, of class com.sun.jbi.engine.xslt.util.TemplateRegistry.
     */
    public void testGet()
    {
        /*System.out.println("testGet");
        TemplateCommand command = new TemplateCommand("key2", mSource);
        command.execute();
        mTemplateRegistry = TemplateRegistry.getRegistry();
        Templates tpl = (Templates) mTemplateRegistry.get("key2");
        assertNotNull("Retrieved Template is Null", tpl);*/
    }

    /**
     * Test of put method for class com.sun.jbi.engine.xslt.util.TemplateRegistry.
     */
    public void testPut()
    {
	/*
        System.out.println("testPut");
        TemplateCommand command = new TemplateCommand("key3", mSource);
        command.execute();
        mTemplateRegistry = TemplateRegistry.getRegistry();
        Templates tpl = (Templates) mTemplateRegistry.get("key3");
        assertNotNull("Retrieved Template is Null", tpl);
        mTemplateRegistry.put("key4", tpl);  
        Templates tpl2 = (Templates) mTemplateRegistry.get("key4");
        assertNotNull("Template is NULL" , tpl2);
	*/
    }
}
