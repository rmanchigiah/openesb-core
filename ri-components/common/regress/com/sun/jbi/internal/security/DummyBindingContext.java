/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DummyBindingContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  DummyBindingContext.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 3, 2004, 7:01 PM
 */

package com.sun.jbi.internal.security;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class DummyBindingContext implements com.sun.jbi.component.ComponentContext
{
   
    
    /** Name */
    String mName;
    
    /** 
     * Creates a new instance of SampleBindingContext 
     * @param name is the Component Name
     */
    public DummyBindingContext (String name)
    {

        mName = name; 
    }
    
    /**
     * Registers the named endpoint with the NMR.
     * @param serviceName the qualified name of the service exposed by the
     * endpoint.
     * @param endpointName the name of the endpoint to be registered.
     * @return a reference to the registered endpoint.
     * @throws javax.jbi.JBIException if the endpoint cannot be registered.
     */
    public javax.jbi.servicedesc.ServiceEndpoint activateEndpoint(
        javax.xml.namespace.QName serviceName, String endpointName)
        throws javax.jbi.JBIException
    {
        return null;
    }

    /**
     * Queries the NMR for endpoints registered against the specified service.
     * @param serviceName the qualified name of the service.
     * @return array of available endpoints for the specified service, can
     * be empty if none exist.
     * @throws javax.jbi.JBIException if the service reference is invalid.
     */
    public javax.jbi.servicedesc.ServiceEndpoint[] getEndpointsForService(
        javax.xml.namespace.QName serviceName)
    {
        return null;
    }

    /**
     * Deregisters the specified endpoint with the NMR.
     * @param endpoint reference to the endpoint to be deregistered.
     * @throws javax.jbi.JBIException if the endpoint cannot be deregistered.
     */
    public void deactivateEndpoint(
        javax.jbi.servicedesc.ServiceEndpoint endpoint)
        throws javax.jbi.JBIException
    {
    }

    /**
     * Deregisters the specified external endpoint with the NMR.  This indicates
     * to the NMR that external service consumers can no longer access the
     * internal service by this name.
     * @param externalEndpoint the external endpoint to be deregistered.
     * @throws javax.jbi.JBIException if the endpoint cannot be deregistered.
     */
    public void deregisterExternalEndpoint(
        javax.jbi.servicedesc.ServiceEndpoint externalEndpoint)
        throws javax.jbi.JBIException
    {
    }

    /** 
     * @return null, never used in junit tests.
     */
    public void registerXAResource(javax.transaction.xa.XAResource resource) 
        throws javax.jbi.JBIException
    {
    }

    /** 
     * @return null, never used in junit tests.
     */
    public javax.jbi.messaging.DeliveryChannel getBindingChannel () 
        throws javax.jbi.messaging.MessagingException
    {
        return null;
    }
    
    
    /**
     * @return the Component Name
     */
    public String getComponentName ()
    {
        return mName;
    }
    
    /**
     * @return null, not needed by junit tests
     */
    public String getComponentRoot ()
    {
        return null;
    }
    
    /** 
     * @return null, never used in junit tests.
     */
    public javax.jbi.messaging.DeliveryChannel getDeliveryChannel () 
        throws javax.jbi.messaging.MessagingException
    {
        return null;
    }
    
    /**
     * Get the service description for the named endpoint, if any exists.
     * @param service the qualified name of the endpoint's service.
     * @param name the name of the endpoint.
     * @return the named endpoint, or <code>null</code> if the named endpoint
     * is not active.
     */
    public javax.jbi.servicedesc.ServiceEndpoint getEndpoint(
        javax.xml.namespace.QName service,
        String name)
    {
        return null;
    }
    
    /**
     * Queries the NMR for active endpoints that implement the given interface.
     * This will return the endpoints for all services and endpoints that 
     * implement the named interface (portType in WSDL 1.1). This method does 
     * NOT include external endpoints (those registered using 
     * registerExternalEndpoint(ServiceEndpoint)).
     * @param interfaceName qualified name of interface/portType that is 
     *  implemented by the endpoint
     * @return ServiceEndpoint[] list of available endpoints for the specified 
     *  interface name; potentially zero-length.
     */
    public javax.jbi.servicedesc.ServiceEndpoint[] getEndpoints(
        javax.xml.namespace.QName interfaceName)
    {
        return null;
    }

    /**
     * Retrieve metadata for the specified endpoint.
     * @param endpoint reference to the endpoint.
     * @return the metadata describing the endpoint or <code>null</code> if
     * metadata is unavailable.
     * @throws javax.jbi.JBIException if the endpoint reference is invalid.
     */
    public org.w3c.dom.Document getEndpointDescriptor(
        javax.jbi.servicedesc.ServiceEndpoint endpoint)
        throws javax.jbi.JBIException
    {
        return null;
    }

    /**
     * Queries the NMR for external endpoints that implement the specified
     * interface name.
     * @param interfaceName the qualified name of the interface/portType that
     * is implemented by the endpoints.
     * @return array of available external endpoints for the specified interface
     * name; can be empty if none exist.
     * @throws javax.jbi.JBIException if the interface name is invalid.
     */
    public javax.jbi.servicedesc.ServiceEndpoint[] getExternalEndpoints(
       javax.xml.namespace.QName interfaceName)
    {
        return null;
    }

    /**
     * Queries the NMR for external endpoints that are part of the specified
     * service.
     * @param serviceName the qualified name of the service that contains the
     * endpoints.
     * @return array of available external endpoints for the specified service
     * name; can be empty if none exist.
     * @throws javax.jbi.JBIException if the service name is invalid.
     */
    public javax.jbi.servicedesc.ServiceEndpoint[] getExternalEndpointsForService(
        javax.xml.namespace.QName serviceName)
    {
        return null;
    }

    /**
     * @return null, not needed by junit tests
     */
    public String getInstallRoot ()
    {
        return null;
    }
    
    /**
     * @return null, not needed by junit tests
     */
    public java.util.logging.Logger getLogger(String name, String rbName)
    {
        return null;
    }
    
    /**
     *  @return null, not needed by junit tests
     */
    public javax.jbi.management.MBeanNames getMBeanNames ()
    {
        return null;
    }
    
    /**
     *  @return null, not needed by junit tests
     */
    public javax.management.MBeanServer getMBeanServer ()
    {
        return null;
    }
    
    /**
     *  @return null, not needed by junit tests
     */
    public com.sun.jbi.management.ManagementMessageFactory getManagementMessageFactory ()
    {
        return null;
    }
    
    /**
     *  @return null, not needed by junit tests
     */
    public javax.naming.InitialContext getNamingContext ()
    {
        return null;
    }
    
    /**
     * Queries the NMR for registered endpoints that implement the specified
     * interface. This will return the endpoints for all services and endpoints
     * that implement the named interface.
     * @param interfaceName the qualified name of the interface/portType that
     * is implemented by the endpoint.
     * @return array of available endpoints for the specified interface name;
     * can be empty if none exist.
     * @throws javax.jbi.JBIException if the interface reference is invalid.
     */
    public javax.jbi.servicedesc.ServiceEndpoint[] getRegisteredEndpoints(
        javax.xml.namespace.QName interfaceName)
        throws javax.jbi.JBIException
    {
        return null;
    }

    public com.sun.jbi.StringTranslator getStringTranslator (String packageName)
    { 
        return null;
    }
    
    /**
     */
    public com.sun.jbi.StringTranslator getStringTranslatorFor (Object object)
    {
        return null;
    }
    
    /**
     * @return null, not needed by junit tests
     */
    public String getWorkspaceRoot ()
    {
        return null;
    }
    
    /** 
     * @return the SecurityHandler for the Binding
     */
    public com.sun.jbi.binding.security.SecurityHandler getSecurityHandler()
    {
        return null;
    }
    
    /**
     * Get a TransactionManager from the AppServer.
     * @return A TransactionManager instance.
     */
    public Object getTransactionManager()
    {
        return null;
    }
    
    /**
     * Register a logger.
     * @param logger The logger instance.
     * @param name The logger name.
     * @throws javax.jbi.JBIException if the logger cannot be registered.
     */
    public void registerLogger(java.util.logging.Logger logger, String name)
        throws javax.jbi.JBIException
    {
        return;
    }
    
    /**
     * Unregister a logger.
     * @param logger The logger instance.
     * @throws javax.jbi.JBIException if the logger cannot be unregistered.
     */
    public void unregisterLogger(java.util.logging.Logger logger)
        throws javax.jbi.JBIException
    {
        return;
    }
    
        
    /**
     */
    public java.util.logging.Logger getLogger(String logger)
    {
        return null;
    }
    
    /**
     * Registers the specified external endpoint with the NMR.  This indicates
     * to the NMR that the specified endpoint is used as a proxy for external
     * service consumers to access an internal service by the same name.
     * @param externalEndpoint the external endpoint to be registered.
     */
    public void registerExternalEndpoint(
        javax.jbi.servicedesc.ServiceEndpoint externalEndpoint)
    {
    }

    
    public javax.jbi.servicedesc.ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment endpointReference)
    {
        return null;
    }
	
    public com.sun.jbi.wsdl2.WsdlFactory getWsdlFactory () throws 
			com.sun.jbi.wsdl2.WsdlException
    {
	return null;
    }

    /**
     * Returns a KeyStoreUtil object that allows components to create,
     * read, update, and delete keys.  The KeyStoreUtil also allows the
     * encryption and decryption of text using the keys stored in the
     * private KeyStore.
     *
     * @return       An instance of a KeyStoreUtil
     */
    public com.sun.jbi.security.KeyStoreUtil getKeyStoreUtil() {
        return null;
    }


}
