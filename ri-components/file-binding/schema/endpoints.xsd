<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)endpoints.xsd
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->

<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
	     targetNamespace="http://www.sun.com/ns/jbi/bindings/filebinding/deploy/endpoint-config"
	     xmlns="http://www.sun.com/ns/jbi/bindings/filebinding/deploy/endpoint-config"
	     elementFormDefault="qualified">
<!-- Aggregate tag for all endpoints -->            
<xs:element name="endpoint-list">
  <xs:complexType>
    <xs:sequence>      
      <!-- Information corresponding to an Endpoint -->
      <xs:element name="endpoint" minOccurs="1" maxOccurs="unbounded">
        <xs:complexType>
          <xs:sequence>               
          
          <!-- Service name for which this endpoint has to be exposed -->         
           <xs:element name="service" type="qnameType" minOccurs="1" maxOccurs="1"/>          
          
           <!-- Name of the interface which the above service implements -->
           
           <xs:element name="interface" type="qnameType" minOccurs="1" maxOccurs="1"/>    
            <!-- The name of this endpoint , in case of a provider endpoint it is
            a user defined value in case of a provider endpoint , for a consumer endpoint
            it could be one of the valid endpoints that are activated by Service engines.
            For a consumer , In case this endpoint name does not macth any of the 
            available endpoints, then a default endpoint for that service will be picked-->
            <!-- It is compulsory to give a name, and it is used by the binding as above-->
                                  
            <xs:element name="endpoint-name" type="xs:NCName" minOccurs="1" maxOccurs="1" />
            
            <!-- This should be either "provider" or "consumer" and denotes the role 
            of this endpoint in a message exchange scenario -->             
            
            <xs:element name="endpoint-role" minOccurs="1" maxOccurs="1">
                <xs:simpleType>
			<xs:restriction base="xs:string">
				<xs:enumeration value="provider"/>
				<xs:enumeration value="consumer"/>							
  			</xs:restriction>
		</xs:simpleType>  
            </xs:element>

            
            <!-- A folder name , that should exist in the local or mounted file schemaorg_apache_xmlbeans.system
            from which files for this endpoint will be read. 
            Should have read/execute permissions. If its not present during
            deployment the binding tries to create it. -->
                        
            <xs:element name="input-dir" type="xs:string" minOccurs="1" maxOccurs="1"/>
            
            <!-- A folder in the local or mounted file schemaorg_apache_xmlbeans.system, to which the files received
            from the service will be written to. Should have write permissions.
            This folder will be created by binding if it does not exist during startup
            -->
            
	    <xs:element name="output-dir" type="xs:string" minOccurs="1" maxOccurs="1"/>
            
            <!-- A folder in the local or mounted file schemaorg_apache_xmlbeans.system, to which the files are
            are moved after sending to a provider and receiving a response. 
            Should have write permissions.
            This folder will be created by binding if it does not exist during startup
            -->
            
            <xs:element name="processed-dir" type="xs:string" minOccurs="1" maxOccurs="1"/>            
            
            <!-- Java regular expression based pattern used for filtering out files. Files
            that match this pattern will be read from the input folder and processed
            -->
            <xs:element name="input-pattern" type="xs:string" minOccurs="0" maxOccurs="1"/>	
                        
            <!-- Denotes the WSDL operation name that has to be invoked in the target service -->
            <xs:element name="operation" type="operationType" minOccurs="1" maxOccurs="unbounded"/>	
                 
          </xs:sequence>
        </xs:complexType>
      </xs:element>
    </xs:sequence>
  </xs:complexType>
</xs:element>

<!-- This is a common type to represent a QName object
-->

<xs:complexType name="qnameType">
    <xs:sequence minOccurs="1" maxOccurs="1">
          
            <!-- The namespace for this service. Corresponds to 
                 target namespace in WSDL-->     

            <xs:element name="namespace-uri" type="xs:anyURI"/>
          
            <!-- Name of service for which this endpoint has to be activated -->
            <!-- Corresponds to WSDL service name -->

            <xs:element name="local-part" type="xs:NCName"/>
     </xs:sequence>
</xs:complexType> 

<!-- Aggregate type for operations information -->
<xs:complexType name="operationType">
	<xs:sequence minOccurs="1" maxOccurs="1">
        
        <!-- 
            The namespace of the operation which has to be infvoked on the service
        -->
        
        <xs:element name = "name" type="qnameType"/>
        
        
        <!-- WSDL Message Exchange pattern for this operation. Should be a valid 
        URI according to WSDL specification -->
	
        <!-- Only in-only, inout and robust inonly are supported by file binding-->
        		
        <xs:element name="mep" minOccurs="1" maxOccurs="1">	
		<xs:simpleType>
			<xs:restriction base="xs:anyURI">
				<xs:enumeration value="http://www.w3.org/ns/wsdl/in-only"/>
				<xs:enumeration value="http://www.w3.org/ns/wsdl/in-out"/>							
				<xs:enumeration value="http://www.w3.org/ns/wsdl/in-opt-out"/>
				<xs:enumeration value="http://www.w3.org/ns/wsdl/robust-in-only"/>	
  			</xs:restriction>
		</xs:simpleType>
	</xs:element>
        
        <!-- Should be XML or attachments. This governs where the file 
        goes into a normalized message, content or attachment Ths binding expects
        any message received for this endpoint to be of this type -->
        
        <xs:element name="input-message-type" type="messageType" minOccurs="0" maxOccurs="1"/>	        
        
        <!-- This should be either an attachment or an XML file, the behavior is
		of file binding in creating an output file is governed by this -->
        
 	    <xs:element name="output-message-type" type="messageType" minOccurs="0" maxOccurs="1"/>

            <!-- Prefix which will be attached to the file name before being written to the
            output folder. Could be useful in identifying or mappgin out messages
            with corresponding operation names, or anything else the user wishes -->
            <xs:element name="output-file-prefix" type="xs:string" minOccurs="0" default="out"/>
            
            <!-- User defined extension that will be used to create the output file name in 
            a response or a provider scenario,
            This overrides the extension of the file name which is being read and sent.
            Extension, if any supplied by the engine in any Message headers will override this
            -->
            <xs:element name="output-file-extension" type="xs:string" minOccurs="0" default="xml"/>        
        </xs:sequence>
        </xs:complexType>
        
        <!-- Possible message types in file binding  This is work in progress
         could undergo changes -->
        
        <xs:simpleType name="messageType">
          	<xs:restriction base="xs:string">
			<xs:enumeration value="attachment"/>
			<xs:enumeration value="xml"/>
		</xs:restriction>
				
</xs:simpleType>

</xs:schema>
