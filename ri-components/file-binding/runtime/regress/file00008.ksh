#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)file00008.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "filebinding00005.ksh- Test MEPs - file binding test"

. ./regress_defs.ksh

# create test engine

$JBI_ANT -Djbi.install.file=$FILEBINDING_BLD_DIR/testengine/testengine.jar install-component

# start out tests tests

$JBI_ANT -Djbi.component.name=TESTENGINE_FOR_FILEBINDING start-component

echo
echo "Testing out-only with valid message"
grep "Test-GOODOUTONLY-Passed" $DOMAIN_LOG | wc -l
ls $FILEBINDING_BLD_DIR/test/outboundoutput | wc -l
echo

echo
echo "Testing out-only with invalid message"
grep "Test-BADOUTONLY-Passed" $DOMAIN_LOG | wc -l
ls $FILEBINDING_BLD_DIR/test/outboundoutput | wc -l
echo

# put  a file in input folder to start in-out exchange

# starting in-out test

cp $FILEBINDING_BLD_DIR/test/sample_files/inout.xml $FILEBINDING_BLD_DIR/test/input/transforminout

sleep 2

echo
echo "Testing in-out with valid message"
grep "Test-GOODINOUT-Passed" $DOMAIN_LOG | wc -l
ls $FILEBINDING_BLD_DIR/test/output | wc -l
ls $FILEBINDING_BLD_DIR/test/processed | wc -l
echo

cp $FILEBINDING_BLD_DIR/test/sample_files/inonly.xml $FILEBINDING_BLD_DIR/test/input/transforminonly

sleep 2

echo
echo "Testing in-only with valid message"
grep "Test-GOODINONLY-Passed" $DOMAIN_LOG | wc -l
ls $FILEBINDING_BLD_DIR/test/output | wc -l
ls $FILEBINDING_BLD_DIR/test/processed | wc -l
echo

cp $FILEBINDING_BLD_DIR/test/sample_files/robustinonly.xml $FILEBINDING_BLD_DIR/test/input/transformrobustinonly

sleep 2


$JBI_ANT -Djbi.component.name=TESTENGINE_FOR_FILEBINDING stop-component

$JBI_ANT -Djbi.component.name=TESTENGINE_FOR_FILEBINDING shut-down-component

$JBI_ANT -Djbi.component.name=TESTENGINE_FOR_FILEBINDING uninstall-component

# stop shutdown uninstall

echo Completed Testing file binding MEPs
