#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)file00006.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "filebinding00006.ksh- UNDEPLOY file binding test"

. ./regress_defs.ksh

$JBI_ANT -Djbi.service.assembly.name="9ae51feb-0fc3-4ff6-9104-cfd59d2a7815" stop-service-assembly

$JBI_ANT -Djbi.service.assembly.name="9ae51feb-0fc3-4ff6-9104-cfd59d2a7815" shut-down-service-assembly

$JBI_ANT -Djbi.service.assembly.name=9ae51feb-0fc3-4ff6-9104-cfd59d2a7815 undeploy-service-assembly

echo Completed UnDeploying from file binding
