/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployDescriptorReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.FileBindingResources;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.List;
import java.util.ArrayList;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * This class reads the SU jbi.xml and loads up the values.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeployDescriptorReader
    extends UtilBase
    implements FileBindingResources
{
    /**
     * Document object of the XML config file.
     */
    private Document mDoc;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * i18n
     */
    private StringTranslator mTranslator;

    /**
     * List of endpoints in the SU jbi.xml.
     */
    private EndpointInfo [] mEPList;

    /**
     * Number of consumers.
     */
    private int mNoOfConsumers;

    /**
     * Number of providers.    
     */
    private int mNoOfProviders;

    /**
     * The total number of end points in the config file
     */
    private int mTotalEndpoints = 0;
    
    /**
     * Type of deployment, WSDL, XML or WSDL11
     */
    private String mType;

    /**
     * Creates a new ConfigReader object.
     */
    public DeployDescriptorReader()
    {
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        setValid(true);
    }

    
    public List getKeyList()
    {    
        ArrayList list = new ArrayList();       
        try
        {
        for (int ep = 0; ep < mEPList.length; ep++)
        {
                list.add(mEPList[ep].getKey());
        }
        }
        catch (Exception e)
        {
            ;
        }
        return list;
    }
    /**
     * Returns the number of consumer endpoints.
     *
     * @return consumer endpoint count.
     */
    public int getConsumerCount()
    {
        return mNoOfConsumers;
    }

    /**
     * Sets the endpoitn attributes.
     *
     * @param node provider/consumer node.
     * @param ep endpoint information.
     */
    public void setEndpoint(
        Node node,
        EndpointInfo ep)
    {
        NamedNodeMap map = node.getAttributes();

        try
        {
            String epname = map.getNamedItem("endpoint-name").getNodeValue();
            String sername = map.getNamedItem("service-name").getNodeValue();
            String intername =
                map.getNamedItem("interface-name").getNodeValue();
            ep.setServiceName(new QName(getNamespace(sername),
                    getLocalName(sername)));
            ep.setInterfaceName(new QName(getNamespace(intername),
                    getLocalName(intername)));
            ep.setEndpointName(epname);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setError(getError() + mTranslator.getString(FBC_LOAD_DD_FAILED)
                + "\n" + e.getMessage());
            setValid(false);
        }
    }
    /**
     * Sets the type of artifact.
     */    
    private void setType(String type)
    {
        mType = type;
    }
    
    /**
     * Gets the type of artifact
     */
    
    public String getType()
    {
        return mType;
    }
     private void setArtifacts()
    {
        NodeList namelist = mDoc.getElementsByTagNameNS("*", "artifactstype");

        if (namelist == null)
        {
            /* This means the tag is not present. default type is WSDL20
             */            
            setType("WSDL20");
            return;
        }

        Element name = (Element) namelist.item(0);
        String sValue = null;

        try
        {
            sValue =
                ((Node) (name.getChildNodes().item(0))).getNodeValue().trim();
        }
        catch (NullPointerException ne)
        {
            setType("WSDL20");
            return;
        }
        
        setType(sValue);
    }
    /**
     * Verifies if the endpoint in the artifacts XML file is present in
     * the deployment descriptor.
     *
     * @param sername  service name.
     * @param intername  interface name.
     * @param epname  endpoint name.
     * @param role  role.
     *
     * @return  true if present.
     */
    public boolean isPresent(
        QName sername,
        QName intername,
        String epname,
        int role)
    {

        for (int ep = 0; ep < mEPList.length; ep++)
        {

            if ((mEPList[ep].getServiceName().toString().equals(sername
                            .toString()))
                    && (
                        mEPList[ep].getInterfaceName().toString().equals(intername
                            .toString())
                    ) && (mEPList[ep].getEndpointName().equals(epname))
                    && (mEPList[ep].getStyle() == role))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the number of provider endpoints.
     *
     * @return provider endpoint count.
     */
    public int getProviderCount()
    {
        return mNoOfProviders;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Document doc)
    {
        try
        {
            mDoc = doc;
            mDoc.getDocumentElement().normalize();
            load();
        }
        catch (Exception genException)
        {
            mLog.severe(mTranslator.getString(FBC_LOAD_DD_FAILED));
            genException.printStackTrace();
            setException(genException);
            setError(getError() + mTranslator.getString(FBC_LOAD_DD_FAILED)
                + "\n" + genException.getMessage());
            setValid(false);
        }
    }

    /**
     * Loads the data.
     */
    public void load()
    {
        try
        {
            NodeList providers = mDoc.getElementsByTagName("provides");
            mNoOfProviders = providers.getLength();

            NodeList consumers = mDoc.getElementsByTagName("consumes");
            mNoOfConsumers = consumers.getLength();
            mEPList = new EndpointInfo[mNoOfConsumers + mNoOfProviders];
            setArtifacts();
            for (int i = 0; i < mNoOfProviders; i++)
            {
                Node node = providers.item(i);
                EndpointInfo sb = new EndpointInfo();
                setEndpoint(node, sb);
                sb.setProvider();

                if (!isValid())
                {
                    setError(mTranslator.getString(FBC_LOAD_DD_FAILED) + "\n"
                        + getError());

                    return;
                }

                mEPList[i] = sb;
            }

            for (int i = 0; i < mNoOfConsumers; i++)
            {
                Node node = consumers.item(i);
                EndpointInfo sb = new EndpointInfo();
                setEndpoint(node, sb);

                if (!isValid())
                {
                    setError(mTranslator.getString(FBC_LOAD_DD_FAILED) + "\n"
                        + getError());

                    return;
                }

                mEPList[i + mNoOfProviders] = sb;
            }
        }
        catch (Exception e)
        {
            mLog.severe("Failed loading DD");
            e.printStackTrace();
            setError(getError() + mTranslator.getString(FBC_LOAD_DD_FAILED)
                + "\n" + e.getMessage());
            setValid(false);
        }
    }

    /**
     * Gets the local name from the quname.
     *
     * @param qname Qualified name of service.
     *
     * @return String local name
     */
    private String getLocalName(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");

        try
        {
            if (tok.countTokens() == 1)
            {
                return qname;
            }

            tok.nextToken();

            return tok.nextToken();
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * Gets the namespace from the qname.
     *
     * @param qname Qname of service
     *
     * @return namespace namespace of service
     */
    private String getNamespace(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");
        String prefix = null;

        try
        {
            if (tok.countTokens() == 1)
            {
                return "";
            }

            prefix = tok.nextToken();

            NamedNodeMap map = mDoc.getDocumentElement().getAttributes();

            for (int j = 0; j < map.getLength(); j++)
            {
                Node n = map.item(j);

                if (n.getLocalName().trim().equals(prefix.trim()))
                {
                    return n.getNodeValue();
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

        return "";
    }

    /**
     * Class which holds the endpoint information.
     *
     * @author Sun Microsystems, Inc.
     */
    public class EndpointInfo
    {
        /**
         * Interface name.
         */
        QName interfacename;

        /**
         * Service name.
         */
        QName servicename;

        /**
         *  Endpoint name.
         */
        String endpointname;

        /**
         * Provider endpoint.
         */
        boolean provider = false;

        /**
         * Sets the endpoint name.
         *
         * @param epname endpoint name.
         */
        public void setEndpointName(String epname)
        {
            endpointname = epname;
        }

        /**
         * Returns the endpoint name.
         *
         * @return endpoint name.
         */
        public String getEndpointName()
        {
            return endpointname;
        }

        /**
         * Sets the interface name.
         *
         * @param intername interface name.
         */
        public void setInterfaceName(QName intername)
        {
            interfacename = intername;
        }

        /**
         * Returns the interface name.
         *
         * @return interface name.
         */
        public QName getInterfaceName()
        {
            return interfacename;
        }

        /**
         * Sets the endpoint as provider.
         */
        public void setProvider()
        {
            provider = true;
        }

        /**
         * Returns true if the endpoint is provider.
         *
         * @return true if provider endpoint.
         */
        public boolean getProvider()
        {
            return provider;
        }

        /**
         * Sets the service name.
         *
         * @param sername service name.
         */
        public void setServiceName(QName sername)
        {
            servicename = sername;
        }

        /**
         * Returns the service name.
         *
         * @return the service name.
         */
        public QName getServiceName()
        {
            return servicename;
        }

        /**
         * Returns the style of endpoint. 
         *
         * @return  provider or consumer.
         */
        public int getStyle()
        {
            if (getProvider())
            {
                return ConfigData.PROVIDER;
            }
            else
            {
                return ConfigData.CONSUMER;
            }
        }
        
        /**
         * Returns a unique key combination
         *
         * @return string
         */
        public String getKey()
        {
            if (getStyle() == ConfigData.PROVIDER)
            {
                return servicename.toString() + interfacename.toString() 
                        + endpointname +  ConfigData.PROVIDER_STRING;
            }
            else
            {
                return servicename.toString() + interfacename.toString() 
                        + endpointname +  ConfigData.CONSUMER_STRING;
            }
        }
    }
}
