/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import com.sun.jbi.binding.file.FileBindingContext;
import com.sun.jbi.binding.file.util.StringTranslator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.sun.jbi.binding.file.util.ConfigData;
import javax.xml.namespace.QName;

/**
 * This is a registry which maintains the servicename, endpoint name    across
 * the bean object. This is an in-memory representation of all endpoints
 * deployed to file binding.
 *
 * @author Sun Microsystems, Inc.
 */
public final class DeploymentRegistry
    implements FileBindingResources
{
    /**
     * Singleton reference.
     */
    private static DeploymentRegistry sMe;

    /**
     * List of all registered channels.
     */
    private HashMap mDeployments;

    /**
     * Logger object
     */
    private Logger mLog;

    /**
     * Helper for i18n
     */
    private StringTranslator mTranslator;

    /**
     * Private constructor.
     */
    private DeploymentRegistry()
    {
        mDeployments = new HashMap();
        mLog = FileBindingContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
    }

    /**
     * Used to grab a reference of this object.
     *
     * @return an initialized DeploymentRegistry reference
     */
    public static synchronized DeploymentRegistry getInstance()
    {
        if (sMe == null)
        {
            sMe = new DeploymentRegistry();
        }

        return sMe;
    }

    /**
     * Returns all the deployments to file binding.
     *
     * @return an array of Deployment Ids.
     */
    public synchronized String [] getAllDeployments()
    {
        List deps = new ArrayList();
        Collection col = mDeployments.values();
        String [] deployments = null;

        try
        {
            Iterator iter = col.iterator();

            while (iter.hasNext())
            {
                EndpointBean eb = (EndpointBean) iter.next();
                String depid = eb.getDeploymentId();

                if (!deps.contains(depid))
                {
                    deps.add(depid);
                }
            }

            deployments = new String[deps.size()];

            Iterator depiter = deps.iterator();
            int i = 0;

            while (depiter.hasNext())
            {
                deployments[i] = (String) depiter.next();
                i++;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return deployments;
    }

    /**
     * Util method to find if a service has been deployed.
     *
     * @param serviceendpoint serviceendpoint information.
     *
     * @return true if deployed, false otherwise.
     */
    public boolean isDeployed(String serviceendpoint)
    {
        if (mDeployments.get(serviceendpoint) == null)
        {
            return false;
        }

        return true;
    }

    /**
     * Gets the status of the su id.
     *
     * @param suId SU Id
     *
     * @return true if deployed , false otherwise.
     */
    public boolean getDeploymentStatus(String suId)
    {
        Set keyset = mDeployments.keySet();
        Iterator iter = keyset.iterator();

        while (iter.hasNext())
        {
            String ser = (String) iter.next();
            EndpointBean eb = (EndpointBean) mDeployments.get(ser);

            if (eb.getDeploymentId().trim().equals(suId))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the enpoint list associated with the SU id.
     *
     * @param suId SU ID.
     *
     * @return list of endpoint Beans.
     */
    public List getEndpoints(String suId)
    {
        Set keyset = mDeployments.keySet();
        Iterator iter = keyset.iterator();
        List eplist = new LinkedList();

        while (iter.hasNext())
        {
            String ser = (String) iter.next();
            EndpointBean eb = (EndpointBean) mDeployments.get(ser);

            if (eb.getDeploymentId().trim().equals(suId))
            {
                eplist.add(eb);
            }
        }

        return eplist;
    }

    /**
     * Removes all the entries from the registry.
     */
    public void clearRegistry()
    {
        if (mDeployments == null)
        {
            return;
        }

        mDeployments = new HashMap();
    }

    /**
     * Removes an endpoint from the registry.
     *
     * @param serviceendpoint service:endpoint combination.
     */
    public synchronized void deregisterEndpoint(String serviceendpoint)
    {
        mLog.info(mTranslator.getString(FBC_DEREGISTER_ENDPOINT, serviceendpoint));

        EndpointBean eb = null;

        try
        {
            eb = (EndpointBean) mDeployments.remove(serviceendpoint);
        }
        catch (Exception e)
        {
            ;
        }

        if (eb == null)
        {
            mLog.severe(mTranslator.getString(FBC_DEREGISTER_ENDPOINT_FAILED,
                    serviceendpoint));
        }
    }

    /**
     * Used to find a registered endpoint which matches the specified service.
     *
     * @param serviceendpoint the service to match against
     *
     * @return the appropriate endpoint bean, or no such endpoint exists.
     */
    public EndpointBean findEndpoint(String serviceendpoint)
    {
        EndpointBean eb;
        eb = (EndpointBean) mDeployments.get(serviceendpoint);

        return eb;
    }

    
    /**
     * Used to find a registered endpoint which matches the specified interface.
     *
     * @param interface name the service to match against
     *
     * @return the appropriate endpoint bean, or no such endpoint exists.
     */
    public EndpointBean findEndpointByInterface(String interfacename)
    {
        Set keyset = mDeployments.keySet();
        Iterator iter = keyset.iterator();

        while (iter.hasNext())
        {
            String ser = (String) iter.next();
            EndpointBean eb = (EndpointBean) mDeployments.get(ser);
            QName in = new QName(eb.getValue(ConfigData.INTERFACE_NAMESPACE),
                eb.getValue(ConfigData.INTERFACE_LOCALNAME));
            if ((in.toString().trim().equals(interfacename)) && 
                 (eb.getRole() == ConfigData.CONSUMER))
            {
                return eb;
            }
        }
        return null;
    }
    
    /**
     * List all endpoints currently present in the registry.
     *
     * @return an iterator over all endpoints in the registry.
     */
    public Iterator listAllEndpoints()
    {
        if (mDeployments == null)
        {
            return null;
        }

        Set tmp = mDeployments.keySet();

        if (tmp == null)
        {
            return null;
        }

        return tmp.iterator();
    }

    /**
     * Registers an endpoint with the registry.  Duplicates are allowed and no
     * validation is performed at this point.
     *
     * @param serviceendpoint the endpoint to register
     * @param endpoint endpoint bean object.
     */
    public synchronized void registerEndpoint(
        String serviceendpoint,
        EndpointBean endpoint)
    {
        mLog.info(mTranslator.getString(FBC_REGISTER_ENDPOINT, serviceendpoint));
        mDeployments.put(serviceendpoint, endpoint);
    }
}
