/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDL11FileValidator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.util;

import com.sun.jbi.binding.file.FileBindingResources;

import org.w3c.dom.DocumentFragment;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.util.logging.Logger;

import javax.wsdl.Binding;
import javax.wsdl.Definition;
import javax.wsdl.OperationType;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.extensions.UnknownExtensibilityElement;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;


import java.util.Collection;
import java.util.Map;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.xml.namespace.QName;
/**
 * This class is uses to validate the configuration file supplied during
 * deployment conforms to the schema.
 *
 * @author Sun Microsystems, Inc.
 */
public final class WSDL11FileValidator
    extends UtilBase
    implements FileBindingResources
{
    /**
     * Definitions object
     */
    private static Definition sDefinition;

    /**
     * Inbound / Outbound
     */
    private String mCurValue = "";

    /**
     * Name of the file to parse.
     */
    private String mFileName;

    /**
     * Translator for i18n messages.
     */
    private StringTranslator mTranslator;

    /**
     * Creates a new WSDL11FileValidator object.
     *
     * @param wsdlfile schema file name
     */
    public WSDL11FileValidator(String wsdlfile)
    {
        mFileName = wsdlfile;
        mTranslator = new StringTranslator();
    }

    /**
     * Returns the document object obtained as a result of parsing.
     *
     * @return document object
     */
    public Definition getDocument()
    {
        return sDefinition;
    }

    /**
     * This method has to be invoked to check the validity of the input
     * document.
     */
    public void validate()
    {
        parse();

        if (isValid())
        {
            validateEndpoints();
        }
    }

    /**
     * Checks if a binging is file binding
     *
     * @param df Document fragment
     *
     * @return true if file binding
     */
    private boolean isFileBinding(Binding aBinding)
    {
       java.util.List extList = aBinding.getExtensibilityElements();
        List boper = aBinding.getBindingOperations();
        if (extList != null)
        {
            for (int i = 0; i < extList.size(); i++)
            {
                if (extList.get(i) instanceof javax.wsdl.extensions.UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement sb =
                        (UnknownExtensibilityElement) extList.get(i);

                    if ((sb.getElementType().getNamespaceURI().equals(ConfigData.FILENAMESPACE))
                            && (sb.getElementType().getLocalPart().equals("binding")))
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks the Endpoint.
     *
     * @param ep endpoint
     */
    private void checkEndpoint(Port ep)
    {
       List extList = ep.getExtensibilityElements();

        if (extList != null)
        {
            for (int i = 0; i < extList.size(); i++)
            {
                if (extList.get(i) instanceof javax.wsdl.extensions.UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement sb =
                        (UnknownExtensibilityElement) extList.get(i);

                    if (sb.getElementType().getNamespaceURI().equals(ConfigData.FILENAMESPACE)
                            && sb.getElementType().getLocalPart().equals("artifacts"))
                    {
                        Element epnode = sb.getElement();
                        NamedNodeMap epattributes = epnode.getAttributes();
                        checkRequired(epattributes, ConfigData.ENDPOINT_TYPE);
                        if (mCurValue.trim().equalsIgnoreCase(ConfigData.CONSUMER_STRING))
                        {
                            checkRequired(epattributes, ConfigData.WSDL_INPUT_LOCATION);
                            checkRequired(epattributes, ConfigData.WSDL_PROCESSED_LOCATION);
                        }
                        checkRequired(epattributes, ConfigData.WSDL_OUTPUT_LOCATION);                    
                    }
                }
            }
            if (!isValid())
            {
                setError("\n\tEndpoint : " + ep.getName() + getError());
            }
        }
    }

    /**
     * Checks the required attributes.
     *
     * @param map Node map
     * @param attr attributes that has to be checked.
     */
    private void checkRequired(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);

        if ((attrnode == null))
        {
            setValid(false);
            setError(getError() + "\n\t" + "Required attribute \"" + attr
                + "\" not found");

            return;
        }

        try
        {
            mCurValue = attrnode.getNodeValue().trim();

            if (mCurValue.equals(""))
            {
                setValid(false);
                setError(getError() + "\n\t" + "Required attribute \"" + attr
                    + "\" has to have a value, cannot be null");
            }
        }
        catch (Exception e)
        {
            setValid(false);
            setError(getError() + "\n\t" + "Cannot get attribute value of "
                + attr);
            setException(e);
        }
    }

    /**
     * Parses the input XML file.
     */
    private void parse()
    {
        try
        {
            javax.wsdl.factory.WSDLFactory mFactory = WSDLFactory.newInstance();
            javax.wsdl.xml.WSDLReader mReader = mFactory.newWSDLReader();
            sDefinition = mReader.readWSDL(null, mFileName);
        }
        catch (Exception e)
        {
            setError("WSDL file " + mFileName + " parse error, reason "
                + e.getMessage());
            setException(e);
            setValid(false);
            e.printStackTrace();
        }
    }

    /**
     * Validates the endpoints.
     */
    private void validateEndpoints()
    {
        boolean found = false;

        java.util.Map servicemap = sDefinition.getServices();
        List eplist = new LinkedList();
        Collection services = servicemap.values();

        Iterator iter = services.iterator();

        while (iter.hasNext())
        {
            try
            {
                javax.wsdl.Service service = (javax.wsdl.Service) iter.next();

                Map endpointsmap = service.getPorts();
                Collection endpoints = endpointsmap.values();

                Iterator epiter = endpoints.iterator();

                while (epiter.hasNext())
                {
                    javax.wsdl.Port ep = (javax.wsdl.Port) epiter.next();

                    javax.wsdl.Binding b = ep.getBinding();

                    if (!isFileBinding(b))
                    {
                        continue;
                    }

                    found = true;
                    checkEndpoint(ep);

                    if (!isValid())
                    {
                        setError("\n\tBinding " + " : " + b.getQName()
                            + getError());

                        return;
                    }
                }
            }
            catch (Exception e)
            {
                setError(mTranslator.getString(FBC_WSDL_ENDPOINT_ERROR,
                        e.getMessage()));
                setException(e);
                setValid(false);
                e.printStackTrace();

                return;
            }
        }

        if (!found)
        {
            setError(getError() + mTranslator.getString(FBC_WSDL_NO_ENDPOINTS));
            setValid(false);
        }
    }
}
