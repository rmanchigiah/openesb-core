/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FileBindingResolver.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file;

import org.w3c.dom.Document;

import java.util.Hashtable;

import javax.jbi.servicedesc.ServiceEndpoint;


import javax.xml.parsers.DocumentBuilderFactory;

/**
 * This class implements the Resolver contract with the NMR. Resolver is used
 * by other components of JBI to query meta-data of endpoints deployed in file
 * binding.
 *
 * @author Sun Microsystems, Inc.
 */
public class FileBindingResolver
{
    /**
     * Holds the descriptor objects.
     */
    private Hashtable mDescriptorCache;

    /**
     * Creates a new FileBindingResolver object.
     */
    public FileBindingResolver()
    {
        mDescriptorCache = new Hashtable();
    }

    /**
     * Contract with framework. This method is used by other components to
     * query meta-data.
     *
     * @param ef endpoint reference.
     *
     * @return descriptor object.
     */
    public Document getServiceDescription(ServiceEndpoint ef)
    {
        try
        {
            Document df =
                (Document) mDescriptorCache.get(ef.getServiceName().toString()
                    + ef.getEndpointName());

            return df;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /**
     * Adds endpoint meta-data.
     *
     * @param epname endpoint name.
     * @param df document fragment.
     */
    public void addEndpointDoc(
        String epname,
        Document df)
    {
        if ((epname != null) && (df != null))
        {
            try
            {
                mDescriptorCache.put(epname, df);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    /**
     * Adds an endpoint document.
     *
     * @param epname endpoint name.
     * @param filename wsdl file.
     */
    public void addEndpointDoc(
        String epname,
        String filename)
    {
        if ((epname != null) && (filename != null))
        {
            try
            {
               org.w3c.dom.Document d =
                    DocumentBuilderFactory.newInstance().
                            newDocumentBuilder().parse(filename);
                mDescriptorCache.put(epname, d);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }    
    /**
     * Clears the descriptor cache. Removes all information.
     */
    public void clearCache()
    {
        mDescriptorCache = new Hashtable();
    }
}
