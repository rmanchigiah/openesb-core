/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PatternRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

/**
 * Simple, extensible pattern registry.
 *
 * @author Sun Microsystems, Inc.
 */
public class PatternRegistry
{
    /**
     * In Only MEP.
     */
    public static final String IN_ONLY =
        "http://www.w3.org/ns/wsdl/in-only";

    /**
     * In Out MEP.
     */
    public static final String IN_OUT = "http://www.w3.org/ns/wsdl/in-out";

    /**
     * In Optional Out MEP.
     */
    public static final String IN_OPTIONAL_OUT =
        "http://www.w3.org/ns/wsdl/in-opt-out";

    /**
     * Robust In Only MEP.
     */
    public static final String ROBUST_IN_ONLY =
        "http://www.w3.org/ns/wsdl/robust-in-only";

    /**
     * Out Only MEP.
     */
    public static final String OUT_ONLY =
        "http://www.w3.org/ns/wsdl/out-only";

    /**
     * Out In MEP.
     */
    public static final String OUT_IN = "http://www.w3.org/ns/wsdl/out-in";

    /**
     * Out Optional In MEP.
     */
    public static final String OUT_OPTIONAL_IN =
        "http://www.w3.org/ns/wsdl/out-opt-in";

    /**
     * Robust Out Only MEP.
     */
    public static final String ROBUST_OUT_ONLY =
        "http://www.w3.org/ns/wsdl/robust-out-only";
}
