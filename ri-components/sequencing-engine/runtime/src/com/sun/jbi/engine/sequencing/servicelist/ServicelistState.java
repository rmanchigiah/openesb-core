/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServicelistState.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

/**
 * This class maintains permitted servicelist states.
 *
 * @author Sun Microsystems, Inc.
 */
public final class ServicelistState
{
    /**
     * Demotes the list is ready.
     */
    public static final int READY = 0;

    /**
     * Demotes the list is runnning.
     */
    public static final int RUNNING = 1;

    /**
     * Waiting.
     */
    public static final int WAITING = 2;

    /**
     * Timed out state.
     */
    public static final int TIMED_OUT = 3;

    /**
     * Aborted.
     */
    public static final int ABORTED = 4;

    /**
     * Completed.
     */
    public static final int COMPLETED = 5;

    /**
     * Error state.
     */
    public static final int ERROR = 6;

    /**
     * Returns the string representation of the state.
     *
     * @param state state
     *
     * @return String rep of state.
     */
    public static String getStringValue(int state)
    {
        switch (state)
        {
        case READY :
            return "READY";

        case RUNNING :
            return "RUNNING";

        case WAITING :
            return "WAITING";

        case TIMED_OUT :
            return "TIMED OUT";

        case ABORTED :
            return "ABORTED";

        case COMPLETED :
            return "COMPLETED";

        case ERROR :
            return "ERROR";

        default :
            return "INVALID STATE";
        }
    }
}
