/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.framework.Servicelist;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;
import com.sun.jbi.engine.sequencing.servicelist.SimpleServicelist;
import com.sun.jbi.engine.sequencing.util.ConfigData;
import com.sun.jbi.engine.sequencing.util.StringTranslator;

import java.net.URI;

import java.util.Vector;
import java.util.logging.Logger;

import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.messaging.RobustInOnly;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.MessageExchange;


/**
 * MessageProcessor.
 *
 * @author Sun Microsystems, Inc.
 */
public class MessageProcessor
    implements SequencingEngineResources
{
    /**
     * Engine channel.
     */
    private DeliveryChannel mChannel;

    /**
     * Deployment Registry.
     */
    private DeploymentRegistry mRegistry;

    /**
     * Logger.
     */
    private Logger mLog;

    /**
     * Message exchange
     */
    private MessageExchange mExchange;

    /**
     * Message registry.
     */
    private MessageRegistry mMessageReg;

    /**
     * Error holder
     */
    private String mError = "";

    /**
     * Pattern of current exchange.
     */
    private String mPattern;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * Access List which maintains a list of services to avoid.
     */
    private Vector mAccessList;

    /**
     * Flag to check
     */
    private boolean mCanAccept = true;

    /**
     * Denotes valdiity of the exchange.
     */
    private boolean mValid = true;

    /**
     * Creates a new MessageProcessor object.
     *
     * @param chnl engine channel
     */
    public MessageProcessor(DeliveryChannel chnl)
    {
        mChannel = chnl;
        mRegistry = DeploymentRegistry.getInstance();
        mMessageReg = MessageRegistry.getInstance();
        mCanAccept = true;
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        mAccessList = new Vector();
    }

    /**
     * Sets the message exchange.
     *
     * @param exchange message exchange
     */
    public void setExchange(MessageExchange exchange)
    {
        mValid = true;
        mExchange = exchange;
    }

    /**
     * Gets the pattern for the current exchange.
     *
     * @return string pattern name
     */
    public String getPattern()
    {
        return mPattern;
    }

    /**
     * Method to add a service to accesslist.
     *
     * @param servicename service name
     */
    public void addToAccessList(String servicename)
    {
        try
        {
            mAccessList.add(servicename);
        }
        catch (Exception e)
        {
            ;
        }
    }

    /**
     * Method to parse the exchange and identify a servicelist.
     *
     * @return Serviclist list object.
     */
    public Servicelist process()
    {
        URI pattern = mExchange.getPattern();
        Servicelist list = null;
        //mLog.info("** ACEPT - MSG REG *** ");
        //mMessageReg.dump();
        String pat = pattern.toString().trim();
        String sname =
            mExchange.getEndpoint().getServiceName().getNamespaceURI()
            + mExchange.getEndpoint().getServiceName().getLocalPart();

        /* Check deployment registry for service name.
         * If its not there in deployment registry, then this service is not
         * serviced by us. We do not know how the NMR routed the message to
         * us , but anyway we check.
         */
        ServicelistBean slb = null;

        /* In out messages are requests from other
         * engines or bindings to invoke a servicelist
         * So, process it accordingly , See whether our list operation
         * supports the pattern.
         * In Out can also be responses after from other components.
         */
        mLog.info(mTranslator.getString(SEQ_RECEIVED_MESSAGE,
                mExchange.getPattern(), mExchange.getExchangeId()));

        if (mExchange instanceof InOut
                || mExchange instanceof InOnly
                || mExchange instanceof RobustInOnly)
        {
            list = processInbound();
        }
        else if (mExchange instanceof InOptionalOut)
        {
            mLog.severe(mTranslator.getString(SEQ_RECEIVED_INOPTIONALOUT,
                    mExchange.getExchangeId()));
            mValid = false;
        }
        else if (pat.equals(PatternRegistry.OUT_OPTIONAL_IN.trim()))
        {
            mLog.severe(mTranslator.getString(SEQ_RECEIVED_OUTOPTIONALIN,
                    mExchange.getExchangeId()));
            mValid = false;
        }
        else
        {
            mLog.severe(mTranslator.getString(SEQ_RECEIVED_UNSUPPORTED_MEP,
                    mExchange.getExchangeId()));
            mValid = false;
        }

        if (list != null)
        {
            list.setMessageExchange(mExchange);

            javax.transaction.Transaction trans =
                (javax.transaction.Transaction) mExchange.getProperty(MessageExchange.JTA_TRANSACTION_PROPERTY_NAME);

            try
            {
                if ((trans != null)
                        && (
                            trans == SequencingEngineContext.getInstance()
                                                                .getTransactionManager()
                                                                .getTransaction()
                        ))
                {
                    SequencingEngineContext.getInstance().getTransactionManager()
                                           .suspend();
                    mLog.fine(mTranslator.getString(SEQ_SUSPEND_TX,
                            mExchange.getExchangeId()));
                }
            }
            catch (javax.transaction.SystemException se)
            {
                mLog.severe(mTranslator.getString(SEQ_SUSPEND_TX_FAILED,
                        mExchange.getExchangeId()));
            }

            list.setTransactionContext(trans);
        }

        setPattern(pat);

        return list;
    }

    /**
     * Removes a service from access list.
     *
     * @param servicename service name to be removed.
     */
    public void removeFromAccessList(String servicename)
    {
        try
        {
            mAccessList.remove(servicename);
        }
        catch (Exception e)
        {
            ;
        }
    }

    /**
     * This method stops the engine from processing any more new requests.
     */
    public void stopNewRequests()
    {
        mCanAccept = false;
        mLog.info(mTranslator.getString(SEQ_ENGINE_SHUTTING_DOWN));
    }

    /**
     * Validity.
     *
     * @return true if valid
     */
    public boolean valid()
    {
        return mValid;
    }

    /**
     * Sets the error.
     *
     * @param er error message.
     */
    private void setError(String er)
    {
        mError = er;
    }

    /**
     * Sets the pattern.
     *
     * @param pat mep
     */
    private void setPattern(String pat)
    {
        mPattern = pat;
    }

    /**
     * Returns true if a message for the service can be accepted. Access lists
     * are not implemented now.
     *
     * @param servicename service name to be checked.
     *
     * @return true if can be accepted.
     */
    private boolean canAccept(String servicename)
    {
        return mCanAccept;
    }

    /**
     * Checks if the operation supports the pattern of exchange received.
     *
     * @param slb list bean
     * @param pat pattern
     * @param sname service name
     */
    private void checkSupport(
        ServicelistBean slb,
        String pat,
        String sname)
    {
        if (slb == null)
        {
            mLog.severe(mTranslator.getString(SEQ_SERVICE_NOT_HOSTED, sname));
            setError(mTranslator.getString(SEQ_SERVICE_NOT_HOSTED, sname));
            mValid = false;

            return;
        }

        String oper = mExchange.getOperation().getLocalPart();

        if (!oper.trim().equals(slb.getOperation().trim()))
        {
            mLog.severe(mTranslator.getString(SEQ_UNSUPPORTED_OPER,
                    slb.getOperation(), slb.getServicename(),
                    mExchange.getExchangeId()));
            setError(mTranslator.getString(SEQ_UNSUPPORTED_OPER,
                    slb.getOperation(), slb.getServicename(),
                    mExchange.getExchangeId()));
            mValid = false;

            return;
        }

        mLog.info("slb.getMep: " + slb.getMep().trim());
        mLog.info("pat: " + pat.trim());

        if (!slb.getMep().trim().equals(pat.trim()))
        {
            mLog.severe(mTranslator.getString(SEQ_UNSUPPORTED_PATTERN_OPER,
                    slb.getOperation(), slb.getServicename(),
                    mExchange.getExchangeId()));
            setError(mTranslator.getString(SEQ_UNSUPPORTED_PATTERN_OPER,
                    slb.getOperation(), slb.getServicename(),
                    mExchange.getExchangeId()));
            mValid = false;
        }
    }

    /**
     * Creates a nee service list instance.
     *
     * @param sname service name
     * @param pat pattern
     *
     * @return list
     */
    private Servicelist newList(
        String sname,
        String pat)
    {
        // definitely a new request no doubt about it                                    
        ServicelistBean slb = mRegistry.findService(sname);
        Servicelist list = null;
        checkSupport(slb, pat, sname);

        if (mValid && canAccept(sname))
        {
            list = new SimpleServicelist();
            list.setServicelistBean(slb);
        }
        else
        {
            sendUnsupportedError();
        }

        return list;
    }

    /**
     * Processes all messages.
     *
     * @return list
     */
    private Servicelist processInbound()
    {
        String requestseqid =
            (String) mExchange.getProperty(ConfigData.REQUEST_SEQ_ID);
        String responseseqid =
            (String) mExchange.getProperty(ConfigData.RESPONSE_SEQ_ID);
        String sname =
            mExchange.getEndpoint().getServiceName().getNamespaceURI()
            + mExchange.getEndpoint().getServiceName().getLocalPart()
            + mExchange.getEndpoint().getEndpointName()
            + mExchange.getOperation().getLocalPart();
        String pat = "";
        Servicelist list = null;
        int type = -1;
        
        if (mExchange instanceof InOut)
        {
            pat = PatternRegistry.IN_OUT;
            mLog.info("PatternRegistry.IN_OUT");
        }
        else if (mExchange instanceof InOnly)
        {
            pat = PatternRegistry.IN_ONLY;
            mLog.info("PatternRegistry.IN_ONLY");
        }
        else if (mExchange instanceof RobustInOnly)
        {
            pat = PatternRegistry.ROBUST_IN_ONLY;
            mLog.info("PatternRegistry.ROBUST_IN_ONLY");
        }
        else if (mExchange instanceof InOptionalOut)
        {
            pat = PatternRegistry.IN_OPTIONAL_OUT;
            mLog.info("PatternRegistry.IN_OPTIONAL_OUT");
        }
        else
        {
            mLog.info("PatternRegistry.NOTHING");
        }

        /* There could be other efficient ways to express the same logic
         * but to maintain clarity of whats being done
         * below  is followed
         */
        String messageid = null;
        if (mExchange.getStatus() == ExchangeStatus.ACTIVE)
        {
            if (requestseqid == null)
            {
                mLog.info("Break point 1");
                list = newList(sname, pat);
                type = ConfigData.REQUEST_TYPE;
            }
            else if ((requestseqid != null) && (responseseqid != null))
            {
                /**
                 * this should be a response from sequencing engine itself
                 */
                if (mMessageReg.isTimedOut(mExchange.getExchangeId()
                            + requestseqid))
                {
                    mLog.info(mTranslator.getString(SEQ_SERVICE_TIMED_OUT,
                            mExchange.getExchangeId()));
                    mValid = false;
                }
                else
                {
                    messageid = mExchange.getExchangeId() + requestseqid;
                    list = mMessageReg.getServicelist(messageid);                    
                    type = ConfigData.RESPONSE_TYPE;
                }
            }
            else if ((requestseqid != null) && (responseseqid == null))
            {
                if (mExchange.getRole().equals(MessageExchange.Role.PROVIDER))
                {
                    /*
                     * This is definitely a request from sequencing engine
                     */
                    mLog.info("Break point 2");
                    list = newList(sname, pat);
                    type = ConfigData.REQUEST_TYPE;
                }
                else
                {
                    if (mMessageReg.isTimedOut(mExchange.getExchangeId()
                                + requestseqid))
                    {
                        mLog.info(mTranslator.getString(SEQ_SERVICE_TIMED_OUT,
                                mExchange.getExchangeId()));
                        mValid = false;
                    }
                    else
                    {
                        /*
                         * If req id not nul then it cud a response or a new
                         * request from seq engine itself
                         */
                        messageid = mExchange.getExchangeId() + requestseqid;
                        list =
                            mMessageReg.getServicelist(messageid);
                        type = ConfigData.RESPONSE_TYPE;
                    }
                }
            }
        }
        else if (mExchange.getStatus() == ExchangeStatus.DONE)
        {
            if ((requestseqid != null) && (responseseqid != null))
            {
                if (mExchange instanceof InOut)
                {
                    messageid = mExchange.getExchangeId() + responseseqid;
                    list = mMessageReg.getServicelist(messageid);
                    type = ConfigData.REQUEST_TYPE;
                }
                else
                {
                    messageid = mExchange.getExchangeId() + requestseqid;
                    list =  mMessageReg.getServicelist(messageid);
                    type = ConfigData.RESPONSE_TYPE;
                }
            }
            else if ((requestseqid == null) && (responseseqid != null))
            {
                /* This is a case when seq engine receives a request
                 * from another compontn
                 */
                messageid = mExchange.getExchangeId() + responseseqid;
                list = mMessageReg.getServicelist(messageid);
                type = ConfigData.REQUEST_TYPE;
            }
            else
            {
                messageid = mExchange.getExchangeId() + requestseqid;
                list =  mMessageReg.getServicelist(messageid);
                type = ConfigData.RESPONSE_TYPE;
            }
        }
        else
        {
            if (requestseqid != null)
            {
                messageid = mExchange.getExchangeId() + requestseqid;
                list =
                    mMessageReg.getServicelist(messageid);
                type = ConfigData.RESPONSE_TYPE;
            }
            else if (responseseqid != null)
            {
                messageid = mExchange.getExchangeId() + responseseqid;
                list = mMessageReg.getServicelist(messageid);

                type = ConfigData.REQUEST_TYPE;
            }
            else
            {
                mValid = false;
            }
        }

        if (list == null)
        {
            mLog.info(mTranslator.getString(SEQ_SERVICE_TIMED_OUT,
                    mExchange.getExchangeId()));
            mValid = false;
        }
        else
        {
            /* We found the list, so deregister the message now.
             * After all we registered it only to find the list the next time.
             */
            if (messageid != null)
            {
                mMessageReg.deregisterExchange(messageid);
            }
            list.setType(type);
        }
        
        return list;
    }

    /**
     * Sends an error if servicename or operationb is not supported.
     */
    private void sendUnsupportedError()
    {
        Exception exp = new Exception(mError);

        try
        {
            mExchange.setError(exp);
            mChannel.send(mExchange);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
