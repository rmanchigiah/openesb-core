#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)seq00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#create a JBI test installation and start it up
echo Starting framework for tests
. ./regress_defs.ksh

# rm -f $JBI_DOMAIN_ROOT/logs/server.log

#remove our old test installations BEFORE we restart the appserver:
rm -rf $JBI_DOMAIN_ROOT/jbi/bindings/SunFileBinding
rm -rf $JBI_DOMAIN_ROOT/jbi/bindings/SunSequencingEngine
rm -rf $JBI_DOMAIN_ROOT/jbi/engines/29ae7208-b07d-45c7-8662-e1389d6b2ff
rm -rf $JBI_DOMAIN_ROOT/jbi/tempdeploy
rm -rf $JBI_DOMAIN_ROOT/jbi/engines/TESTENGINE_FOR_SEQBINDING

# remove seq engine specific folders and create new ones

rm -rf $SRCROOT/engine/sequencing/bld/regress/test/inoutinput
rm -rf $SRCROOT/engine/sequencing/bld/regress/test/inoutoutput
rm -rf $SRCROOT/engine/sequencing/bld/regress/test/inoutproc

mkdir $SRCROOT/engine/sequencing/bld/regress/test/inoutinput
mkdir $SRCROOT/engine/sequencing/bld/regress/test/inoutoutput
mkdir $SRCROOT/engine/sequencing/bld/regress/test/inoutproc

echo Started the Test framework
