/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import junit.framework.*;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServiceManager extends TestCase
{
    /**
     * Creates a new TestServiceManager object.
     *
     * @param testName
     */
    public TestServiceManager(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestServiceManager.class);

        return suite;
    }

    /**
     * Test of activateServices method, of class
     * com.sun.jbi.engine.sequencing.ServiceManager.
     */
    public void testActivateServices()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getRunningServicesCount method, of class
     * com.sun.jbi.engine.sequencing.ServiceManager.
     */
    public void testGetRunningServicesCount()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setChannel method, of class
     * com.sun.jbi.engine.sequencing.ServiceManager.
     */
    public void testSetChannel()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of startDeployment method, of class
     * com.sun.jbi.engine.sequencing.ServiceManager.
     */
    public void testStartDeployment()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of stopAllServices method, of class
     * com.sun.jbi.engine.sequencing.ServiceManager.
     */
    public void testStopAllServices()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of stopDeployment method, of class
     * com.sun.jbi.engine.sequencing.ServiceManager.
     */
    public void testStopDeployment()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
